ECHO OFF
SET /A auto_upload=0
CLS
:MENU
CLS
:START
ECHO ZEEK GPS CORDOVA BUILDER
ECHO ...............................................
ECHO SELCCIONA UNA OPCION
ECHO ...............................................
ECHO.
Rem ECHO [1] - Preparar Cordova (Actualiza proyecto)
Rem ECHO [2] - Reset Cordova Android
Rem ECHO [3] - Zeek GPS App: Generar APK
ECHO [1] - Zeek GPS App: Huawei APK
Rem ECHO [5] - Subir APK de ZeekGPS App
ECHO [2] - Subir APK HUAWEI de ZeekGPS App 
ECHO [3] - Transporte Empresarial App: Huawei APK
ECHO [4] - Subir APK HUAWEI de Transporte Empresarial App 
ECHO [5] - Limpiar directorios
IF /I "%auto_upload%" EQU "0" ECHO [6] - Activar Auto Subida
IF /I "%auto_upload%" EQU "1" ECHO [6] - Desactivar Auto Subida
ECHO [0] - SALIR
ECHO.
SET /P M=Introduce una opcion: 
Rem IF %M%==1 GOTO PREP
Rem IF %M%==2 GOTO RESET
Rem IF %M%==3 GOTO GPS
IF %M%==1 GOTO GPS_HUAWEI
Rem IF %M%==5 GOTO UPGPS
IF %M%==2 GOTO UPGPS_HUAWEI
IF %M%==3 GOTO TEAPP_HUAWEI
IF %M%==4 GOTO UPTEAPP_HUAWEI
IF %M%==5 GOTO CLEAN
IF %M%==6 GOTO SET_AUTOUP
IF %M%==0 GOTO EOF
GOTO MENU
:PREP
ECHO Preparando proyecto...
ECHO.
CALL cordova prepare android
@PAUSE
GOTO MENU
:RESET
ECHO Reiniciando proyecto...
ECHO.
CALL cordova platform rm android
CALL cordova platform add android
@PAUSE
GOTO MENU
:CLEAN
ECHO.
ECHO Limpiando ZeekGPS_Huawei...
DEL /F/Q/S C:\ZeekGPS_Huawei\*.* > NUL
RMDIR C:\ZeekGPS_Huawei /S /Q
ECHO Limpiando TEApp_Huawei...
DEL /F/Q/S C:\TEApp_Huawei\*.* > NUL
RMDIR C:\TEApp_Huawei /S /Q
ECHO.
@PAUSE
GOTO MENU
:SET_AUTOUP
IF /I "%auto_upload%" EQU "0" (
	SET /A auto_upload=1
) ELSE (
	SET /A auto_upload=0
)
GOTO MENU
:GPS
ECHO Compilando Zeek GPS App...
ECHO.
CALL cordova clean
CALL cordova build android -Pandroid.debug.obsoleteApi=true --release -- --keystore="C:\\ZeekGPS\\package-assets\\certs\\argus-tecnologias.keystore" --storePassword="argus@tech" --alias="argustecnologias" --password="argus@tech"
mkdir "output"
move "platforms\android\app\build\outputs\apk\release\app-release.apk" "output\zeekgps.apk"
@PAUSE
GOTO MENU
:GPS_HUAWEI
ECHO.
DEL /F/Q/S C:\ZeekGPS_Huawei\*.* > NUL
RMDIR C:\ZeekGPS_Huawei /S /Q
MKDIR C:\ZeekGPS_Huawei 
@PAUSE
XCOPY "C:\ZeekGPS\www" "C:\ZeekGPS_Huawei\www" /h /i /c /k /e /r /y
XCOPY "C:\ZeekGPS\package-assets" "C:\ZeekGPS_Huawei\package-assets" /h /i /c /k /e /r /y
COPY "C:\ZeekGPS\config.xml" "C:\ZeekGPS_Huawei\config.xml"  
CD C:\ZeekGPS_Huawei
@PAUSE
CALL cordova platform add "android"
CALL npm i cordova-common
CALL cordova platform rm "android"
CALL cordova platform add "android"
CALL cordova build android -Pandroid.debug.obsoleteApi=true --release -- --keystore="C:\\ZeekGPS\\package-assets\\certs\\argus-tecnologias.keystore" --storePassword="argus@tech" --alias="argustecnologias" --password="argus@tech"
mkdir "output"
move "platforms\android\app\build\outputs\apk\release\app-release.apk" "output\zeekgps.apk"
IF /I "%auto_upload%" EQU "1" (
	GOTO UPGPS_HUAWEI
) ELSE (
	@PAUSE
	GOTO MENU
)
:TEAPP_HUAWEI
ECHO.
DEL /F/Q/S C:\TEApp_Huawei\*.* > NUL
RMDIR C:\TEApp_Huawei /S /Q
MKDIR C:\TEApp_Huawei 
@PAUSE
XCOPY "C:\ZeekGPS\www" "C:\TEApp_Huawei\www" /h /i /c /k /e /r /y
XCOPY "C:\ZeekGPS\package-assets" "C:\TEApp_Huawei\package-assets" /h /i /c /k /e /r /y
COPY "C:\ZeekGPS\config.xml" "C:\TEApp_Huawei\config.xml"  
CD C:\TEApp_Huawei
@PAUSE
CALL cordova platform add "android"
CALL npm i cordova-common
CALL cordova platform rm "android"
CALL cordova platform add "android"
CALL cordova build android -Pandroid.debug.obsoleteApi=true --release -- --keystore="C:\\ZeekGPS\\package-assets\\certs\\argus-tecnologias.keystore" --storePassword="argus@tech" --alias="argustecnologias" --password="argus@tech"
mkdir "output"
move "platforms\android\app\build\outputs\apk\release\app-release.apk" "output\transporteempresarial.apk"
IF /I "%auto_upload%" EQU "1" (
	GOTO UPTEAPP_HUAWEI
) ELSE (
	@PAUSE
	GOTO MENU
)
:UPGPS
ECHO.
CALL python scripts/upload.py "output/zeekgps.apk" "zeekgps.apk" "zeekgps.imeev.com"
@PAUSE
GOTO MENU
:UPGPS_HUAWEI
ECHO.
CALL python "C:/ZeekGPS/scripts/upload.py" "C:/ZeekGPS_Huawei/" "C:/ZeekGPS_Huawei/output/zeekgps.apk" "zeekgps_huawei.apk" "zeekgps.imeev.com"
CALL python "C:/ZeekGPS/scripts/upload_gradle_huawei.py" "C:/ZeekGPS_Huawei/platforms/android/" "zeekgps.imeev.com"
@PAUSE
GOTO MENU
:UPTEAPP_HUAWEI
ECHO.
CALL python "C:/ZeekGPS/scripts/upload.py" "C:/TEApp_Huawei/" "C:/TEApp_Huawei/output/transporteempresarial.apk" "transporteempresarial_huawei.apk" "teapp.imeev.com"
CALL python "C:/ZeekGPS/scripts/upload_gradle_huawei.py" "C:/TEApp_Huawei/platforms/android/" "teapp.imeev.com"
@PAUSE
GOTO MENU
:EOF
CLS
ECHO ON