openssl x509 -in "zeekgps.cer" -inform der -outform pem -out "zeekgps.cert.pem"
type "zeekgps.cert.pem" "zeekgps.key.pem" > "zeekgps.pem"
openssl pkcs12 -export -in "zeekgps.pem" -out zeekgps.p12 -name "zeekgps"    