openssl x509 -in ios_development.cer -inform DER -out ios_development.pem -outform PEM
openssl pkcs12 -export -inkey ios_key.key -in ios_development.pem -out ios_development.p12