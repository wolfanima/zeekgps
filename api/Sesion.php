<?php
	header("Access-Control-Allow-Origin: *");
	header('Content-type: text/json; charset=utf-8');
	$blockApp = false;
	
	//include_once("Connection.php");
	include_once("Vars.php");
	include_once("Functions.php");
	
	switch($_REQUEST['api']){
		case "IniciarSesion":
		case "IniciaSesion":	
			$data = array(
				"usuario"=>$_REQUEST['user'],
				"contrasena"=>$_REQUEST['pass'],
				"recordar"=> "true"
			);

			$result = sendPost($data, $GLOBALS['authUrl']."IniciaSesionAPP",false);
			preg_match_all("/^Set-cookie: (.*?);/ism", $GLOBALS['headers'], $cookies);
			foreach($cookies[1] as $cookie) {
				$buffer_explode = strpos($cookie, "=");
				$cookies[ substr($cookie,0,$buffer_explode) ] = substr($cookie,$buffer_explode+1);
			}
			$loginResponse = $GLOBALS['response'];
			if($blockApp == true ){
				$json = array(
					'status'=>'BLOCK',
					'token'=>null,
					'user'=>array(
						'Status'=>0
					)
				);
			}else{
				if( sizeof($cookies[1]) > 0 ){
					$res = getUserData($cookies[1][0]);
					list($time,$tipo,$permisos,$autos,$soporte) = $res;
					$userData = object_to_array(processResult($loginResponse,false));
					$preProcess = $userData['Nombre'];
					
					$json = array(
						'status'=>'OK',
						'user'=>processResult($loginResponse),
						'token'=>$cookies[1][0],
						'serverTime'=>processResult($time), 
						'tipo'=>processResult($tipo),
						'permisos'=>processResult($permisos),
						'vehiculos'=>processResult($autos,false,true),
						'soporte'=>json_decode($soporte)->d,
						'loginResponse'=>$preProcess
					);
					
					$pos = strpos($userData['Nombre'],">");

					if($pos > 0){
						$userData['Nombre'] = substr($userData['Nombre'],$pos+1,strlen($userData['Nombre']));
					}
					$json['user'] = $userData;
				}else{
					$json = array(
						'status'=>'DENIED',
						'token'=>null,
						'user'=>processResult($loginResponse),
						'response'=>$GLOBALS['headers'],
						'res'=>$result
					);
				}
			}
		break;
		case "GetDatosReporteServicio":
			$res = getDatosRS($_REQUEST['token']);
			list($resp,$vars) = $res;
			$json = array(
				'status'=>'OK',
				'reporteServicio'=>json_decode($resp),
				'vars'=>$vars
			);
		break;
		case "getUserData":
			$res = getUserData($_REQUEST['token']);
			list($time,$tipo,$permisos,$autos,$soporte) = $res;
			if($blockApp == true){
				$json = array(
					'status'=>'BLOCK',
					'token'=>null,
					'user'=>array(
						'Status'=>0
					)
				);
			}else{
				$json = array(
					'status'=>'OK',
					'serverTime'=>processResult($time), 
					'tipo'=>processResult($tipo),
					'permisos'=>processResult($permisos),
					'vehiculos'=>processResult($autos,false,true),
					'soporte'=>json_decode($soporte)->d,
					'user'=>array(
						'Status'=>1
					)
				);
			}
		break;
		case "ModificaVehiculo":
			$vehiculo = array(
				"vehiculo"=> array(
					  "Anio"=>         	$_REQUEST['Anio'],
					  "Carga"=>       	$_REQUEST['Carga'],
					  "Chofer"=>      	$_REQUEST['Chofer'],
					  "Color"=>     	$_REQUEST['Color'],
					  "Descripcion"=>  	$_REQUEST['Descripcion'],
					  "Detalle"=>      	$_REQUEST['Detalle'],
					  "EstadoLimite"=> 	intval($_REQUEST['EstadoLimite']),
					  "Etiquetas"=>    	$_REQUEST['Etiquetas'],
					  "IconoID"=>      	intval($_REQUEST['IconoID']),
					  "Limite"=>      	$_REQUEST['Limite'],
					  "Marca"=>        	$_REQUEST['Marca'],
					  "Modelo"=>       	$_REQUEST['Modelo'],
					  "NoSerie"=>      	$_REQUEST['NoSerie'],
					  "Placas"=>       	$_REQUEST['Placas'],
					  "Precio"=>       	intval($_REQUEST['Precio']),
					  "Rendimiento"=>  	intval($_REQUEST['Rendimiento']),
					  "TipoVehiculo"=> 	intval($_REQUEST['TipoVehiculo']),
					  "Unidad"=>       	$_REQUEST['Unidad']
				)
			);
			sendPost($vehiculo,$GLOBALS['authUrl']."ModificaVehiculo",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>processResult($GLOBALS['response']),
				"sent"=>$vehiculo,
				"response"=>$GLOBALS['response']
			);
		break;
		case "getServerTime":
			sendPost("",$GLOBALS['rasAutoUrl']."getServerTime",true,$_REQUEST['token'],true); 
			
			$json = array(
				'status'=>'OK',
				'time'=>processResult($GLOBALS['response'])
			);
		break;
		case "getPermisosNuevos":
			sendPost("",$GLOBALS['authUrl']."getPermisosNuevos",true,$_REQUEST['token'],true); 
			
			$json = array(
				'status'=>'OK',
				'permisos'=>processResult($GLOBALS['response'])
			);
		break;
		case "getTipoPlataforma":
			sendPost("",$GLOBALS['authUrl']."getTipoPlataforma",true,$_REQUEST['token'],true); 
			
			$json = array(
				'status'=>'OK',
				'data'=>processResult($GLOBALS['response'])
			);
		break;
	}
	echo json_encode($json);
?>