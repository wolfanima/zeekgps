<?php
	$GLOBALS['authUrl'] = "https://apps.zeekgps.com/zeektemp/AutentificacionWS.asmx/";
	$GLOBALS['authUrl_alpha'] = "https://apps.zeekgps.com/alpha/AutentificacionWS.asmx/";
	$GLOBALS['rasStaticUrl'] = "https://apps.zeekgps.com/zeektemp/RastreoEstaticoWS.asmx/";
	$GLOBALS['rasAutoUrl'] = "https://apps.zeekgps.com/zeektemp/ReportesAutomaticosWS.asmx/";
	$GLOBALS['geoUrl'] = "https://apps.zeekgps.com/zeektemp/GeocercasWS.asmx/";
	$GLOBALS['pointUrl'] = "https://apps.zeekgps.com/zeektemp/PuntoGrupoWS.asmx/";
	$GLOBALS['interfaceUrl'] = "https://apps.zeekgps.com/zeektemp/InterfacesWS.asmx/";
	$GLOBALS['headers'] = "";
	$GLOBALS['response'] = ""; 
	$json = array(); 
?>