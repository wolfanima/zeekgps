<?php 
	ini_set('max_execution_time', 300);
	function getUserData($token) {
		$resp = array();
		sendPost("",$GLOBALS['rasAutoUrl']."getServerTime",true,$token,true); 
		array_push($resp,$GLOBALS['response']);
		
		sendPost("",$GLOBALS['authUrl']."getTipoPlataforma",true,$token,true); 
		array_push($resp,$GLOBALS['response']);
		
		sendPost("",$GLOBALS['authUrl']."getPermisosNuevos",true,$token,true); 
		array_push($resp,$GLOBALS['response']);
		
		sendPost("",$GLOBALS['rasStaticUrl']."getVehiculosCliente",true,$token,true);
		array_push($resp,$GLOBALS['response']);
		
		array_push($resp,getDatosRS($token));
		return $resp;
	}
	function getUserData2($token) {
		$resp = array();
		sendPost("",$GLOBALS['rasAutoUrl']."getServerTime",true,$token,true); 
		array_push($resp,$GLOBALS['response']);
		
		sendPost("",$GLOBALS['authUrl']."getTipoPlataforma",true,$token,true); 
		array_push($resp,$GLOBALS['response']);
		
		sendPost("",$GLOBALS['authUrl_alpha']."getPermisosNuevos",true,$token,true); 
		array_push($resp,$GLOBALS['response']);
		
		sendPost("",$GLOBALS['rasStaticUrl']."getVehiculosCliente",true,$token,true);
		array_push($resp,$GLOBALS['response']);
		
		array_push($resp,getDatosRS($token));
		return $resp;
	}
	function getDatosRS($token) {
		$resp = array();
		parse_str(str_replace("+","%2B",$token),$datos);
		$vars = array(
			"licencia"=>"stakentecomMYona",
			"clienteID"=>$datos['Cliente']
		);
		sendPost($vars,"https://apps.zeekgps.com/zeekapijs/CMovilApi3WS.asmx/GetDatosReporteServicio",true,false,true);
		return $GLOBALS['response'];
	}
	function sendPost($data,$url,$showHeaders,$cookie = false,$useJson = false){
		//http header
		$headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8');
		//curl connection
		$ch = curl_init();
		if($cookie != false){
			curl_setopt($ch, CURLOPT_COOKIE, $cookie);
			//curl_setopt($ch, CURLOPT_HTTPHEADER, array("Cookie: $cookie"));
		}
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		//curl_setopt($ch, CURLOPT_HEADER_OUT, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION , false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch, CURLOPT_TIMEOUT, 90);
		if($data != ''){
			if($useJson != false){
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(          
					'Content-Type: application/json; charset=UTF-8',                                                                                
					'Content-Length: ' . strlen( json_encode($data)))                                                                       
				);    
			}else{
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
			}
		}else{
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array()));
		}
		 
		$result = curl_exec($ch);
		$GLOBALS['result'] = $result;
		 // Added Code
		$header_len = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$GLOBALS['headers'] = substr($result, 0, $header_len);
		$GLOBALS['response'] = substr($result, $header_len);

		
		if ($result === false) {
			$result = curl_error($ch);
		}
		curl_close($ch);
		return $result;
	}
	function object_to_array($data) {

    if (is_object($data)) {
        $data = get_object_vars($data);
    }

    if (is_array($data)) {
        return array_map(__FUNCTION__, $data);
    }
    else {
        return $data;
    }
}
	/**function processResult($res,$slashes = true){
		$use_errors = libxml_use_internal_errors(true);
		$xml = simplexml_load_string($res);
		if (false === $xml) {
			$ret = null;
		}else{
			$ret = json_decode(($slashes == true ? stripslashes($xml) : $xml));
		}
		return $ret;
	}*/
	function processResult($res,$slashes = true,$force = false){
		$use_errors = libxml_use_internal_errors(true);
		$xml = simplexml_load_string($res);
		if (false === $xml && $force == false) {
			$ret = null;
		}else{
			$ret = json_decode(($slashes == true ? stripslashes($xml) : $xml));
		}
		return $ret;
	}
?>