<?php
	header("Access-Control-Allow-Origin: *");
	header('Content-type: text/json; charset=utf-8');
	
	//include_once("Connection.php");
	include_once("Vars.php");
	include_once("Functions.php");
	
	switch($_REQUEST['api']){
		case "ConsultarGeocercas":
			sendPost("",$GLOBALS['geoUrl']."ConsultarGeocercas",true,$_REQUEST['token']);
			
			$json = array(
				"status"=>"OK",
				"data"=>processResult($GLOBALS['response'])
			);
		break;
		case "ConsultarPuntosGeocerca":
			$data = array(
				"geoLista"=>json_decode($_REQUEST['geoLista'])
			);
			sendPost($data,$GLOBALS['geoUrl']."ConsultarPuntosGeocerca",true,$_REQUEST['token'],true);
			
			$respJson = json_decode($GLOBALS['response']);
			$parsed = json_decode($respJson->d);
			$parsed = $parsed[0];
			$puntos = array();
			
			for($i = 0;$i<count($parsed->Puntos);$i++){
				$puntos[] = array(
					"lat"=>$parsed->Puntos[$i]->Latitud,
					"lng"=>$parsed->Puntos[$i]->Longitud
				);
			}
			$json = array(
				"status"=>"OK",
				"data"=>array(
					"GeocercaID"=>	$parsed->GeocercaID,
					"Nombre"=>		$parsed->Nombre,
					"Activada"=>	$parsed->Activada,
					"Zona"=>		$parsed->Zona,
					"Puntos"=>		$puntos
				)
			);
		break;
		case "GuardarPuntosGeocerca":
			$correos = "";
			if($_REQUEST['correos'] != ''){
				for($i = 0;$i<count($_REQUEST['correos']);$i++){
					$correos= $correos.$_REQUEST['correos'][$i];
					if($i+1 < count($_REQUEST['correos'])){
						$correos = $correos.",";
					}
				}
			}
			$data = array(
				"accion"=>$_REQUEST['accion'],
				"geocerca"=>array(
					"Activada"=>($_REQUEST['activada'] == "true" ? true : false),
					"Ciudad"=>"ng",
					"CorreoElectronico"=>$correos,
					"Descripcion"=>(!isset($_REQUEST['descripcion']) ? "" : $_REQUEST['descripcion']),
					"Nombre"=>$_REQUEST['nombre'],
					"Puntos"=>json_decode($_REQUEST['puntos']),
					"PuntosEditables"=>array(),
					"Tipo"=>$_REQUEST['tipo'],
					"GeocercaID"=>(!isset($_REQUEST['id']) ? "new" : $_REQUEST['id']),
					"Zona"=>(!isset($_REQUEST['zona']) ? 0 : $_REQUEST['zona']),
					"visual"=>array(
						"Color"=>$_REQUEST['color']
					)
					
				)
			);
			sendPost($data,$GLOBALS['geoUrl']."GuardarPuntosGeocerca",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>json_decode($GLOBALS['response'])
			);
		break;
		case "actualizarGeocerca":
			$correos = "";
			if($_REQUEST['correos'] != ''){
				for($i = 0;$i<count($_REQUEST['correos']);$i++){
					$correos= $correos.$_REQUEST['correos'][$i];
					if($i+1 < count($_REQUEST['correos'])){
						$correos = $correos.",";
					}
				}
			}
			$data = array(
				"geocerca"=>array(
					"Activada"=>($_REQUEST['activada'] == "true" ? true : false),
					"Ciudad"=>"ng",
					"CorreoElectronico"=>$correos,
					"Descripcion"=>(!isset($_REQUEST['descripcion']) ? "" : $_REQUEST['descripcion']),
					"Nombre"=>$_REQUEST['nombre'],
					"Tipo"=>$_REQUEST['tipo'],
					"GeocercaID"=>(!isset($_REQUEST['id']) ? "new" : $_REQUEST['id']),
					"Zona"=>(!isset($_REQUEST['zona']) ? 0 : $_REQUEST['zona'])
					
				)
			);
			sendPost($data,$GLOBALS['geoUrl']."actualizarGeocerca",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>json_decode($GLOBALS['response']),
				"sent"=>$data
			);
		break;
		case "EliminarGeocercas":
			$data = array(
				"geocerca"=>json_decode($_REQUEST['request'])
			);
			sendPost($data,$GLOBALS['geoUrl']."EliminarGeocercas",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>json_decode($GLOBALS['response'])
			);
		break;
		case "ConsultarRelaciones":
			$data = array(
				"geocercaID"=>$_REQUEST['id']
			);
			sendPost($data,$GLOBALS['geoUrl']."ConsultarRelaciones",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>json_decode($GLOBALS['response'])
			);
		break;
		case "NuevaRelacion":
			$relaciones = array();
			for($i = 0;$i<count($_REQUEST['relaciones']);$i++){
				$relaciones[] = array(
					"GeocercaID"=>$_REQUEST['id'],
					"ServicioSolicitado"=>intval($_REUQEST['servicio']),
					"TipoNotificacion"=>intval($_REQUEST['tipo']),
					"UnidadID"=>$_REQUEST['relaciones'][$i],
					"UnidadSesionID"=>0
				);
			}
			$data = array(
				"relaciones"=>$relaciones
			);
			sendPost($data,$GLOBALS['geoUrl']."NuevaRelacion",true,$_REQUEST['token'],true);
			
			$json = array( 
				"status"=>"OK",
				"data"=>json_decode($GLOBALS['response'])
			);
		break;
		case "EliminarRelacion":
			$relaciones = array($_REQUEST['relacion']);
			
			$data = array(
				"relaciones"=>$relaciones
			);
			sendPost($data,$GLOBALS['geoUrl']."EliminarRelacion",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>json_decode($GLOBALS['response'])
			);
		break;
		case "ConsultarRestricciones":
			$data = array(
				"geocercaID"=>$_REQUEST['id']
			);
			sendPost($data,$GLOBALS['geoUrl']."ConsultarRestricciones",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>json_decode($GLOBALS['response'])
			);
		break;
		case "GuardarRestriccion":
			if(isset($_REQUEST['restID'])){
				$restricciones = array(
					"restriccionID"=>$_REQUEST['restID'],
					"geocercaID"=>	$_REQUEST['id'],
					"hInicial"=>	$_REQUEST['hIni'],
					"hFinal"=>		$_REQUEST['hFin']
				);

			}else{
				$restricciones = array(
					"geocercaID"=>	$_REQUEST['id'],
					"hInicial"=>	$_REQUEST['hIni'],
					"hFinal"=>		$_REQUEST['hFin']
				);
			}
			$data = array(
				"restricciones"=>$restricciones
			);
			sendPost($data,$GLOBALS['geoUrl']."GuardarRestriccion",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>json_decode($GLOBALS['response']),
				"sent"=>$data
			);
		break;
		case "EliminarRestricciones":
			$data = array(
				"restriccionesID"=> array($_REQUEST['time'])
			);
			sendPost($data,$GLOBALS['geoUrl']."EliminarRestricciones",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>json_decode($GLOBALS['response']),
				"sent"=>$data
			);
		break;
	}
	
	echo json_encode($json);
	
?>