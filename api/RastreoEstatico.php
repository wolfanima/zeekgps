<?php
	header("Access-Control-Allow-Origin: *");
	header('Content-type: text/json; charset=utf-8');
	
	//include_once("Connection.php");
	include_once("Vars.php");
	include_once("Functions.php");
	
	switch($_REQUEST['api']){
		case "getVehiculosCliente":
			sendPost("",$GLOBALS['rasStaticUrl']."getVehiculosCliente",true,$_REQUEST['token']);
			
			$json = array(
				"status"=>"OK",
				"data"=>processResult($GLOBALS['response'],false,true)
			);
		break;
		case "getVehiculosCliente2";
			sendPost("",$GLOBALS['rasStaticUrl']."getVehiculosCliente",true,$_REQUEST['token']);
			$json = array(
				"status"=>"OK",
				"data"=>processResult2($GLOBALS['response'],false,true),
				"res"=>$GLOBALS['response']
			);
		break;
		case "getPermisosApps":
			sendPost("",$GLOBALS['rasStaticUrl']."getPermisosApps",true,$_REQUEST['token']);
			
			$json = array(
				"status"=>"OK",
				"data"=>processResult($GLOBALS['response'])
			);
		break;
		case "UltimaUbicacionTransporte":
			$data = array(
				"unidades"=>$_REQUEST['unidades'],
				"zona"=> intval($_REQUEST['zona'])
			);
			sendPost($data,$GLOBALS['rasStaticUrl']."UltimaUbicacionTransporte",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>json_decode($GLOBALS['response']),
				"sent"=>$data
			);
		break;
		case "ConsultaHistoricoTransporte":
			$data = array(
				"unidad"=>$_REQUEST['unidad'],
				"fechaFinal"=>$_REQUEST['fin'],
				"fechaInicial"=>$_REQUEST['ini'],
				"zona"=> intval($_REQUEST['zona'])
			);
			sendPost($data,$GLOBALS['rasStaticUrl']."ConsultaHistoricoTransporte",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>json_decode($GLOBALS['response'])
			);
		break;
		case "ConsultaTiempoMuerto":
			$data = array(
				"unidad"=>$_REQUEST['unidad'],
				"fechaFinal"=>$_REQUEST['fin'],
				"fechaInicial"=>$_REQUEST['ini'],
				"minutos"=>$_REQUEST['mins'],
				"puntos"=>null,
				"zona"=> intval($_REQUEST['zona'])
			);
			sendPost($data,$GLOBALS['rasStaticUrl']."ConsultaTiempoMuerto",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>json_decode($GLOBALS['response'])
			);
		break;
		case "ConsultaEventos":
			$data = array(
				"unidad"=>$_REQUEST['unidad'],
				"fechaFinal"=>$_REQUEST['fin'],
				"fechaInicial"=>$_REQUEST['ini'],
				"zona"=> intval($_REQUEST['zona'])
			);
			sendPost($data,$GLOBALS['rasStaticUrl']."ConsultaEventos",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>json_decode($GLOBALS['response'])
			);
		break;
		case "ConsultaExcesos":
			$data = array(
				"unidad"=>$_REQUEST['unidad'],
				"fechaFinal"=>$_REQUEST['fin'], 
				"fechaInicial"=>$_REQUEST['ini'],
				"velocidad"=>intval($_REQUEST['speed']),
				"zona"=> intval($_REQUEST['zona'])
			);
			sendPost($data,$GLOBALS['rasStaticUrl']."ConsultaExcesos",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>json_decode($GLOBALS['response'])
			);
		break;
		case "ConsultaRalenti":
			$data = array(
				"unidad"=>$_REQUEST['unidad'],
				"fechaFinal"=>$_REQUEST['fin'], 
				"fechaInicial"=>$_REQUEST['ini'],
				"zona"=> intval($_REQUEST['zona'])
			);
			sendPost($data,$GLOBALS['rasStaticUrl']."ConsultaRalenti",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK", 
				"data"=>json_decode($GLOBALS['response'])
			);
		break;
		case "getOrdenesDeTrabajo":
			$data = array(
				"fechaFinal"=>$_REQUEST['fin'], 
				"fechaInicial"=>$_REQUEST['ini'],
				"unidades"=>array()
			);
			sendPost($data,$GLOBALS['rasStaticUrl']."getOrdenesDeTrabajo",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK", 
				"data"=>json_decode($GLOBALS['response'])
			);
		break;
		case "MultiEvento":
			$data = array(
				"unidades"=>$_REQUEST['unidades'],
				"fechaFinal"=>$_REQUEST['fechaFinal'],
				"fechaInicial"=>$_REQUEST['fechaInicial'],
				"zona"=> intval($_REQUEST['zona'])
			);
			sendPost($data,$GLOBALS['rasStaticUrl']."MultiEvento",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>json_decode($GLOBALS['response'])
			);
		break;
	}
	
	echo json_encode($json);
?>