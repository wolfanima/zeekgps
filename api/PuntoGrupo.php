<?php
	header("Access-Control-Allow-Origin: *");
	header('Content-type: text/json; charset=utf-8');
	
	//include_once("Connection.php");
	include_once("Vars.php");
	include_once("Functions.php");
	
	switch($_REQUEST['api']){
		case "ObtenerPuntosConGrupos":
			sendPost("",$GLOBALS['pointUrl']."ObtenerPuntosConGrupos",true,$_REQUEST['token']);
			
			$json = array(
				"status"=>"OK",
				"data"=>processResult($GLOBALS['response'],false),
				"resp"=>$GLOBALS['response']
			);
		break;
		case "ObtenerGrupos":
			sendPost("",$GLOBALS['pointUrl']."ObtenerGrupos",true,$_REQUEST['token']);
			
			$json = array(
				"status"=>"OK",
				"data"=>processResult($GLOBALS['response'])
			);
		break;
		case "CrearPunto":
			$grupos = array();
			$punto = array();
			for($i = 0;$i<count($_REQUEST['grupos']);$i++){
					$grupos[] = array(
						"GrupoID"=>			intval($_REQUEST['grupos'][$i]['gid']),
						"Nombre"=>			$_REQUEST['grupos'][$i]['name'],
						"Color"=>			intval($_REQUEST['grupos'][$i]['color']),
						"id"=>				'<input type="checkbox" value="edicionPG'.$_REQUEST['grupos'][$i]['gid'].'"/>',
						"NombreDisplay"=>	'<span title="'.$_REQUEST['grupos'][$i]['name'].'"alt="'.$_REQUEST['grupos'][$i]['name'].'">'.$_REQUEST['grupos'][$i]['name'].'</span>' 
					);
				}
			$punto = array(
				"punto"=>array(
					"PuntoID"=>-1,
					"Nombre"=>$_REQUEST['name'],
					"x"=>floatval($_REQUEST['x']),
					"y"=>floatval($_REQUEST['y']),
					"Telefono"=>$_REQUEST['tel'],
					"Email"=>$_REQUEST['mail'],
					"Pagina"=>$_REQUEST['pag'],
					"Horario"=>$_REQUEST['hora'],
					"Direccion"=>$_REQUEST['dir'],
					"Nota"=>$_REQUEST['nota'],
					"Grupos"=>$grupos,
					"oGrupos"=>$grupos,
					"Calle"=>"N/A",
					"Colonia"=>"N/A", 
					"Ciudad"=>"N/A"
				) 
			);
			sendPost($punto,$GLOBALS['pointUrl']."CrearPunto",true,$_REQUEST['token'],true); 
			
			$json = array(
				"status"=>"OK",
				"data"=>$GLOBALS['response'],
				"sent"=>$punto 
			);
		break;
		case "ActualizarPunto":
			$grupos = array();
			$punto = array();
			for($i = 0;$i<count($_REQUEST['grupos']);$i++){
					$grupos[] = array(
						"GrupoID"=>			intval($_REQUEST['grupos'][$i]['gid']),
						"Nombre"=>			$_REQUEST['grupos'][$i]['name'],
						"Color"=>			intval($_REQUEST['grupos'][$i]['color']),
						"id"=>				'<input type="checkbox" value="edicionPG'.$_REQUEST['grupos'][$i]['gid'].'"/>',
						"NombreDisplay"=>	'<span title="'.$_REQUEST['grupos'][$i]['name'].'"alt="'.$_REQUEST['grupos'][$i]['name'].'">'.$_REQUEST['grupos'][$i]['name'].'</span>' 
					);
				} 
			$punto = array(
				"punto"=>array(
					"PuntoID"=>$_REQUEST['pid'],
					"Nombre"=>$_REQUEST['name'],
					"x"=>floatval($_REQUEST['x']),
					"y"=>floatval($_REQUEST['y']),
					"Telefono"=>$_REQUEST['tel'],
					"Email"=>$_REQUEST['mail'],
					"Pagina"=>$_REQUEST['pag'],
					"Horario"=>$_REQUEST['hora'],
					"Direccion"=>$_REQUEST['dir'],
					"Nota"=>$_REQUEST['nota'],
					"Grupos"=>$grupos,
					"oGrupos"=>$grupos,
					"Calle"=>"N/A",
					"Colonia"=>"N/A", 
					"Ciudad"=>"N/A"
				)
			);
			sendPost($punto,$GLOBALS['pointUrl']."ActualizarPunto",true,$_REQUEST['token'],true); 
			
			$json = array(
				"status"=>"OK",
				"data"=>$GLOBALS['response']
			);
		break;
		case "CrearGrupo":
			$grupo = array(
				"grupo"=> array(
					"Color"=>		$_REQUEST['color'],
					"Nombre"=>		$_REQUEST['name'] 
				)
			);
			sendPost($grupo,$GLOBALS['pointUrl']."CrearGrupo",true,$_REQUEST['token'],true); 
			
			$json = array(
				"status"=>"OK",
				"data"=>$GLOBALS['response']
			);
		break;
		case "ActualizarGrupo":
			$grupo = array(
				"grupo"=> array(
					"GrupoID"=>		$_REQUEST['gid'],
					"Color"=>		$_REQUEST['color'],
					"Nombre"=>		$_REQUEST['name'] 
				)
			);
			sendPost($grupo,$GLOBALS['pointUrl']."ActualizarGrupo",true,$_REQUEST['token'],true); 
			
			$json = array(
				"status"=>"OK",
				"data"=>$GLOBALS['response']
			);
		break;
		case "EliminarPuntos":
			$puntos = array(
				"puntos"=>array($_REQUEST['pid'])
			);
			sendPost($puntos,$GLOBALS['pointUrl']."EliminarPuntos",true,$_REQUEST['token'],true); 
			
			$json = array(
				"status"=>"OK",
				"data"=>$GLOBALS['response']
			);
		break;
		case "EliminarGrupos":
			$grupo = array(
				"grupos"=>array($_REQUEST['gid'])
			);
			sendPost($grupo,$GLOBALS['pointUrl']."EliminarGrupos",true,$_REQUEST['token'],true); 
			
			$json = array(
				"status"=>"OK",
				"data"=>$GLOBALS['response']
			);
		break;
		case "EliminarGruposConPuntos":
			$grupo = array(
				"grupos"=>array($_REQUEST['gid'])
			);
			sendPost($grupo,$GLOBALS['pointUrl']."EliminarGruposConPuntos",true,$_REQUEST['token'],true); 
			
			$json = array( 
				"status"=>"OK",
				"data"=>$GLOBALS['response']
			);
		break;
	}
	echo json_encode($json);
?>