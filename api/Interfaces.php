<?php
	header("Access-Control-Allow-Origin: *");
	header('Content-type: text/json; charset=utf-8');
	
	//include_once("Connection.php");
	include_once("Vars.php");
	include_once("Functions.php");
	
	switch($_REQUEST['api']){
		case "AccionIO":
			$data = array(
				"interfaz"=>$_REQUEST['interfaz'],
				"activar"=>	$_REQUEST['activar'],
				"unidad"=>	$_REQUEST['unidad']
			);
			sendPost($data,$GLOBALS['interfaceUrl']."AccionIO",true,$_REQUEST['token']);
			
			$json = array(
				"status"=>"OK",
				"data"=>processResult($GLOBALS['response'])
			);
		break;
		case "GetInterfaces":
			$data = array(
				"unidades"=> $_REQUEST['unidades']
			);
			sendPost($data,$GLOBALS['interfaceUrl']."GetInterfaces",true,$_REQUEST['token'],true);
			
			$json = array(
				"status"=>"OK",
				"data"=>json_decode(json_decode($GLOBALS['response'])->d)
			);
		break;
	}
	
	echo json_encode($json);
	
?>