/// <reference path ="jquery.d.ts"/> 
/**
 * MAPCONTROLS
 * Clase con funciones simples para el control del mapa.
 */
class MapControls {
    /**
     * Centra el mapa en una coordenada con un zoom determinado
     * @param lat Latitud del nuevo centro de mapa
     * @param lng Longitud del nuevo centro de mapa
     * @param zoom Nivel de zoom del mapa deseado
     */
	public FocusOnCoords(lat:number,lng:number,zoom : number = 15){
        map.setCenter({lat:lat, lng: lng});
        map.setZoom(zoom);
    }
    /**
     * Limpia el mapa y detiene el Rastreo Continuo, si lo hay
     */
    public ClearMap() {
        PuntoGrupo._pointsOnMap = new Array();
        Tracking._isLastPosition = false;
        Tracking._isHistory = false;
        Tracking._isTracking = false;
        Track.StopTracking();
        Checkpoints.RemoveCheckpoints();
        
        
        Geocercas.selectedShapes = new Array();
        Geocercas.polyGroup.removeAll();
        Tracking._eventGroup.removeAll();
    }
}