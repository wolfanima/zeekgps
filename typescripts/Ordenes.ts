/// <reference path ="jquery.d.ts"/> 
/**
 * [DEPRECATED]
 * ORDENES
 * Clase que engloba vista y metodos de Ordenes de Trabajo.
 */
class Ordenes {
    static _orders;

    static _dataFieldsEN = [
        ["",                    "Choose an option"],
        ["Nombre",              "Order"],
        ["UnidadDescripcion",   "Unit"],
        ["GeocercaInicio",      "Start"],
        ["GeocercaFin",         "End"],
        ["Estatus",             "Status"],
        ["FechaInicio",         "Start Date"],
        ["FechaFin",            "End Date"]
    ];
    static _dataFieldsES = [
        ["",                    "Elija una opción"],
        ["Nombre",              "Orden"],

        ["UnidadDescripcion",   "Unidad"],
        ["GeocercaInicio",      "Inicio"],
        ["GeocercaFin",         "Fin"],
        ["Estatus",             "Estatus"],
        ["FechaInicio",         "Fecha Inicio"],
        ["FechaFin",            "Fecha Fin"]
    ];
    static _orderFieldsEN = [
        ["ASC", "Ascendant"],
        ["DESC","Descendant"]
    ];
    static _orderFieldsES = [
        ["ASC", "Ascendente"],
        ["DESC","Descendente"]
    ];

    public BuildSelects(){
        var data, order;
        if(idioma == "en"){
            data = Ordenes._dataFieldsEN;
            order = Ordenes._orderFieldsEN;
        }else{
            data = Ordenes._dataFieldsES;
            order = Ordenes._orderFieldsES;
        }
        $("select.orderOrders").html('');
        for(var i =0;i<data.length;i++){
            if(data[i][0] == ""){
                $("select.orderOrders").append('<option value="'+data[i][0]+'" disabled>'+data[i][1]+'</option>');
            }else{
                $("select.orderOrders").append('<option value="'+data[i][0]+'">'+data[i][1]+'</option>');
            }
        }
        $("select.orderType").html('');
        for(var i =0;i<order.length;i++){
            if(order[i][0] == ""){
                $("select.orderType").append('<option value="'+order[i][0]+'" disabled>'+order[i][1]+'</option>');
            }else{
                $("select.orderType").append('<option value="'+order[i][0]+'">'+order[i][1]+'</option>');
            }
        }
        
        $('select').material_select();
        //$('.tooltipped').tooltip({delay: 50});
    }
    public OpenWorkOrders(){
        this.BuildSelects();
        this.getOrdenesDeTrabajo();
        $("#ordenes").removeClass("hidden");
        $("#ordenes .toolbox").html('');
        var mes = new moment();
        $("#ordenes .toolbox").append('<div class="toolhead">'+
            '<span class="label"><es>Rango</es><en>Range</en></span>'+
        '</div>');
        for(var i=0;i<3;i++){
            var selected = '';
            if(i == 0){
                selected = 'selected';
            }
            $("#ordenes .toolbox").append('<div onclick="Orders.ChangeMonth('+i+')" _id="'+i+'">'+
                '<span class="check '+selected+'"><i class="fas fa-check"></i></span>'+
                '<span class="label">'+mes.format('MMMM')+'</span>'+
            '</div>');
            mes.subtract(1,'month');
        }
    }
    public CloseWorkOrders() {
        $("#ordenes").addClass("hidden");
    }

    public OpenToolbox() {
        $("#ordenes .toolbox").removeClass("hidden");
    }
    public CloseToolbox(){
        $("#ordenes .toolbox").addClass("hidden");
    }
    public ChangeMonth(mid : number = 0) {
        console.log("Change Month:",mid);
        $("#ordenes .toolbox div span.check").removeClass("selected");
        $("#ordenes .toolbox div[_id="+mid+"] span.check").addClass("selected");
        var ini = new moment().startOf('month').subtract(mid,'months').toDate().getTime();
        var fin = new moment().endOf('month').subtract(mid,'months').toDate().getTime();
        this.getOrdenesDeTrabajo(ini,fin);
    }
    public getOrdenesDeTrabajo(ini : any = null,fin : any = null) {
        wait("<es>Obteniendo Ordenes de Trabajo</es><en>Fetching Work Orders</en>...");
        if(ini == null){
            var fechaIni = new moment().startOf('month').toDate().getTime();
        }else{
            var fechaIni = ini;
        }
        if(fin == null){
            var fechaFin = new moment().toDate().getTime();
        }else{
            var fechaFin = fin;
        }
        ajax("POST",rasStaticUrl  + "getOrdenesDeTrabajo",{
            token:  window["userData"]["token"],
            fin:    fechaFin,
            ini:    fechaIni
        },
        function(data){
            console.log(JSON.parse(data.data.d));
            Ordenes._orders = JSON.parse(data.data.d);
            Orders.FillOrders();
            closeWait();
        },
        function(data){
            closeWait();
            toasty(lang("tryagain"),"error");
        });
    }

    public FillOrders(by : string = "Nombre",order : string = "ASC") {
        console.log("Ordering by: "+by+" ("+order+")");
        $(".orderList tbody").html('');
       
        var tmp = ordenar(Ordenes._orders,by,order);
        for(var i=0;i<tmp.length;i++){
            var fechaIni = new moment(tmp[i].FechaInicio).subtract(Math.abs(window.zona),'hours').format('YYYY/MM/DD HH:mm:ss');
            if('FechaFin' in tmp[i] === false) {
                var fechaFin = '';
            }else{
                var fechaFin = new moment(tmp[i].FechaFin).subtract(Math.abs(window.zona),'hours').format('YYYY/MM/DD HH:mm:ss');
            }
            $(".orderList tbody").append('<tr onclick="Orders.OpenOrder(\''+tmp[i].ID+'\')">'+
                '<td class="order">'+tmp[i].Nombre+'</td>'+
                '<td class="unit">'+tmp[i].UnidadDescripcion+'</td>'+
                '<td class="ini">'+tmp[i].GeocercaInicio+'</td>'+
                '<td class="fin">'+tmp[i].GeocercaFin+'</td>'+
                '<td class="status">'+tmp[i].Estatus +'</td>'+
                '<td class="dateini">'+fechaIni+'</td>'+
                '<td class="datefin">'+(tmp[i].FechaFin == undefined ? "-" : fechaFin)+'</td>'+
            '</tr>');
        }
        if(tmp.length == 0){
            $(".orderList tbody").append('<tr>'+
                '<td colspan="7" align="center" style="text-align:center !important"><es>No hay información para mostrar</es><en>No data available</en></td>'+
            '</tr>');
        }
        var options = {
            valueNames: [ 'order', 'unit','ini','fin','status','dateini','datefin'],
            pagination: true,
            page: $("#ordenes select.itemsPage").val()
          };
          
        window.orderList = new List('orderList', options);
        window.orderList.on('updated',function(){
            $("#orderList .list").scrollLeft(0);
            //console.log("Click!");
        });
    }

    public OpenOrder (id : number) {
        var oid = objectIndex(Ordenes._orders,"ID",id);
        
        

    }
}