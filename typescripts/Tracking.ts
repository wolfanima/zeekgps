/**
 * TRACKING
 * Esta clase engloba:
 *  Rastreo Continuo
 *  Ultima Posicion
 *  Historico
 */
/// <reference path ="jquery.d.ts"/> 
class Tracking {
    public static _rastrearUnidades = new Array();
    public static _gruposSeleccionados = new Array();
    public static _tempUnidades = new Array();
    public static _tempGrupos = new Array();
    public static _watchingUnit = null; //Revisando Eventos
    public static _followingUnit = null; //Siguiendo en el Mapa
    public static _markerInstance = null;
    public static _trackTimer = null;
    public static _markerGroup; 
    public static _eventGroup;
    public static _eventMarker;
    public static _mapExpanded = false;
    public static _isTracking = false;
    public static _isLastPosition = false;
    public static _isHistory = false;
    public static _isHistoryPoints = false;
    public static _isDeadTime = false;
    public static _isEvents = false;
    public static _isSpeedLimit = false;
    public static _isIdleTime = false;
    public static _isVisited = false;
    public static _drawTrack = true;
    public static _isDefault = true;
    public static _locationArray = new Array();
    public static _locationGroup;
    public static _historyLimit = false;
    public static _timeProgress;
    public static _timeCounter = 0;
    public static _timeSeconds = 60;
    public static _spotProximity = 100; //METERS
    public static _spotDeadTime = 5; //MINUTES
    public static _eventArray = new Array();
    public static _focusedCar;
    public static _histFechaIni = "";
    public static _histFechaFin = "";
    public static _histUnidad = "";
    public static _bannerList = new Array();

    static _rangeFieldsES = [
        [-1,    "Seleccione una opción"],
        ["2h",  "&Uacute;ltimas 2 Horas"],
        ["8h",  "&Uacute;ltimas 8 Horas"],
        ["12h", "&Uacute;ltimas 12 Horas"],
        ["td",  "Hoy"],
        ["yd",  "Ayer"],
        ["week","Semana en curso"],
        ["month","Mes en curso"]
    ];
    static _rangeFieldsEN = [
        [-1,    "Select an option"],
        ["2h",  "Last 2 Hours"],
        ["8h",  "Last 8 Hours"],
        ["12h", "Last 12 Hours"],
        ["td",  "Today"],
        ["yd",  "Yesterday"],
        ["week","Current Week"],
        ["month","Current Month"]
    ];

    constructor(){
        Tracking._markerGroup  = new H.map.Group();
        Tracking._eventGroup  = new H.map.Group();
        Tracking._locationGroup = new H.map.Group();
        map.addObject(Tracking._markerGroup);
        map.addObject(Tracking._eventGroup);
        map.addObject(Tracking._locationGroup);
        Tracking._timeProgress = $('#time');
        $(document).on("circle-animation-end","#time",function(){
            if(Tracking._isTracking == true){
                Track.UltimaUbicacionTransporte();
                Track.Timer();
            }
        });
        $(document).on("circle-animation-progress","#time",function(e, animationProgress, stepValue){
            $("#progress .bar").css("width",(stepValue*100)+"%");
        });
        if(!!window.cordova === true){
            $('#histIni').attr("type","datetime-local");
            $('#histFin').attr("type","datetime-local");
        }
        this.AdjustTitle();
    }
    /**
     * Construye y rellena selects
     */
    public BuildSelects() {
        $("#timeMetric").html('');
        //$.datetimepicker.setLocale(window.idioma);
        var maxDate = new moment();
        var today = new moment();
        if (userData.tipo === "transporte") {
            maxDate.subtract(13,'week');
        } else {
            maxDate.subtract(4,'week');
        } 
        if(!!window.cordova === false){
            /*$('#histIni,#histFin').datetimepicker({
                format: 'Y-m-d H:i',
                step:5,
                minDate: maxDate.format("YYYY/MM/DD"),
                maxDate: 0
            });*/
        }
        $('#histIni,#histFin').attr("min", maxDate.format("YYYY-MM-DD[T]hh:mm:ss"));
        $('#histIni,#histFin').attr("MAX", today.format("YYYY-MM-DD[T]hh:mm:ss"));
        if(idioma == "en") {
            for(var i=0;i<Tracking._rangeFieldsEN.length;i++){
                var select ="";
                if(i == 0){
                    select = "disabled selected";
                }
                $("#timeMetric").append('<option value="'+Tracking._rangeFieldsEN[i][0]+'" '+select+'>'+Tracking._rangeFieldsEN[i][1]+'</option>');
            }
        }else{
            for(var i=0;i<Tracking._rangeFieldsES.length;i++){
                var select ="";
                if(i == 0){
                    select = "disabled selected";
                }
                $("#timeMetric").append('<option value="'+Tracking._rangeFieldsES[i][0]+'" '+select+'>'+Tracking._rangeFieldsES[i][1]+'</option>');
            }
        }
        $("#timeMetric").val(-1);
        formSelect();
    }
    /**
     * Abre vista de Rastreo Continuo
     */
    public SelectCars(){
        selectMenu(".RastreoContinuo");
        try{
            masterClose = ()=>{this.CloseTrackingSelect();};
        }catch(e){}
        $("#rastreoContinuo .selectall i").removeClass("fa-check-square").addClass("fa-square");

        Tracking._tempUnidades = new Array();
        $(".searchCars").val('');
        $("#rastreoContinuo .top .title").html("<es>Rastreo Continuo</es><en>Real Time Tracking</en>");
        $("#rastreoContinuo .top .tool.right").attr("onclick","Track.StartTracking()");
        $("#rastreoContinuo").removeClass("hidden");
       
        this.FillCarList();
        this.FillTagList();
        $(".subVista").scrollTop(0);
    }
    /**
     * Rellena la lista de Etiquetas
     * @param vista Vista del modulo actual
     * @param speedLimit Limite de velocidad?
     */
    public FillTagList(vista : string = "rastreoContinuo", speedLimit : boolean = false) {
        $("#"+vista+" .trackAddTagList .taglist").html('');
        $("#"+vista+" #trackAddTagList-"+vista).html('');
        /*$("#"+vista+" .trackAddTagList").append('<p class="tagselect"><label for="rttag-all-'+vista+'" onclick="Track.FillCarList(\'\',\''+vista+'\','+speedLimit+')" _id="0" >'+
                '<input name="rttag" type="radio" id="rttag-all-'+vista+'" checked value="" />'+
                '<span><es>Todos</es><en>All</en></span></label>'+
            "</p>");*/
        $("#"+vista+" .trackAddTagList .taglist").append(`<tr onclick="Track.FillCarList('0','${vista}',${speedLimit});check('rttag-all-${vista}',true)">`+
            `<td class="check"><label for="rttag-all-${vista}" _id="0"><input name="rttag" type="radio" id="rttag-all-${vista}" value="0" checked="true" /><span></span></label></td>`+
            `<td><es>Todos</es><en>All</en></td>`+
        '</tr>');
        $("#"+vista+" #trackAddTagList-"+vista).append('<option value="0" vista="'+vista+'">'+lang("all")+'</option>');

        for(var i =0;i<etiquetas.length;i++){
            $("#"+vista+" #trackAddTagList-"+vista).append('<option value="'+(i+1)+'" vista="'+vista+'">'+etiquetas[i]+'</option>');

            /*$("#"+vista+" .trackAddTagList").append('<p class="tagselect "><label for="rttag-'+etiquetas[i]+'-'+vista+'" _id="'+(i+1)+'" onclick="Track.FillCarList(\''+etiquetas[i]+'\',\''+vista+'\','+speedLimit+')">'+
                '<input name="rttag" type="radio" id="rttag-'+etiquetas[i]+'-'+vista+'" value="'+etiquetas[i]+'" />'+
                '<span>'+etiquetas[i]+'</span></label>'+
            "</p>");*/
            $("#"+vista+" .trackAddTagList .taglist").append( `<tr onclick="Track.FillCarList('${etiquetas[i]}','${vista}',${speedLimit});check('rttag-${cleanSpaces(etiquetas[i])}-${vista}',true)">`+
                `<td class="check"><label for="rttag-${cleanSpaces(etiquetas[i])}-${vista}" _id="${i+1}"><input name="rttag" type="radio" id="rttag-${cleanSpaces(etiquetas[i])}-${vista}" value="${etiquetas[i]}" /><span></span></label></td>`+
                `<td>${etiquetas[i]}</td>`+
            '</tr>');
        }
        formSelect();
    }
    /**
     * Rellena la lista de autos disponibles para rastrear o consultar
     * @param tag Etiqueta
     * @param vista Vista del modulo actual
     * @param speedLimit Limite de Velocidad?
     */
    public FillCarList(tag :string = "0",vista : string = "rastreoContinuo", speedLimit : boolean = false){
        console.log("Filling list for "+vista);
        $("#"+vista+" #trackAddTagList-"+vista).val($("#"+vista+" .trackAddTagList label[for='rttag-"+(tag == "0" ? "all" : cleanSpaces(tag))+"-"+vista+"']").attr("_id"));
        formSelect();
        $("#"+vista +" .trackAddCarList .list").html('');
        for(var i =0;i<vehiculos.length;i++){
            var checked = 'class="filled-in"';
            var found = false;

            if(Tracking._tempUnidades.indexOf(vehiculos[i].Unidad) > -1){
                checked = 'class="filled-in checked" checked="checked"';
            }
            var _tags = vehiculos[i].Etiquetas.split(",");

            if(tag != '0'){
                found = (_tags.indexOf(tag) > -1 ? true : false);
            }
            if(tag == "0" || found == true){

                    if(speedLimit == true || speedLimit == false){
                        if(vehiculos[i].Estado == -1){
                            $("#"+vista+" .trackAddCarList .list").append(`<tr onclick='Track.AddCarToTracking("${vehiculos[i].Unidad}")'>`+
                                (vista === "historial" ? `<td class="check" onclick="check('rtcar-${vehiculos[i].Unidad}-${vista}')"><label><input type="radio" name="onecar" id="rtcar-${vehiculos[i].Unidad}-${vista}" ${checked} value="${vehiculos[i].Unidad}" /><span></span></label></td>` : `<td class="check" onclick="check('rtcar-${vehiculos[i].Unidad}-${vista}')"><label><input type="checkbox" id="rtcar-${vehiculos[i].Unidad}-${vista}" ${checked} value="${vehiculos[i].Unidad}" /><span></span></label></td>`)+
                                `<td class="carname" onclick="check('rtcar-${vehiculos[i].Unidad}-${vista}')">${vehiculos[i].Descripcion}</td> `+
                                `<td class="carmake" onclick="check('rtcar-${vehiculos[i].Unidad}-${vista}')">${vehiculos[i].Marca == undefined ? '---' : vehiculos[i].Marca}</td> `+
                                `<td class="carmodel" onclick="check('rtcar-${vehiculos[i].Unidad}-${vista}')">${vehiculos[i].Modelo == undefined ? '---' : vehiculos[i].Modelo}</td> `+
                            +'</tr>');
                        }else{
                            $("#"+vista+" .trackAddCarList .list").append(`<tr class="unableToSelect">`+
                                (vista === "historial" ? `<td class="check" onclick=""><label><input type="radio" name="onecar" disabled="true" id="rtcar-${vehiculos[i].Unidad}-${vista}" ${checked} value="${vehiculos[i].Unidad}" /><span></span></label></td>` : `<td class="check" onclick=""><label><input type="checkbox"  disabled="true" id="rtcar-${vehiculos[i].Unidad}-${vista}" ${checked} value="${vehiculos[i].Unidad}" /><span></span></label></td>`)+
                                `<td class="carname" >${vehiculos[i].Descripcion}</td> `+
                                `<td class="carmake" >${vehiculos[i].Marca == undefined ? '---' : vehiculos[i].Marca}</td> `+
                                `<td class="carmodel">${vehiculos[i].Modelo == undefined ? '---' : vehiculos[i].Modelo}</td> `+
                            +'</tr>');
                        }
                    }
                
            }
        }
        var options = {
           
            searchClass:    'searchCars',
    
            valueNames: [ 'carname','carmake','carmodel'],
            pagination: true,
            page: ($(document).height() >=850 ? 10 : 5)
          };
          
        window.carRTSel = new List(vista, options);
        //window.carRTSel.sort("carname",{order:"asc"});
        if(carRTSel.items.length == Tracking._tempUnidades.length || 
            Tracking._tempUnidades.length > carRTSel.items.length) {
            $("#rastreoContinuo .selectall i").addClass("fa-check-square").removeClass("fa-square");
        }else{
            $("#rastreoContinuo .selectall i").removeClass("fa-check-square").addClass("fa-square");
        }
    }
    /**
     * Llena lista de Grupos de Puntos
     */
    public FillPointGroup(){
        $(".trackAddGroupList").html('');
        for(var i=0;i<window.gruposPuntos.length;i++){
            $(".trackAddGroupList").append("<p>"+
                '<input type="checkbox" id="rtgr-'+gruposPuntos[i].GrupoID+'" class="filled-in" value="'+gruposPuntos[i].GrupoID+'" />'+
                '<label for="rtgr-'+gruposPuntos[i].GrupoID+'" onclick="Track.AddGroupToTracking(\''+gruposPuntos[i].GrupoID+'\')" class="groupname">'+gruposPuntos[i].Nombre+'</label>'+
            "</p>");
        }
        var options = {
            listClass :     'trackAddGroupList',
            searchClass:    'searchGroups',
            item:           "<p><label class='groupname'></label></p>",
            valueNames:     [ 'groupname']
          };
          
        window.groupRTSel = new List("historial", options);
    }
    /**
     * Obtiene cantidad de Vehiculos seleccionados para Rastreo Continuo
     */
    public getSelectedCars(): number {
        var conteo = 0;
        for(var i=0;i<window.carRTSel.items.length;i++) {
            if($(window.carRTSel.items[i].elm.children[0].children[0]).children("input[type='checkbox']").is(":checked")) {
                conteo++;
            }
        }
        return conteo;
    }
    /**
     * Agrega una unidad al rastreo
     * @param unit Identificador de Vehiculo
     */
    public AddCarToTracking(unit : string) {
        var id = Tracking._tempUnidades.indexOf(unit);
        if(id > -1){
           
            if (!$("#historial").is(":visible")){
                //console.log("Forget: ",unit);
                Tracking._tempUnidades.splice(id,1);
                $("#rastreoContinuo .selectall i").removeClass("fa-check-square").addClass("fa-square");
            }
        }else{
            //Solo se puede seleccionar una unidad en Historico
            if ($("#historial").is(":visible")){
                //console.log("Forget: ","All");
                Tracking._tempUnidades = [];
            }
            if(Tracking._historyLimit == true) {
                if(Tracking._tempUnidades.length >= 10) {
                    toasty(lang("historylimit"),"warn");
                    setTimeout(function(){
                        $("#rtcar-"+unit+"-historial").prop("checked",false);
                    },100);
                   
                    return;
                }
            }
            //console.log("Tracking: ",unit);
            
            Tracking._tempUnidades.push(unit);
            if(Tracking._tempUnidades.length == window.carRTSel.items.length) {
                $("#rastreoContinuo .selectall i").addClass("fa-check-square").removeClass("fa-square");
            }
        }
    }
    /**
     * Agrega grupos enteros para rastrear
     * @param group Grupo a rastrear
     */
    public AddGroupToTracking(group : string) {
        var id = Tracking._tempGrupos.indexOf(group);
        if(id > -1){
            console.log("Forget: ",group);
            Tracking._tempGrupos.splice(id,1);
        }else{
            console.log("Tracking: ",group);
            Tracking._tempGrupos.push(group);
        }
    }
    /**
     * Muestra/Oculta opciones de tiempo para rastreo
     */
    public TrackingOptions(){
        if($(".timeOptions").is(":visible")){
            $(".timeOptions").hide();
        }else{
            $(".timeOptions").show();
        }
    }
    /**
     * Cierra vista de Rastreo Continuo
     */
    public CloseTrackingSelect(){
        $("#rastreoContinuo").addClass("hidden");
        resetBack();
    }
    /**
     * Cierra vista de Historico
     */
    public CloseHistory(){
        $("#historial .selectAll").show();
        Tracking._historyLimit = false;
        $("#historial .deadTime").addClass("hidden");
        $("#historial .pointsGroup").addClass("hidden");
        $("#historial").addClass("hidden");
    }
    /**
     * Abre vista de Solo Rastreo (permiso SoloRastreo requerido)
     */
    public SoloRastreo(enableExit: boolean = true) {
        if(enableExit == true){
            $("#soloRastreo .top .tool.left").removeClass("hidden");
            $("#soloRastreo .top .title").removeClass("lonely");
        }else{
            $("#soloRastreo .top .tool.left").addClass("hidden");
            $("#soloRastreo .top .title").addClass("lonely");
        }
        $("#soloRastreo").removeClass("hidden");
        $(".subVista").scrollTop(0);
    }
    /**
     * Cierra vista de Solo Rastreo
     */
    public CloseSoloRastreo() {
        $("#soloRastreo").addClass("hidden");
    }
    /**
     * Rastrea una sola unidad (on-demand)
     */
    public TrackUnit(unidad: string) {
        this.StopTracking();
        Tracking._tempUnidades = new Array();
        this.StartTracking(unidad, true);
        $("#soloRastreo").addClass("hidden");
    }
    /**
     * Inicia el Rastreo Continuo
     * @param unidad Identificador de unidad (solo utilizado on-demand)
     * @param noMenu TRUE si quieres que se muestre el Sidebar
     */
    public StartTracking(unidad: string = "", noMenu: boolean = false, showRCIcon: boolean = false){
        window.currentZoom = map.getZoom();
        if(unidad != ""){
            Tracking._tempUnidades.push(unidad);
            toasty(lang("searching_unit") );
        }
        if(Tracking._tempUnidades.length == 0){
            this.StopTracking();
            this.CloseTrackingSelect();
        }else{
            if(Tracking._tempUnidades.length >= 20 && isTEApp == false){
                toasty(lang("slowwarning"));
            }
            this.StopTracking();
            Tracking._rastrearUnidades = Tracking._tempUnidades;
            Tracking._isTracking = true;
            Tracking._isDefault = false;
            this.CloseTrackingSelect();
            this.UltimaUbicacionTransporte();
            this.StartTrackingTimer();
            if(noMenu == false){
                this.Information();
                $(".mainTop .tool.right i").removeClass("zicon_eventos").addClass("zicon_rastreo-continuo");
                $(".mainTop .tool.right").removeClass("hide");
            }
            if(showRCIcon == true) {
                $(".mainTop .tool.right i").removeClass("zicon_eventos").addClass("zicon_rastreo-continuo");
                $(".mainTop .tool.right").removeClass("hide");
            }
            waitSidebar("<es>Cargando información...</es><en>Loading information...</en>");
            if(new Sesion().Permisos("ApagadoMotor") == true && new Sesion().Permisos("SoloRastreo") == false && isTEApp == false) {
               setTimeout(function(){
                $(".lookatme").addClass("fadeInLeft").css("z-index",10);
                    window.lookatmeTimeout = setTimeout(function(){
                        $(".lookatme").addClass("fadeOut");
                        setTimeout(function(){
                            $(".lookatme").removeClass("fadeOut fadeInLeft").css("z-index",0);
                        },1000);
                    },3000);
                },1000);
                $("#map .apagadoMotor").show();
                Interface.GetInterfaces(true);
            }else{
                $("#map .apagadoMotor").hide();
            }
        }
    }
    /**
     * Selecciona todos los vehiculos en una lista
     * @param vista Vista del modulo actual
     */
    public SelectAllCars(vista :string) {
        for(var i=0;i<window.carRTSel.items.length;i++) {
            $(window.carRTSel.items[i].elm.children[0].children[0]).children("input[type='checkbox']").click();
            //this.AddCarToTracking(  $(window.carRTSel.items[i].elm.children[0].children[0].children[0]).val());
            //check("rtcar-"+$(window.carRTSel.items[i].elm.children[0].children[0].children[0]).val()+"-rastreoContinuo");
        }

        this.FillCarList($("#"+vista+" .trackAddTagList input:checked").val(),vista);
        $("#trackAddTagList-"+vista).val($("#"+vista+" .trackAddTagList input:checked").val());
        formSelect();
       // console.log(vista);
    }
    /**
     * Detiene el Rastreo Continuo por completo y limpia el mapa
     */
    public StopTracking(){
        window.selectedRTitem = null;
        try {
            clearTimeout(window.lookatmeTimeout);
            $(".lookatme").removeClass("fadeOut fadeInLeft");
        }catch(e){}
        $("#map .apagadoMotor").hide();
        $("#historial .selectAll").show();
        //$(".mainTop .tool.right").addClass("hide");
        Tracking._historyLimit = false;
        window.ultimasUbicaciones = undefined;
        Tracking._rastrearUnidades = new Array();
        Tracking._locationGroup.removeAll();
        this.CloseBubbles();
        this.CloseSidebar('carEventList');
        if($(".carInformation").is(":visible")){
            //this.LoadCarInfoList();
            this.CloseSidebar('carInfoList');
        }
        $(".mainTop .tool.right i").addClass("zicon_eventos").removeClass("zicon_rastreo-continuo zicon_ultima-posicion zicon_historial");

        $(".list.infoCars").html("");
        $(".timeOptions").hide();
        Tracking._isTracking = false;
        Tracking._isLastPosition = false;
        Tracking._isHistory = false;
        Tracking._isHistoryPoints = false;
        Tracking._isDeadTime = false;
        Tracking._isEvents = false;
        Tracking._isSpeedLimit = false;
        Tracking._isIdleTime = false;
        Tracking._isDefault = true;
        this.StopTrackingTimer();
        Tracking._markerGroup.removeAll();
        Tracking._markerInstance = null;
        $("#timeOptions").addClass("hidden");
        $("#timeControls").addClass("hidden");
        $(".timeBg").addClass("hidden");
        $(".eventMenu").addClass("hidden");
        /*try{
            if( device.platform.toLowerCase() === "android" ) {
                $("#progress").hide();
            }
        }catch(e){}*/
    }
    /**
     * Inicia el proceso del temporizador de RC
     */
    public StartTrackingTimer(){
        this.StopTrackingTimer();
        $("#time").removeClass("hidden");
        /*try{
            if( device.platform.toLowerCase() === "android" ) {
                $("#progress").show();
            }
        }catch(e){}*/
        this.Timer();
        $("#timeOptions").removeClass("hidden");
        $("#timeControls").removeClass("hidden");
        $(".timeBg").removeClass("hidden");
    }
    /**
     * Pausa el temporizador de Rastreo Continuo
     */
    public PauseTimer(){
        this.StopTrackingTimer();
        $("#timeControls i").removeClass('fa-pause-circle').addClass('fa-play-circle');
        $("#timeControls").attr("onclick","Track.Timer()");
    }
    /**
     * Inicializa el temporizador de Rastreo Continuo
     */
    public Timer() {
        $("#timeControls i").removeClass('fa-play-circle').addClass('fa-pause-circle');
        //$("#timeControls").attr("onclick","Track.PauseTimer()");
        Tracking._timeProgress.circleProgress({
            value: 1,
            animation: {'duration':Tracking._timeSeconds*1000,'easing':'linear'},
            size: 41,
            thickness: 5,
            startAngle:(3/2)*Math.PI,
            fill: {
                color: ["#333333"]
            }
        });
    }
    /**
     * Detiene el temporizador del Rastreo Continuo
     */
    public StopTrackingTimer(){
        try{
            var circleProgress = Tracking._timeProgress.data('circle-progress');
            $(circleProgress.canvas).remove();
            Tracking._timeProgress.removeData('circle-progress');
        }catch(e){}
    }
    /**
     * Actualiza posiciones de vehiculos y la lista del modulo, si aplica
     */
    public UpdateTracking(){
        if(Tracking._markerInstance == null){
            Tracking._markerInstance = new Array();
        }
        for(var i=0; i<ultimasUbicaciones.length;i++){
            //console.log("LAST LOCATION OF: "+ultimasUbicaciones[i].Unidad+", "+ultimasUbicaciones[i].Descripcion);
            var carId = objectIndex(vehiculos,"Unidad",ultimasUbicaciones[i].Unidad);
            if(carId >=0) {
                vehiculos[carId].PorcentajeBateria = ultimasUbicaciones[i].porcentajeBateria;
            }
            this.AddUnitToMap(ultimasUbicaciones[i].Unidad,
                ultimasUbicaciones[i].Descripcion,
                ultimasUbicaciones[i].Latitud,
                ultimasUbicaciones[i].Longitud,
                ultimasUbicaciones[i].Velocidad,
                null,
                ultimasUbicaciones[i].Fecha);
                if(Tracking._watchingUnit == ultimasUbicaciones[i].Unidad){
                    map.setCenter({lat:ultimasUbicaciones[i].Latitud,lng:ultimasUbicaciones[i].Longitud});
                }
        }
        if($(".eventMenu").hasClass("hidden")){
            $(".eventMenu").removeClass("hidden");
            this.LoadCarInfoList();
        }
        closeWaitSidebar();
    }
    /**
     * Mueve un marcador de unidad en el mapa.
     * @param id Identificador de instancia de Vehiculo
     * @param lat Latitud nueva
     * @param lng Longitud nueva
     */
    public MoveCarInstance(id : number, lat : number, lng : number) {
        //Tracking._markerInstance[id].marker.setIcon( new H.map.DomIcon(carIcon));
        Tracking._markerInstance[id].marker.setPosition({lat: lat, lng: lng});
        Tracking._markerInstance[id].label.setPosition({lat: lat, lng: lng});
    }
    /**
     * Agrega un Vehiculo al mapa y genera su Info Bubble.
     * @param unidad Identificador del Vehiculo
     * @param descripcion Texto del DOM Label
     * @param latitud Latitud del Vehiculo
     * @param longitud Longitud del Vehiculo
     * @param velocidad Velocidad del Vehiculo
     * @param customIcon Icono diferente al original?
     * @param fecha Timestamp
     */
    public AddUnitToMap(unidad : string, descripcion : string, latitud : number, longitud : number, velocidad : number, customIcon : string = null,fecha = null) {
        try{
            var carid = objectIndex(vehiculos,"Unidad",unidad);
            var carIcon = getCarIconUri(vehiculos[carid].IconoID, (velocidad >0));

            if(customIcon != null){
                carIcon = customIcon;
            }
            //var _length = getLabelWidth(descripcion, "icon");
            var _id = objectIndex(Tracking._markerInstance,"id",unidad);
            if(fecha != null) {
                var f = new moment(fecha);
                var ahora = moment(new Date());
                var horas = ahora.diff(f,'hours');
                //console.warn("Horas diff: ",horas);
                if(horas >=4) {
                    carIcon = 'img/eventos/98.png';
                }
            }
            var iconElem = this.CreateDomIcon(carIcon);
            if(_id > -1){ //YA EXISTE EN EL MAPA
                //ACTUALIZANDO ELEMENTOS DINAMICOS
                $(`.ma-${unidad} .car-icon span`).css({'background-image':`url('${carIcon}')"`});
                $(`.ma-${unidad} .infoLiveStatus`).html(`<strong>${velocidad == 0 ? '<es>Detenido</es><en>Stopped</en>' : '<es>En Movimiento</es><en>In Transit</en>'}</strong>`);
                $(`.ma-${unidad} .infoLastReport`).html(timeSince(fecha));
                if(fecha != null) {$(`.${unidad}-bubble_speed`).html(distConv(velocidad,true,true,true)+"/h");}
                $(`.${unidad}-bubble_date`).html(f.format("YYYY/MM/DD HH:mm:ss"));
                $(`.${unidad}-bubble_coords`).attr("onclick",'openMaps('+latitud+','+longitud+')');
                
               // Tracking._markerInstance[_id].marker.setIcon( new H.map.DomIcon(carIcon,{size: {w:40*DPR,h:40*DPR} }));
                Tracking._markerInstance[_id].marker.setIcon( new H.map.DomIcon(iconElem));
                var _puntosArr = Tracking._markerInstance[_id].points.getLatLngAltArray();
                if(_puntosArr[_puntosArr.length-3] != latitud && _puntosArr[_puntosArr.length-2] != longitud) {
                    //console.log("UPDATING RT: ",unidad+", "+descripcion+", "+latitud+", "+longitud+", "+velocidad);
                    
                    try{
                        if(window.ibubble.unidad == unidad) {
                            window.ibubble.setPosition({lat:latitud, lng: longitud});
                        }
                    }catch(e){}
                    if(Tracking._isHistory == true) {
                        this.LoadHistoryList();
                    }else{
                        this.LoadCarInfoList();
                    }
                    Tracking._markerInstance[_id].marker.setPosition({lat: latitud, lng: longitud});
                    Tracking._markerInstance[_id].label.setPosition({lat: latitud, lng: longitud});
                    if(Tracking._drawTrack == true && Tracking._isHistory == false && Tracking._isHistoryPoints == false && Tracking._isVisited == false){
                        Tracking._markerInstance[_id].line.setVisibility(true);
                    }
                    if(Tracking._isHistory == false && Tracking._isHistoryPoints == false) {
                        Tracking._markerInstance[_id].points.pushPoint({lat:latitud, lng:longitud});
                        
                        Tracking._markerInstance[_id].line.setGeometry(Tracking._markerInstance[_id].points);
                    }
                }
            }else{
                //console.log("ADDING RT: ",unidad+", "+descripcion+", "+latitud+", "+longitud+", "+velocidad);
                var _puntos = new H.geo.LineString();
                var randomColor = ColorLuminance(getRandomColor(),-0.2);
                _puntos.pushPoint({lat:latitud, lng:longitud});
                _puntos.pushPoint({lat:latitud, lng:longitud});
                
                var iconLabel = this.CreateDomLabel(descripcion);
                Tracking._markerInstance.push({
                    "id":       unidad,
                    "marker":   new H.map.DomMarker(
                        {lat:latitud, lng:longitud},
                        {
                            icon: new H.map.DomIcon(iconElem),
                            zIndex:1
                        }
                    ),
                    "label":    new H.map.DomMarker(
                        {lat:latitud, lng:longitud},
                        {
                            zIndex:1,
                            icon: new H.map.DomIcon(iconLabel)
                        }
                    ),
                    "line": new H.map.Polyline(
                        _puntos, 
                        { 
                            style: { 
                                lineWidth: 6,
                                strokeColor: hexToRgbA(randomColor,0.75),
                                fillColor: hexToRgbA(randomColor,0.75),
                            },
                            arrows: { 
                                fillColor: 'white', 
                                frequency: 15, 
                                width: 2, 
                                length: 1.8 
                            }
                        }
                    ),
                    "points": _puntos,
                    "ogcolor":randomColor
                });
                if(fecha !==null){
                    var f = new moment(fecha);
                }
                Tracking._markerInstance[ Tracking._markerInstance.length-1].marker.setData(
                    `<div class="inputGroup">
                        <h6><es>Información</es><en>Information</en></h6>
                        <div class="markerInfo">
                            <div><span class="ibHeader"><es>Vehículo</es><en>Vehicle</en>:</span><span>${vehiculos[carid].Descripcion}</span></div>
                            <div><span class="ibHeader"><es>Chofer</es><en>Driver</en>:</span><span>${na(vehiculos[carid].Chofer)}</span></div>
                            <div class="hidden fuelibHeader"><span class="ibHeader"><es>Combustible</es><en>Fuel</en>:</span><span class="${unidad}-bubble_fuel"></span></div>
                            <div><span class="ibHeader"><es>Dirección</es><en>Address</en>:</span><span class="${unidad}-bubble_address"><es>Buscando...</es><en>Locating...</en></span></div>
                            ${(fecha != null ? `<div><span class="ibHeader"><es>Fecha y Hora</es><en>Fecha y Hora</en>:</span><span class="${unidad}-bubble_date">${f.format("YYYY/MM/DD HH:mm:ss")}</span></div>` : '')}
                            <div><span class="ibHeader"><es>Velocidad</es><en>Speed</en>:</span><span class="${unidad}-bubble_speed">${distConv(velocidad,true,true,true)}/h</span></div>
                            <div><span class="ibHeader"><es>Cerca de</es><en>Places Near</en>:</span><span class="${unidad}-bubble_near"><es>Explorando...</es><en>Exploring...</en></span></div>
                            ${typeof vehiculos[carid].PorcentajeBateria != "undefined"  && this.isBatteryEnabled(carid)? `<div><span class="ibHeader"><es>Batería</es><en>Battery</en>:</span><span class="${unidad}-bubble_battery">${vehiculos[carid].PorcentajeBateria}%</span></div>` : ''}
                            <div class="${typeof vehiculos[carid].EstadoMotor == "undefined" ? 'hidden' : ''} ${unidad}-motoribHeader"><span class="ibHeader"><es>Paro de Motor</es><en>Engine Shutdown</en>:</span><span class="${unidad}-bubble_motorstate">${vehiculos[carid].EstadoMotor == false ? '<es>Activado</es><en>Enabled</en>' : '<es>Desactivado</es><en>Disabled</en>'}</span></div>
                            <div><span class="ibHeader"><es>Mapa</es><en>Map</en>:</span><span onclick="openMaps('${latitud}','${longitud}')" class="'${unidad}-bubble_coords"><es>Abrir Mapa Externo</es><en>Open External Map</en> <i class="fas fa-external-link-alt"></i></span></div>
                            
                        </div>
                    </div>`
                );
                
                Tracking._markerInstance[ Tracking._markerInstance.length-1].marker.unidad = unidad;
                Tracking._markerInstance[Tracking._markerInstance.length-1].line.setVisibility(false);
                Tracking._markerGroup.addObject(Tracking._markerInstance[Tracking._markerInstance.length-1].marker);
                Tracking._markerGroup.addObject(Tracking._markerInstance[Tracking._markerInstance.length-1].label);
                if(isTEApp == false){
                    Tracking._markerGroup.addObject(Tracking._markerInstance[Tracking._markerInstance.length-1].line);
                }
                Tracking._markerInstance[Tracking._markerInstance.length-1].points.removePoint(0);
                map.setViewBounds(Tracking._markerGroup.getBounds());
                if(Tracking._isHistory == false){
                    try{
                        Tracking._markerGroup.removeEventListener('tap',Track.CreateUnitBubble,false);
                    }catch(e){}
                    if(isTEApp == false){
                        Tracking._markerGroup.addEventListener('tap',Track.CreateUnitBubble,false);
                    }
                }
                if(Tracking._rastrearUnidades.length ==  Tracking._markerInstance.length) {
                    //map.setZoom(map.getZoom()-1);
                }
            }
        }catch(e){
            console.error("AddUnitToMap Error: "+e.message);
        }
    }
    /**
     * Crea icono en el DOM que puede personalizarse
     * @param carIcon Nombre del icono
     * @param auxClass Clase auxiliar para el icono
     */
    public CreateDomIcon(carIcon: string, auxClass: string = ""): any {
        var icon = document.createElement('div');
        icon.className = "domIcon "+auxClass;
        icon.style.background = `url(${carIcon}) no-repeat center center`;
        return icon;
    }
    /**
     * Crea un Label en el DOM que puede personalizarse
     * @param text Texto del Label
     * @param type Tipo del Label
     */
    public CreateDomLabel(text: string = "Test", type: string = "icon"): any {
        var _length = getLabelWidth(text, type);
        var label = document.createElement('div');
        label.className = "domLabel_"+type;
        label.style.left = ((_length / 2)*-1)+"px";
        label.innerHTML = text;
        return label;
    }
    /**
     * Obtene las ultimas posiciones de una lista de Unidades
     * (Utilizado en Rastreo Continuo)
     */
    public UltimaUbicacionTransporte(){
       // waitSidebar("<es>Cargando información...</es><en>Loading information...</en>");
        $.ajax({
            url:    rasStaticUrl  + "UltimaUbicacionTransporte",
            type:   "POST",
            timeout:10000,
            dataType:"json",
            data: {
                "token":    cultureMetric()+window["userData"]["token"],
                "unidades": Tracking._rastrearUnidades,
                "zona":     window.zona
            },
            success: (data)=>{
                //console.log(data);
                try{
                    window.ultimasUbicaciones = JSON.parse(data.data.d);
                    for(var i=0;i<ultimasUbicaciones.length;i++){
                        try{
                            if(ultimasUbicaciones[i].DatosGeocerca != undefined){
                                ultimasUbicaciones[i].DatosGeocerca.Latitud = ultimasUbicaciones[i].Latitud;
                                ultimasUbicaciones[i].DatosGeocerca.Longitud = ultimasUbicaciones[i].Longitud;
                                this.AddEvent(ultimasUbicaciones[i].Unidad,"geo",ultimasUbicaciones[i].DatosGeocerca.FechaHora,ultimasUbicaciones[i].DatosGeocerca);
                            }
                        }catch(e){}
                    }
                    if(Tracking._isTracking == true){
                        this.UpdateTracking();
                        this.LoadCarInfoList();
                        this.MultiEvento();
                    }
                    if(Tracking._isLastPosition == true){
                        this.UpdateTracking();
                        this.LoadCarInfoList();
                    }
                    
                }catch(e){
                    console.log(e);
                }
            },
            error: ()=>{
                if(Tracking._isDefault == false){
                    //waitSidebar("<es>Reintentando...</es><en>Retrying...</en>");
                    
                    setTimeout(()=>{
                        this.UltimaUbicacionTransporte();
                    },3000);
                }else{
                    closeWaitSidebar();
                }
            }
        });
    }
    /**
     * Abre vista de Ultima Posicion
     */
    public OpenLastPosition(){
        selectMenu(".UltimaPosicion");
        try{
            masterClose = ()=>{this.CloseTrackingSelect();};
        }catch(e){}
        Tracking._tempUnidades = new Array();
        $(".searchCars").val('');
        $("#rastreoContinuo .top .title").html("<es>&Uacute;ltima Posición</es><en>Last Location</en>");
        $("#rastreoContinuo .top .tool.right").attr("onclick","Track.GetLastPosition()");
        $("#rastreoContinuo").removeClass("hidden");
        this.FillCarList('0','rastreoContinuo');
        this.FillTagList('rastreoContinuo');
        $(".subVista").scrollTop(0);
    }
    /**
     * Inicia la solicitud de Ultima Posicion
     */
    public GetLastPosition() {   
        if(Tracking._tempUnidades.length == 0){
            this.StopTracking();
            this.CloseTrackingSelect();
        }else{
            this.StopTracking();
            Tracking._rastrearUnidades = Tracking._tempUnidades;
            Tracking._isLastPosition = true;
            Tracking._isDefault = false;
            this.CloseTrackingSelect();
            this.UltimaUbicacionTransporte();
            $(".mainTop .tool.right i").removeClass("zicon_eventos").addClass("zicon_ultima-posicion");
            $(".mainTop .tool.right").removeClass("hide");
            this.Information();
            waitSidebar("<es>Cargando información...</es><en>Loading information...</en>");
        }
    }
    /**
     * Asigna la primera unidad disponible al arreglo temporal de Unidades a rastrear
     * (Utilizado en Histórico)
     */
    public AsignFirstUnit() {
        Tracking._tempUnidades = new Array();
        for(var i =0;i<vehiculos.length;i++){
            if(vehiculos[i].Estado == -1 && Tracking._tempUnidades.length == 0){
                Tracking._tempUnidades.push(vehiculos[i].Unidad);
            }
        }
    }
    /**
     * Abre la vista de Histórico
     */
    public OpenHistory(){
        selectMenu(".Historico");
        try{
            masterClose = ()=>{this.CloseHistory();};
        }catch(e){}
        Tracking._historyLimit = true;
        $(".searchCars").val('');
        $("#historial .selectAll").hide();
        Tracking._tempUnidades = new Array();
        this.AsignFirstUnit();
        $("#historial .top .title").html("<es>Histórico</es><en>History</en>");
        $("#historial .top .tool.right").attr("onclick","Track.GetHistory()");
        $("#historial").removeClass("hidden");
        this.FillCarList('0','historial');
        this.FillTagList('historial');
        $("#histIni,#histFin").val('');
        this.BuildSelects();
        $(".subVista").scrollTop(0);
    }
    /**
     * Abre la vista de Historico con Puntos
     */
    public OpenHistoryWithPoints(){
        Tracking._historyLimit = true;
        $("#historial .selectAll").hide();
        Tracking._tempUnidades = new Array();
        $("#historial .top .title").html("<es>Histórico con Puntos</es><en>History & Checkpoints</en>");
        $("#historial .top .tool.right").attr("onclick","Track.GetHistoryPoints()");
        $("#historial").removeClass("hidden");
        this.FillCarList('0','historial');
        this.FillTagList('historial');
        $("#histIni,#histFin").val('');
        this.BuildSelects();
    }
    /**
     * Abre la vista de Tiempo Muerto
     */
    public OpenDeadTime() {
        Tracking._tempUnidades = new Array();
        $("#historial .top .title").html("<es>Tiempo Muerto</es><en>Dead Time</en>");
        $("#historial .top .tool.right").attr("onclick","Track.GetDeadTime()");
        $("#historial").removeClass("hidden");
        $("#historial .deadTime").removeClass("hidden");
        this.FillCarList('0','historial');
        this.FillTagList('historial');
        $("#histIni,#histFin").val('');
        this.BuildSelects();
    }
    /**
     * Abre la vista de Eventos
     */
    public OpenCarEvents() {
        Tracking._tempUnidades = new Array();
        $("#historial .top .title").html("<es>Eventos</es><en>Events</en>");
        $("#historial .top .tool.right").attr("onclick","Track.GetCarEvents()");
        $("#historial").removeClass("hidden");
        this.FillCarList('0','historial');
        this.FillTagList('historial');
        $("#histIni,#histFin").val('');
        this.BuildSelects();
    }
    /**
     * Abre la vista de Puntos Visitados
     */
    public OpenVisited() {
        Tracking._historyLimit = true;
        $("#historial .selectAll").hide();
        wait("<es>Cargando Puntos y Grupos...</es><en>Loading Checkpoints & Groups...</en>");
        Checkpoints.ObtenerPuntosConGrupos();
        Tracking._tempUnidades = new Array();
        Tracking._tempGrupos = new Array();
        $("#historial .top .title").html("<es>Puntos Visitados</es><en>Visited Checkpoints</en>");
        $("#historial .top .tool.right").attr("onclick","Track.GetVisitedPoints()");
        $("#historial").removeClass("hidden");
        $(".pointsGroup").removeClass("hidden");
        this.FillCarList('0','historial');
        this.FillTagList('historial');
        $("#histIni,#histFin").val('');
        this.BuildSelects();
    }
    /**
     * Abre vista de Excesos de Velocidad
     */
    public OpenSpeedLimit() {
        Tracking._tempUnidades = new Array();
        $("#historial .top .title").html("<es>Excesos de Velocidad</es><en>Speed Limit</en>");
        $("#historial .top .tool.right").attr("onclick","Track.GetSpeedLimit()");
        $("#historial").removeClass("hidden");
        this.FillCarList('0','historial',true);
        this.FillTagList('historial',true);
        $("#histIni,#histFin").val('');
        this.BuildSelects();
    }
    /**
     * Abre la vista de Ralentí
     */
    public OpenIdleTime() {
        Tracking._tempUnidades = new Array();
        $("#historial .top .title").html("<es>Ralentí</es><en>Idle</en>");
        $("#historial .top .tool.right").attr("onclick","Track.GetIdleTime()");
        $("#historial").removeClass("hidden");
        this.FillCarList('0','historial');
        this.FillTagList('historial');
        $("#histIni,#histFin").val('');
        this.BuildSelects();
    }
    /**
     * Inicia la solicitud de Ralentí
     */
    public GetIdleTime() {
        Tracking._locationArray = new Array();
        if(Tracking._tempUnidades.length == 0){
            this.StopTracking();
            this.CloseHistory();
        }else{
            if($("#histIni").val() == "") {
                alerta(lang("error"),lang("histfini"));
            }else{
                if($("#histFin").val() == ""){
                    alerta(lang("error"),lang("histfini"));
                }else{
                    var ini = new moment($("#histIni").val());
                    var fin = new moment($("#histFin").val());
                    if(ini.toDate().getTime() > fin.toDate().getTime() ) {
                        alerta(lang("error"),lang("invalidhist"));
                    }else{
                        this.StopTracking();
                        Tracking._rastrearUnidades = Tracking._tempUnidades;
                        Tracking._isIdleTime = true;
                        this.CloseHistory();
                        this.Information();
                        waitSidebar("<es>Cargando información...</es><en>Loading information...</en>");
                        for(var i =0;i<Tracking._rastrearUnidades.length;i++) {
                            this.ConsultaRalenti(ini.toDate().getTime(),fin.toDate().getTime(),Tracking._rastrearUnidades[i]);
                        }
                    }
                }
            }
        }
    }
    /**
     * Inicia la solicitud de Tiempos Muertos
     */
    public GetDeadTime() {
        Tracking._locationArray = new Array();
        if(Tracking._tempUnidades.length == 0){
            this.StopTracking();
            this.CloseHistory();
        }else{
            if($("#histIni").val() == "") {
                alerta(lang("error"),lang("histfini"));
            }else{
                if($("#histFin").val() == ""){
                    alerta(lang("error"),lang("histfini"));
                }else{
                    var ini = new moment($("#histIni").val());
                    var fin = new moment($("#histFin").val());
                    if(ini.toDate().getTime() > fin.toDate().getTime() ) {
                        alerta(lang("error"),lang("invalidhist"));
                    }else{
                        this.StopTracking();
                        Tracking._rastrearUnidades = Tracking._tempUnidades;
                        Tracking._isDeadTime = true;
                        this.CloseHistory();
                        this.Information();
                        waitSidebar("<es>Cargando información...</es><en>Loading information...</en>");
                        for(var i =0;i<Tracking._rastrearUnidades.length;i++) {
                            this.ConsultaTiempoMuerto(ini.toDate().getTime(),fin.toDate().getTime(),Tracking._rastrearUnidades[i]);
                        }
                    }
                }
            }
        }
    }
    /**
     * Inicia la solicitud de Excesos de Velocidad
     */
    public GetSpeedLimit() {
        Tracking._locationArray = new Array();
        if(Tracking._tempUnidades.length == 0){
            this.StopTracking();
            this.CloseHistory();
        }else{
            if($("#histIni").val() == "") {
                alerta(lang("error"),lang("histfini"));
            }else{
                if($("#histFin").val() == ""){
                    alerta(lang("error"),lang("histfini"));
                }else{
                    var ini = new moment($("#histIni").val());
                    var fin = new moment($("#histFin").val());
                    if(ini.toDate().getTime() > fin.toDate().getTime() ) {
                        alerta(lang("error"),lang("invalidhist"));
                    }else{
                        this.StopTracking();
                        Tracking._rastrearUnidades = Tracking._tempUnidades;
                        Tracking._isSpeedLimit = true;
                        this.CloseHistory();
                        this.Information();
                        waitSidebar("<es>Cargando información...</es><en>Loading information...</en>");
                        for(var i =0;i<Tracking._rastrearUnidades.length;i++) {
                            this.ConsultaExcesos(ini.toDate().getTime(),fin.toDate().getTime(),Tracking._rastrearUnidades[i]);
                        }
                    }
                }
            }
        }
    }
    /**
     * Inicia la solicitud de Eventos Ocurridos
     */
    public GetCarEvents() {
        Tracking._locationArray = new Array();
        if(Tracking._tempUnidades.length == 0){
            this.StopTracking();
            this.CloseHistory();
        }else{
            if($("#histIni").val() == "") {
                alerta(lang("error"),lang("histfini"));
            }else{
                if($("#histFin").val() == ""){
                    alerta(lang("error"),lang("histfini"));
                }else{
                    var ini = new moment($("#histIni").val());
                    var fin = new moment($("#histFin").val());
                    if(ini.toDate().getTime() > fin.toDate().getTime() ) {
                        alerta(lang("error"),lang("invalidhist"));
                    }else{
                        this.StopTracking();
                        Tracking._rastrearUnidades = Tracking._tempUnidades;
                        Tracking._isEvents = true;
                        this.CloseHistory();
                        this.Information();
                        waitSidebar("<es>Cargando información...</es><en>Loading information...</en>");
                        for(var i =0;i<Tracking._rastrearUnidades.length;i++) {
                            this.ConsultaEventos(ini.toDate().getTime(),fin.toDate().getTime(),Tracking._rastrearUnidades[i]);
                        }
                    }
                }
            }
        }
    }
    /**
     * Inicia la solicitud de Histórico con Puntos
     */
    public GetHistoryPoints() {   
        Tracking._locationArray = new Array();
        if(Tracking._tempUnidades.length == 0){
            this.StopTracking();
            this.CloseHistory();
        }else{
            if($("#histIni").val() == "") {
                alerta(lang("error"),lang("histfini"));
            }else{
                if($("#histFin").val() == ""){
                    alerta(lang("error"),lang("histfini"));
                }else{
                    var ini = new moment($("#histIni").val());
                    var fin = new moment($("#histFin").val());
                    if(ini.toDate().getTime() > fin.toDate().getTime() ) {
                        alerta(lang("error"),lang("invalidhist"));
                    }else{
                        Checkpoints.ObtenerPuntosConGrupos();
                        this.StopTracking();
                        Tracking._rastrearUnidades = Tracking._tempUnidades;
                        Tracking._isHistoryPoints = true;
                        this.CloseHistory();
                        this.Information();
                        waitSidebar("<es>Cargando información...</es><en>Loading information...</en>");
                        for(var i =0;i<Tracking._rastrearUnidades.length;i++) {
                            this.ConsultaHistoricoTransporte(ini.toDate().getTime(),fin.toDate().getTime(),Tracking._rastrearUnidades[i]);
                        }
                    }
                }
            }
        }
    }
    /**
     * Inicia la solicitud de Puntos Visitados
     */
    public GetVisitedPoints() {   
        Tracking._locationArray = new Array();
        if(Tracking._tempUnidades.length == 0){
            this.StopTracking();
            this.CloseHistory();
        }else{
            if($("#histIni").val() == "") {
                alerta(lang("error"),lang("histfini"));
            }else{
                if($("#histFin").val() == ""){
                    alerta(lang("error"),lang("histfini"));
                }else{
                    
                    var ini = new moment($("#histIni").val());
                    var fin = new moment($("#histFin").val());
                    if(ini.toDate().getTime() > fin.toDate().getTime() ) {
                        alerta(lang("error"),lang("invalidhist"));
                    }else{
                        if(Tracking._tempGrupos.length == 0){
                            alerta(lang("error"),lang("selectgroup"));
                        }else{
                            this.StopTracking();
                            Tracking._rastrearUnidades = Tracking._tempUnidades;
                            Tracking._gruposSeleccionados = Tracking._tempGrupos;
                            Tracking._isVisited = true;
                            this.CloseHistory();
                            this.Information();
                            waitSidebar("<es>Cargando información...</es><en>Loading information...</en>");
                            Checkpoints.ShowGroupsOnMap(Tracking._gruposSeleccionados);
                            for(var i =0;i<Tracking._rastrearUnidades.length;i++) {
                                this.ConsultaHistoricoTransporte(ini.toDate().getTime(),fin.toDate().getTime(),Tracking._rastrearUnidades[i]);
                            }
                        }
                    }
                }
            }
        }
    }
    /**
     * Inicia la solicitud de Histórico
     */
    public GetHistory() {   
        Tracking._locationArray = new Array();
        if(Tracking._tempUnidades.length == 0){
            this.StopTracking();
            this.CloseHistory();
        }else{
            if($("#histIni").val() == "" && $("#histFin").val() == "") {
                changeTimeMetric("12h");
            }
            if($("#histIni").val() == "") {
                alerta(lang("error"),lang("histfini"));
            }else{
                if($("#histFin").val() == ""){
                    alerta(lang("error"),lang("histfini"));
                }else{
                    var ini = new moment($("#histIni").val());
                    var fin = new moment($("#histFin").val());
                    if(ini.toDate().getTime() > fin.toDate().getTime() ) {
                        alerta(lang("error"),lang("invalidhist"));
                    }else{
                        this.StopTracking();
                        Tracking._rastrearUnidades = Tracking._tempUnidades;
                        Tracking._isHistory = true;
                        Tracking._isDefault = false;
                        this.CloseHistory();
                        this.Information();
                        $(".mainTop .tool.right i").removeClass("zicon_eventos").addClass("zicon_historial");
                        $(".mainTop .tool.right").removeClass("hide");
                        waitSidebar("<es>Cargando información...</es><en>Loading information...</en>");
                        for(var i =0;i<Tracking._rastrearUnidades.length;i++) {
                            this.ConsultaHistoricoTransporte(ini.toDate().getTime(),fin.toDate().getTime(),Tracking._rastrearUnidades[i]);
                        }
                    }
                }
            }
        }
    }
    /**
     * Solicita a plataforma los Tiempos Muertos de una unidad
     * @param fechaIni Fecha Inicial
     * @param fechaFin Fecha Final
     * @param unit Identificador de Vehiculo a consultar
     */
    public ConsultaTiempoMuerto(fechaIni : string, fechaFin : string,unit : string){
        ajax("POST",rasStaticUrl + "ConsultaTiempoMuerto",{
            token:          window['userData']['token'],
            ini:            fechaIni,
            fin:            fechaFin,
            unidad:         unit,
            zona:           window.zona,
            mins:           $("#deadTime").val()
        },
        (data)=>{
            var locations = JSON.parse(data.data.d);
            if(locations.Ubicaciones.length > 0){
                Tracking._locationArray.push({
                    "id":           unit,
                    "locations":    locations.Ubicaciones,
                    "line":         null,
                    "points":       new Array()
                });
                //this.DrawDeadTime(unit);
            }else{
                closeWaitSidebar();
            }
        },
        ()=>{
            setTimeout( ()=>{
                this.ConsultaTiempoMuerto(fechaIni,fechaFin,unit);
            },3000);
        });
    }
    /**
     * Consulta los Ralentí de una unidad
     * @param fechaIni Fecha Inicial
     * @param fechaFin Fecha Final
     * @param unit Identificador de Vehiculo a consultar
     */
    public ConsultaRalenti(fechaIni : string, fechaFin : string,unit : string){
        ajax("POST",rasStaticUrl + "ConsultaRalenti",{
            token:          window['userData']['token'],
            ini:            fechaIni,
            fin:            fechaFin,
            unidad:         unit,
            zona:           window.zona
        },
        (data)=>{
            var locations = JSON.parse(data.data.d);
            if(locations.Ubicaciones.length > 0){
                Tracking._locationArray.push({
                    "id":           unit,
                    "locations":    locations.Ubicaciones,
                    "line":         null,
                    "points":       new Array()
                });
                //this.DrawIdleTime(unit);
            }else{
                closeWaitSidebar();
            }
        },
        ()=>{
            setTimeout(()=>{
                this.ConsultaRalenti(fechaIni,fechaFin,unit);
            },3000);
        });
    }
    /**
     * Obtiene los Excesos de Velocidad de una unidad
     * @param fechaIni Fecha Inicial
     * @param fechaFin Fecha Final
     * @param unit Identificador de Vehiculo a consultar
     */
    public ConsultaExcesos(fechaIni : string, fechaFin : string,unit : string){
        var carid = objectIndex(vehiculos,"Unidad",unit);
        ajax("POST",rasStaticUrl + "ConsultaExcesos",{
            token:          window['userData']['token'],
            ini:            fechaIni,
            fin:            fechaFin,
            unidad:         unit,
            speed:          vehiculos[carid].Limite,
            zona:           window.zona
        },
        (data)=>{
            var locations = JSON.parse(data.data.d);
            if(locations.Ubicaciones.length > 0){
                Tracking._locationArray.push({
                    "id":           unit,
                    "locations":    locations.Ubicaciones,
                    "line":         null,
                    "points":       new Array()
                });
                //this.DrawSpeedLimit(unit);
            }else{
                closeWaitSidebar();
            }
        },
        ()=>{
            setTimeout(()=>{
                this.ConsultaExcesos(fechaIni,fechaFin,unit);
            },3000);
        });
    }
    /**
     * Solicita los Eventos ocurridos para una unidad en un lapso de tiempo
     * @param fechaIni Fecha Inicial
     * @param fechaFin Fecha Final
     * @param unit Identificador de Vehiculo
     */
    public ConsultaEventos(fechaIni : string, fechaFin : string,unit : string){
        ajax("POST",rasStaticUrl + "ConsultaEventos",{
            token:          window['userData']['token'],
            ini:            fechaIni,
            fin:            fechaFin,
            unidad:         unit,
            zona:           window.zona
        },
        function(data){
            var locations = JSON.parse(data.data.d);
            if(locations.Ubicaciones.length > 0){
                Tracking._locationArray.push({
                    "id":           unit,
                    "locations":    locations.Ubicaciones,
                    "line":         null,
                    "points":       new Array()
                });
                Track.DrawCarEvents(unit);
            }else{
                closeWaitSidebar();
            }
        },
        function(){
            setTimeout(function(){
                Track.ConsultaEventos(fechaIni,fechaFin,unit);
            },3000);
        });
    }
    /**
     * Solicita a plataforma un Histrico para una unidad 
     * (esto puede incluir Ultima Posicion)
     * @param fechaIni Fecha Inicial
     * @param fechaFin Fecha Final
     * @param unit Identificador de Vehiculo
     */
    public ConsultaHistoricoTransporte(fechaIni : string = "", fechaFin : string = "",unit : string = ""){
        waitSidebar("<es>Cargando información...</es><en>Loading information...</en>");
        if(fechaIni == "") {
            fechaIni = Tracking._histFechaIni;
        }else{
            Tracking._histFechaIni = fechaIni;
        }
        if(fechaFin == "") {
            fechaFin = Tracking._histFechaFin;
        }else{
            Tracking._histFechaFin = fechaFin;
        }
        if(unit == "") {
            unit = Tracking._histUnidad;
        }else{
            Tracking._histUnidad = unit;
        }
        ajax("POST",rasStaticUrl + "ConsultaHistoricoTransporte",{
            token:          cultureMetric()+window['userData']['token'],
            ini:            fechaIni,
            fin:            fechaFin,
            unidad:         unit,
            zona:           window.zona
        },
        (data)=>{
            var locations = JSON.parse(data.data.d);
            window.locationsFetched = locations;
            if(locations.Ubicaciones.length > 0){
                Tracking._locationArray.push({
                    "id":           unit,
                    "locations":    summary(locations.Ubicaciones),
                    "line":         null,
                    "points":       new Array(),
                    "rawLocations": locations.Ubicaciones
                });
                if(Tracking._isHistory == true){
                    this.DrawHistory(unit);
                }
            }else{
                closeWaitSidebar();
            }
        },
        ()=>{
            if(Tracking._isHistory == true){
                waitSidebar("<es>Reintentando...</es><en>Retrying...</en>");
                setTimeout(()=>{
                    this.ConsultaHistoricoTransporte(fechaIni,fechaFin,unit);
                },3000);
            }
        });
    }
    /**
     * Dibuja los Eventos ocurridos de una unidad
     * @param unidad Identificador de Vehiculo
     */
    /*public DrawCarEvents(unidad : string = "") {
        for(var i =0;i<Tracking._locationArray.length;i++){ 
            var carid = objectIndex(vehiculos,"Unidad",Tracking._locationArray[i].id);
            if(unidad == "" || unidad == Tracking._locationArray[i].id){
                for(var j =0;j<Tracking._locationArray[i].locations.length;j++){
                    var evento = Tracking._locationArray[i].locations[j].Evento;
                    Tracking._locationArray[i].points.push(
                        new H.map.DomMarker(
                            {lat: Tracking._locationArray[i].locations[j].Latitud, lng: Tracking._locationArray[i].locations[j].Longitud},
                            {
                                icon: new H.map.DomIcon("img/eventos/"+evento+".png",{size: {w:40,h:40} })
                            }
                        )
                    );
                    var f = new moment(Tracking._locationArray[i].locations[j].Fecha);
                    
                    Tracking._locationArray[i].points[j].setData(
                        '<div class="inputGroup">'+
                            '<h6><es>Información</es><en>Information</en></h6>'+
                            '<div class="markerInfo">'+
                                '<div><span class="ibHeader"><es>Vehículo</es><en>Vehicle</en>:</span><span>'+vehiculos[carid].Descripcion+'</span></div>'+
                                (evento == -1 ? "" : '<div><span class="ibHeader"><es>Evento</es><en>Event</en>:</span><span>'+this.GetEvento(evento)+'</span></div>')+
                                '<div><span class="ibHeader"><es>Fecha y Hora</es><en>Time Date</en>:</span><span>'+f.format("YYYY/MM/DD HH:mm:ss")+'</span></div>'+
                                '<div><span class="ibHeader"><es>Velocidad</es><en>Speed</en>:</span><span>'+distConv(Tracking._locationArray[i].locations[j].Velocidad,true,true,true)+'/h</span></div>'+
                                (typeof Tracking._locationArray[i].locations[j].aPuntoDist === "undefined" ? '' : '<div><span class="ibHeader"><es>Punto</es><en>Point</en>:</span><span>'+Tracking._locationArray[i].locations[j].aPuntoLabel.replace("__DIST__",parseDist(Tracking._locationArray[i].locations[j].aPuntoDist))+'</span></div>' )+
                            '</div>'+
                        '</div>'
                    );
                   
                    Tracking._locationArray[i].points[j]._i = i;
                    Tracking._locationArray[i].points[j]._j = j;
                    Tracking._locationArray[i].points[j]._evento = evento;
                    Tracking._locationArray[i].points[j].unidad = unidad;
                    Tracking._locationGroup.addObject(Tracking._locationArray[i].points[j]);
                }
            }
        }
        try{
            Tracking._locationGroup.removeEventListener('tap',Track.CreatePointBubble,false);
        }catch(e){}
        Tracking._locationGroup.addEventListener('tap',Track.CreatePointBubble,false);
        if($(".eventMenu").hasClass("hidden")){
            $(".eventMenu").removeClass("hidden");
           //toasty(lang("vehicleevents"),"success");
        }
        closeWaitSidebar();
        map.setViewBounds(Tracking._locationGroup.getBounds(),false);
        this.LoadHistoryList();
    }*/
    /**
     * Dibuja el Tiempo Muerto en el mapa
     * @param unidad Identificador de Vehiculo
     */
    /*public DrawIdleTime(unidad : string = "") {
        for(var i =0;i<Tracking._locationArray.length;i++){ 
            var carid = objectIndex(vehiculos,"Unidad",Tracking._locationArray[i].id);
            if(unidad == "" || unidad == Tracking._locationArray[i].id){
                for(var j =0;j<Tracking._locationArray[i].locations.length;j++){
                    var evento = Tracking._locationArray[i].locations[j].Evento;
                    Tracking._locationArray[i].points.push(
                        new H.map.Marker(
                            {lat: Tracking._locationArray[i].locations[j].Latitud, lng: Tracking._locationArray[i].locations[j].Longitud},
                            {
                                icon: new H.map.DomIcon("img/eventos/"+evento+".png",{size: {w:40,h:40} })
                            }
                        )
                    );
                    var f = new moment(Tracking._locationArray[i].locations[j].Fecha);
                    
                    Tracking._locationArray[i].points[j].setData(
                        '<div class="inputGroup">'+
                            '<h6><es>Información</es><en>Information</en></h6>'+
                            '<div class="markerInfo">'+
                                '<div><span class="ibHeader"><es>Vehículo</es><en>Vehicle</en>:</span><span>'+vehiculos[carid].Descripcion+'</span></div>'+
                                (evento == -1 ? "" : '<div><span class="ibHeader"><es>Evento</es><en>Event</en>:</span><span>'+this.GetEvento(evento)+'</span></div>')+
                                '<div><span class="ibHeader"><es>Fecha y Hora</es><en>Time Date</en>:</span><span>'+f.format("YYYY/MM/DD HH:mm:ss")+'</span></div>'+
                                '<div><span class="ibHeader"><es>Velocidad</es><en>Speed</en>:</span><span>'+distConv(Tracking._locationArray[i].locations[j].Velocidad,true,true,true)+'/h</span></div>'+
                                '<div><span class="ibHeader"><es>Ralentí</es><en>Idle</en>:</span><span>'+timeConverter(Tracking._locationArray[i].locations[j].Tiempo*60000) +'</span></div>' +
                            '</div>'+
                        '</div>'
                    );
                   
                    Tracking._locationArray[i].points[j]._i = i;
                    Tracking._locationArray[i].points[j]._j = j;
                    Tracking._locationArray[i].points[j]._evento = evento;
                    Tracking._locationGroup.addObject(Tracking._locationArray[i].points[j]);
                }
            }
        }
        try{
            Tracking._locationGroup.removeEventListener('tap',Track.CreatePointBubble,false);
        }catch(e){}
        Tracking._locationGroup.addEventListener('tap',Track.CreatePointBubble,false);
        if($(".eventMenu").hasClass("hidden")){
            $(".eventMenu").removeClass("hidden");
            //toasty(lang("vehicleevents"),"success");
        }
        closeWaitSidebar();
        map.setViewBounds(Tracking._locationGroup.getBounds(),false);
        this.LoadHistoryList();
    }*/
    /**
     * Dibuja los Excesos de Velocidad en el mapa
     * @param unidad Identificador de Vehiculo
     */
    /*public DrawSpeedLimit(unidad : string = "") {
        for(var i =0;i<Tracking._locationArray.length;i++){ 
            var carid = objectIndex(vehiculos,"Unidad",Tracking._locationArray[i].id);
            var iconid = vehiculos[carid].IconoID;
            var carIcon = objectIndex(Vehiculos._iconList,"id",iconid);
            carIcon = "img/iconos/"+Vehiculos._iconList[carIcon].icon+"_moviendo.png";

            if(unidad == "" || unidad == Tracking._locationArray[i].id){
                for(var j =0;j<Tracking._locationArray[i].locations.length;j++){
                    var evento = (typeof Tracking._locationArray[i].locations[j].Evento == "undefined" ? -1 : Tracking._locationArray[i].locations[j].Evento);
                    Tracking._locationArray[i].points.push(
                        new H.map.Marker(
                            {lat: Tracking._locationArray[i].locations[j].Latitud, lng: Tracking._locationArray[i].locations[j].Longitud},
                            {
                                icon: new H.map.DomIcon(carIcon,{size: {w:40,h:40} })
                            }
                        )
                    );
                    var f = new moment(Tracking._locationArray[i].locations[j].Fecha);
                    
                    Tracking._locationArray[i].points[j].setData(
                        '<div class="inputGroup">'+
                            '<h6><es>Información</es><en>Information</en></h6>'+
                            '<div class="markerInfo">'+
                                '<div><span class="ibHeader"><es>Vehículo</es><en>Vehicle</en>:</span><span>'+vehiculos[carid].Descripcion+'</span></div>'+
                                (evento == -1 ? "" : '<div><span class="ibHeader"><es>Evento</es><en>Event</en>:</span><span>'+this.GetEvento(evento)+'</span></div>')+
                                '<div><span class="ibHeader"><es>Fecha y Hora</es><en>Time Date</en>:</span><span>'+f.format("YYYY/MM/DD HH:mm:ss")+'</span></div>'+
                                '<div><span class="ibHeader"><es>Velocidad</es><en>Speed</en>:</span><span>'+distConv(Tracking._locationArray[i].locations[j].Velocidad,true,true,true)+'/h</span></div>'+
                                (typeof Tracking._locationArray[i].locations[j].aPuntoDist === "undefined" ? '' : '<div><span class="ibHeader"><es>Punto</es><en>Point</en>:</span><span>'+Tracking._locationArray[i].locations[j].aPuntoLabel.replace("__DIST__",parseDist(Tracking._locationArray[i].locations[j].aPuntoDist))+'</span></div>' )+
                            '</div>'+
                        '</div>'
                    );
                   
                    Tracking._locationArray[i].points[j]._i = i;
                    Tracking._locationArray[i].points[j]._j = j;
                    Tracking._locationArray[i].points[j]._evento = evento;
                    Tracking._locationGroup.addObject(Tracking._locationArray[i].points[j]);
                }
            }
        }
        try{
            Tracking._locationGroup.removeEventListener('tap',Track.CreatePointBubble,false);
        }catch(e){}
        Tracking._locationGroup.addEventListener('tap',Track.CreatePointBubble,false);
        if($(".eventMenu").hasClass("hidden")){
            $(".eventMenu").removeClass("hidden");
           //toasty(lang("vehicleevents"),"success");
        }
        closeWaitSidebar();
        map.setViewBounds(Tracking._locationGroup.getBounds(),false);
        this.LoadHistoryList();
    }*/
    /**
     * Dibuja el Tiempo Muerto en el mapa
     * @param unidad Identificador de Vehiculo
     */
    /*public DrawDeadTime(unidad : string = "") {
        for(var i =0;i<Tracking._locationArray.length;i++){ 
            var carid = objectIndex(vehiculos,"Unidad",Tracking._locationArray[i].id);
            var iconid = vehiculos[carid].IconoID;
            var carIcon = objectIndex(Vehiculos._iconList,"id",iconid);
            carIcon = "img/iconos/"+Vehiculos._iconList[carIcon].icon+"_detenido.png";

            if(unidad == "" || unidad == Tracking._locationArray[i].id){
                for(var j =0;j<Tracking._locationArray[i].locations.length;j++){
                    Tracking._locationArray[i].points.push(
                        new H.map.Marker(
                            {lat: Tracking._locationArray[i].locations[j].Latitud, lng: Tracking._locationArray[i].locations[j].Longitud},
                            {
                                icon: new H.map.DomIcon(carIcon,{size: {w:40,h:40} })
                            }
                        )
                    );
                    var f = new moment(Tracking._locationArray[i].locations[j].Fecha);
                    var evento = (typeof Tracking._locationArray[i].locations[j].Evento == "undefined" ? -1 : Tracking._locationArray[i].locations[j].Evento);
                    Tracking._locationArray[i].points[j].setData(
                        '<div class="inputGroup">'+
                            '<h6><es>Información</es><en>Information</en></h6>'+
                            '<div class="markerInfo">'+
                                '<div><span class="ibHeader"><es>Vehículo</es><en>Vehicle</en>:</span><span>'+vehiculos[carid].Descripcion+'</span></div>'+
                                (evento == -1 ? "" : '<div><span class="ibHeader"><es>Evento</es><en>Event</en>:</span><span>'+this.GetEvento(evento)+'</span></div>')+
                                '<div><span class="ibHeader"><es>Fecha y Hora</es><en>Time Date</en>:</span><span>'+f.format("YYYY/MM/DD HH:mm:ss")+'</span></div>'+
                                '<div><span class="ibHeader"><es>Velocidad</es><en>Speed</en>:</span><span>'+distConv(Tracking._locationArray[i].locations[j].Velocidad,true,true,true)+'/h</span></div>'+
                                (typeof Tracking._locationArray[i].locations[j].aPuntoDist === "undefined" ? '' : '<div><span class="ibHeader"><es>Punto</es><en>Point</en>:</span><span>'+Tracking._locationArray[i].locations[j].aPuntoLabel.replace("__DIST__",parseDist(Tracking._locationArray[i].locations[j].aPuntoDist))+'</span></div>' )+
                            '</div>'+
                        '</div>'
                    );
                   
                    Tracking._locationArray[i].points[j]._i = i;
                    Tracking._locationArray[i].points[j]._j = j;
                    Tracking._locationArray[i].points[j]._evento = evento;
                    Tracking._locationArray[i].points[j]._carid = carid;
                    Tracking._locationGroup.addObject(Tracking._locationArray[i].points[j]);
                }
            }
        }
        try{
            Tracking._locationGroup.removeEventListener('tap',Track.CreatePointBubble,false);
        }catch(e){}
        Tracking._locationGroup.addEventListener('tap',Track.CreatePointBubble,false);
        if($(".eventMenu").hasClass("hidden")){
            $(".eventMenu").removeClass("hidden");
           //toasty(lang("vehicleevents"),"success");
        }
        closeWaitSidebar();
        map.setViewBounds(Tracking._locationGroup.getBounds(),false);
        this.LoadHistoryList();
    }*/

    /**
     * Dibuja la ruta de Historico de una unidad en especifico
     * @param unidad Identificador de Vehiculo
     */
    public DrawHistory(unidad : string = ""){
        console.log("DRAWING HISTORY...");
        if(Tracking._markerInstance == null){
            Tracking._markerInstance = new Array();
        }

        for(var i =0;i<Tracking._locationArray.length;i++){ 
            var carid = objectIndex(vehiculos,"Unidad",Tracking._locationArray[i].id);
           
            console.log("DRAWING ROUTE");
            if(unidad == "" || unidad == Tracking._locationArray[i].id){
                var puntos = new H.geo.LineString();
                for(var j =0;j<Tracking._locationArray[i].locations.length;j++){
                    puntos.pushPoint({lat: Tracking._locationArray[i].locations[j].Latitud, lng:  Tracking._locationArray[i].locations[j].Longitud});
                }
                var color = ColorLuminance(getRandomColor(),-0.4);
                Tracking._locationArray[i].line = new H.map.Polyline(
                    puntos, 
                    { 
                        style: { 
                            lineWidth: 6,
                            strokeColor: hexToRgbA(color,0.6),
                            fillColor: hexToRgbA(color,0.6),
                            },
                        arrows: { 
                            fillColor: 'white', 
                            frequency: 15, 
                            width: 2, 
                            length: 1.8 
                        }
                    }
                );
                
                Tracking._locationGroup.addObject(Tracking._locationArray[i].line);
                for(var j =0;j<Tracking._locationArray[i].locations.length;j++){
                    if(j == 0) { //PIN DE PARTIDA
                        var iconElem = this.CreateDomIcon("img/iconos/pin-start.png","startend");
                        var startMarker =  new H.map.DomMarker(
                            {lat: Tracking._locationArray[i].locations[j].Latitud , lng: Tracking._locationArray[i].locations[j].Longitud },
                            {
                                icon: new H.map.DomIcon(iconElem)
                            }
                        );
                        var labelElem = this.CreateDomLabel(lang("route_start"),"point");
                        var startLabel = new H.map.DomMarker(
                            {lat:Tracking._locationArray[i].locations[j].Latitud , lng: Tracking._locationArray[i].locations[j].Longitud},
                            {
                                min: 15,
                                icon: new H.map.DomIcon(labelElem)
                            }
                        );
                        startMarker.setZIndex(0);
                        startLabel.setZIndex(0);
                        Tracking._locationGroup.addObject(startMarker);
                        Tracking._locationGroup.addObject(startLabel);
                    }
                    if(j == Tracking._locationArray[i].locations.length-1) { //PIN DE META
                        var iconElem = this.CreateDomIcon("img/iconos/pin-end.png","startend");
                        var endMarker =  new H.map.DomMarker(
                            {lat: Tracking._locationArray[i].locations[j].Latitud , lng: Tracking._locationArray[i].locations[j].Longitud },
                            {
                                icon:  new H.map.DomIcon(iconElem)
                            }
                        );
                        var labelElem = this.CreateDomLabel(lang("route_end"),"point");
                        var endLabel = new H.map.DomMarker(
                            {lat:Tracking._locationArray[i].locations[j].Latitud , lng: Tracking._locationArray[i].locations[j].Longitud},
                            {
                                min: 15,
                                icon: new H.map.DomIcon(labelElem)
                            }
                        );
                        endMarker.setZIndex(0);
                        endLabel.setZIndex(0);
                        Tracking._locationGroup.addObject(endMarker);
                        Tracking._locationGroup.addObject(endLabel);
                    }
                    Tracking._locationArray[i].points.push(new H.map.Circle(
                        // The central point of the circle
                        {lat: Tracking._locationArray[i].locations[j].Latitud, lng: Tracking._locationArray[i].locations[j].Longitud},
                        // The radius of the circle in meters
                        8*DPR,
                        {
                            style: {
                                strokeColor: hexToRgbA(color,0.9), // Color of the perimeter
                                lineWidth: 2,
                                fillColor: 'rgba(255, 255, 255, 0.8)'  // Color of the circle
                            }
                        }
                    ));
                    if(Tracking._isHistoryPoints == true){
                        this.CalcularDistanciaPuntos(i,j);
                    }
                    var f = new moment(Tracking._locationArray[i].locations[j].Fecha);
                    var evento = ('Evento' in Tracking._locationArray[i].locations[j] === false ? -1 : Tracking._locationArray[i].locations[j].Evento);
                    Tracking._locationArray[i].points[j].setData(
                        `<div class="inputGroup">
                            <h6><es>Información</es><en>Information</en></h6>
                            <div class="markerInfo">
                                <div><span class="ibHeader"><es>Vehículo</es><en>Vehicle</en>:</span><span>${vehiculos[carid].Descripcion}</span></div>
                                ${(evento == -1 ? "" : `<div><span class="ibHeader"><es>Evento</es><en>Event</en>:</span><span>${this.GetEvento(evento)}</span></div>`)}
                                <div><span class="ibHeader"><es>Dirección</es><en>Address</en>:</span><span class="${unidad}-bubble_address"><es>Buscando...</es><en>Locating...</en></span></div>
                                <div><span class="ibHeader"><es>Fecha y Hora</es><en>Time Date</en>:</span><span class="${unidad}-bubble_date">${f.format("YYYY/MM/DD HH:mm:ss")}</span></div>
                                <div><span class="ibHeader"><es>Velocidad</es><en>Speed</en>:</span><span class="${unidad}-bubble_speed">${distConv(Tracking._locationArray[i].locations[j].Velocidad,true,true,true)}/h</span></div>
                                <div><span class="ibHeader"><es>Cerca de</es><en>Places Near</en>:</span><span class="${unidad}-bubble_near"><es>Explorando...</es><en>Exploring...</en></span></div>

                                ${('porcentajeBateria' in Tracking._locationArray[i].locations[j]  && this.isBatteryEnabled(carid) ? `<div><span class="ibHeader"><es>Batería</es><en>Battery</en>:</span><span>${Tracking._locationArray[i].locations[j].porcentajeBateria}%</span></div>`:'' )}

                                ${('aPuntoDist' in Tracking._locationArray[i].locations[j] === false ? '' : `<div><span class="ibHeader"><es>Punto</es><en>Point</en>:</span><span>${Tracking._locationArray[i].locations[j].aPuntoLabel.replace("__DIST__",parseDist(Tracking._locationArray[i].locations[j].aPuntoDist))}</span></div>` )}
                                
                                <div><span class="ibHeader"><es>Mapa</es><en>Map</en>:</span><span onclick="openMaps('${Tracking._locationArray[i].locations[j].Latitud}','${Tracking._locationArray[i].locations[j].Longitud}')" class="${unidad}-bubble_coords"><es>Abrir Mapa Externo</es><en>Open External Map</en> <i class="fas fa-external-link-alt"></i></span></div>
                            </div>
                        </div>`
                    );
                   
                    Tracking._locationArray[i].points[j]._i = i;
                    Tracking._locationArray[i].points[j]._j = j;
                    Tracking._locationArray[i].points[j]._evento = evento;
                    Tracking._locationArray[i].points[j]._carid = carid;
                    Tracking._locationArray[i].points[j].unidad = unidad;
                    Tracking._locationGroup.addObject(Tracking._locationArray[i].points[j]);
                }
                Track.AddUnitToMap(Tracking._locationArray[i].id, vehiculos[carid].Descripcion, Tracking._locationArray[i].locations[Tracking._locationArray[i].locations.length-1].Latitud, Tracking._locationArray[i].locations[Tracking._locationArray[i].locations.length-1].Longitud, Tracking._locationArray[i].locations[Tracking._locationArray[i].locations.length-1].Velocidad,null,Tracking._locationArray[i].locations[Tracking._locationArray[i].locations.length-1].Fecha);
            }
        }
        try{
            Tracking._locationGroup.removeEventListener('tap',Track.CreatePointBubble,false);
        }catch(e){}
        Tracking._locationGroup.addEventListener('tap',Track.CreatePointBubble,false);
        if($(".eventMenu").hasClass("hidden")){
            $(".eventMenu").removeClass("hidden");
           //toasty(lang("vehicleevents"),"success");
        }
        closeWaitSidebar();
        map.setViewBounds(Tracking._locationGroup.getBounds(),false);
        this.LoadHistoryList();
    }
    /**
     * Reinicia el Z-Index de los puntos en el mapa y los coloca al mismo eje.
     * @param _id Identificador de Ubicaciones de un Vehiculo en especifico
     */
    public ResetCirclesZ(_id : number) {
        for(var i =0;i<Tracking._locationArray[_id].points.length;i++){
            Tracking._locationArray[_id].points[i].setZIndex(4);
        }
    }
    
    /**
     * Crea una Burbuja de Informacion para los Vehiculos en el mapa
     * @param evt Variable auxiliar
     */
    public CreateUnitBubble(evt : any) {
        //console.log("Bubble data: "+evt.target.getData())
        try{
            if(evt.target.getData() != undefined){
                Track.CloseBubbles();
                
                var coords = evt.target.getPosition();

                //Track.MoveCarInstance(coords.lat,coords.lng);
                window.ibubble = new H.ui.InfoBubble(coords, {
                    // read custom data
                    content: evt.target.getData()
                });
                var closeWrapper = window.ibubble.close;
                window.ibubble.close = function() { 
                    $(".infoCars li").removeClass("selected");
                    closeWrapper.apply(window.ibubble);
                }
                window.ibubble.unidad = evt.target.unidad;
                // show info bubble
                mapui.addBubble(window.ibubble);
                $("#sidebar").scrollTop($(".rtItem-"+evt.target.unidad)[0].offsetTop);
                
                $(".infoCars li").removeClass("selected");
                $(".rtItem-"+evt.target.unidad).addClass("selected");
                window.selectedRTitem = evt.target.unidad;
                try{
                    var uuid = objectIndex(ultimasUbicaciones,"Unidad",evt.target.unidad);
                    $(`.${evt.target.unidad}-bubble_speed`).html(distConv(ultimasUbicaciones[uuid].Velocidad,true,false,true)+"/h");
                    var f = new moment(ultimasUbicaciones[uuid].Fecha);
                    $(`.${evt.target.unidad}-bubble_date`).html(f.format("YYYY/MM/DD HH:mm:ss"));
                    if('porcentajeBateria' in ultimasUbicaciones[uuid]) {
                        $(`.${evt.target.unidad}-bubble_battery`).html(ultimasUbicaciones[uuid].porcentajeBateria+"%");
                    }
                }catch(e){}
                Track.getUnitFuelData(evt.target.unidad);
                Interface.GetInterfaces(true);
                geocoder.reverseGeocode(
                    {
                        mode:       'retrieveAddresses',
                        prox:       coords.lat+','+coords.lng,
                        maxresults: 1
                    },
                    (data) => {
                        if(data.Response.View.length >0){
                            $("."+evt.target.unidad+"-bubble_address").html(data.Response.View[0].Result[0].Location.Address.Label);
                        }else{
                            $("."+evt.target.unidad+"-bubble_address").html("N/A");
                        }
                    },
                    ()=>{
                        $("."+evt.target.unidad+"-bubble_address").html("N/A");
                    }
                );
                places.request({
                    at:     coords.lat+','+coords.lng,
                    size:   5
                    }, 
                    {}, 
                    (data)=>{
                        var lugares = new Array();
                        if(data.results.items.length >0){
                            for(var i =0;i<data.results.items.length;i++){
                                lugares.push(data.results.items[i].title);
                            }
                            $("."+evt.target.unidad+"-bubble_near").html(lugares.join(', '));
                        }else{
                            $("."+evt.target.unidad+"-bubble_near").html("N/A");
                        }
                    }, 
                    ()=>{
                        $("."+evt.target.unidad+"-bubble_near").html("N/A");
                    }
                );
                var markerInsId = objectIndex(Tracking._markerInstance,"id",evt.target.unidad);
                Tracking._markerInstance[markerInsId].label.setVisibility(true);
            }
        }catch(e){
            console.error("Bubble Error",e);
            window._evt = evt;
        }
    }
    /**
     * Crea una burbuja de informacion en un Punto o Evento
     * @param evt Variable auxiliar
     */
    public CreatePointBubble(evt : any) {
        try{
            var newIcon = null;
            try{
                if(evt.target._evento > -1){
                    newIcon = "img/eventos/"+evt.target._evento+".png";
                }
            }catch(e){}
            if(Tracking._isDeadTime == false && 
                Tracking._isEvents == false && 
                Tracking._isSpeedLimit == false && 
                Tracking._isIdleTime == false && 
                Tracking._isDefault == false &&
                evt.target.noLocation != true){
                Track.AddUnitToMap(evt.target.unidad, 
                    vehiculos[evt.target._carid].Descripcion, 
                    Tracking._locationArray[evt.target._i].locations[evt.target._j].Latitud, 
                    Tracking._locationArray[evt.target._i].locations[evt.target._j].Longitud, 
                    Tracking._locationArray[evt.target._i].locations[evt.target._j].Velocidad,
                    newIcon,
                    Tracking._locationArray[evt.target._i].locations[evt.target._j].Fecha);
            }
            Track.CloseBubbles();
            if(Tracking._isDeadTime == true || 
                Tracking._isEvents == true || 
                Tracking._isSpeedLimit == true || 
                Tracking._isIdleTime == true || 
                Tracking._isDefault == true ||
                Tracking._isTracking == true ||
                Tracking._isLastPosition == true){
                var coords = evt.target.getPosition();
            }else{
                var coords = evt.target.getCenter();
            }
            if(Tracking._isDefault == false && evt.target.noLocation != true) {
                Track.ResetCirclesZ(evt.target._i);
            }
            evt.target.setZIndex(5);
            //Track.MoveCarInstance(coords.lat,coords.lng);
            window.ibubble = new H.ui.InfoBubble(coords, {
                // read custom data
                content: evt.target.getData()
            });
            // show info bubble
            mapui.addBubble(window.ibubble);
            try{
                $("."+evt.target.unidad+"-bubble_speed").html(distConv(Tracking._locationArray[evt.target._i].locations[evt.target._j].Velocidad,true,true,true)+"/h");
            }catch(e){}
            
            geocoder.reverseGeocode(
                {
                    mode:       'retrieveAddresses',
                    prox:       coords.lat+','+coords.lng,
                    maxresults: 1
                },
                (data) => {
                    if(data.Response.View.length >0){
                        $("."+evt.target.unidad+"-bubble_address").html(data.Response.View[0].Result[0].Location.Address.Label);
                    }else{
                        $("."+evt.target.unidad+"-bubble_address").html("N/A");
                    }
                },
                ()=>{
                    $("."+evt.target.unidad+"-bubble_address").html("N/A");
                }
            );
            places.request({
                at:     coords.lat+','+coords.lng,
                size:   5
                }, 
                {}, 
                (data)=>{
                    var lugares = new Array();
                    if(data.results.items.length >0){
                        for(var i =0;i<data.results.items.length;i++){
                            lugares.push(data.results.items[i].title);
                        }
                        $("."+evt.target.unidad+"-bubble_near").html(lugares.join(', '));
                    }else{
                        $("."+evt.target.unidad+"-bubble_near").html("N/A");
                    }
                }, 
                ()=>{
                    $("."+evt.target.unidad+"-bubble_near").html("N/A");
                }
            );
            try{
                $("#sidebar").scrollTop($(".carEvent-"+evt.target._i+evt.target._j)[0].offsetTop);
                $(".carEventList .list li").removeClass("activo");
                $(".carEventList .list li.carEvent-"+evt.target._i+evt.target._j).addClass("activo");
            }catch(e){
                //console.error(e);
                window._evt = evt;
            }

        }catch(e){
            console.error("Bubble Error",e);
            window._evt = evt;
        }
    }
    /**
     * Cierra burbujas de informacion
     */
    public CloseBubbles(){
        if(window.ibubble != null){
            window.ibubble.close();
            window.ibubbles = null;
        }
    }
    /**
     * Obtiene Eventos de unidades en los ultimos 2 minutos
     */
    public MultiEvento() {
        var m =             new moment();
        var fechaFinal =    m.toDate().getTime();
        var fechaInicial =  m.subtract(2,'minutes');
        $.ajax({
            url:    rasStaticUrl  + "MultiEvento",
            type:   "POST",
            timeout:10000,
            dataType:"json",
            data: {
                "token":        window["userData"]["token"],
                "fechaFinal":   fechaFinal,
                "fechaInicial": fechaInicial.toDate().getTime(),
                "unidades":     Tracking._rastrearUnidades,
                "zona":         window.zona
            },
            success: (data)=>{
                //console.log(data);
                try{
                    window.multiEvento = JSON.parse(data.data.d);
                    this.VerificacionEventos();
                }catch(e){
                    
                }
            },
            error: function(data){
                console.log(data);
            }
        });
    }
    /**
     * Agrega un Evento al arreglo de Eventos
     * @param unidad Identificador de Vehiculo
     * @param tipo Tipo de Evento
     * @param fecha Fecha del Evento
     * @param data Datos extra del Evento
     */
    public AddEvent(unidad : string, tipo : string, fecha : string, data : any){
        var found = false;
        if(isTEApp == false){
            try{
                var preDate = new moment(fecha);
                if(tipo == "geo"){ //Eventos de geocercas llegan a hora de UTC.
                    var fechaFixed = preDate.subtract(Math.abs(window.zona),'hours');
                }else{
                    var fechaFixed = preDate;
                }
                for(var i=0;i<Tracking._eventArray.length;i++){
                    if(Tracking._eventArray[i].unidad == unidad && 
                        Tracking._eventArray[i].tipo == tipo && 
                        /*Tracking._eventArray[i].data.Latitud == data.Latitud && 
                        Tracking._eventArray[i].data.Longitud == data.Longitud*/
                        Tracking._eventArray[i].fecha == fechaFixed.format('YYYY-MM-DD[T]HH:mm:ss')){
                            found = true; //SE ENCONTRO UN EVENTO IGUAL, ENTONCES NO SE AGREGA COMO NUEVO
                    }
                }
                if(found == false){
                    //console.log(data);
                    
                    Tracking._eventArray.push({
                        "tipo":     tipo,
                        "unidad":   unidad,
                        "fecha":    fechaFixed.format('YYYY-MM-DD[T]HH:mm:ss'),
                        "data":     data
                    });
                    var events = this.GetCarEventCount(unidad);
                    $(".rtItem-"+unidad+" .event-counter").html(`${events}&nbsp;${events == 1 ? '<es>evento</es><en>event</en>' : '<es>eventos</es><en>events</en>'}`).attr("events",events);
                    $(".rtItem-"+unidad+" .rtDate").html(fechaFixed.format("Y/MMM/DD [<es>a las</es><en>at</en>] HH:mm:ss"));
                    if(Tracking._watchingUnit == unidad){
                        this.CarEventList(Tracking._watchingUnit);
                    }else{
                        if(tipo != "geo"){
                            Tracking._bannerList.push(
                                {
                                    "lat":      data.Latitud,
                                    "lng":      data.Longitud,
                                    "evento":   tipo,
                                    "unidad":   unidad,
                                    "fecha":    fechaFixed.format('YYYY-MM-DD[T]HH:mm:ss'),
                                    "velocidad":data.Velocidad,
                                    "visto":    false,
                                    "mostrado": false
                                }
                            );
                            this.DisplayBanner();
                        }
                    }
                }
            }catch(e){
                console.log(e);
            }
        }
    }
    /**
     * Verifica los Eventos obtenidos en multiEvento y los agrega a la app
     */
    public VerificacionEventos(){
        for(var me in multiEvento){
            for(var j=0;j<multiEvento[me].length;j++){
                this.AddEvent(multiEvento[me][j].Unidad,multiEvento[me][j].Evento,multiEvento[me][j].Fecha,multiEvento[me][j]);
            }
        }
    }
    /**
     * Muestra el banner con si ocurre un Evento en RC o por Push
     */
    public DisplayBanner() {
        if(new Sesion().Permisos("SoloRastreo") == false){
            var bannerID = -1;
            for(var i=0; i < Tracking._bannerList.length;i++){
                if(bannerID == -1 && Tracking._bannerList[i].mostrado == false) {
                    bannerID = i;
                }
            }
            if(bannerID >= 0 && $("#push-popup").hasClass("visible") == false) {
                if(objectIndex(vehiculos,"Unidad",Tracking._bannerList[bannerID].unidad) >= 0) {
                    showBanner(Tracking._bannerList[bannerID].evento,
                        Tracking._bannerList[bannerID].unidad,
                        Tracking._bannerList[bannerID].fecha,
                        Tracking._bannerList[bannerID].lat,
                        Tracking._bannerList[bannerID].lng,
                        Tracking._bannerList[bannerID].velocidad);
                    Tracking._bannerList[bannerID].mostrado = true;
                    setTimeout(() => {
                        $("#push-popup").removeClass("visible");
                        setTimeout(()=>{
                            PushNotifs.DisplayPush();
                            this.DisplayBanner();
                        },1500);
                    },5000);
                }
            }
        }
    }
    /**
     * Wrapper para abrir el Sidebar
     */
    public Information() {
        try{
            clearTimeout(window.showcaseTimeout);
        }catch(e){}
        try{
            masterClose = ()=>{this.CloseSidebar("carInfoList");};
        }catch(e){}
        $(".showcase").fadeOut("fast");
        $(".mainTop .fade,#map .fade").hide();
        if(!$("#settings").is(":visible")) {
            $("#main .mainTop").addClass("no-logo");
            this.AdjustTitle();
            this.OpenSidebar("carInfoList");
        }
    }
    /**
     * Cambia el titulo del top y del showcase
     * dependiendo del modulo abierto
     */
    public AdjustTitle() {
        if(Tracking._isTracking == true){
            $("#main .mainTop.no-logo .title,.tap-title,.showcase-tap-title").html("<es>Rastreo Continuo</es><en>Real Time Tracking</en>");
            $(".tap-subtitle,.showcase-tap-subtitle").html("<es>Cuando quiera ver información de sus unidades rastreadas después de seleccionar Rastreo Continuo, solo toque este ícono.</es><en>When you want to see information about your tracked units after you start Real Time Tracking just tap this icon.</en>");
        }else if(Tracking._isHistory == true){
            $("#main .mainTop.no-logo .title,.tap-title,.showcase-tap-title").html("<es>Histórico</es><en>History</en>");
            $(".tap-subtitle,.showcase-tap-subtitle").html("<es>Cuando quiera ver una línea de tiempo con eventos de sus unidades después de seleccionar Histórico, solo toque este ícono.</es><en>When you want to see information about the timeline of events and routes of your units after you start a History request just tap this icon.</en>");
        }else if(Tracking._isLastPosition == true){
            $("#main .mainTop.no-logo .title,.tap-title,.showcase-tap-title").html("<es>&Uacute;ltima Posici&oacute;n</es><en>Last Position</en>");
            $(".tap-subtitle,.showcase-tap-subtitle").html("<es>Cuando quiera ver información detallada o eventos de sus unidades después de seleccionar &Uacute;ltima Posici&oacute;n, solo toque este ícono.</es><en>When you want to see the last detailed information of your units after you select Last Position just tap this icon.</en>");
        }else{
            $("#main .mainTop.no-logo .title,.tap-title,.showcase-tap-title").html("<es>Eventos de Unidades</es><en>Vehicle Events</en>");
            $(".tap-subtitle,.showcase-tap-subtitle").html("<es>Si selecciona unidades en Rastreo Continuo, &Uacute;ltima Posici&oacute;n o Hist&oacute;rico seleccione este ícono para ver más información.</es><en>If you select units on Real Time Tracking, Last Position or History, you can see more information about them if you tap on this icon.</en>");
        }
    }
    /**
     * Enfoca una unidad en especifico en el mapa y ajusta el zoom
     * @param unit Identificador de Vehiculo
     * @param zoom Nivel de Zoom a utilizar
     */
    public GoToCar(unit : string, zoom : number = 18){
        var id = objectIndex(Tracking._markerInstance,"id",unit);
        map.setCenter(Tracking._markerInstance[id].marker.getPosition());
        map.setZoom(zoom);
    }
    /**
     * Abre el Sidebar
     * @param section Seccion en especifico del Sidebar a abrir
     */
    public OpenSidebar(section : string){
        $("#sidebar .toHide").hide();
		$("#sidebar .carInformation").show();
        $("#sidebar,#close-btn").removeClass("hidden");
		$("#sidebar .carInformation > div").hide();
		$(".carInformation ."+section).show();
        $("#close-btn").attr("onclick","Track.CloseSidebar('"+section+"')");
        console.warn(`Open Sidebar isTracking?: ${Tracking._isTracking}`);
        if(Tracking._isTracking == true || 
        Tracking._isLastPosition == true){
            if(Tracking._rastrearUnidades.length>=10){
                $("#sidebar .search-input").removeClass("hidden");
            }else{
                $("#sidebar .search-input").addClass("hidden");
            }
            this.LoadCarInfoList();
        }else if(Tracking._isHistory == true    ||
            Tracking._isHistoryPoints == true   ||
            Tracking._isDeadTime == true        ||
            Tracking._isEvents == true          ||
            Tracking._isSpeedLimit == true      || 
            Tracking._isIdleTime == true        || 
            Tracking._isVisited == true){
                if(Tracking._rastrearUnidades.length>=10){
                    $("#sidebar .search-input").removeClass("hidden");
                }else{
                    $("#sidebar .search-input").addClass("hidden");
                }
            this.LoadHistoryList();
        }else{
            var savedPush = PushNotifs.RealSavedPushLength();
            if(savedPush > 0){
                if(savedPush >= 10){
                    $("#sidebar .search-input").removeClass("hidden");
                }else{
                    $("#sidebar .search-input").addClass("hidden");
                }
                $(".carInformation > .noContent").addClass("hidden");
                $("#sidebar").removeClass("emptylist");
                $("#sidebar .emptycar").remove();
                PushNotifs.FillSavedPush();
            }else{
                $("#sidebar .search-input").addClass("hidden");
                $(".carInformation > .noContent").removeClass("hidden").show();
                $("#sidebar").addClass("emptylist");
                $("#sidebar").append("<div class='emptycar'></div>");
            }
        }
        $(".mainTop .tool.right").hide();
        views();
        Config.disableFullApp();
    }
    /**
     * Cierra el Sidebar y cancela la seccion en la que se encontraba
     * @param section Seccion del Sidebar a cancelar
     */
    public CloseSidebar(section : string){
        if($(".carEventList").is(":visible") || Tracking._mapExpanded == true){
            $("#carEventList .search-input .filter-btn").removeClass("hide");
            $("#carEventList .search-input .search-field").removeClass("s12").addClass("s10");
            $("#filter-abovezero").prop("checked",false);
            $("#filter-onlyrelevant").prop("checked",false);
            $("#event-times").html('');
            if(Tracking._mapExpanded == true) {
                this.CompressMap();
            }
            Tracking._watchingUnit = null;
            $(".carEventList").hide();
            $(".carInfoList").show();
            Tracking._eventGroup.removeAll();
            this.AdjustTitle();
            $("#sidebar").removeClass("emptylist");
            $("#sidebar .emptycar").remove();
            $(".mainTop .tool.right").attr("onclick","Track.Information()").hide();
            $(".mainTop .tool.right i").removeClass("zicon_maximizar fas fa-arrow-alt-circle-up");
            if(Tracking._isHistory == true){
                $(".mainTop .tool.right i").addClass("zicon_historial");
            }else if(Tracking._isTracking == true) {
                $(".mainTop .tool.right i").addClass("zicon_rastreo-continuo");
            }else if(Tracking._isLastPosition == true){
                $(".mainTop .tool.right i").addClass("zicon_ultima-posicion"); 
            }else{
                $(".mainTop .tool.right i").addClass("zicon_eventos"); 
            }
            if(Tracking._rastrearUnidades.length >=10){
                $("#sidebar .search-input").removeClass("hidden");
            }else{
                $("#sidebar .search-input").addClass("hidden");
            }
        }else{
            resetBack();
            $("#main .mainTop").removeClass("no-logo");
            $("#main .mainTop .title").html("");
            $("#sidebar,#add-btn,#close-btn").addClass("hidden");
            $(".carInformation ."+section).hide();
            $(".undoButton").addClass("disabled");
           
            $("#sidebar").removeClass("emptylist");
            $("#sidebar .emptycar").remove();
            if(Settings._featureDeclared == false){
                Settings._featureDeclared = true;
                if(!!window.cordova == false){ //PC
                    $('.tap-target').tapTarget();
                }else{
                    if (device.platform.toLowerCase() == "ios") {
                        $('.tap-target').tapTarget();
                    }
                }
            }
            if(Tracking._isTracking == true) {
                if(getKey("zeekFeatureTracking-"+userData.username) == null) {
                    Config.openShowcase();
                    setKey("zeekFeatureTracking-"+userData.username,true);
                }
            }
            if(Tracking._isLastPosition == true) {
                if(getKey("zeekFeatureLastPosition-"+userData.username) == null) {
                    Config.openShowcase();
                    setKey("zeekFeatureLastPosition-"+userData.username,true);
                }
            }
            if(Tracking._isHistory == true) {
                if(getKey("zeekFeatureHistory-"+userData.username) == null) {
                    Config.openShowcase();
                    setKey("zeekFeatureHistory-"+userData.username,true);
                }
            }
            if(SesionEngine.Permisos("SoloRastreo") == false){
                $(".mainTop .tool.right").show();
            }
        }
        
        views();
        try{
            map.setViewBounds(Tracking._markerGroup.getBounds());
        }catch(e){}
        Config.enableFullApp();
    }
    /**
     * Abre el UI de Filtrado de Eventos
     */
    public OpenFilter() {
        $(".filterCarEvents").removeClass("hidden");
        try{
            masterClose = ()=>{this.CloseFilter();};
        }catch(e){}
        var hourSelected = null;
        if( $("#event-times").val() != undefined){
            hourSelected = $("#event-times").val();
        }
        $("#event-times").html('');
        for(var i=0;i<window.eventTimes.length;i++) {
            $("#event-times").append(`<option value="${eventTimes[i].timestamp}" ${eventTimes[i].timestamp == hourSelected ? 'selected' : '' }>${eventTimes[i].hora}:00</option>`);
        }
    }
    /**
     * Cierra el UI de filtro de Eventos
     */
    public CloseFilter() {
        
        $(".filterCarEvents").addClass("hidden");
        try{
            masterClose = ()=>{this.CloseSidebar('carInfoList');};
        }catch(e){}
        //NECESARIO DOBLE PARA ACTUALIZAR CORRECTAMENTE
        if(Tracking._isHistory == true){
            this.CarHistoryEventList(Tracking._focusedCar);
            this.CarHistoryEventList(Tracking._focusedCar);
        }else if(Tracking._isTracking == true || Tracking._isLastPosition == true ){
            this.CarEventList(Tracking._watchingUnit);
            this.CarEventList(Tracking._watchingUnit);
        }
    }
    /**
     * Expande el mapa a pantalla completa (excepto el top)
     */
    public ExpandMap() {
        $(".mainTop .tool.right i").removeClass("zicon_maximizar").addClass("zicon_minimizar");
        $(".mainTop .tool.right").attr("onclick","Track.CompressMap()");
        $("#sidebar").hide();
        views();
        Tracking._mapExpanded = true;
    }
    /**
     * Regresa el mapa a su tamaño regular
     */
    public CompressMap() {
        $(".mainTop .tool.right i").removeClass("zicon_minimizar").addClass("zicon_maximizar");
        $(".mainTop .tool.right").attr("onclick","Track.ExpandMap()");
        $("#sidebar").show();
        views();
        Tracking._mapExpanded = false;
    }
    /**
     * Abre el Sidebar cargando la lista de Eventos acorde al 
     * modulo que fue invocado usando la solicitud de Historico
     * (Tiempo Muerto, Eventos, Exceso de Velocidad, Tiempo Muerto)
     */
    public LoadHistoryList() {
        masterClose=()=>{this.CloseSidebar('carInfoList');};
        console.log("LoadHistoryList");
        $(".carInfoList .list").html('');
        
        if(Tracking._rastrearUnidades.length == 0){
            $(".carInformation > .noContent").removeClass("hidden").show();
        }else{
            $(".carInformation > .noContent").addClass("hidden");
            for(var i=0;i < Tracking._rastrearUnidades.length;i++){
                
                var carid = objectIndex(vehiculos,"Unidad",Tracking._rastrearUnidades[i]);
                var iconId = objectIndex(Vehiculos._iconList,"id",vehiculos[carid].IconoID);
                var _id = objectIndex(Tracking._locationArray,"id",Tracking._rastrearUnidades[i]);
                var info = '';
                if('Marca' in vehiculos[carid]){
                    info+=vehiculos[carid].Marca+"&nbsp;";
                }
                if('Modelo' in vehiculos[carid]) {
                    info+=vehiculos[carid].Modelo;
                }
                if(typeof vehiculos[carid].PorcentajeBateria != "undefined" && this.isBatteryEnabled(carid)) {
                    info+=`<span class="car_battery"><i class="fas fa-bolt"></i> <es>Batería</es><en>Battery</en>: ${vehiculos[carid].PorcentajeBateria}%</span>`;
                }
                if(Tracking._isVisited == true){ //CALCULAR DISTANCIA A PUNTOS
                    _id = this.CalcPointProximity(Tracking._rastrearUnidades[i]);
                }
                if(_id >-1){
                    if(Tracking._isVisited == true){
                        $(".carInfoList .list").append(`<li class="rtItem-${Tracking._rastrearUnidades[i]}" onclick="Track.CarVisitedPoints('${Tracking._rastrearUnidades[i]}')">
                            <div class="flex-row">
                                <div class="left-icon car-icon"><span style="background-image:url('${getCarIconUri(vehiculos[carid].IconoID,true)}')"></span></div>
                                <div class="textBox">
                                    <div class="listText infoCarname">${vehiculos[carid].Descripcion}</div>
                                    ${(info == '' ? '' : `<div class="listSubtext infoDetails">${info}</div>`)}
                                    <div class="listSubtext rtDate">${Tracking._locationArray[_id].visitados.length} <es>visitados</es><en>visited</en></div>
                                </div>
                                <div class="chevron"><span class="lsf lsf-icon" icon="right"></span></div>
                            </div>
                        </li>`);
                    }else{
                        var fecha_ini = new moment(Tracking._locationArray[_id].locations[0].Fecha);
                        var fecha_fin = new moment(Tracking._locationArray[_id].locations[Tracking._locationArray[_id].locations.length-1].Fecha);
                       
                        $(".carInfoList .list").append(`<li class="rtItem-${Tracking._rastrearUnidades[i]}" onclick="Track.CarHistoryEventList('${Tracking._rastrearUnidades[i]}')">
                            <div class="flex-row">
                                <div class="left-icon car-icon"><span style="background-image:url('${getCarIconUri(vehiculos[carid].IconoID,true)}')"></span></div>
                                <div class="textBox">
                                    <div class="listText infoCarname">${vehiculos[carid].Descripcion}</div>
                                    ${(info == '' ? '' : `<div class="listSubtext infoDetails">${info}</div>`)}
                                    <div class="listSubtext rtDate rtFecha">${fecha_ini.format("DD / MMMM / YYYY")}</div>
                                </div>
                                <div class="chevron"><span class="lsf lsf-icon" icon="right"></span></div>
                            </div>
                            <div class="row">
                                <div class="col s6" style="margin-top:3px"><span class="svg-icon pin-start"></span> <span class="event-snippet">${fecha_ini.format("hh:mm:ss a")}</span></div>
                                <div class="col s6" style="margin-top:3px"><span class="svg-icon pin-end"></span> <span class="event-snippet">${fecha_fin.format("hh:mm:ss a")}</span></div>
                                <div class="col s6" style="margin-top:3px"><span class="svg-icon recorrido dyn-svg"></span> <span class="event-snippet">${fixDistAprox(locationsFetched.KmAprox)}</span></div>
                                <div class="col s6" style="margin-top:3px"><span class="svg-icon velocidad dyn-svg"></span> <span class="event-snippet">${fixVProm(locationsFetched.VPromedio)}/h</span></div>
                            </div>
                        </li>`);
                    }
                }else{
                    //noHistory.push(vehiculos[carid].Descripcion);
                    $(".carInfoList .list").append(`
                    <li class="rtItem-${Tracking._rastrearUnidades[i]} no-effect no-history">
                        <div class="flex-row">'+
                            <div class="left-icon car-icon"><span style="background-image:url('${getCarIconUri(vehiculos[carid].IconoID,true)}')"></span></div>
                            <div class="textBox">
                                <div class="listText infoCarname">${vehiculos[carid].Descripcion}</div>
                                ${(info == '' ? '' : `<div class="listSubtext infoDetails">${info}</div>`)}
                                <div class="listSubtext rtDate"><es>Sin información</es><en>No information</en></div>
                            </div>
                        </div>
                    </li>`);
                }
            }
            var options = {
                listClass : 'infoCars',
                valueNames: [ 'infoCarname','rtDate','infoDetails','event-snippet']
              };
              
            window.carInfoList = new List("carInfoList", options);
            window.carInfoList.sort("infoCarname",{order:"asc"});
            /*if(noHistory.length>0){
                var lista = "";
                for(var i =0;i<noHistory.length;i++){
                    lista+="\n•"+noHistory[i];
                }
                alerta(lang("nohistory"),lang("nohistorybody")+lista);
                if(noHistory.length == Tracking._rastrearUnidades.length){
                    $(".eventMenu").addClass("hidden");
                }
            }*/
        }
    }
    /**
     * Calcula la distancia entre posicion del Vehiculo y los Puntos
     * @param unit Identificador de Vehiculo
     */
    public CalcPointProximity(unit : string){
        var carid = objectIndex(Tracking._locationArray,"id",unit);
        if(carid > -1){
            Tracking._locationArray[carid].visitados = new Array();
            for(var j=0;j<PuntoGrupo._pointsOnMap.length;j++){
                var pid = objectIndex(listaPuntos,"PuntoID",PuntoGrupo._pointsOnMap[j]);
                for(var i =0;i<Tracking._locationArray[carid].rawLocations.length;i++){
                    var dist = getDistance(
                        Tracking._locationArray[carid].rawLocations[i].Latitud,
                        Tracking._locationArray[carid].rawLocations[i].Longitud,
                        listaPuntos[pid].x,
                        listaPuntos[pid].y
                    );
                    /*var time = (typeof Tracking._locationArray[carid].locations[i].ResumenTiempo == "undefined" ? 
                                Tracking._locationArray[carid].locations[i].Tiempo : Tracking._locationArray[carid].locations[i].ResumenTiempo) ;*/
                    var time = Tracking._locationArray[carid].rawLocations[i].Tiempo;
                    /*if(time == 0 && typeof Tracking._locationArray[carid].locations[i].ResumenTiempo != "undefined"){
                        time = Tracking._locationArray[carid].locations[i].ResumenTiempo/60000;
                    }*/
                   // var time =Tracking._locationArray[carid].locations[i].Tiempo;
                    if(Tracking._spotProximity >= dist && ( time >= Tracking._spotDeadTime || Tracking._locationArray[carid].rawLocations[i].ResumenTiempo/60000 >= Tracking._spotDeadTime ) ){
                        //console.log("CARID: "+carid+", LOC: "+i+", PID: "+pid+", DIST: "+dist+", TIME: "+Tracking._locationArray[carid].rawLocations[i].Tiempo + ", RTIME: "+(Tracking._locationArray[carid].rawLocations[i].ResumenTiempo/60000));
                    }
                    if(Tracking._spotProximity >= dist && time >= Tracking._spotDeadTime) {
                        Tracking._locationArray[carid].visitados.push({
                            'Locid':        i,
                            'Punto':        listaPuntos[pid].Nombre,  
                            'PuntoID':      listaPuntos[pid].PuntoID,      
                            'Fecha':        Tracking._locationArray[carid].rawLocations[i].Fecha,
                            'Distancia':    dist,
                            'Recorrido':    this.GetPointDistance(carid,i),
                            'Tiempo'   :    time,
                            'Lat'      :    Tracking._locationArray[carid].rawLocations[i].Latitud,
                            'Lng'      :    Tracking._locationArray[carid].rawLocations[i].Longitud
                        });
                    }
                }
            }
            if( Tracking._locationArray[carid].visitados.length == 0){
                return -1;
            }else{
                return carid;
            }
        }else{
            return -1;
        }
    }
    /**
     * Obtiene la distancia entre puntos recorridos
     * @param carid Identificador de ubicaciones de un Vehiculo en especifico
     * @param locid Identificador de ubicacion especifica
     */
    public GetPointDistance(carid : number,locid : number) {
        var suma = 0;
        for(var i =1;i<=locid;i++){
            suma+=getDistance(Tracking._locationArray[carid].rawLocations[i-1].Latitud,Tracking._locationArray[carid].rawLocations[i-1].Longitud,
            Tracking._locationArray[carid].rawLocations[i].Latitud,Tracking._locationArray[carid].rawLocations[i].Longitud);
        }
        
        return suma;
    }
    /**
     * Carga la lista de Vehiculos en Rastreo Continuo
     */
    public LoadCarInfoList(){
        
        $(".carInfoList .list").html('');
        //console.warn(`UU?: ${typeof ultimasUbicaciones}`);
        if(typeof ultimasUbicaciones != "undefined"){
            //console.warn(`Rastrear Unidades Length: ${Tracking._rastrearUnidades.length}`);
            if(Tracking._rastrearUnidades.length == 0){
                $(".carInformation > .noContent").removeClass("hidden").show();
            }else{
                $(".carInformation > .noContent").addClass("hidden");
                for(var i=0;i < Tracking._rastrearUnidades.length;i++){
                    
                    var carid = objectIndex(vehiculos,"Unidad",Tracking._rastrearUnidades[i]);
                    var iconId = objectIndex(Vehiculos._iconList,"id",vehiculos[carid].IconoID);
                    var uu = objectIndex(ultimasUbicaciones,"Unidad",Tracking._rastrearUnidades[i]);
                    var noInfo = false;
                    if(uu == -1){
                        console.warn("NO INFO UU",Tracking._rastrearUnidades[i], "Unidad",vehiculos[carid].Descripcion);
                        noInfo = true;
                    }
                    
                    if(uu >=0){
                        var fecha = new moment(ultimasUbicaciones[uu].Fecha);
                    }else{
                        var fecha = new moment();
                    }
                    
                    var info = '';
                    if('Marca' in vehiculos[carid]){
                        info+=vehiculos[carid].Marca+"&nbsp;";
                    }
                    if('Modelo' in vehiculos[carid]) {
                        info+=vehiculos[carid].Modelo;
                    }
                    if(typeof vehiculos[carid].PorcentajeBateria != "undefined" && this.isBatteryEnabled(carid)) {
                        info+=`<span class="car_battery batt_${Tracking._rastrearUnidades[i]}"><i class="fas fa-bolt"></i> <es>Batería</es><en>Battery</en>: ${vehiculos[carid].PorcentajeBateria}%</span>`;
                    }
                    var datosGeo = "";
                    if('DatosGeocerca' in ultimasUbicaciones[uu] && noInfo == false) {
                        datosGeo = ultimasUbicaciones[uu].DatosGeocerca.GeocercaNombre;
                    }
                    var ahora = moment(new Date());
                    var horas = ahora.diff(fecha,'hours');
                    var carIcon = '';
                    if(horas >=4) {
                        carIcon = 'img/eventos/98.png';
                    }else {
                        carIcon = getCarIconUri(vehiculos[carid].IconoID,(ultimasUbicaciones[uu].Velocidad>0));
                    }
                    $(".carInfoList .list").append(`<li class="rtItem-${Tracking._rastrearUnidades[i]} ${window.selectedRTitem == Tracking._rastrearUnidades[i] ? 'selected' : ''} ${noInfo ? 'no-info' :''}" onclick="${noInfo ? '' : `Track.CarEventList('${Tracking._rastrearUnidades[i]}')`}">
                        <div class="flex-row">
                            <div class="left-icon car-icon"><span style="background-image:url('${carIcon}')"></span></div>
                            <div class="textBox">
                                <div class="listText infoCarname">${vehiculos[carid].Descripcion}</div>
                                ${info == '' ? '' : `<div class="listSubtext infoDetails">${info}</div>`}
                                <span class="no-info-label">Sin Información</span>
                            </div>
                            <div class="chevron"><span class="lsf lsf-icon" icon="right"></span></div>
                        </div>
                        <div class="row">
                            <div class="col s6" style="margin-top:3px"><span class="svg-icon velocidad dyn-svg"></span> <span class="event-snippet">${distConv(ultimasUbicaciones[uu].Velocidad,true,false,true)}/h</span></div>
                            <div class="col s6" style="margin-top:3px"><span class="svg-icon calendario dyn-svg"></span> <span class="event-snippet">${fecha.format("DD / MMM / YYYY")}</span></div>
                            <div class="col s6" style="margin-top:3px"><span class="svg-icon eventos dyn-svg"></span> <span class="event-snippet event-last">${this.GetEvento(this.GetLastEvent(Tracking._rastrearUnidades[i]),datosGeo)}</span><span class="event-snippet event-counter" events="${this.GetCarEventCount(Tracking._rastrearUnidades[i])}">${this.GetCarEventCount(Tracking._rastrearUnidades[i])}&nbsp;${this.GetCarEventCount(Tracking._rastrearUnidades[i]) == 1 ? '<es>evento</es><en>event</en>' : '<es>eventos</es><en>events</en>'}</span></div>
                            <div class="col s6" style="margin-top:3px"><span class="svg-icon tiempo dyn-svg"></span> <span class="event-snippet">${fecha.format("hh:mm:ss a")}</span></div>
                        </div>
                    </li>`);
                }
                toggleLastEvent();
                var options = {
                    listClass : 'infoCars',
                    valueNames: [ 'infoCarname','rtFecha','infoDetails','event-snippet']
                  };
                  
                window.carInfoList = new List("carInfoList", options);
                window.carInfoList.sort("infoCarname",{order:"asc"});
            }
        }else{
            $(".carInformation > .noContent").removeClass("hidden").show();
        }
        setTimeout(()=>{
            masterClose = ()=>{this.CloseSidebar('carInfoList');};
        },0);
    }
    /**
     * [DEPRECATED]
     * Muestra informacion extra en la lista de unidades
     */
    public showMeMore(unidad: string) {
        if(!$(`.rtItem-${unidad} .extraStuff`).is(":visible")){
            $(`.rtItem-${unidad} .extraStuff`).slideDown();
        }else{
            $(`.rtItem-${unidad} .extraStuff`).slideUp();
        }
    }
    /**
     * Vamonos pa' arriba del Sidebar
     */
    public ScrollUp() {
        $("#sidebar").scrollTop(0);
    }
    /**
     * Abre la vista para Eventos de un Vehiculo en especifico
     * @param unidad Identificador de Vehiculo
     */
    public CarHistoryEventList(unidad : string) {
        try{
            window.carEventsCluster.destroy();
        }catch(e){}
        window.carEventsCluster = null;
        window.carEventsCluster = undefined;

        Tracking._focusedCar = unidad;
        window.eventTimes = new Array();
        $("#sidebar").scrollTop(0);
        $(".carEventList .search").val('');
        
        if(Tracking._isDeadTime  == false && Tracking._isEvents == false && Tracking._isSpeedLimit == false && Tracking._isIdleTime == false){
            this.GoToCar(unidad);
        }
        $(".carEventList .list").html('');
        var carid = objectIndex(vehiculos,"Unidad",unidad);
        var locid = objectIndex(Tracking._locationArray,"id",unidad);
        if(Tracking._isDeadTime == true){
            var title = "<es>Tiempo Muerto de&nbsp;</es><en>Dead Time of&nbsp;</en>";
        }else if(Tracking._isEvents == true){
            var title = "<es>Eventos de&nbsp;</es><en>Events of&nbsp;</en>";
        }else if(Tracking._isSpeedLimit == true){
            var title = "<es>Excesos de Velocidad de&nbsp;</es><en>Speed Limit of&nbsp;</en>";
        }else if(Tracking._isIdleTime == true){
            var title = "<es>Ralentí de&nbsp;</es><en>Idle Time of&nbsp;</en>";
        }else{
            var title = "<es>Histórico de&nbsp;</es><en>History of&nbsp;</en>";
        }
        $("#main .mainTop .title").html(title+vehiculos[carid].Descripcion);
        $(".carEventList .noContent").addClass("hidden");

        for(var i =0;i<Tracking._locationArray[locid].locations.length;i++){
            var fecha = new moment(Tracking._locationArray[locid].locations[i].Fecha);
            if(objectIndex(eventTimes,"hora",fecha.hours()) == -1 ){
                eventTimes.push({
                    hora:       fecha.hours(),
                    timestamp:  fecha.unix()
                });
            }
            var evento = ('Evento' in Tracking._locationArray[locid].locations[i] === false? -1 : Tracking._locationArray[locid].locations[i].Evento);
            var idle = ('ResumenTiempo' in Tracking._locationArray[locid].locations[i] === false ? -1 : Math.floor(Tracking._locationArray[locid].locations[i].ResumenTiempo/1000/60));
            if(idle == -1 && (Tracking._isDeadTime == true || Tracking._isIdleTime == true)){
                idle = ('Tiempo' in Tracking._locationArray[locid].locations[i] === false ? -1 : Math.floor(Tracking._locationArray[locid].locations[i].Tiempo));
            }
            if(idle >=60){
                if(Tracking._isDeadTime == true || Tracking._isIdleTime == true){
                    idle = timeConverter(Tracking._locationArray[locid].locations[i].Tiempo*60000);
                }else{
                    idle = timeConverter(Tracking._locationArray[locid].locations[i].ResumenTiempo);
                }
                if(evento == 70){
                    idle = '+&nbsp;' + idle +'&nbsp;<es><!--de Ralentí--></es><en><!--stopped--></en>';
                }
            }else{
                if(idle >0){
                    if(Tracking._isDeadTime == true || Tracking._isIdleTime == true){
                        idle = timeConverter(Tracking._locationArray[locid].locations[i].Tiempo*60000);
                    }else{
                        idle+='&nbsp;mins';
                        if(evento == 70){
                            idle = '+&nbsp;' + idle +'&nbsp;<es><!--de Ralentí--></es><en><!--stopped--></en>';
                        }
                    }
                }
            }
            var validEvent = true;
            if($("#filter-onlyrelevant").is(":checked") == true && evento == -1){validEvent = false;}
            if($("#filter-abovezero").is(":checked") == true && Tracking._locationArray[locid].locations[i].Velocidad == 0){validEvent=false;}
            if(( $("#event-times").val() == undefined ? true : fecha.unix() >= parseInt($("#event-times").val()) ) && validEvent == true) {
                $(".carEventList .list").append(`<li class="carEvent-${locid}${i}" onclick="Track.PrintLocationData('${locid}','${i}');Track.ShowEvent('${evento}','${unidad}','${Tracking._locationArray[locid].locations[i].Latitud}','${Tracking._locationArray[locid].locations[i].Longitud}','${Tracking._locationArray[locid].locations[i].Velocidad}')">
                    <div class="left-icon"><span class="car-event" style="background-image:url(img/eventos/${(evento == -1 ? "posicion" : evento)}.png)"></span></div>
                    
                    <div class="textBox">
                        <div class="listText rtDetail infoCarname">${this.GetEvento(evento)}</div>
                        <div class="listSubtext rtDate rtFecha">${fecha.format("DD / MMMM / YYYY")}</div>
                        <div class="row">
                            <div class="col s6" style="margin-top:3px"><span class="svg-icon pin-start"></span> <span class="event-snippet">${fecha.format("hh:mm:ss a")}</span></div>
                            
                            <div class="col s6" style="margin-top:3px"><span class="svg-icon velocidad dyn-svg"></span> <span class="event-snippet">${distConv(Tracking._locationArray[locid].locations[i].Velocidad,true,true)}/h</span></div>
                            
                            ${('aPuntoDist' in Tracking._locationArray[locid].locations[i] === false ? '' : `<div class="col s6" style="margin-top:3px"><span class="svg-icon velocidad dyn-svg"></span> <span class="event-snippet">${Tracking._locationArray[locid].locations[i].aPuntoLabel.replace("__DIST__",parseDist(Tracking._locationArray[locid].locations[i].aPuntoDist))}/h</span></div>`)}
                            
                            ${(idle == -1 ? '' : `<div class="col s6" style="margin-top:3px"><span class="svg-icon pin-end"></span> <span class="event-snippet">${idle}</span></div>`)}

                            ${'porcentajeBateria' in Tracking._locationArray[locid].locations[i]  && this.isBatteryEnabled(carid) ? `<div class="col s6 event-battery" style="margin-top:3px"><i class="fas fa-bolt"></i> <span class="event-snippet"><es>Batería</es><en>Battery</en>: ${Tracking._locationArray[locid].locations[i].porcentajeBateria}%</span></div>` : ''}
                            
                        </div>
                    </div>
                </li>`);
            }
        }
        if(Tracking._locationArray[locid].locations.length >= 10) {
            $("#carEventList .search-input").removeClass("hidden");
            $(".mainTop .tool.right i").removeClass("zicon_historial").addClass("fas fa-arrow-alt-circle-up");
            $(".mainTop .tool.right").attr("onclick","Track.ScrollUp()");
        }else{
            $("#carEventList .search-input").addClass("hidden");
        }
        if(Tracking._isDeadTime == false && Tracking._isEvents == false && Tracking._isSpeedLimit == false && Tracking._isIdleTime == false){
            map.setViewBounds(Tracking._locationArray[locid].line.getBounds(),false);
        }
        $(".carInfoList").hide();
        $(".carEventList").show();
        var options = {
            listClass : 'carEvents',
            valueNames: [ 'rtCarname','rtDate','rtSpeed','rtDetail','rtPoint','event-snippet']
          };
       
        window.carEventsList = new List("carEventList", options);
        
        if (window.carEventsCluster === undefined) {
            //CLUSTERIZE
            window.carEventsCluster = new Clusterize({
                scrollId: 'sidebar',
                contentId: 'carEvents',
                show_no_data_row: false,
                rows_in_block: 20
            });
        }
    }
    /*public CarVisitedPoints(unidad : string) {
        $(".carEventList .search").val('');
        $(".carEventList .list").html('');
        var carid = objectIndex(vehiculos,"Unidad",unidad);
        var locid = objectIndex(Tracking._locationArray,"id",unidad);
        var title = "<es>Puntos Visitados</es><en>Visited Checkpoints</en>";
        
        $("#main .mainTop .title").html(title);
        $(".carEventList .noContent").addClass("hidden");

        for(var i =0;i<Tracking._locationArray[locid].visitados.length;i++){
            var fecha = new moment(Tracking._locationArray[locid].visitados[i].Fecha);
            var tiempo =timeConverter(Tracking._locationArray[locid].visitados[i].Tiempo*60000);
            $(".carEventList .list").append('<li onclick="Track.PrintLocationData('+locid+','+i+');MapEngine.FocusOnCoords('+Tracking._locationArray[locid].visitados[i].Lat+','+Tracking._locationArray[locid].visitados[i].Lng+',18)">'+
                '<div class="left-icon"><span class="car-event" style="background-image:url(img/eventos/EventosDriving-pos.png)"></span></div>'+
                '<div class="textBox">'+
                    '<div class="listText rtCarname">'+Tracking._locationArray[locid].visitados[i].Punto+'</div>'+
                    '<div class="listSubtext rtDate">'+fecha.format("Y/MMM/DD [<es>a las</es><en>at</en>] HH:mm:ss")+'</div>'+
                    '<div class="listSubtext rtSpeed"><es>Recorrido</es><en>Traveled</en>: '+(Tracking._locationArray[locid].visitados[i].Recorrido == 0 ? '<es>Inicial</es><en>Start</en>' : distConv(Tracking._locationArray[locid].visitados[i].Recorrido,true,false,false,true))+'</div>'+
                    '<div class="listSubtext rtDetail"><es>Proximidad</es><en>Proximity</en>: '+distConv(Tracking._locationArray[locid].visitados[i].Distancia,true,false,false,true)+'</div>'+
                    '<div class="listSubtext rtDetail"><es>Tiempo</es><en>Time</en>: '+tiempo+'</div>'+
            '</li>');
        }
        $(".carInfoList").hide();
        $(".carEventList").show();
        var options = {
            listClass : 'carEvents',
            valueNames: [ 'rtCarname','rtDate','rtSpeed','rtDetail','rtPoint']
          };
          
        window.carEventsList = new List("carEventList", options);
    }*/
    /**
     * Calcula la distancia entre Puntos
     * @param locid Identificador de ubicaciones de un Vehiculo en especifico
     * @param id Identificador de Ubicacion especifica
     */
    public CalcularDistanciaPuntos(locid : number, id : number) {
        var pastDist = 0;
    
        for(var j=0;j<listaPuntos.length;j++){
            var dist = getDistance(Tracking._locationArray[locid].locations[id].Latitud,
                Tracking._locationArray[locid].locations[id].Longitud,
                listaPuntos[j].x,
                listaPuntos[j].y);
            if(j == 0){
                pastDist = dist;
            }
            if(dist <= pastDist){
                pastDist = dist;
                if(dist <= 40) {
                    Tracking._locationArray[locid].locations[id].aPuntoLabel = '<es>Está en</es><en>At</en> <b>'+listaPuntos[j].Nombre+'</b>';
                    Tracking._locationArray[locid].locations[id].aPuntoDist = 0;
                }else if(dist > 40 && dist < 50000) {
                    Tracking._locationArray[locid].locations[id].aPuntoLabel = '<es>A</es><en>At</en> __DIST__ <es>de distancia de</es><en>of</en> <b>'+listaPuntos[j].Nombre+'</b>';
                    Tracking._locationArray[locid].locations[id].aPuntoDist = dist;
                }else{
                    Tracking._locationArray[locid].locations[id].aPuntoLabel = '';
                    Tracking._locationArray[locid].locations[id].aPuntoDist = 0;
                }
            }
        }
    }
    /**
     * Imprime los datos de la ubicacion y lo remarca en la lista
     * @param locid Identificador de ubicaciones de un Vehiculo en especifico
     * @param locpoint Identificador de una Ubicacion en especifico dentro de Ubicaciones
     */
    public PrintLocationData(locid : number, locpoint : number) {
        //console.log(JSON.stringify(Tracking._locationArray[locid].locations[locpoint]));
        $(".carEventList .list li").removeClass("activo");
        $(".carEventList .list li.carEvent-"+locid+locpoint).addClass("activo");
    }
    /**
     * getUnitFuelData
     */
    public getUnitFuelData(unidad_id:string) {
        if(SesionEngine.Permisos("Fuel") == true){
            ajax("POST",cmovilUrl+"UltimaUbicacionFuel",{
                licencia:   licencia,
                token:      -1,
                cliente:    userData.clienteID,
                unidad:     unidad_id
            },
            (xml)=>{
                var json = $.xml2json(xml);
                
                if(parseInt(json.CantidadTanques) >0){
                    var data= json.ListaTanques.Tanque;
                    $(".fuelibHeader").removeClass("hidden");
                    $(`.${unidad_id}-bubble_fuel`).html('');
                    if('length' in json.ListaTanques.Tanque == false){
                        data = [];
                        data.push(json.ListaTanques.Tanque);
                    }
                    for(var ft=0;ft<data.length;ft++){
                        $(`.${unidad_id}-bubble_fuel`).append(`
                            <div class="bubble-fuel-tank">
                                <span class="bubble-fuel-tank-id">T${data[ft].TanqueID}</span>
                                <span class="bubble-fuel-tank-lts">${data[ft].VolumenLTS} L - ${data[ft].VolumenPER}%</span>
                            </div>
                        `);
                    }
                }
                
            },
            (err)=>{
                console.error(err);
            },10000,"xml");
        }else{
            $(".fuelibHeader").addClass("hidden");
        }
    }
    /**
     * Muestra la lista de Eventos de Vehiculo
     * @param unidad Identificador de Vehiculo
     */
    public CarEventList(unidad : string) {
        try{
            window.carEventsCluster.destroy();
        }catch(e){}
        window.carEventsCluster = null;
        window.carEventsCluster = undefined;

        Tracking._watchingUnit = unidad;
        this.GoToCar(unidad);
        $(".carEventList .list").html('');
        $(".carEventList .search").val('');
        
        $(".mainTop .tool.right").attr("onclick","Track.ExpandMap()").show();
        $(".mainTop .tool.right i").removeClass("zicon_rastreo-continuo zicon_ultima-posicion zicon_historial").addClass("zicon_maximizar");
        
        if(this.GetCarEventCount(unidad) == 0 ){
            $("#sidebar .search-input").addClass("hidden");
            $(".carEventList .noContent").removeClass("hidden");
            $("#sidebar").addClass("emptylist");
            $("#sidebar").append('<div class="emptycar"></div>');
        }else{
           
            $("#sidebar").removeClass("emptylist");
            $("#sidebar .emptycar").remove();
            $(".carEventList .noContent").addClass("hidden");
            var eventos = 0;
            for(var i =0;i<Tracking._eventArray.length;i++){
                if(Tracking._eventArray[i].unidad == unidad){
                    eventos++;
                    var fecha = new moment(Tracking._eventArray[i].fecha);
                   
                    if(  Tracking._eventArray[i].tipo == "geo"){
                        //1 = Salida, 0 = Entrada
                        //var geoTipo = (Tracking._eventArray[i].data.Accion == 1 ? ' (<es>Salida</es><en>Departure</en>)' : ' (<es>Entrada</es><en>Arrival</en>)' );
                        var geoTipo = (Tracking._eventArray[i].data.Accion == 1 ? 46 : 45 );
                        var evento = this.GetEvento(geoTipo)+`: ${Tracking._eventArray[i].data.GeocercaNombre}`;
                        var evIcon = geoTipo+".png";
                        var eventoID = geoTipo;
                    }else{
                        var evento = this.GetEvento(Tracking._eventArray[i].tipo);
                        var evIcon = Tracking._eventArray[i].tipo+".png";
                        var eventoID = Tracking._eventArray[i].tipo;
                    }
                    var validEvent = true;
                    /*if($("#filter-onlyrelevant").is(":checked") == true && evento == -1){validEvent = false;}
                    if($("#filter-abovezero").is(":checked") == true && Tracking._locationArray[locid].locations[i].Velocidad == 0){validEvent=false;}*/
                    if(( $("#event-times").val() == undefined ? true : fecha.unix() >= parseInt($("#event-times").val()) ) && validEvent == true) {
                        $(".carEventList .list").append(`<li class="${i}" onclick="Track.PrintEventData(${i});Track.ShowEvent('${eventoID}','${unidad}',${Tracking._eventArray[i].data.Latitud},${Tracking._eventArray[i].data.Longitud},${Tracking._eventArray[i].data.Velocidad})">
                            <div class="left-icon"><span class="car-event" style="background-image:url(img/eventos/${evIcon})"></span></div>
                            <div class="textBox">
                                <div class="listText evName no-flex">${evento}</div>
                                <div class="listSubtext rtDate">${fecha.format("Y/MMM/DD [<es>a las</es><en>at</en>] HH:mm:ss")}</div>
                            </div>
                        </li>`);
                    }
                }
            }
            if(eventos >= 10){ //Si hay 10 o mas eventos en lista se muestra el filtro buscador
                $("#carEventList .search-input .filter-btn").addClass("hide");
                $("#carEventList .search-input .search-field").removeClass("s10").addClass("s12");
                $("#carEventList .search-input").removeClass("hidden");
                //$(".mainTop .tool.right i").removeClass("zicon_rastreo-continuo zicon_ultima-posicion").addClass("fas fa-arrow-alt-circle-up");
                //$(".mainTop .tool.right").attr("onclick","Track.ScrollUp()").show();
            }else{
                $("#carEventList .search-input").addClass("hidden");
            }
            var options = {
                listClass : 'carEvents',
                valueNames: [ 'evName','rtDate']
              };
              
            window.carEventsList = new List("carEventList", options);

            //CLUSTERIZE
            if (window.carEventsCluster === undefined) {
                //CLUSTERIZE
                window.carEventsCluster = new Clusterize({
                    scrollId: 'sidebar',
                    contentId: 'carEvents',
                    show_no_data_row: false,
                    rows_in_block: 20
                });
            }
        }
        $(".carInfoList").hide();
        $(".carEventList").show();
    }
    /**
     * [DEBUG]
     * Imprime los datos del evento
     * @param ev Identificador de Evento obtenido por RC
     */
    public PrintEventData() {
        //console.log(Tracking._eventArray[ev]);
    }
    /**
     * Muestra el Evento ocurrido en el mapa
     * @param tipo ID del Evento
     * @param unidad Identificador del Vehiculo
     * @param _lat Latitud donde ocurrio el Evento
     * @param _lng Longitud donde ocurrio el Evento
     * @param vel Velocidad reportada en el Evento
     */
    public ShowEvent(tipo : any, unidad : string, _lat : number, _lng : number,vel : number = 0){
        Tracking._eventGroup.removeAll();
        this.CloseBubbles();
        if(Tracking._isDeadTime == false && 
            Tracking._isEvents == false && 
            Tracking._isSpeedLimit == false && 
            Tracking._isIdleTime == false){
            var carid = objectIndex(Tracking._markerInstance,"id",unidad);
        }
        MapEngine.FocusOnCoords(_lat ,_lng,16 );
        var car = objectIndex(vehiculos,"Unidad",unidad);
        var icon = objectIndex(Vehiculos._iconList,"id",vehiculos[car].IconoID);

        if(Tracking._isHistory == true || 
            Tracking._isHistoryPoints == true){
            if(tipo !=  -1){
                //Tracking._markerInstance[carid].marker.setIcon(  new H.map.DomIcon("img/eventos/"+tipo +".png",{size: {w:40*DPR,h:40*DPR} }) );
                var iconElem = this.CreateDomIcon("img/eventos/"+tipo +".png");
                Tracking._markerInstance[carid].marker.setIcon(  new H.map.DomIcon(iconElem));
            }else{
                if(vel > 0){
                    var iconElem = this.CreateDomIcon(getCarIconUri(vehiculos[car].IconoID,true));
                    Tracking._markerInstance[carid].marker.setIcon(  new H.map.DomIcon(iconElem));
                }else{
                    var iconElem = this.CreateDomIcon(getCarIconUri(vehiculos[car].IconoID,false));
                    Tracking._markerInstance[carid].marker.setIcon(  new H.map.DomIcon(iconElem));
                }
            }
            Tracking._markerInstance[carid].marker.setPosition({lat: _lat , lng:_lng });
            Tracking._markerInstance[carid].label.setPosition({lat: _lat , lng:_lng });
        }else{
            if(tipo != -1 ){
                if(tipo !=  "geo"){
                    var iconElem = this.CreateDomIcon("img/eventos/"+tipo +".png");
                    Tracking._eventMarker =   new H.map.DomMarker(
                        {lat:_lat , lng:_lng },
                        {
                            icon:   new H.map.DomIcon(iconElem),
                            zIndex:1
                        }
                    );
                    Tracking._eventMarker.setData(
                        `<div class="inputGroup">
                            <h6><es>Información</es><en>Information</en></h6>
                            <div class="markerInfo">
                                <div><span class="ibHeader"><es>Vehículo</es><en>Vehicle</en>:</span><span>${vehiculos[car].Descripcion}</span></div>
                                <div><span class="ibHeader"><es>Chofer</es><en>Driver</en>:</span><span>${na(vehiculos[car].Chofer)}</span></div>
                                <div><span class="ibHeader"><es>Evento</es><en>Event</en>:</span><span>${Track.GetEvento(parseInt(tipo))}</span></div>
                                <div><span class="ibHeader"><es>Dirección</es><en>Address</en>:</span><span class="${unidad}-bubble_address"><es>Buscando...</es><en>Locating...</en></span></div>
                                <div><span class="ibHeader"><es>Velocidad</es><en>Speed</en>:</span><span class="${unidad}-bubble_speed">${distConv(vel,true,true,true)}/h</span></div>
                                <div><span class="ibHeader"><es>Cerca de</es><en>Places Near</en>:</span><span class="${unidad}-bubble_near"><es>Explorando...</es><en>Exploring...</en></span></div>
                                ${typeof vehiculos[car].PorcentajeBateria != "undefined"  && this.isBatteryEnabled(car) ? `<div><span class="ibHeader"><es>Batería</es><en>Battery</en>:</span><span class="${unidad}-bubble_battery">${vehiculos[car].PorcentajeBateria}%</span></div>` : ''}
                                <div><span class="ibHeader"><es>Mapa</es><en>Map</en>:</span><span onclick="openMaps('${_lat}','${_lng}')" class="${unidad}-bubble_coords"><es>Abrir Mapa Externo</es><en>Open External Map</en> <i class="fas fa-external-link-alt"></i></span></div>
                            </div>
                        </div>`
                    );
                    Tracking._eventMarker.unidad = unidad;
                    Tracking._eventMarker.noLocation = true;
                    Tracking._eventMarker._i = "";
                    Tracking._eventMarker._j = "";
                    Tracking._eventGroup.addObject(Tracking._eventMarker);
                    try{
                        Tracking._eventGroup.removeEventListener('tap',Track.CreatePointBubble,false);
                    }catch(e){}
                    if(isTEApp == false){
                        Tracking._eventGroup.addEventListener('tap',Track.CreatePointBubble,false);
                    }
                }else{

                    var iconElem = this.CreateDomIcon(getCarIconUri(vehiculos[car].IconoID,(vel>0)));
                    Tracking._eventMarker =   new H.map.DomMarker(
                        {lat:_lat , lng:_lng },
                        {
                            icon: new H.map.DomIcon(iconElem)
                        }
                    );
                }
                
            }
        }
    }
    /**
     * Regresa el conteo de Eventos de una Unidad
     * @param unidad Identificador de Vehiculo
     */
    public GetCarEventCount(unidad : string) {
        var count = 0;
        for(var i =0;i<Tracking._eventArray.length;i++){
            if(Tracking._eventArray[i].unidad == unidad){
                count++;
            }
        } 
        return count;
    }
    /**
     * GetLastEvent
     */
    public GetLastEvent(unidad: string) {
        var evento = 0;
        for(var i =Tracking._eventArray.length-1;i>=0;i--){
            if(Tracking._eventArray[i].unidad == unidad){
                evento = Tracking._eventArray[i].tipo;
            }
        } 
        return evento;
    }
    /**
     * Regresa la cadena del Evento
     * @param numEvento Id del Evento
     * @param aux Cadena extra o auxiliar
     */
    public GetEvento(numEvento : any, aux : string = "") {
        switch (numEvento) {
            case "geo": return window.idioma == "en" ? "Geofence: "+aux : "Geocerca: "+aux;
            case -1: return window.idioma == "en" ? "Position" : "Posición";
            case 0: return window.idioma == "en" ? "" : "";
            case 1: return window.idioma == "en" ? "" : "";
            case 2: return window.idioma == "en" ? "Panic Alert" : "Botón de Pánico";
            case 3: return window.idioma == "en" ? "Ignition ON" : "Motor Encendido";
            case 4: return window.idioma == "en" ? "Ignition OFF" : "Motor Apagado";
            case 5: return window.idioma == "en" ? "Opened Door" : "Puerta Abierta";
            case 6: return window.idioma == "en" ? "Closed Door" : "Puerta Cerrada";
            case 7: return window.idioma == "en" ? "Opened Valve" : "Válvula Abierta";
            case 8: return window.idioma == "en" ? "Closed Valve" : "Válvula Cerrada";
            case 9: return window.idioma == "en" ? "Over Speed Limit" : "Exceso de Velocidad";
            case 10: return window.idioma == "en" ? "Low Battery (External)" : "Batería Baja (Externa)";
            case 11: return window.idioma == "en" ? "Requires Communication" : "Requiere Comunicación";
            case 12: return window.idioma == "en" ? "Battery Disconnected" : "Batería Desconectada";
            case 13: return window.idioma == "en" ? "Battery Reconnected" : "Batería Reconectada";
            case 14: return window.idioma == "en" ? "Car Tow Alert" : "Arrastre de Vehículo";
            case 15: return window.idioma == "en" ? "Idle" : "Ralentí";
            case 16: return window.idioma == "en" ? "Tractor Cage Donnected" : "Conexión de Caja";
            case 17: return window.idioma == "en" ? "Tractor Cage Disconnected" : "Desconexión de Caja";
            //case 18: return window.idioma == "en" ? "Unload" : "Descarga";
            case 19: return window.idioma == "en" ? "Open Hood" : "Cofre Abierto";
            case 20: return window.idioma == "en" ? "Closed Hood" : "Cofre Cerrado";
            case 21: return window.idioma == "en" ? "Delivered" : "Entregado";
            case 22: return window.idioma == "en" ? "Not Delivered" : "No Entregado";
            case 23: return window.idioma == "en" ? "Low Battery (Internal)" : "Batería Baja (Interna)";
            case 30: return window.idioma == "en" ? "Energy Saving" : "Ahorro de energía";
            case 33: return window.idioma == "en" ? "Jamming" : "Deteccion de Interferencia";
            case 34: return window.idioma == "en" ? "Tamper ON" : "Tamper ON";
            case 35: return window.idioma == "en" ? "Tamper OFF" : "Tamper OFF";
            case 36: return window.idioma == "en" ? "Mixing" : "Mezclando";
            case 37: return window.idioma == "en" ? "Unload" : "Descarga";
            case 43: return window.idioma == "en" ? "Side Door Opened" : "Puerta Lateral Abierta";
            case 44: return window.idioma == "en" ? "Side Door Closed" : "Puerta Lateral Cerrada";
            case 45: return window.idioma == "en" ? "Arrival" : "Entrada";
            case 46: return window.idioma == "en" ? "Departure" : "Salida";
            case 47: return window.idioma == "en" ? "Internal Battery Connected" : "Conexión Batería Interna";
            case 48: return window.idioma == "en" ? "Internal Battery Disconnected" : "Desconexión Batería Interna";
            case 50: return window.idioma == "en" ? "Temperature Normal" : "Temperatura Normal";
            case 51: return window.idioma == "en" ? "Temperature Low" : "Temperatura Baja";
            case 52: return window.idioma == "en" ? "Temperature High" : "Temperatura Alta";
            case 53: return window.idioma == "en" ? "Pressure Normal" : "Presión Normal";
            case 54: return window.idioma == "en" ? "Presión Low" : "Presión Baja";
            case 55: return window.idioma == "en" ? "Presión High" : "Presión Alta";
            case 56: return window.idioma == "en" ? "Level Normal" : "Nivel Normal";
            case 57: return window.idioma == "en" ? "Level Low" : "Nivel Bajo";
            case 70: return window.idioma == "en" ? "Idle Begin" : "Inicio de Ralentí";
            case 71: return window.idioma == "en" ? "Idle End" : "Fin de Ralentí";
            case 99: return window.idioma == "en" ? "Arrival" : "Entrada";
            case 100: return window.idioma == "en" ? "Departure" : "Salida";
            case 101: return window.idioma == "en" ? "Thermo ON" : "Termo Encendido";
            case 102: return window.idioma == "en" ? "Thermo OFF" : "Termo Apagado";
            case 103: return window.idioma == "en" ? "Rear Door Opened" : "Puerta Principal Abierta";
            case 104: return window.idioma == "en" ? "Rear Door Closed" : "Puerta Principal Cerrada";
            case 105: return window.idioma == "en" ? "Fuel Load" : "Carga Combustible";
            case 106: return window.idioma == "en" ? "Fuel Discharge" : "Descarga Combustible";
            case 107: return window.idioma == "en" ? "Fuel Transfer" : "Trasvase";
            case 110: return window.idioma == "en" ? "Sensor 2 Connected" : "Conexión Sensor 2";
            case 111: return window.idioma == "en" ? "Sensor 2 Disconnected" : "Desconexión Sensor 2";
            case 112: return window.idioma == "en" ? "Fatigue" : "Fatiga";
            case 113: return window.idioma == "en" ? "Distraction" : "Distracción";
            case 114: return window.idioma == "en" ? "Badly Positioned Camera" : "Cámara Mal Posicionada";
            case 115: return window.idioma == "en" ? "Sleeping Driver" : "Conductor Dormido";
            case 120: return window.idioma == "en" ? "Service" : "Servicio";		/* Added by Armando */
            case 130: return window.idioma == "en" ? "Speeding Start" : "Inicio de Exceso de Velocidad";
            case 131: return window.idioma == "en" ? "Speeding End" : "Fin de Exceso de Velocidad";
            case 133: return window.idioma == "en" ? "Intense Acceleration" : "Aceleración Intensa";
            case 134: return window.idioma == "en" ? "Intense Braking" : "Frenado Intenso";
            case 136: return window.idioma == "en" ? "Sharp Turn" : "Giro Brusco";
            case 137: return window.idioma == "en" ? "Dangerous Curve" : "Curva con Velocidad Alta";
            case 139: return window.idioma == "en" ? "Impact" : "Impacto";
            case 140: return window.idioma == "en" ? "Collision" : "Colisión";
            case 142: return window.idioma == "en" ? "Excess continuous driving time" : "Exceso de tiempo continuo manejando";
            case 143: return window.idioma == "en" ? "Driving after hours: Start" : "Inicio de manejo fuera de horario";
            case 144: return window.idioma == "en" ? "Driving after hours: End" : "Fin de manejo fuera de horario";
            case 159: return window.idioma == "en" ? "DTC Removed" : "DTC Eliminado";
            case 160: return window.idioma == "en" ? "DTC Detected" : "DTC Detectado";
            case 161: return window.idioma == "en" ? "Out of range (RPM)" : "Fuera de rango (RPM)";
            case 162: return window.idioma == "en" ? "Out of range (Odometer)" : "Fuera de rango (Odómetro)";
            case 163: return window.idioma == "en" ? "Out of range (Fuel - upper limit)" : "Fuera de rango (Combustible - limite superior)";
            case 164: return window.idioma == "en" ? "Out of range (Fuel - lower limit)" : "Fuera de rango (Combustible - limite inferior)";
            case 165: return window.idioma == "en" ? "Out of range (Throttle position)" : "Fuera de rango (Posición del acelerador)";
            case 166: return window.idioma == "en" ? "Out of range (Antifreeze temperature)" : "Fuera de rango (Temperatura del anticongelante)";
            case 167: return window.idioma == "en" ? "Out of range (Horometer)" : "Fuera de rango (Horómetro)";
            case 254: return window.idioma == "en" ? "Accumulated Still Time" : "Tiempo Inmovil Acumulado";
            default: return window.idioma == "en" ? "Undefined Event: " + numEvento : "Evento Indefinido: " + numEvento;
        }
    }
    /**
     * isBatteryEnabled
     */
    public isBatteryEnabled(carId) {
        try{
            return (vehiculos[carId].TipoPlaca.indexOf('.caja') != -1);
        }catch(e){
            return false;
        }
    }
}