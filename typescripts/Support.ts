class SupportClass {
    public supportToken: string;
    public package: any = {};
    public started: boolean = false;
    public uploadInterval: any;

    /**
     * InitRemoteConsole
     */
    public InitRemoteConsole() {
        var td = getKey("zeekRemoteDate-"+userData.username);
        this.supportToken = genSupportToken(10);
        console.warn(`==TOKEN DE SOPORTE: ${this.supportToken} ==`);
        this.started = true;
        $(".support-token").html(this.supportToken);
        $(".support-box").removeClass("hidden");
        this.generatePackage();
        this.uploadInterval = setInterval(()=>{
            this.uploadPackage(true);
        }, 60000 ); //CADA 5 MINUTOS SUBIMOS UN PAQUETE
    }

    /**
     * uploadPackage
     */
    public uploadPackage(notSound:boolean = false) {
        if(this.started == true){
            this.generatePackage();
            if(this.package.connection != "WEB"){
                ajax("POST","https://zeek.imeev.com/api/saveConsole",
                {
                    token:      this.supportToken,
                    usermail:   userData.UsuarioMail,
                    package:    this.package,
                    v:          2
                },
                (data)=>{
                    if(notSound == false){
                        playSound('correct');
                    }
                },
                (err)=>{

                });
            }else{
                if(notSound == false){
                    playSound('correct');
                }
            }
        }
    }

    /**
     * generatePackage
     */
    public generatePackage() {
        this.package = {
            app:        (isTEApp ? "TEA" : "GPS"),
            packDate:   moment().unix(),
            token:      this.supportToken,
            userData:   JSON.stringify(userData),
            console:    {
                log:    JSON.stringify(window.remoteLog),
                warn:   JSON.stringify(window.remoteWarn),
                error:  JSON.stringify(window.remoteError)
            },
            connection: checkConnection(),
            aux: {
                build:  buildDate,
                doze:   (parseInt(getKey("ZeekDoze")) == 1 ? "Off" : "On"),
                push:   {
                    isHMS:      isHuawei,
                    activado:   pushActivated,
                    registrado: pushRegistered,
                    regID:      getKey('regId'),
                    channels:   listChannels()
                },
                position: 'NONE'
            }
        };
        //console.warn("==New support package generated.==");
    }
}