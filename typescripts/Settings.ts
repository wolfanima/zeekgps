/// <reference path ="jquery.d.ts"/> 
/**
 * SETTINGS
 * Clase de vista de configuracion de la app
 */
class Settings {
    public _fullApp: boolean = true;
    public _autoRCList = [];
    public _autoRCCount = 0;
    public dozeCheckInterval:any;

    static _distFieldsES = [
        ["Km",  "Kilómetros"],
        ["Mi",  "Millas"]
    ];
    static _distFieldsEN = [
        ["Km",  "Kilometers"],
        ["Mi",  "Miles"]
    ];
    static _fluidFieldsES = [
        ["Lt",  "Litros"],
        ["Gal", "Galones"]
    ];
    static _fluidFieldsEN = [
        ["Lt",  "Liters"],
        ["Gal", "Gallons"]
    ];
    static _tempFieldsES = [
        ["c",   "Centígrados"],
        ["f",   "Fahrenheit"]
    ];
    static _tempFieldsEN = [
        ["c",   "Celsius"],
        ["f",   "Fahrenheit"]
    ];

    static _featureDeclared: boolean = false;

    constructor(){
        this.UpdateMetrics();
        this.BuildSelects();
    }
    /**
     * Construye los inputs selects llenandolos de info acorde
     * al idioma de la app
     */
    public BuildSelects(){
        $("#distSelect").html('');
        $("#volSelect").html('');
        $("#tempSelect").html('');
        if(window.idioma == "es") {
            var distFields = Settings._distFieldsES;
            var fluidFields = Settings._fluidFieldsES;
            var tempFields = Settings._tempFieldsES;
        }else{
            var distFields = Settings._distFieldsEN;
            var fluidFields = Settings._fluidFieldsEN;
            var tempFields = Settings._tempFieldsEN;
        }
        for(var i = 0;i<distFields.length;i++){
            var sel = "";
            if(window.distMetric == distFields[i][0]) {
                sel = "selected='selected'";
            }
            $("#distSelect").append('<option value="'+distFields[i][0]+'" '+sel+'>'+distFields[i][1]+'</option>');
        }
        for(var i = 0;i<fluidFields.length;i++){
            var sel = "";
            if(window.fluidMetric == fluidFields[i][0]) {
                sel = "selected='selected'";
            }
            $("#volSelect").append('<option value="'+fluidFields[i][0]+'" '+sel+'>'+fluidFields[i][1]+'</option>');
        }
        for(var i = 0;i<tempFields.length;i++){
            var sel = "";
            if(window.tempMetric == tempFields[i][0]) {
                sel = "selected='selected'";
            }
            $("#tempSelect").append('<option value="'+tempFields[i][0]+'" '+sel+'>'+tempFields[i][1]+'</option>');
        }
    }
    /**
     * Abre vista de Configuracion
     */
    public OpenSettings(){
        selectMenu(".settings-menu");
        try{
            masterClose = ()=>{this.CloseSettings();};
        }catch(e){}

        this.BuildSelects();
        this.UpdateMetrics();
        $("#settings").removeClass("hidden");
        $(".subVista").scrollTop(0);
        if(isTEApp){ //APP de TE?
            $(".langSelect,.eventBanner,.autoRCOption,.measureGroup").addClass("hidden");
        }
        hideActionCircle();
    }
    /**
     * Cierra vista de Configuracion
     */
    public CloseSettings() {
        if($("#autoRCSelector").is(":visible")) {
            this.CloseAutoRCSelector();
        }else{
            resetBack();
            $("#settings").addClass("hidden");
        }
    }
    /**
     * Abre vista de Ayuda
     */
    public OpenHelp(){
        selectMenu(".help-menu");
        hideActionCircle();
        try{
            masterClose = ()=>{this.CloseHelp();};
        }catch(e){}
        if(SesionEngine.isLogged() == false){
            $("#loginPage").addClass("hidden");
        }
        $("#ayuda").removeClass("hidden");
        views();
    }
    /**
     * Cierra Ayuda/FAQ/Reporte
     */
    public CloseHelp() {
        if($(".faq").is(":visible")) {
            this.CloseFAQ();
        }else if($(".serviceForm").is(":visible")) {
            this.CloseServiceForm();
        }else{
            if(SesionEngine.isLogged() == false){
                $("#loginPage").removeClass("hidden");
            }
            resetBack();
            $("#ayuda").addClass("hidden");
        }
    }
    /**
     * setFullApp
     */
    public setFullApp(status:boolean = true) {
        this._fullApp = status;
        if(status) {
            setKey("zeekgpsFull-"+userData.username,1);
            this.enableFullApp();
        }else{
            setKey("zeekgpsFull-"+userData.username,0);
           this.disableFullApp();
        }
    }
    /**
     * enableFullApp
     */
    public enableFullApp() {
        if(this._fullApp){
            $("#main,body").addClass("sidenav-open");
            $("#sidenav").addClass("sidenav-fixed");
            views();
            try{
                map.getViewPort().resize();
            }catch(e){}
        }
    }
    /**
     * disableFullApp
     */
    public disableFullApp() {
        $("#sidenav,#main,body").removeClass("sidenav-open sidenav-fixed");
        try{
            map.getViewPort().resize();
        }catch(e){}
    }
    /**
     * [DEMO]
     * Cierra la Consola de debug
     */
    public OpenConsole(){
        hideActionCircle();
        selectMenu(".console-menu");
        try{
            masterClose = ()=>{this.CloseConsole();};
        }catch(e){}
        $("#console").removeClass("hidden");
        $("#console_").html(print_console());
    }
    /**
     * [DEMO]
     * Cierra la consola de debug
     */
    public CloseConsole() {
        resetBack();
        $("#console").addClass("hidden");
        $("#console_").html('');
    }
    /**
     * Abre la vista de Reporte de Servicio
     */
    public OpenServiceForm() {
        $("#support_name").html(userData.soporte.nombre_comercial);
        $("#support_social").html(userData.soporte.razon_social);
        $("#ayuda .title").html(lang("servicereport"));
        $(".serviceForm").removeClass("hidden");
    }
    /**
     * Cierra la vista de Reporte de Servicio
     */
    public CloseServiceForm() {
        $("#ayuda .title").html(lang("help"));
        $(".serviceForm").addClass("hidden");
    }
    /**
     * Abre la vista de Preguntas Frecuentes
     */
    public OpenFAQ() {
        $("#ayuda .title").html(lang("faq"));
        $(".faq").removeClass("hidden");
    }
    /**
     * Cierra la vista de Preguntas Frecuentes
     */
    public CloseFAQ() {
        $("#ayuda .title").html(lang("help"));
        $(".faq").addClass("hidden");
    }
    /**
     * Pide confirmacion para el envio de Reporte de Servicio
     */
    public SendReport() {
        if(trim($("#sr_contacto").val()) == "") {
            $("#sr_contacto").focus();
            toasty(lang("plzfillreport"));
        }else if( trim($("#sr_tel").val()) == "" ) {
            $("#sr_tel").focus();
            toasty(lang("plzfillreport"));
        }else if( trim($("#sr_email").val()) == "" ) {
            $("#sr_email").focus();
            toasty(lang("plzfillreport"));
        }else if( trim($("#sr_details").val()) == "" ) {
            $("#sr_details").focus();
            toasty(lang("plzfillreport"));
        }else{
            confirmar(lang("confirmreport"),lang("sendreportsure"),lang("sendreport"),lang("cancel"),"Config.ConfSendReport()")
        }
    }
    /**
     * Envia el Reporte de Servicio una vez confirmado
     * por el usuario
     */
    public ConfSendReport() {
        var soporte = new Array();
        if(userData.soporte.listaContactos.length > 0){
            for(var i=0;i<userData.soporte.listaContactos.length;i++){
                soporte.push(userData.soporte.listaContactos[i].correo);
            }
        }
        ajax("POST",
            "https://auto.zeekgps.com/zeekphp/sreport.php",
            {
                to:     $("#sr_email").val(),
                name:   $("#sr_contacto").val(),
                tel:    $("#sr_tel").val(),
                mail:   $("#sr_email").val(),
                desc:   $("#sr_details").val(),
                soporte: soporte.join(","),
                company:userData.user.Nombre
            },
            function(data) {
                toasty(lang("reportsent"));
                Config.ResetReportForm();
            },
            function(err) {
                toasty(lang("tryagain"));
            },10000,
            "html"
        );
        
    }
    /**
     * Reinicia la forma de Reporte de Servicio
     */
    public ResetReportForm() {
        $("#sr_contacto,#sr_tel,#sr_email,#sr_details").val('');
        $("label[for=sr_contacto],label[for=sr_tel],label[for=sr_email],label[for=sr_details").removeClass("active");
    }
    /**
     * Verifica si ya se le mostró al usuario
     * sobre el funcionamiento de un modulo
     */
    public OpenFeature() {
        if(Settings._featureDeclared == false && isTEApp == false){
            Settings._featureDeclared = true;
            if(!!window.cordova == false){ //PC
                $('.tap-target').tapTarget();
            }else{
                if (device.platform.toLowerCase() == "ios") {
                    $('.tap-target').tapTarget();
                }
            }
        }
        this.openShowcase();
    }
    /**
     * Abre el showcase
     * (un recuadro que indica en que modulo nos encontramos
     * y da instrucciones de uso)
     */
    public openShowcase() {
        if(isTEApp == false){
            this.CloseSettings();
            if(new Sesion().Permisos("SoloRastreo") == false) {
                $(".mainTop .fade,#map .fade").show();
            
                $(".showcase").fadeIn("fast",function(){
                    /*window.showcaseTimeout = setTimeout(function(){
                        $(".showcase").fadeOut("fast");
                        $(".mainTop .fade,#map .fade").hide();
                    },7000);*/
                });
            }
        }
    }
    /**
     * Cierra el showcase
     */
    public closeShowcase() {
        try{
            clearTimeout(window.showcaseTimeout);
        }catch(e){}
        $(".showcase").fadeOut("fast");
		$(".mainTop .fade,#map .fade").hide();
    }
    /**
     * [DEMO]
     * Cambia el tiempo en que deba consultarse el RC
     */
    public ChangePollSeconds() {
        Tracking._timeSeconds = parseInt($("#pollSeconds").val());
        $(".pollSeconds .listSubtext").html(`${Tracking._timeSeconds}&nbsp;<es>segundos</es><en>seconds</en>`);
        if(Tracking._isTracking == true){
            Track.Timer();
        }
    }
    /**
	 * ChangeDoze
	 */
	public ChangeDoze(fromConfig:boolean = false) {
		try{
			cordova.plugins.DozeOptimize.IsIgnoringBatteryOptimizations( (response)=>{
				if(response=="false") {
					cordova.plugins.DozeOptimize.RequestOptimizations( (response)=>{
						//alerta("DozeOptimize",response);
						//setKey("ZeekDoze",1);
						//dozeTexts();
					},  (error)=>{
								
					});
				}else{
					if(fromConfig) {
						alerta("Activar Ahorro de Batería",`<b>Al activar el Ahorro de Batería puede dejar de recibir notificaciones push de sus unidades.</b><br/><br/>Dentro del optimizador de batería de su dispositivo móvil seleccione <em>${appName}</em> de la lista para activar el ahorro de batería.`,"Aceptar",'Config.ShowDozeMenu()');
					}
					console.log("Application already Ignoring Battery Optimizations");
					setKey("ZeekDoze",1);
					this.dozeTexts();
				}		
			},  (error)=>{
				$("#settings .doze").addClass("hidden");
			});
		}catch(e){
			$("#settings .doze").addClass("hidden");
		}
	}
    /**
	 * ShowDozeMenu
	 */
	public ShowDozeMenu() {
		try{
			cordova.plugins.DozeOptimize.RequestOptimizationsMenu(
				(response)=>{
                
            },  (error)=>{

            });
		}catch(e){}
	}
    public dozeTexts() {
		switch(parseInt(getKey("ZeekDoze"))) {
			case 1: //DESACTIVADO
				$("#settings .doze").addClass("no-effect");
				$("#settings .doze .configSquare i").removeClass("fa-chevron-right").addClass("fa-check");
				$("#settings .doze .listText").html(`Ahorro de Batería Desactivado`);
				$("#settings .doze .listSubtext").html(`El mantener el Ahorro de Batería desactivado ayuda a que reciba las Notificaciones Push.`);
			break;
			case 0: //ACTIVADO
				$("#settings .doze").removeClass("no-effect");
				$("#settings .doze .configSquare i").removeClass("fa-check").addClass("fa-chevron-right");
				$("#settings .doze .listText").html(`Desactivar Ahorro de Batería`);
				$("#settings .doze .listSubtext").html(`Si tiene problemas para recibir Notificaciones Push toque aquí para desactivar el Ahorro de Batería.`);
			break;
		}
	}
	public checkDoze() {
		try{
            if(!!window.cordova == true){
                this.dozeCheckInterval = setInterval(()=>{
                    cordova.plugins.DozeOptimize.IsIgnoringBatteryOptimizations( (response)=>{
                        if(response=="false") {
                            setKey("ZeekDoze",0);
                            this.dozeTexts();
                        }else{
                            //Application already Ignoring Battery Optimizations
                            setKey("ZeekDoze",1);
                            this.dozeTexts();
                        }		
                    },  (error)=>{
                        clearInterval(this.dozeCheckInterval);
                        $("#settings .doze").addClass("hidden");
                    });
                },2000);
            }
		}catch(e){
			clearInterval(this.dozeCheckInterval);
			$("#settings .doze").addClass("hidden");
		}
	}
    /**
     * Cambia el lenguaje de la app a la seleccionada
     * en la vista de Configuracion
     */
    public ChangeLanguage(){
        changeLang($("#langSelect").val());
        if($("#langSelect").val() == "es") {
            $("#loginPage .langSwitch input").prop("checked",false);
        }else{
            $("#loginPage .langSwitch input").prop("checked",true);
        }
        var built = new moment(buildDate);
	    $("#ayuda .version").html(version+"<br/>"+built.format("[<es>]DD [de] MMMM [de] YYYY [-] HH:mm[</es>][<en>]MMMM Do YYYY [-] HH:mm[</en>]"));
        this.BuildSelects();
        this.UpdateMetrics();
    }
    /**
     * Refleja las unidades de medicion actuales
     * en la visa de Configuracion
     */
    public UpdateMetrics() {
        if(window.distMetric == "Km"){
            unitLabel = "<es>Kilómetros</es><en>Kilometers</en>";
        }else{
            unitLabel = "<es>Millas</es><en>Miles</en>";
        }
        $(".distMetricLabel").html(unitLabel);

        if(window.fluidMetric == "Lt"){
            unitLabel = "<es>Litros</es><en>Liters</en>";
        }else{
            unitLabel = "<es>Galones</es><en>Gallons</en>";
        }
        $(".fluidMetricLabel").html(unitLabel);

        if(window.tempMetric == "c"){
            unitLabel = "<es>Centígrados</es><en>Celsius</en>";
        }else{
            unitLabel = "Fahrenheit";
        }
        $(".tempMetricLabel").html(unitLabel);

        /*if(Tracking._isTracking == true){
            Track.Timer();
        }
        if(Tracking._isLastPosition == true) {
            Track.UltimaUbicacionTransporte();
        }

        if(Tracking._isHistory == true) {
            Track.ConsultaHistoricoTransporte();
        }*/
    }
    /**
     * Cambia la unidad de distancia
     * @param unit Unidad de medicion de distancia
     */
    public ChangeDist(unit : string = "") {
        if(unit != ''){
            window.distMetric = unit;
            $("#distSelect").val(unit);
        }else{
            window.distMetric = $("#distSelect").val();
        }
        Track.CloseBubbles();
        this.UpdateMetrics();
        setKey("zeekgpsDistMetric",window.distMetric);
    }
    /**
     * Cambia la unidad de medicion volumetrica
     * @param unit Unidad de medicion volumetrica (lt o gal)
     */
    public ChangeVol(unit : string = "") {
        if(unit != ''){
            window.fluidMetric = unit;
            $("#volSelect").val(unit);
        }else{
            window.fluidMetric = $("#volSelect").val();
        }
        this.UpdateMetrics();
        setKey("zeekgpsFluidMetric",window.fluidMetric);
    }
    /**
     * Cambia la unidad de medicion de temperatura
     * @param unit Unidad de temperatura nueva (C o F)
     */
    public ChangeTemp(unit : string = "") {
        if(unit != ''){
            window.tempMetric = unit;
            $("#tempSelect").val(unit);
        }else{
            window.tempMetric = $("#tempSelect").val();
        }
        this.UpdateMetrics();
        setKey("zeekgpsTempMetric",window.tempMetric);
    }
    /**
     * [DEMO]
     * Cambia el tipo de cuenta del usuario (Light, Transporte, Flotillas)
     * @param tipo Nuevo tipo de cuenta de usuario
     */
    public ChangeAccountType(tipo : string = "") {
        if(tipo == "") {
            userData.tipo = $("#accTypeSelect").val();
            SesionEngine.PrepUser();
        }else{
            userData.tipo = tipo;
            SesionEngine.PrepUser();
        }
        $(".accTypeLabel").html($("#accTypeSelect option:selected").text());
    }
    /**
     * Prueba de toast
     */
    public ToastTest() {
        toasty(lang("helloworld"));
    }
    /**
     * fullAppToggle
     */
    public fullAppToggle() {
        if($(".fullSwitch input").is(":checked") == true) {
            this.setFullApp(true);
        }else{
            this.setFullApp(false);
        }
    }
    /**
     * Activa/Desactiva el modo Auto RC
     */
    public AutoRCToggle() {
        if($(".autoRCSwitch input").is(":checked") == true) {
            setKey("zeekgpsAutoRC-"+userData.username,1);
        }else{
            setKey("zeekgpsAutoRC-"+userData.username,0);
        }
    }
    /**
     * AutoRCUnitSelect
     */
    public AutoRCUnitSelect() {
        $(".search").val('');
        this._autoRCCount = 0;
        $("#settings .top .title").html('<es>Vehículos Habilitados para Rastreo Continuo</es><en>Vehicles for Auto Real Time Tracking</en>');
        $("#autoRCSelector .list").html('');
        vehiculos.forEach(elem => {
            if(elem.Estado == -1) {
                this._autoRCCount++;
                $("#autoRCSelector .list").append(`
                    <li class="rcs-${elem.Unidad} no-effect">
                        <div class="left-icon car-icon"><span style="background-image:url('${getCarIconUri(elem.IconoID,true)}')"></span></div>
                        <div class="textBox">
                            <div class="listText infoCarname">${elem.Descripcion}</div>
                        </div>
                        <div class="configSquare">
                            <label class="switchy">
                                <input type="checkbox" id="rcs-${elem.Unidad}" onchange="Config.ToggleRCCar('${elem.Unidad}')" ${this._autoRCList.indexOf(elem.Unidad) >=0 ? 'checked="checked"' : '' }/>
                                <span class="swslider round"></span> 
                            </label>
                        </div>
                    </li>
                `);
            }
            var options = {
                valueNames: [ 'infoCarname'],
                listClass: "list",
                item:       '<li><div><div class="infoCarname"></div></div></li>',
                searchClass: "autoRCSearch"
              };
              
            window.autoRCList = new List("autoRCSelector", options);
            //window.motorOffList.sort("infoCarname",{order: 'asc'});
        });
        $("#settings .mainConfig").addClass("hidden");
        $("#autoRCSelector").removeClass("hidden");
        if(this._autoRCList.length == this._autoRCCount) {
            //$(".selectall-autorc").html('<es>Deseleccionar Todos</es><en>Unselect All</en>');
            $(".selectall-autorc").html('<i class="far fa-check-square"></i>');
        }else{
            //$(".selectall-autorc").html('<es>Seleccionar Todos</es><en>Select All</en>');
            $(".selectall-autorc").html('<i class="far fa-square"></i>');
        }
        try{
            masterClose = ()=>{this.CloseSettings();};
        }catch(e){}
    }
    /**
     * CloseAutoRCSelector
     */
    public CloseAutoRCSelector() {
        $("#settings .top .title").html('<es>Configuración</es><en>Settings</en>');
        $("#autoRCSelector").addClass("hidden");
        $("#settings .mainConfig").removeClass("hidden");
        try{
            masterClose = ()=>{this.CloseSettings();};
        }catch(e){}
    }
    /**
     * SelectAllRCCars
     */
    public SelectAllRCCars() {
        if(this._autoRCList.length == this._autoRCCount) {
            $("#autoRCSelector input").each(function(i,e){
                if($(this).is(":checked")){
                    $(this).click();
                }
            });
        }else{
            $("#autoRCSelector input").each(function(i,e){
                if($(this).is(":not(:checked)")){
                    $(this).click();
                }
            });
        }
    }
    /**
     * ToggleRCCar
     */
    public ToggleRCCar(unidad) {
        var entry_id = this._autoRCList.indexOf(unidad);
        if($("#rcs-"+unidad).is(":checked")) {
            if(entry_id == -1) {
                this._autoRCList.push(unidad);
            }
        }else{
            if(entry_id >=0) {
                this._autoRCList.splice(entry_id,1);
            }
        }
        saveSet("zeekgpsAutoRCList-"+userData.username,this._autoRCList);
        if(this._autoRCList.length == this._autoRCCount) {
            //$(".selectall-autorc").html('<es>Deseleccionar Todos</es><en>Unselect All</en>');
            $(".selectall-autorc").html('<i class="far fa-check-square"></i>');
        }else{
            //$(".selectall-autorc").html('<es>Seleccionar Todos</es><en>Select All</en>');
            $(".selectall-autorc").html('<i class="far fa-square"></i>');
        }
    }
    /**
     * TestBattery
     */
    public TestBattery() {
        try{
            alerta("Estado de Batería",`Porcentaje: ${bateria.level}%, Cargandose?: ${bateria.isPlugged ? 'Si' : 'No'}`);
            console.warn(bateria);
        }catch(e){}
    }
    /**
     * Activa/Desactiva el plugin de Insomnia
     */
    public ScreenToggle() {
        if($(".screenSwitch input").is(":checked") == true) {
            setKey("zeekgpsScreen-"+userData.username,1);
            try{
				window.plugins.insomnia.keepAwake();
			}catch(e){
                console.error("Indomnia Awake: "+e.message);
            }
        }else{
            setKey("zeekgpsScreen-"+userData.username,0);
            try{
				window.plugins.insomnia.allowSleepAgain();
			}catch(e){
                console.error("Indomnia Sleep Again: "+e.message);
            }
        }
    }

    /**
     * Activa/Desactiva el banner de notificacion
     */
    public BannerToggle() {
        if($(".bannerSwitch input").is(":checked") == true) {
            setKey("zeekgpsBanner-"+userData.username,1);
            
        }else{
            setKey("zeekgpsBanner-"+userData.username,0);
        }
    }
    /**
     * Activa/Desactiva la recepcion de Notificaciones Push
     */
    public PushToggle() {
        if($(".pushSwitch input").is(":checked") == true) {
            console.warn("PUSH:", "ON");
            PushNotifs.Register("(Settings)");
        }else{
            console.warn("PUSH:", "OFF");
            pushUnreg(userData.user_id);
        }
    }
}