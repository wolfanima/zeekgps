class ShareClass {
    public shareTime: string = "15mins";
    
    /**
     * SelectTime
     */
    public SelectTime(time: string="15mins") {
        this.shareTime = time;
        $("#share .button").removeClass("white-button");
        $("#share .button.share-"+time).addClass("white-button");
    }
    public ShareUnits() {
       if(vehiculos.length>0){
            var timeLabel = "";
            switch(this.shareTime) {
                case "15mins": timeLabel = lang("trackshare_15mins"); break;
                case "30mins": timeLabel = lang("trackshare_30mins"); break;
                case "1hour": timeLabel = lang("trackshare_1hour"); break;
                case "4hour": timeLabel = lang("trackshare_4hour"); break;
                case "8hour": timeLabel = lang("trackshare_8hour"); break;
                case "12hour": timeLabel = lang("trackshare_12hour"); break;
                case "24hour": timeLabel = lang("trackshare_24hour"); break;
            }
            $(".shareComplete span").html(timeLabel);
            var confirmLabel = (this.CreateUnitJSON().length ==1 ? lang("trackshare_conf_one") : lang("trackshare_conf"));
            $("#share h7 span").html(timeLabel);
            confirmar(lang("trackshare"),confirmLabel.replace("%s",timeLabel),lang("share"),lang("cancel"),`Share.ShareLocation()`);
        }else{
            alerta(lang("trackshare_error"),lang("trackshare_nounits"));
        }
    }
    /**
     * PreShare
     */
    public PreShare() {
        this.shareTime = "15mins";
        $("#share .preShare").removeClass("hidden");
        $("#share .shareComplete").addClass("hidden");
        $("#share .share-time").removeClass("white-button");
        $("#share .button.share-15mins").addClass("white-button");
    
        if(this.CreateUnitJSON().length>0){
            $("#share").removeClass("hidden");
        }else{
            alerta(lang("trackshare_error"),lang("trackshare_selectpls"));
        }
    }
    /**
     * Comparte la ubicación de las unidades seleccionadas
     */
    public ShareLocation() {
        ajax("POST", apiBaseUrl+"/ShareLocation", 
            {
                licencia:   'y9xmUEj8m5S2DlYn',
                unidades:   JSON.stringify(this.CreateUnitJSON()),
                cliente:	userData.clienteID,
                usuario:	userData.user_id,
                loginName:  getKey("zeekgpsUser"),
                pass:       getKey("zeekgpsPass"),
                app:        "GPS",
                time:       this.shareTime,
                sharing_all:0
            },
            (data) =>{
                if(data.status == "OK") {
                    $(".shareLink").val(data.shareURL);
                    $("#share .preShare").addClass("hidden");
                    $("#share .shareComplete").removeClass("hidden");
                }else{
                    alerta("Error", lang("trackshare_error_req")); 
                }
            },
            ()=> {
                alerta("Error", lang("trackshare_error_req")); 
            }
        );
    }
  
    /**
     * ClosePopup
     */
    public ClosePopup() {
        $("#share").addClass("hidden");
    }

    /**
     * ThroughApp
     */
    public ThroughApp() {
        var options = {
            message: `${lang("trackshare_sharemsg")}:`, // not supported on some apps (Facebook, Instagram)
            subject: lang("trackshare_sharemsg"), // fi. for email
            url: $(".shareLink").val(),
            chooserTitle: lang("trackshare_chooseapp") // Android only, you can override the default share sheet title,
        };
        
        var onSuccess = function(result) {
            console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
            console.log("Shared to app: " + result.app); // On Android result.app since plugin version 5.4.0 this is no longer empty. On iOS it's empty when sharing is cancelled (result.completed=false)
        };
        
        var onError = function(msg) {
            console.log("Sharing failed with message: " + msg);
        };
        try{
            window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
        }catch(e){}
    }

    public OpenShare() {
        selectMenu(".share-menu");
        $("#compartir .lista").html('');
        if(vehiculos.length == 0){
            $("#compartir .noContent").removeClass("hidden");
            $("#compartir .tool.right").addClass("hidden");
        }else{
            $("#compartir .noContent").addClass("hidden");
            $("#compartir .tool.right").removeClass("hidden");
            for(var i = 0; i< vehiculos.length; i++) {

                $("#compartir .lista").append(`
                    <li class="share-${vehiculos[i].Unidad}">
                        <div class="left-icon car-icon"><div style="background-image:url('${getCarIconUri(vehiculos[i].IconoID,true)}')"></div></div>
                        <div class="textBox">
                            <div class="listText infoCarname">${vehiculos[i].Descripcion}</div>
                        </div>
                        <div class="configSquare">
                            <label class="switchy">
                                <input type="checkbox" id="share-${vehiculos[i].Unidad}" value="${vehiculos[i].Unidad}"/>
                                <span class="swslider round"></span> 
                            </label>
                        </div> 
                    </li>
                `);
            }
        }
        $("#compartir").removeClass("hidden");
        try{
            masterClose = ()=>{ this.CloseShare(); };
        }catch(e){}
    }
    /**
     * CloseShare
     */
    public CloseShare() {
        $("#compartir").addClass("hidden");
        resetBack();
    }

    public CreateUnitJSON(): Array<string> {
        let json = new Array();
        $("#compartir input[type=checkbox]:checked").each((i,e)=>{
            json.push($(e).val());
        });
        return json;
    }
}