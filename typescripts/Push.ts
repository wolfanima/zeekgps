/// <reference path ="jquery.d.ts"/> 
/**
 * PUSH
 * Clase que maneja todo sobre Push Notifications
 */
class Push {
    public regToken: string ="";
    static _pushEvents = [
        133,    //Aceleracion Intensa
        14,     //Arrastre de Vehículo
        23,     //Bateria Baja (Interna)
        10,     //Bateria Baja (Externa),
        12,     //Bateria Desconectada
        13,     //Bateria Reconectada
        2,      //Boton de Panico
        105,    //Carga Combustible
        19,     //Cofre Abierto
        20,     //Cofre Cerrado
        140,    //Colision
        47,     //Conexion Bateria Interna
        16,     //Conexion de Caja
        137,    //Curva con velocidad alta
        37,     //Descarga
        106,    //Descarga Combustible
        48,     //Desconexion Bateria Interna
        17,     //Desconexion de Caja
        33,     //Deteccion de Interferencia
        160,    //DTC Detectado
        159,    //DTC Eliminado
        21,     //Entregado
        9,      //Exceso de velocidad
        131,    //Fin de Exceso de Velocidad
        71,     //Fin de Ralentí
        134,    //Frenado Intenso
        164,    //Fuera de rango (Combustible - limite inferior)
        163,    //Fuera de rango (Combustible - limite superior)
        167,    //Fuera de rango (Horómetro)
        162,    //Fuera de rango (Odómetro)
        165,    //Fuera de rango (Posición del acelerador)
        161,    //Fuera de rango (RPM)
        166,    //Fuera de rango (Temperatura del anticongelante)
        136,    //Giro Brusco
        139,    //Impacto
        130,    //Inicio de exceso de velocidad
        70,     //Inicio de Ralenti
        99,     //Llegada
        36,     //Mezclando
        4,      //Motor Apagado
        3,      //Motor Encendido
        22,     //No Entregado
        5,      //Puerta Abierta
        6,      //Puerta Cerrada
        43,     //Puerta Lateral Abierta
        44,     //Puerta Lateral Cerrada
        103,    //Puerta Principal Abierta
        104,    //Puerta Principal Cerrada
        15,     //Ralenti
        11,     //Requiere Comunicacion
        100,     //Salida
        120,    //Servicio
        35,     //Tamper OFF
        34,     //Tamper ON
        102,    //Termo Apagado
        101,    //Termo Encendido
        254,    //Tiempo Inmovil
        107,    //Trasvase
        7,      //Valvula Abierta
        8,      //Valvula Cerrada
        112,	//Fatiga
        113,	//Distracción
        114,	//Camara mal posicionada
        115,	//Conductor dormido

    ];
    static _editingPush: boolean = false;
    static _pushEditID: string;
    static _savedPush;

    constructor() {
        Push._savedPush = new Array();
    }
    /**
     * Abre la vista de configuracion de Notificaciones Push
     */
    public Open() {
        selectMenu(".push-menu");
        try{
            masterClose = ()=>{this.Close();};
        }catch(e){}
        $(".pushList.list").html('');
        this.GetPush();
        $("#push").removeClass("hidden");
        this.FillGeoList();
        this.FillCarList();
        this.FillEventList();
        $(".content").scrollTop(0);
        Geos.ConsultarGeocercas(false);
    }
    /**
     * Retrocede en subvistas de Notificaciones Push, de lo 
     * contrario cierra la vista de configuracion de Notificaciones Push.
     */
    public Close() {
        if($(".pushEvents").is(":visible")) {
            try {
                window.pushEvList.search();
                $(".search").val('');
            }catch(e){}
            if(Push._editingPush === true){
                Push._editingPush = false;
                $(".pushEvents").addClass("hidden");
                $(".pushMain").removeClass("hidden");
                this.FillEventList();
                this.ResetGeos();
                this.ShowList();
               
            }else{
                $("#push .tool.right").attr("onclick","PushNotifs.CreateNext()");
                $("#push .tool.right").html('<i class="zicon_next"></i>');
                $(".pushEvents").addClass("hidden");
                $(".pushUnits").removeClass("hidden");
                this.EvaluateUnits();
            }
        }else if($(".pushUnits").is(":visible")) {
            $("#push .tool.right").addClass("hidden");
            $(".pushMain").removeClass("hidden");
            $(".pushUnits").addClass("hidden");
            this.FillEventList();
            this.ResetGeos();
            this.FillCarList();
            this.ShowList();
            
        } else {
            resetBack();
            $("#push").addClass("hidden");
        }
    }
    /**
     * Deselecciona las Geocercas seleccionadas
     */
    public ResetGeos() {
        $(".pushGeoList input:checked").each(function(i,e){
            $(this).click();
        });
    }
    /**
     * Muestra la lista de Notificaciones Push configuradas
     */
    public ShowList() {
        if (window.pushList.length == 0) {
            $(".pushMain .noContent").removeClass("hidden");
            $("#push .tool.right").addClass("hidden");
        } else {
            this.FillPushList();
        }
    }

    /**
     * Obtiene de plataforma la lista de Notificaciones Push configuradas
     */
    public GetPush() {
        wait(lang("loadingpush"));
        ajax("POST",
            pushUrl + "GetNotificacionesPushZeek",
            {
                licencia:   licencia,
                user_id:    userData.user_id
            },
            (xml) =>{
                try{
                    
                    var data = toJSON(xml);
                    window.pushData = data;
                    console.warn(data);
                    if(data === false){
                        closeWait();
                        toasty(lang("errorloadingpush"));
                        pushList = [];
                        this.ShowList();
                    }else{
                        console.log("GET Push (Response): "+JSON.stringify(data));
                        window.pushList = data;
                        this.ShowList();
                        closeWait();
                    }
                }catch(e){
                    console.error("GetNotificacionesPushZeek: "+e.message);
                    closeWait();
                    toasty(lang("errorloadingpush"));
                    pushList = [];
                    this.ShowList();
                }
            },
            (data,err) =>{
                console.error("GetNotificacionesPushZeek: "+err.message);
                closeWait();
                toasty(lang("errorloadingpush"));
                pushList = [];
                this.ShowList();
            },10000,"xml"
        );
    }
    /**
     * Abre la vista para editar una Notificacion Push
     */
    public EditPush(unidad: string) {
        Push._editingPush = true;
        Push._pushEditID = unidad;
        this.GetGeosRelationship(unidad);
        $(".pushMain").addClass("hidden");
        $(".pushEvents").removeClass("hidden");
        this.FillEventList();
        this.ResetGeos();
        var _id = objectIndex(pushList,"unidad",unidad);
        for (var ev in pushList[_id].eventos) {
            if($(`#pushEvent-${pushList[_id].eventos[ev]}`).is(":checked") == false) {
                $(`#pushEvent-${pushList[_id].eventos[ev]}`).click();
            }
        }
       
        this.EvaluateEvents();
        
        $(".content").scrollTop(0);
    }
    /**
     * Pide confirmacion para eliminar una Notificacion Push de la lista
     */
    public DeletePush(id: number) {
        window.deletingPushID = id;
        confirmar(lang("deletepush_head"),lang("deletepush_question"),lang("delete"),lang("cancel"),`PushNotifs.ConfirmDelete()`);
    }
    /**
     * Elimina una Notificacion Push de plataforma y la desliga de la cuenta
     */
    public ConfirmDelete() {
        console.warn("Deleting Push: "+deletingPushID);
        var _id = objectIndex(window.pushList,"unidad",deletingPushID);
        var eventos = "";

        for(var e=0;e<pushList[_id].eventosID.length;e++){
            eventos+="eventoIDs="+pushList[_id].eventosID[e]+"&";
        }
        for(var e=0;e<pushList[_id].geosID.length;e++){
            eventos+="eventoIDs="+pushList[_id].geosID[e]+"&";
        }
        eventos = eventos.substr(0,eventos.length-1);
        wait("<es>Eliminando Push...</es><en>Removing Push Notification...</en>");
        ajax("GET",
            pushUrl + "DeleteNotificacionesPushZeek?"+eventos,
            {
                licencia:   licencia,
                user_id:    userData.user_id
            },
            function(xml) {
                var data = toJSON(xml);
                console.log("DEL Push: "+JSON.stringify(data));
                if(data == true){
                    window.pushList.splice(_id,1);
                    $(`#push-${deletingPushID}`).remove();
                    PushNotifs.ShowList();
                    toasty(lang("pushdeleted"));
                }
                closeWait();
            },
            function(data) {
                toasty(lang("pushdelerror"));
                closeWait();
            },10000,"xml"
        );
    }
    /**
     * Selecciona todas las unidades de la cuenta
     */
    public SelectAllCars() {
        $(".pushCarList input").each(function(i,e){
            $(this).click();
        });
    }
    /**
     * Rellena la lista con las Notificaciones Push de la cuenta
     */
    public FillPushList() {
        var validCars = 0;
        $(".pushList").html('');
        for(var push in window.pushList) {
            var car = objectIndex(vehiculos,"Unidad",pushList[push].unidad);
            if ( car >= 0 ){
                validCars++;
                $(".pushList").append(`<li class="no-effect" id="push-${pushList[push].unidad}">
                    <!-- Dropdown Structure -->
                    <ul id='dropdown${pushList[push].unidad}' class='dropdown-content'>
                      <li><a href='#!' onclick="PushNotifs.EditPush('${pushList[push].unidad}')"><es>Editar</es><en>Edit</en></a></li>
                      <li><a href='#!' onclick="PushNotifs.DeletePush('${pushList[push].unidad}')"><es>Eliminar</es><en>Remove</en></a></li>
                    </ul>
                    <div class='left-icon car-icon'><span style="background-image:url('${getCarIconUri(vehiculos[car].IconoID,true)}')"></span></div>
                    <div class='textBox'>
                        <div class="listText infoCarname">${vehiculos[car].Descripcion}</div>
                        <div class="listText listSubtext"><i class="zicon_eventos"></i> ${pushList[push].eventosID.length}&nbsp;<es>eventos</es><en>events</en></div>
                        <div class="listText listSubtext"><i class="zicon_georeferencias"></i> ${pushList[push].geos.length}&nbsp;<es>georeferencias</es><en>georeferences</en></div>
                    </div>
                    <div class='dropdown-trigger chevron' href='#' data-target='dropdown${pushList[push].unidad}'><i class='material-icons'>more_vert</i></div>
                    
                </li>`);
            }
        }
        if(validCars > 0){ 
            $("#push .tool.right").removeClass("hidden").attr("onclick", "PushNotifs.CreatePush()").html('<i class="fas fa-plus"></i>');
            $(".pushMain .noContent").addClass("hidden");
        }else{
            $("#push .tool.right").addClass("hidden");
        }
        $('.dropdown-trigger').dropdown();
    }
    /**
     * Rellena la lista de carros para configurar Push
     */
    public FillCarList() {
        $("#push .pushCarList .carlist").html('');
        $("#push .pushCarList .list").append(`<tr>
            <td class="check" colspan="2"><div class="btn zeekColor" onclick="PushNotifs.SelectAllCars()"><es>Seleccionar Todos</es><en>Select All</en></div></td>
        </tr>`);
        for(var car in vehiculos){
            $("#push .pushCarList .carlist").append(`<tr>
            <td class="check"><label for="pushCar-${vehiculos[car].Unidad}" onclick="goTop('.inputGroup');PushNotifs.EvaluateUnits()">
                <input type="checkbox" class="filled-in" id="pushCar-${vehiculos[car].Unidad}" value="${vehiculos[car].Unidad}" /><span></span><label></td>
            <td class="carname" onclick="check('pushCar-${vehiculos[car].Unidad}');PushNotifs.EvaluateUnits()">${vehiculos[car].Descripcion}</td>
        </tr>`);
        }
        var options = {
            valueNames: [ 'carname'],
            listClass: "carlist",
            searchClass: "pushcar-search"
          };
          
        window.pushCarList = new List("push", options);
        window.pushCarList.sort("carname",{order: 'asc'});
    }
    /**
     * Rellena la lista con las Geocercas/Georutas para
     * configurarle Push
     */
    public FillGeoList() {
        $("#push .pushGeoList .list").html('');
        //Geos.ConsultarGeocercas(false);
    }
    /**
     * Rellena la lista de Eventos para configurar Push
     */
    public FillEventList() {
        $("#push .pushEventList .evlist").html('');
        for(var ev in Push._pushEvents) {
            $("#push .pushEventList .evlist").append(`<tr>
                <td class="check" width="10%"><label for="pushEvent-${Push._pushEvents[ev]}" onclick="goTop('.inputGroup');PushNotifs.EvaluateEvents()">
                    <input type="checkbox" class="filled-in" id="pushEvent-${Push._pushEvents[ev]}" value="${Push._pushEvents[ev]}"/><span></span><label>
                </td>
                <td  onclick="check('pushEvent-${Push._pushEvents[ev]}');PushNotifs.EvaluateEvents()" class="event-icon"><div class="ev-icon" style="background:url(img/eventos/${Push._pushEvents[ev]}.png) no-repeat center center;background-size:contain"></div></td>
                <td  class="eventname" onclick="check('pushEvent-${Push._pushEvents[ev]}');PushNotifs.EvaluateEvents()">${Track.GetEvento(Push._pushEvents[ev])}</td>
            </tr>`);
        }
        var options = {
            valueNames: [ 'eventname'],
            listClass: "evlist",
            searchClass: "pushevent-search"
          };
          
        window.pushEvList = new List("push", options);
        window.pushEvList.sort("eventname",{order: 'asc'})
    }
    /**
     * Inicia el proceso de creacion de Push
     */
    public CreatePush() {
        $(".pushMain").addClass("hidden");
        $(".pushUnits").removeClass("hidden");
        this.EvaluateUnits();
        $(".content").scrollTop(0);
    }
    /**
     * Sigue el proceso de creacion de Push al 2do paso,
     * y obtiene la relacion de Geocercas
     */
    public CreateNext() {
        console.log("CreateNext");
        
        this.GetGeosRelationship();
    }
    /**
     * Obtiene la relacion de Geocercas y Unidades
     * (que Unidades tienen Geocercas asignadas)
     */
    public GetGeosRelationship(unidad: string = null) {
        wait(lang("geos_push_get"));
        var unidades = "";
        if(Push._editingPush == false){
            $(".pushUnits input:checked").each(function(i,e) {
            unidades +="unidades="+$(e).val()+"&";
            });
            unidades = unidades.substr(0,unidades.length-1);
        }else{
            unidades = "unidades="+Push._pushEditID;
        }
        $(".pushGeos .noContent").removeClass("hidden").html(`
                        <es>Cargando...</es>
                        <en>Loading...</en>`);
        ajax("GET",
            pushUrl + "GetGeocercasRelacionadas?"+unidades,
            {
                licencia:   licencia,
                clienteID:  userData.clienteID
            },
            function(xml) {
                console.log(xml);
                var data = toJSON(xml);
                $("#push .pushGeoList .list").html('');
                if(data.length == 0) {
                    if( window.geocercas.length >0) {
                        $(".pushGeos .noContent").removeClass("hidden").html(`
                        <es>No hay Georeferencias con Notificaciones relacionadas.<div>Para realizar esto vaya a la plataforma web Zeek GPS y seleccione las Georeferencias a editar.</div></es>
                        <en>There are no Georeferences with Notifications.<div>To do this visit Zeek GPS web platform and select the Georeferences you wish to be notified of.</div></en>`);
                    }else{
                        $(".pushGeos .noContent").removeClass("hidden").html(`
                        <es>No hay Georeferencias en esta cuenta</es>
                        <en>There are no Gereferences on this account</en>`);
                    }
                }else{
                    $(".pushGeos .noContent").addClass("hidden");
                    for(var i =0;i<data.length;i++){ 
                        $("#push .pushGeoList .list").append(`<tr>
                        <td class="check"  width="10%"><label for="pushGeo-${data[i].id_geocerca}" onclick="goTop('.inputGroup');PushNotifs.EvaluateEvents()">
                            <input type="checkbox" class="filled-in" id="pushGeo-${data[i].id_geocerca}" value="${data[i].id_geocerca}"/><span></span><label></td>
                            <td width="22%" onclick="check('pushGeo-${data[i].id_geocerca}');PushNotifs.EvaluateEvents()"><div class="geo-icon"><i class="zicon_${(data[i].tipo == "GeoRuta" ? "georuta" : "geocerca")}" icon="pin"></i></div></td>
                            <td onclick="check('pushGeo-${data[i].id_geocerca}');PushNotifs.EvaluateEvents()">${data[i].nombre}<div class="table-subtitle">${(data[i].tipo == "GeoRuta" ? lang("georoute") : lang("geofence"))}</div></td>
                        </tr>`);
                    }
                    var _id = objectIndex(pushList,"unidad",unidad);
                    if(unidad != null) {
                        for (var geo in pushList[_id].geos) {
                            if( $(`#pushGeo-${pushList[_id].geos[geo]}`).is(":checked") == false){
                                $(`#pushGeo-${pushList[_id].geos[geo]}`).click();
                            }
                        }
                    }
                }
                $(".pushUnits").addClass("hidden");
                $(".pushEvents").removeClass("hidden");
                PushNotifs.EvaluateEvents();
                $(".content").scrollTop(0);
                closeWait();
            },
            function(err) {
                if(Push._editingPush == true){
                    $(".pushMain").removeClass("hidden");
                    $(".pushEvents").addClass("hidden");
                }else{
                    $(".pushUnits").removeClass("hidden");
                    $(".pushEvents").addClass("hidden");
                }
                console.error(err);
                closeWait();
                toasty(lang("tryagain"));
            },10000,
            "xml"
        );
    }
    /**
     * Evalua si se seleccionaron Unidades, de lo contrario 
     * no habilitara el boton para seguir el proceso
     */
    public EvaluateUnits() {
        if($(".pushUnits input:checked").length > 0) {
            $("#push .tool.right").removeClass("hidden").html(`<i class="zicon_next"></i>`).attr("onclick","PushNotifs.CreateNext()");
        }else{
            $("#push .tool.right").addClass("hidden");
        }
    }
    /**
     * Evalua si se seleccionaron Eventos, de lo contrario 
     * no habilitara el boton para seguir el proceso
     */
    public EvaluateEvents() {
        if($(".pushEvents input:checked").length > 0) {
            $("#push .tool.right").attr("onclick","PushNotifs.SavePush()");
            $("#push .tool.right").removeClass("hidden").html('<es>GUARDAR</es><en>SAVE</en>');
        }else{
            $("#push .tool.right").attr("onclick","PushNotifs.CreateNext()");
            $("#push .tool.right").addClass("hidden").html('<i class="zicon_next"></i>');
        }
    }
    /**
     * Guarda la Push creada o editada en plataforma
     */
    public SavePush() {
        wait(lang("savingpush"));
        var lstEventos = new Array();
        var eventos = "";

        var pushBody = new Object();
        pushBody.eventos = new Array();
        pushBody.geos = new Array();

        if(Push._editingPush == false){
            
            $(".pushCarList input:checked").each(function(i,e){
                pushBody = new Object();
                //pushBody.pushID = rand(0,9999);
                pushBody.unidad = $(e).val(); 
                pushBody.eventos = new Array();
                $(".pushEventList input:checked").each(function(i,e){
                    pushBody.eventos.push(parseInt($(e).val())); 
                });
                pushBody.geos = new Array();
                $(".pushGeoList input:checked").each(function(i,e){
                    pushBody.geos.push($(e).val()); 
                });
                lstEventos.push(pushBody);
                //window.pushList.push(pushBody);
            });
            ajax("POST",
                pushUrl + "SetNotificacionesPushZeek",
                {
                    licencia:               licencia,
                    user_id:                userData.user_id,
                    json_notificaciones:    JSON.stringify(lstEventos)
                },
                function(xml){
                    var data = toJSON(xml);
                    window._data = data;
                    console.log("SET Push (Response): "+JSON.stringify(data));
                    console.log("SET Push (Data Sent): "+JSON.stringify(lstEventos));
                    if(data != false){
                        $(".pushMain").removeClass("hidden");
                        $(".pushEvents").addClass("hidden");
                        $(".pushUnits").addClass("hidden");
                        PushNotifs.FillEventList();
                        PushNotifs.ResetGeos();
                        PushNotifs.FillCarList();
                        Push._editingPush = false;
                        PushNotifs.GetPush();
                        toasty(lang("pushsaved"));
                    }else{
                        toasty(lang("pushseterror"));
                        closeWait();
                    }
                },
                function(err){
                    toasty(lang("pushseterror"));
                    closeWait();
                },10000,"xml"
            );
        }else{
            var id = objectIndex(pushList,"unidad",Push._pushEditID);
            var newEvents = new Object();
            newEvents.eventos = new Array();
            newEvents.geos = new Array();
            //Verificar primero cuales eventos son nuevos.
            $(".pushEventList input:checked").each(function(i,e){
                var val = parseInt($(e).val());
                if(pushList[id].eventos.indexOf( val ) == -1) {
                    newEvents.eventos.push( val ); 
                }
            });
            
            //Verificar primero cuales geos son nuevos.
            $(".pushGeoList input:checked").each(function(i,e){
                if(pushList[id].geos.indexOf($(this).val()) == -1) {
                    newEvents.geos.push($(this).val()); 
                }
            });

            //Ahora verificamos cuales eventos se iran :( RIP
            var byeEvents = new Object();
            byeEvents.eventosID = new Array();
            $(".pushEventList input:not(:checked)").each(function(i,e){
                var index = pushList[id].eventos.indexOf(parseInt($(this).val()));
                if(index >=0) {
                    byeEvents.eventosID.push(pushList[id].eventosID[index]); 
                }
            });
            //Verificamos cuales geos se iran
            byeEvents.geosID = new Array();
            $(".pushGeoList input:not(:checked)").each(function(i,e){
                var index = pushList[id].geos.indexOf($(this).val());
                if(index >=0) {
                    byeEvents.geosID.push(pushList[id].geosID[index]); 
                }
            });

            newEvents.unidad = Push._pushEditID;
            byeEvents.unidad = Push._pushEditID;
            lstEventos.push(newEvents);
            
            ajax("POST",
                pushUrl + "SetNotificacionesPushZeek",
                {
                    licencia:               licencia,
                    user_id:                userData.user_id,
                    json_notificaciones:    JSON.stringify(lstEventos)
                },
                function(xml){
                    var data = toJSON(xml);
                    console.log("SET Push (Response): "+JSON.stringify(data));
                    console.log("SET Push (Data Sent): "+JSON.stringify(newEvents));
                    if(data != false){
                        if(byeEvents.eventosID.length > 0 || byeEvents.geosID.length > 0) {
                            for(var e=0;e<byeEvents.eventosID.length;e++){
                                eventos+="eventoIDs="+byeEvents.eventosID[e]+"&";
                            }
                            for(var e=0;e<byeEvents.geosID.length;e++){
                                eventos+="eventoIDs="+byeEvents.geosID[e]+"&";
                            }
                            eventos = eventos.substr(0,eventos.length-1);
                            ajax("GET",
                                pushUrl + "DeleteNotificacionesPushZeek?"+eventos,
                                {
                                    licencia:   licencia,
                                    user_id:    userData.user_id
                                },
                                function(xml) {
                                    var data = toJSON(xml);
                                    console.log("DEL Push (Response): "+JSON.stringify(data));
                                    console.log("DEL Push (Data Sent): "+JSON.stringify(byeEvents));
                                    $(".pushMain").removeClass("hidden");
                                    $(".pushEvents").addClass("hidden");
                                    $(".pushUnits").addClass("hidden");
                                    PushNotifs.FillEventList();
                                    PushNotifs.ResetGeos();
                                    PushNotifs.FillCarList();
                                    Push._editingPush = false;
                                    PushNotifs.GetPush();
                                    toasty(lang("pushedited"));
                                },
                                function(data) {
                                    toasty(lang("pushseterror"));
                                    closeWait();
                                },10000,"xml"
                            );
                        }else{
                            $(".pushMain").removeClass("hidden");
                            $(".pushEvents").addClass("hidden");
                            $(".pushUnits").addClass("hidden");
                            PushNotifs.FillEventList();
                            PushNotifs.ResetGeos();
                            PushNotifs.FillCarList();
                            Push._editingPush = false;
                            PushNotifs.GetPush();
                            toasty(lang("pushedited"));
                        }
                    }else{
                        toasty(lang("pushseterror"));
                        closeWait();
                    }
                },
                function(err){
                    toasty(lang("pushseterror"));
                    closeWait();
                },10000,"xml"
            );
        }
        
        
    }
    /**
     * Registra el RegID en plataforma y herramientas
     * que lo requieran
     */
    public Register(where : string = '?') {
        try{
            var m = moment(buildDate);
            console.warn(`Trying to register... (${where})`);
            if(isTEApp){
                this.regToken+= "@TE";
            }
            var pushRegObject = {
                licencia:	licencia,
                info:	    device.model+", "+device.platform+", "+device.version + ", " + device.uuid,
                userID:     userData.user_id,
                RegID:	    this.regToken,
                tipo:	    (device.platform.toLowerCase() === "android") ? 'android' : 'ios',
                appVersion: version,
                hms:        isHuawei
            };
            
            console.warn("pushRegObject",pushRegObject);
            $.ajax({
                url:		    pushUrl+'InsertPushNotif3',
                type:		    "POST",
                timeout:        60000,
                data: pushRegObject,
                success: (xml)=> {
                    var data = toJSON(xml);
                    pushRegistered = true;
                    //toasty("Activando notificaciones Push...","info");
                    //toasty(data);
                    setKey("zeekgpsPush",1);
                    console.warn("Push Registered in System",data);
                    $(".pushSwitch input").prop("checked",true);
                },
                error:	(data) => {
                    console.warn("SaveRegError: "+JSON.stringify(data));	
                    $(".notifToggle").removeClass("disabled");
                    window.pushRetry = setTimeout(()=>{
                        this.Register("Retry");
                        console.warn("Reintentando activación de Notificaciones Push...","info");
                    },10000);
                }
            });
        }catch(e){
            console.error("Zeek Push Register Error: "+JSON.stringify(e));
        }
        try{
            ajax("POST",
                apiBaseUrl+'/saveRegId',
                {
                    info:	    userData.username+", "+device.manufacturer+" "+device.model+", "+device.platform+", "+device.version,
                    cliente:    userData.clienteID,
                    userID:     userData.username,
                    RegID:	    this.regToken,
                    uuid:       device.uuid,
                    tipo:	    (device.platform.toLowerCase() === "android") ? 'android' : 'ios',
                    version:	`${version} (${m.year()}-${fix(m.month()+1)}-${fix(m.date())} ${fix(m.hours())}:${fix(m.minutes())})`,
                    app:	    (isTEApp ? "TEA" : "GPS"),
                    isHMS:      isHuawei,
                    doze: 	(parseInt(getKey("ZeekDoze")) == 1 ? 0 : 1)
                },
                function(data){
                    console.warn("Push Device Registered",where);
                },
                function(e){
                   console.error("Error RD: "+JSON.stringify(e));
                });
        }catch(e){
            console.error("Imeev Push Register Error: "+JSON.stringify(e));
        }
    }
    /**
     * CreatePushChannels
     */
    public CreatePushChannels() {
        try{
            PushNotification.createChannel(() => {
                console.log("Push Channel Created");
            }, (e) => {
                console.log('error');
                //alerta("Error",e);
            }, {
                id: "PushPluginChannel",
                description: (isTEApp ? "Notificaciones Push" : "Zeek GPS Push"),
                importance: 4,
                vibration: true
            });
        }catch(e){}
    }
    /**
     * Activa el servicio de Push en la app,
     * obtiene el RegID y lo guarda en plataforma
     */
    public Activate(register: boolean = false) {
        try{
            var m = moment(buildDate); 
            console.warn("Iniciando Push...");
            if(isHuawei) {
                try{
                    HMSPush.turnOnPush()
                        .then(()=>{
                            console.warn("HMS Push Turned ON");
                            HMSPush.getToken()
                                .then((_token)=>{
                                    window.pushActivated = true;
                                    // save this server-side and use it to push notifications to this device
                                    setKey('regId',_token);
                                    this.regToken = _token;
                                    if(register == true){
                                        pushRegistered = false;
                                        PushNotifs.Register("(on Activate)");
                                    }
                                   //alert(token);
                                    console.warn("regID = " + _token);
                                    $(".systemRegID").html(_token);
                                    HMSPush.enableAutoInit()
                                        .then(()=>{
                                            console.warn("HMS Auto Init ON");
                                        },
                                        (_err)=>{
                                            console.error("HMS Auto Init",_err);
                                        });
                                },
                                (_err)=>{
                                    console.error("HMS Token Error",_err);
                                });
                        },(_err)=>{
                            console.error("HMS Turn On Error",_err);
                        });
                    
                }catch(e){
                    console.error("HMS Error",e);
                }
            }else{
                PushNotification.deleteChannel(() => {
                    console.log("Default Push Channel Deleted");
                }, () => {
                  console.log('error');
                }, 'PushPluginChannel');
                window.pushService = PushNotification.init({
                    android: {
                        senderID: (isTEApp ? "276171859130" : "797135076294")
                    },
                    browser: {
                        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
                    },
                    ios: {
                        alert: "true",
                        badge: "true",
                        sound: "true"
                    },
                    windows: {}
                });
               
                pushService.on('registration', (data) =>{
                    window.pushActivated = true;
                    var _token = data.registrationId;
                    // save this server-side and use it to push notifications to this device
                    setKey('regId',_token);
                    this.regToken = data.registrationId;
                    if(register == true){
                        pushRegistered = false;
                        PushNotifs.Register("(on Activate)");
                    }
                    //alert(token);
                    console.warn("regID = " + _token);
                    $(".systemRegID").html(_token);
                    
                    //toasty("Token: "+this.regToken);
                    
                });
        
                
        
                pushService.on('error', function(e)  {
                    console.error("PushService.on: "+e.message);
                });
            }
            this.MessageReceived();
        }catch(e){
            if(!!window.cordova){
                console.error("Push Activate Error: "+e.message);
            }
        }
    }

    /**
     * Cuando se recibe una Push se valida el payload
     * y se ejecutan las instrucciones dependiendo del tipo de Push
     */
    public MessageReceived() {
        if(isHuawei){
            try{
                HMSPush.onMessageReceived()
                    .then((data)=>{
                        console.warn("HMS Push GET: ",data);
                        this.parsePush(data);
                    },(_err)=>{
                        console.error("HMS Push GET Error: ",_err);
                    });
            }catch(e){
                console.error("Huawei Push Resume",e);
            }
        }else{
            try{
                pushService.on('notification', (data)=>{
                    // data.message,
                    // data.title,
                    // data.count,
                    // data.sound,
                    // data.image,
                    // data.additionalData
                    this.parsePush(data);
                });
            }catch(e){}
        } 
    }
    /**
     * parsePush
     */
    public parsePush(data) {
        if('additionalData' in data){
			data = data.additionalData;
		}
        
        console.warn("Push Received",data);
        if(data.tipo == "test") {
            playSound("notificacion");
            alerta("Push","Has recibido una Notificación Push de prueba exitosamente.");	
        }
        if(data.tipo == "reload"){
            location.reload(); 
        }
        if(data.tipo == "time") {
            var localDate = moment();
            alerta("Time Test","Local offset: "+Math.abs(localDate.utcOffset())+" ("+(Math.abs(localDate.utcOffset())/60)+")\nZone: "+window.userData.Zona+"\nTo Local: "+(Math.abs(localDate.utcOffset())/60-window.userData.Zona));
        }

        if(data.tipo == "sound"){
            playSound(data.file);	
        }
        if(data.tipo == "circle") {
            $("#time").css({
                "top":data.top,
                "left":data.left,
                "display":data.display,
            });
        }
        if(data.tipo == "displayevent") {
            infoMaxWidth = parseInt(data.maxwidth);
            infoMinWidth = parseInt(data.minwidth);
            //RasCon.displayEvent(parseInt(data.id),parseInt(data.car),null);
        }
        if( data.tipo == "evento" || 
            data.tipo == "gpsevento") {
            if(Tracking._isTracking == false || 
                ( Tracking._isTracking == true && 
                    Tracking._rastrearUnidades.indexOf(data.unidad) == -1 ) ) {
                Push._savedPush.push(
                    {
                        "lat":      parseLat(data.latitud),
                        "lng":      parseLon(data.longitud),
                        "evento":   parseInt(data.evento),
                        "unidad":   data.unidad,
                        "fecha":    parseInt(data.ms),
                        "velocidad":parseInt(data.velocidad),
                        "visto":    false,
                        "mostrado": false
                    }
                );
                //console.log(Push._savedPush);
                this.DisplayPush();
                if(Tracking._isTracking == false &&
                    Tracking._isLastPosition == false &&
                    Tracking._isHistory == false && 
                    $("#sidebar").is(":visible")) {
                        this.FillSavedPush();
                }
            }
        }
        if(data.tipo == "clean") {

        }
        if(data.tipo == "func") {
            eval(data.fn);
        }
        if(data.tipo == "debugshow") {
            $(".debug").show();
        }
        if(data.tipo == "debughide") {
            $(".debug").hide();
        }
    }
    /**
     * Muestra la Push en un banner al recibirla,
     * ademas verifica en su buffer si hay Push por mostrar.
     */
    public DisplayPush() {
        var pushID = -1;
        for(var i=0; i < Push._savedPush.length;i++){
            if(pushID == -1 && Push._savedPush[i].mostrado == false) {
                pushID = i;
            }
        }
        if(pushID >= 0 && $("#push-popup").hasClass("visible") == false) {
            if(objectIndex(vehiculos,"Unidad",Push._savedPush[pushID].unidad) >= 0) {
                showPush(pushID);
                Push._savedPush[pushID].mostrado = true;
                setTimeout(() => {
                    $("#push-popup").removeClass("visible");
                    setTimeout(()=>{
                        this.DisplayPush();
                        Track.DisplayBanner();
                    },1500);
                },5000);
            }
        }
    }
    /**
     * Enfoca el mapa en las coordenadas de evento de la Push reportada
     * @param pushID Identificador de la Push
     */
    public PushFocus(pushID: number) {
        map.setCenter({
            "lat": Push._savedPush[pushID].lat,
            "lng": Push._savedPush[pushID].lng
        });
        map.setZoom(10);
    }
    /**
     * Rellena la lista de Notificaciones Push
     */
    public FillSavedPush() {
        $(".carInfoList  .list").html('');
        for(var i = 0;i < Push._savedPush.length; i++) {
            var fecha = UTC2Local(Push._savedPush[i].fecha);
            var carid = objectIndex(vehiculos,"Unidad",Push._savedPush[i].unidad);
            if(carid != -1){
                $(".carInfoList .list").append(`
                    <li onclick="Track.ShowEvent(${Push._savedPush[i].evento}, '${Push._savedPush[i].unidad}', ${Push._savedPush[i].lat}, ${Push._savedPush[i].lng},${Push._savedPush[i].velocidad})">
                        <div class="flex-row">
                            <div class="left-icon car-icon"><span style="background-image:url(img/eventos/${Push._savedPush[i].evento}.png)"></span></div>
                            <div class="textBox">
                                <div class="listText rtDetail infoCarname">${Track.GetEvento(Push._savedPush[i].evento)}</div>
                                <div class="listSubtext rtDate rtFecha"><strong>${vehiculos[carid].Descripcion}</strong></div>
                                <div class="row" style="padding-bottom:10px">
                                    <div class="col s6" style="margin-top:3px"><span class="svg-icon calendario dyn-svg"></span> <span class="event-snippet">${fecha.format("DD/MMM/YYYY")}</span></div>
                                    <div class="col s6" style="margin-top:3px"><span class="svg-icon tiempo"></span> <span class="event-snippet">${fecha.format("hh:mm:ss a")}</span></div>
                                    <div class="col s6" style="margin-top:3px"><span class="svg-icon velocidad dyn-svg"></span> <span class="event-snippet">${distConv(Push._savedPush[i].velocidad,true,true)}/h</span></div>
                                
                                </div>
                            </div>
                        </div>
                    </li>
                `);
            }
        }
    }
    /**
     * Obtiene la longitud del arreglo de las Push
     * verificando la relacion de cada entrada con unidades existentes
     */
    public RealSavedPushLength(): number {
        var counter = 0;
        for(var i = 0;i < Push._savedPush.length; i++) {
            if(objectIndex(vehiculos,"Unidad",Push._savedPush[i].unidad) != -1){
                counter++;
            }
        }
        return counter;
    }
    /**
     * [DEPRECATED]
     * Rellena el arreglo de las Notificaciones Push
     * con datos de demostracion
     */
    public PushEventDemo() {
        Push._savedPush = new Array();
        Push._savedPush.push( {
            "lat":26.82262451516058,
            "lng":-109.64657771980309,
            "evento":3,
            "unidad":"1436317",
            "fecha":1536021179135,
            "velocidad":9,
            "visto":false,
            "mostrado":false
        });
        Push._savedPush.push( {
            "lat":26.75611972150477,
            "lng":-109.61155879890465,
            "evento":4,
            "unidad":"206676283st",
            "fecha":1536021171135,
            "velocidad":25,
            "visto":false,
            "mostrado":false
        });

        Push._savedPush.push( {
            "lat":32.28688944550975,
            "lng":-116.31579760544633,
            "evento":46,
            "unidad":"207226122st",
            "fecha":1536021879135,
            "velocidad":20,
            "visto":false,
            "mostrado":false
        });
        Push._savedPush.push( {
            "lat":32.31474816488082,
            "lng":-115.26111010544635,
            "evento":45,
            "unidad":"862462039785315",
            "fecha":1536022179135,
            "velocidad":30,
            "visto":false,
            "mostrado":false
        });
        Push._savedPush.push( {
            "lat":32.31474816488082,
            "lng":-115.26111010544635,
            "evento":45,
            "unidad":"007211148st",
            "fecha":1536022179135,
            "velocidad":30,
            "visto":false,
            "mostrado":false
        });
    }
    /**
     * loadHuaweiPush
     */
    public loadHuaweiPush() {
        window.pushAllowed = true;
        for(var pq=0;pq<pushQueue.length;pq++){
            try{
                this.parsePush(JSON.parse(pushQueue[pq]));
            }catch(e){
                console.error("PushQueue",e);
            }
        }
    }
}