/// <reference path ="jquery.d.ts"/> 
class FuelClass {
    public _baseInterval: any = {"timeUnit": "second", "count": 1};
    static _rangeFieldsES = [
        [-1,    "Seleccione una opción"],
        ["td",  "Hoy"],
        ["yd",  "Ayer"],
        ["2days","&Uacute;ltimos 2 días"],
        ["week","Semana en curso"],/*
        ["custom","Personalizado"]*/
    ];
    static _rangeFieldsEN = [
        [-1,    "Select an option"],
        ["td",  "Today"],
        ["yd",  "Yesterday"],
        ["2days","Past Two Days"],
        ["week","Current Week"]/*
        ["custom","Custom"]*/
    ];
    public chart:any;
    public _unidadConsultada: number = -1;
    public _tempUnidades: any[];
    public _fuelData: any;
    public _fuelDataBkp: any;
    public cursorPosition: { x: any; y: any; };
    public maxCapacity: number;
    public fuelThreshold: number = 5; //DIFF EN LITROS

    constructor(){
        if(!!window.cordova === true){
            $('#fuelIni').attr("type","datetime-local");
            $('#fuelFin').attr("type","datetime-local");
        }
    }
    /**
     * Construye y rellena selects
     */
    public BuildSelects() {
        $("#FuelDayMetric").html('');
        //$.datetimepicker.setLocale(window.idioma);
        var maxDate = new moment();
        var today = new moment();

        if(idioma == "en") {
            for(var i=0;i<FuelClass._rangeFieldsEN.length;i++){
                var select ="";
                if(i == 0){
                    select = "disabled selected";
                }
                $("#FuelDayMetric").append('<option value="'+FuelClass._rangeFieldsEN[i][0]+'" '+select+'>'+FuelClass._rangeFieldsEN[i][1]+'</option>');
            }
        }else{
            for(var i=0;i<FuelClass._rangeFieldsES.length;i++){
                var select ="";
                if(i == 0){
                    select = "disabled selected";
                }
                $("#FuelDayMetric").append('<option value="'+FuelClass._rangeFieldsES[i][0]+'" '+select+'>'+FuelClass._rangeFieldsES[i][1]+'</option>');
            }
        }
        $("#FuelDayMetric").val(-1);
        formSelect();
    }
    /**
     * Open
     */
    public Open() {
        $("#fuel").removeClass("hidden");
        selectMenu(".fuel-menu");
        try{
            masterClose = ()=>{this.Close();};
        }catch(e){}

       
        $(".searchCars").val('');
        $("#fuel .selectAll").hide();
        this._tempUnidades = new Array();
        this.AsignFirstUnit();
        
        this.FillCarList();
        //this.FillTagList();
        $("#fuelIni,#fuelFin").val('');
        this.BuildSelects();
        $(".subVista").scrollTop(0);
    }
    /**
     * Close
     */
    public Close() {
        if($("#fuel .fuelGraph").is(":visible")){
            $("#fuel .fuelMain").removeClass("hidden");
            $("#fuel .fuelGraph").addClass("hidden");
            $("#fuel .top .title").html("Fuel");
            $("#fuel .top .normaltool").show();
            $("#fuel .top .graphzoom").addClass("hidden");
            this.BuildSelects();
            $("#fuel .custom-fields").addClass("hide");
            $("#fuelIni,#fuelFin").val('');
            //this.chart.dispose();
        }else{
            $(".sidenav li").removeClass("selected-menu");
            resetBack();
            $("#fuel").addClass("hidden");
        }
    }
    /**
     * toggleCursorBehavior
     */
    public toggleCursorBehavior() {
        if(this.chart.chartCursor.pan == true){
            $("#fuel .top .graphzoom").html('<i class="fas fa-search-plus"></i>');
            this.chart.chartCursor.pan = false;
            toasty(lang("graphzoom"));
        }else{
            $("#fuel .top .graphzoom").html('<i class="fas fa-arrows-alt-h"></i>');
            this.chart.chartCursor.pan = true;
            toasty(lang("graphpan"));
        }
    }
    /**
     * getFuelData
     */
    public GetFuelData() {
        if($("#fuelIni").val() != "" && $("#fuelFin").val() != ""){
            if(this._tempUnidades.length == 0){
                toasty(lang("selectvehicle"),"error");
            }else{
                this.ObtenerDatosFuel();
            }
        }else{
            toasty(lang("fuelfini"),"error");
        }
    }
    /**
     * ObtenerDatosFuel
     */
    public ObtenerDatosFuel() {
        var unit_id = objectIndex(vehiculos, "Unidad",this._tempUnidades[0]);
        wait(lang("fuelwait"));
        ajax("POST",zeekappsUrl+"ObtenerDatosFuel",{
            token:  userData.token,
            data:   JSON.stringify({
                "fechaInicial":     new Date($("#fuelIni").val()).getTime(),
                "fechaFinal":       new Date($("#fuelFin").val()).getTime(),
                "unidades":         this._tempUnidades,
                "unidadDistancia":  window.distMetric,
                "unidadVolumen":    window.fluidMetric,
                "zona":             window.zona
            })
        },
        (data)=>{
            $("#fuel .top .normaltool").hide();
            $("#fuel .top .graphzoom").removeClass("hidden");
            $("#fuel .top .title").html(lang("fueldatafor")+vehiculos[unit_id].Descripcion);
            closeWait();
            $(".fuelMain").addClass("hidden");
            $(".fuelGraph").removeClass("hidden");
            this._unidadConsultada = unit_id;
            var json = JSON.parse(data.data);
            this._fuelData = JSON.parse(json.d);
            this.SanitizeFuelData();
            if(this._fuelData.Ubicaciones.length == 0){
                $("#fuelgraph").html('<div class="noContent flex">No hay datos de ubicaciones.</div>');
            }else{
                this.RenderGraph();
            }
            $("#fuel .daterange .graph-value").html(`
                <strong>${moment($("#fuelIni").val()).format("YYYY-MMM-DD - HH:mm:ss")}</strong> 
                    <es>a</es><en>to</en>
                <strong>${moment($("#fuelFin").val()).format("YYYY-MMM-DD - HH:mm:ss")}</strong>
            `);
        },
        (err)=>{
            closeWait();
            toasty(lang("fuelerror","error"));
        },20000);
    }
    /**
     * ConvertFuelDates
     */
    public ConvertFuelDates() {
        this._fuelDataBkp = JSON.parse(JSON.stringify(this._fuelData));
        for(var dt=0;dt<this._fuelData.Ubicaciones.length;dt++){
            this._fuelData.Ubicaciones[dt].Fecha = new Date(this._fuelData.Ubicaciones[dt].Fecha);
            this._fuelData.Ubicaciones[dt].FechaString = moment(this._fuelData.Ubicaciones[dt].Fecha).format("YYYY-MM-DD - HH:mm:ss");
        }
    }
    /**
     * SanitizeFuelData
     */
    public SanitizeFuelData() {
        this.ConvertFuelDates();
        var toDel =[];
        for(var dt=0;dt<this._fuelData.Ubicaciones.length;dt++){
            if(this._fuelData.Ubicaciones[dt].TanquesCombustible == null){
                toDel.push(dt);
            }
        }
        for(var td=toDel.length-1;td>=0;td--){
            this._fuelData.Ubicaciones.splice(toDel[td],1);
        }
        for(var dt=0;dt<this._fuelData.Ubicaciones.length;dt++){
            this._fuelData.Ubicaciones[dt].Indice = dt;
            this._fuelData.Ubicaciones[dt].Base = 0;
            var vol = 0;
            for(var t=0;t<this._fuelData.Ubicaciones[dt].TanquesCombustible.length;t++){
                if(this._fuelData.Ubicaciones[dt].TanquesCombustible[t] > 0) {
                    vol+=this._fuelData.Ubicaciones[dt].TanquesCombustible[t];
                    this._fuelData.Ubicaciones[dt].TanquesCombustible[t] = Math.round(this._fuelData.Ubicaciones[dt].TanquesCombustible[t]/100);
                }
            };
            if(vol > 0){
                this._fuelData.Ubicaciones[dt].Volumen = Math.round(vol/100);
            }

            //BALLOON DATA AND RANGE COLOR
            this._fuelData.Ubicaciones[dt].tanques = "";
            this._fuelData.Ubicaciones[dt].icono = "";
            this._fuelData.Ubicaciones[dt].eventTitle = "";
            for(var t=0;t<this._fuelData.Ubicaciones[dt].TanquesCombustible.length;t++){
                if(this._fuelData.Ubicaciones[dt].TanquesCombustible[t] > 0) {
                    this._fuelData.Ubicaciones[dt].tanques+=`<div><span class="toolHead">T${t+1}:</span> ${this._fuelData.Ubicaciones[dt].TanquesCombustible[t]}</div>`;
                }
            }
            if(this._fuelData.Ubicaciones[dt].tanques != ""){
                this._fuelData.Ubicaciones[dt].tanques+= `<div><span class="toolHead">Total:</span> ${this._fuelData.Ubicaciones[dt].Volumen}</div>`;
            }
            if(parseInt(this._fuelData.Ubicaciones[dt].Evento) == 0){
                
                this._fuelData.Ubicaciones[dt].eventTitle = (this._fuelData.Ubicaciones[dt].Velocidad>0 ? lang("moving") : lang("stopped") );
                this._fuelData.Ubicaciones[dt].icono = `<i style="background-image:url('${getCarIconUri(vehiculos[this._unidadConsultada].IconoID, (this._fuelData.Ubicaciones[dt].Velocidad >0) )}')"></i>`;
                this._fuelData.Ubicaciones[dt].color = "";
            }else{
                if(parseInt(this._fuelData.Ubicaciones[dt].Evento) == 105) { //CARGA
                    this._fuelData.Ubicaciones[dt].color = "#00FF00";
                }else if(parseInt(this._fuelData.Ubicaciones[dt].Evento) == 106) { //DESCARGA
                    this._fuelData.Ubicaciones[dt].color = "#FF0000";
                }else{
                    this._fuelData.Ubicaciones[dt].color = "";
                }
                this._fuelData.Ubicaciones[dt].eventTitle = Track.GetEvento(parseInt(this._fuelData.Ubicaciones[dt].Evento));
                this._fuelData.Ubicaciones[dt].icono = `<i style="background-image:url('img/eventos/${this._fuelData.Ubicaciones[dt].Evento}.png')"></i>`;
            }
            this._fuelData.Ubicaciones[dt].balloonText =            `<div class="fuelTooltip">
                            <div class="eventTitle toolHead">${this._fuelData.Ubicaciones[dt].eventTitle}</div>
                            <div class="tooltipIcon">${this._fuelData.Ubicaciones[dt].icono}</div>
                            <div><span class="toolHead">${lang("date")}:</span> ${this._fuelData.Ubicaciones[dt].FechaString}</div>
                            ${this._fuelData.Ubicaciones[dt].tanques}
                        </div>`;
        }
        this.maxCapacity = 0;
        for(var mc=0;mc<this._fuelData.VolumenesMaximos.length;mc++){
            if(this._fuelData.VolumenesMaximos[mc].Volumen > this.maxCapacity) {
                this.maxCapacity = this._fuelData.VolumenesMaximos[mc].Volumen;
            }
        }
        var compGral = this._fuelData.ReporteGeneral.ComportamientoGeneral;
        $(".graph-value.bruteperformance").html(`${compGral.RendimientoBruto} <span>${distMetric}/${fluidMetric}</span>`);
        $(".graph-value.effperformance").html(`${compGral.RendimientoEfectivo} <span>${distMetric}/${fluidMetric}</span>`);
        $(".graph-value.timemoving").html(compGral.TiempoMovimiento);
        $(".graph-value.deadtime").html(compGral.TiempoMuerto);
    }
    /**
     * RenderGraph
     */
    public RenderGraph() {
        $("#fuel .top .graphzoom").html('<i class="fas fa-arrows-alt-h"></i>');
        this.chart = AmCharts.makeChart("fuelgraph",{
            type: "serial",
            pathToImages: "https://cdn.amcharts.com/lib/3/images/",
            language: window.idioma,
            addClassNames: true,
            categoryField: "Fecha",
            marginTop: 5,
            categoryAxis: {
                minPeriod: "1mm",
                parseDates: true
            },
            mouseWheelZoomEnabled: true,
            chartCursor: {
                pan:    true,
                cursorPosition: "mouse",
                cursorColor: "#F05A30",
                listeners: [],
                categoryBalloonDateFormat: "YYYY-MM-DDTHH:NN:SS"
            },
            graphs: [{
                id: "fuelgraph",
                connect: true,
                title: "graph 1",
                valueField: "Volumen",
                hideBulletsCount: 0,
                bulletAlpha: 0,
                bulletColor: "#95a5a6",
                fillAlphas: 1,
                lineAlpha: 0,
                fillColors: "#2c3e50",
                fillColorsField: "color",
                balloonColor: "#181818",
                balloonFunction: (data)=>{
                    return data.dataContext.balloonText;
                }
            }],
            chartScrollbar: {
                graph: "g5",
                gridAlpha: 0,
                color: "#888888",
                scrollbarHeight: 55,
                backgroundAlpha: 0,
                selectedBackgroundAlpha: 0.1,
                selectedBackgroundColor: "#888888",
                graphFillAlpha: 0,
                autoGridCount: true,
                selectedGraphFillAlpha: 0,
                graphLineAlpha: 0.2,
                graphLineColor: "#c2c2c2",
                selectedGraphLineColor: "#888888",
                selectedGraphLineAlpha: 1
            },
            valueAxes: [{
                id: "ValueAxis-1",
                title: window.fluidMetric,
                maximum: this.maxCapacity,
                minimum: 0
            }],
            balloon: {
                fillColor: "#181818",
                color: "#FFFFFF",
                fillAlpha: 0.9,
                offsetX: 0,
                offsetY: 0,
                pointerWidth: 10,
                fixedPosition: true
            },
            export: {
                enabled: false
            },
            dataProvider: this._fuelData.Ubicaciones
        });
        $(".amcharts-zoom-out-label tspan").html(lang("graphshowall"));
        /*
        am4core.ready(()=> {

            // Themes begin
            //am4core.useTheme(am4themes_material);
            // Themes end
            am4core.options.minPolylineStep = 5;
            this.chart = am4core.create("fuelgraph", am4charts.XYChart);
            
            this.chart.data = this._fuelData.Ubicaciones;
            this.chart.paddingRight = 30;
            window.dateAxis = this.chart.xAxes.push(new am4charts.DateAxis());
            dateAxis.renderer.grid.template.location = 0;
            dateAxis.cursorTooltipEnabled = false;
            dateAxis.groupData = false;
            dateAxis.renderer.minGridDistance = 50;
            dateAxis.baseInterval = this._baseInterval;
            dateAxis.tooltip.getFillFromObject = false;
            dateAxis.tooltip.background.fill = am4core.color("#f05a30");
            dateAxis.tooltip.background.stroke = am4core.color("#f23d0a");
            dateAxis.tooltipDateFormat = "yyyy-MM-ddTHH:mm:ss";

            var valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.cursorTooltipEnabled = false;
            valueAxis.tooltip.disabled = false;
            valueAxis.title.text = window.fluidMetric;
            valueAxis.max = this.maxCapacity;
            valueAxis.renderer.minGridDistance = 50;
            valueAxis.tooltip.getFillFromObject = false;
            valueAxis.tooltip.getStrokeFromObject = false;
            valueAxis.tooltip.background.fill = am4core.color("#f05a30");
            valueAxis.tooltip.background.stroke = am4core.color("#f23d0a");
            
            
            // only for the legend
            var iconSeries = this.chart.series.push(new am4charts.ColumnSeries())
            iconSeries.strokeWidth = 0;
            iconSeries.tooltip.getFillFromObject = false;
            iconSeries.tooltip.getStrokeFromObject = false;
            iconSeries.name = "Fecha";
            iconSeries.dataFields.dateX = "Fecha";
            iconSeries.dataFields.valueY = "Volumen";
            iconSeries.fillOpacity = 0;
            iconSeries.strokeOpacity = 0;
            iconSeries.dataFields.indice = "Indice";
            iconSeries.columns.template.tooltipY = 0;
            iconSeries.tooltip.background.fill = am4core.color("#2c3e50");
            iconSeries.tooltip.pointerOrientation = "horizontal";
            iconSeries.columns.template.adapter.add('tooltipHTML', (text, target)=> {
                var data = target.tooltipDataItem.dataContext;
                
                return `<div class="fuelTooltip">
                            <div class="eventTitle toolHead">${data.eventTitle}</div>
                            <div class="tooltipIcon">${data.icono}</div>
                            <div><span class="toolHead">${lang("date")}:</span> {FechaString}</div>
                            ${data.tanques}
                        </div>`;
            });

            var series = this.chart.series.push(new am4charts.LineSeries());
            series.dataFields.dateX = "Fecha";
            series.dataFields.openValueY = "Volumen";
            series.dataFields.valueY = "Base";
            //series.sequencedInterpolation = true;
            series.stroke = am4core.color("rgba(0,0,0,0)");
            series.strokeWidth = 2;
            series.name = "Lt";
            series.stroke = am4core.color("#2c3e50");
            series.fill = series.stroke;
            series.fillOpacity = 1;

            //CURSOR
            this.chart.cursor = new am4charts.XYCursor();
            this.chart.cursor.xAxis = dateAxis;
            this.chart.scrollbarX = new am4core.Scrollbar();
            if(!!window.cordova == true){
                this.chart.cursor.behavior = "panX";
            }
            this.chart.cursor.lineX.stroke = am4core.color("#f05a30");
            this.chart.cursor.lineX.fill = this.chart.cursor.lineX.stroke;
            this.chart.cursor.lineX.strokeWidth = 1;
            this.chart.cursor.lineX.strokeOpacity =1;
            this.chart.cursor.lineX.strokeDasharray = "";

            this.chart.cursor.lineY.disabled = true;
            this.chart.cursor.lineY.stroke = am4core.color("#f05a30");
            this.chart.cursor.lineY.fill = this.chart.cursor.lineY.stroke;
            this.chart.cursor.lineY.strokeWidth = 1;
            this.chart.cursor.lineY.strokeOpacity =0.5;
            this.chart.dateFormatter.dateFormat = "yyyy-MM-ddTHH:mm:ss";
            
            this.chart.cursor.selection.fill = this.chart.cursor.lineY.stroke;

            // create ranges
            var negativeRange,positiveRange;
            window.prevItem = null;
            // create ranges
            this.chart.events.on("datavalidated", () =>{
                series.dataItems.each((actualItem) =>{
                    
                    if (actualItem.index > 0) { //MAS DE UN DATO
                        window.prevItem = series.dataItems.getIndex(actualItem.index - 1); //DATO ANTERIOR
                        
                        window.rangeStartTime = am4core.time.round(new Date(actualItem.dateX.getTime()), dateAxis.baseInterval.timeUnit, dateAxis.baseInterval.count).getTime();
                        window.prevStartTime = am4core.time.round(new Date(prevItem.dateX.getTime()), dateAxis.baseInterval.timeUnit, dateAxis.baseInterval.count).getTime();

                        if(this._fuelData.Ubicaciones[actualItem.index].Evento == 106) { //DECREMENTO
                            //console.warn("Decremento en",prevItem.index,prevItem.openValueY,"Actual",actualItem.index,actualItem.openValueY);

                            if(!negativeRange){
                                negativeRange = dateAxis.createSeriesRange(series);
                                negativeRange.date = new Date(window.rangeStartTime);
                                negativeRange.contents.stroke = am4core.color("#FF0000");
                                negativeRange.contents.fill = negativeRange.contents.stroke;
                                negativeRange.contents.fillOpacity = 1;
                            }
                        }else if(this._fuelData.Ubicaciones[actualItem.index].Evento == 105) { //AUMENTO
                            //console.warn("Incremento en",prevItem.index,prevItem.openValueY,"Actual",actualItem.index,actualItem.openValueY);

                            if(!positiveRange){
                                positiveRange = dateAxis.createSeriesRange(series);
                                positiveRange.date = new Date(window.rangeStartTime);
                                positiveRange.contents.stroke = am4core.color("#00FF00");
                                positiveRange.contents.fill = positiveRange.contents.stroke;
                                positiveRange.contents.fillOpacity = 1;
                            }
                        }else {
                            if (negativeRange) {
                                negativeRange.endDate = new Date(prevItem.dateX.getTime() + dateAxis.baseDuration / 2);
                                negativeRange = undefined;
                            }
                            if (positiveRange) {
                                positiveRange.endDate = new Date(prevItem.dateX.getTime() + dateAxis.baseDuration / 2);
                                positiveRange = undefined;
                            }
                        }
                        // end if last
                        if (actualItem.index == series.dataItems.length - 1) {
                            if (negativeRange) {
                                negativeRange.endDate = new Date(prevItem.dateX.getTime() + dateAxis.baseDuration / 2);
                                negativeRange = undefined;
                            }
                            if (positiveRange) {
                                positiveRange.endDate = new Date(prevItem.dateX.getTime() + dateAxis.baseDuration / 2);
                                positiveRange = undefined;
                            }
                        }
                    }
                })
            })
        }); // end am4core.ready()*/
    }
    /**
     * Selecciona todos los vehiculos en una lista
     * @param vista Vista del modulo actual
     */
    public SelectAllCars() {
        var vista = "fuel";
        for(var i=0;i<window.carFuelSel.items.length;i++) {
            $(window.carFuelSel.items[i].elm.children[0].children[0]).children("input[type='checkbox']").click();
        }

        this.FillCarList();
        $("#trackAddTagList-"+vista).val($("#"+vista+" .trackAddTagList input:checked").val());
        formSelect();
    }
    /**
     * Rellena la lista de Etiquetas
     * @param vista Vista del modulo actual
     * @param speedLimit Limite de velocidad?
     */
    public FillTagList() {
        var vista = "fuel";
        $("#"+vista+" .trackAddTagList .taglist").html('');
        $("#"+vista+" #trackAddTagList-"+vista).html('');

        $("#"+vista+" .trackAddTagList .taglist").append(`<tr onclick="Fuel.FillCarList('0');check('rttag-all-${vista}',true)">`+
            `<td class="check"><label for="rttag-all-${vista}" _id="0"><input name="rttag" type="radio" id="rttag-all-${vista}" value="0" checked="true" /><span></span></label></td>`+
            `<td><es>Todos</es><en>All</en></td>`+
        '</tr>');
        $("#"+vista+" #trackAddTagList-"+vista).append('<option value="0" vista="'+vista+'">'+lang("all")+'</option>');

        for(var i =0;i<etiquetas.length;i++){
            $("#"+vista+" #trackAddTagList-"+vista).append('<option value="'+(i+1)+'" vista="'+vista+'">'+etiquetas[i]+'</option>');
            $("#"+vista+" .trackAddTagList .taglist").append( `<tr onclick="Fuel.FillCarList('${etiquetas[i]}');check('rttag-${cleanSpaces(etiquetas[i])}-${vista}',true)">`+
                `<td class="check"><label for="rttag-${cleanSpaces(etiquetas[i])}-${vista}" _id="${i+1}"><input name="rttag" type="radio" id="rttag-${cleanSpaces(etiquetas[i])}-${vista}" value="${etiquetas[i]}" /><span></span></label></td>`+
                `<td>${etiquetas[i]}</td>`+
            '</tr>');
        }
        formSelect();
    }
    /**
     * Rellena la lista de autos disponibles para rastrear o consultar
     * @param tag Etiqueta
     * @param vista Vista del modulo actual
     * @param speedLimit Limite de Velocidad?
     */
    public FillCarList(tag :string = "0"){
        var vista = "fuel";
        console.log("Filling list for "+vista);
        $("#"+vista+" #trackAddTagList-"+vista).val($("#"+vista+" .trackAddTagList label[for='rttag-"+(tag == "0" ? "all" : cleanSpaces(tag))+"-"+vista+"']").attr("_id"));
        formSelect();
        $("#"+vista +" .trackAddCarList .list").html('');
        for(var i =0;i<vehiculos.length;i++){
            var checked = 'class="filled-in"';
            var found = false;

            if(this._tempUnidades.indexOf(vehiculos[i].Unidad) > -1){
                checked = 'class="filled-in checked" checked="checked"';
            }
            var _tags = vehiculos[i].Etiquetas.split(",");

            if(tag != '0'){
                found = (_tags.indexOf(tag) > -1 ? true : false);
            }
            if(tag == "0" || found == true){
                try{
                    if(vehiculos[i].TipoPlaca.indexOf(".gas") != -1){
                        $("#"+vista+" .trackAddCarList .list").append(`<tr onclick='Fuel.AddToConsult("${vehiculos[i].Unidad}")'>`+
                            `<td class="check" onclick="check('rtcar-${vehiculos[i].Unidad}-${vista}')"><label><input type="radio" name="onecar" id="rtcar-${vehiculos[i].Unidad}-${vista}" ${checked} value="${vehiculos[i].Unidad}" /><span></span></label></td>`+
                            `<td class="carname" onclick="check('rtcar-${vehiculos[i].Unidad}-${vista}')">${vehiculos[i].Descripcion}</td> `+
                            `<td class="carmake" onclick="check('rtcar-${vehiculos[i].Unidad}-${vista}')">${vehiculos[i].Marca == undefined ? '---' : vehiculos[i].Marca}</td> `+
                            `<td class="carmodel" onclick="check('rtcar-${vehiculos[i].Unidad}-${vista}')">${vehiculos[i].Modelo == undefined ? '---' : vehiculos[i].Modelo}</td> `+
                        +'</tr>');
                    }else{
                    //noFuelUnit(i);
                    }
                }catch(e){
                    //noFuelUnit(i);
                }
            }
        }
        function noFuelUnit(i) {
            $("#fuel .trackAddCarList .list").append(`<tr class="unableToSelect">`+
            `<td class="check" onclick=""><label><input type="radio" name="onecar" disabled="true" id="rtcar-${vehiculos[i].Unidad}-fuel" ${checked} value="${vehiculos[i].Unidad}" /><span></span></label></td>`+
                `<td class="carname" >${vehiculos[i].Descripcion}</td> `+
                `<td class="carmake" >${vehiculos[i].Marca == undefined ? '---' : vehiculos[i].Marca}</td> `+
                `<td class="carmodel">${vehiculos[i].Modelo == undefined ? '---' : vehiculos[i].Modelo}</td> `+
            +'</tr>');
        }
        var options = {
           
            searchClass:    'searchCars',
    
            valueNames: [ 'carname','carmake','carmodel'],
            pagination: true,
            page: ($(document).height() >=850 ? 10 : 5)
          };
          
        window.carFuelSel = new List(vista, options);
    }
    /**
     * Asigna la primera unidad disponible al arreglo temporal de Unidades a rastrear
     * (Utilizado en Histórico)
     */
    public AsignFirstUnit() {
        this._tempUnidades = new Array();
        for(var i =0;i<vehiculos.length;i++){
            try{
                if(this._tempUnidades.length == 0 && vehiculos[i].TipoPlaca.indexOf(".gas") != -1){
                    this._tempUnidades.push(vehiculos[i].Unidad);
                }
            }catch(e){}
        }
    }
    /**
     * Agrega una unidad al rastreo
     * @param unit Identificador de Vehiculo
     */
    public AddToConsult(unit : string) {
        var id = this._tempUnidades.indexOf(unit);
        //Solo se puede seleccionar una unidad en Fuel
        if ($("#fuel").is(":visible")){
            //console.log("Forget: ","All");
            this._tempUnidades = [];
        }
        //console.log("Tracking: ",unit);
        
        this._tempUnidades.push(unit);
    }
    /**
     * ChangeDayMetric
     */
    public ChangeDayMetric(metric:string) {
        if(!!window.cordova == false) { //PC
            var format = "YYYY-MM-DD[ ]";
        }else{
            var format = "YYYY-MM-DD[T]";
        }
        $('#fuelIni').attr("type","text");
        $('#fuelFin').attr("type","text");
        $("#fuel .custom-fields").addClass("hide");
        var now = new moment();
        switch(metric){
            case "td":
                
                $("#fuelFin").val(now.format(format+"HH:mm"));
                $("#fuelIni").val(now.format(format)+"00:00");
            break;
            case "yd":
                var past = moment().subtract(1,'day');
                $("#fuelFin").val(past.format(format)+"23:59");
                $("#fuelIni").val(past.format(format)+"00:00");
            break;
            case "2days":
                $("#fuelFin").val(now.format(format+"HH:mm"));
                var past = moment().subtract(1,'day');
                $("#fuelIni").val(past.format(format)+"00:00");
            break;
            case "week":
                $("#fuelFin").val(now.format(format+"HH:mm"));
                var past = moment().startOf("week");
                $("#fuelIni").val(past.format(format)+"00:00");
            break;
            case "custom":
                var format = "YYYY-MM-DD[T]";
                $('#fuelIni').attr("type","datetime-local");
                $('#fuelFin').attr("type","datetime-local");
                $("#fuel .custom-fields").removeClass("hide");
            break;
        }
    }
}