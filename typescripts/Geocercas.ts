/// <reference path ="jquery.d.ts"/> 
/**
 * GEOCERCAS
 * Esta clase engloba mayormente al controlador de vistas,
 * y administracion de instancias de Georeferencias.
 */
class Geocercas {
	static ShapeList = new Array();
	static ShapeInstance = new Array();
	static selectedShapes = new Array();
    static _CreatingShape = false;
	static _CreatingRoute = false;
	static _CreatingComplex = false;
	static _editing = false;
	static _visibility = false;
	static _creatingType = "";
	static _inputMethod = 0; //0 = Tap, 1 = Cursor
	static _editShapeID = -1;
	static _repainting = false;
	static _polyCircleSides = 15;
	static objectList = null;
	static routeGroup = null;
	static testGroup = null;
	static complexGroup = null;
	static polyGroup = null;
	static routeMarkers = null;
	static Circles = new Array();
	static Markers = new Array();
	static Polygons = new Array();
	static NewPolyGeo = new Object();
	static geoPolyMarker = null;
	static geoSquareMarker = null;
	static complexMarker = null;
	static polyRoute = null;
	static polyComplex = null;
	static _geoEvents = new Array();
    static _routePoints = null;
	static _routePointsArray = new Array();
	static _complexPoints = null; 
	static _complexPointsArray = new Array();
	static polyCircleSlider = null;
	static polySquareSlider = null;

	static _polyGeoCircleProps = new Object();
	static _polyGeoSquareProps = new Object();
	static _polyComplexProps = new Object();
    
	constructor() {
        map.addEventListener('tap', addMarkerToMap,false);
		this.ResetArrays();
		Geocercas.objectList = new H.map.Group();
		Geocercas.routeGroup = new H.map.Group();
		Geocercas.testGroup = new H.map.Group();
		Geocercas.routeMarkers = new H.map.Group();
		Geocercas.complexGroup = new H.map.Group();
		Geocercas.polyGroup = new H.map.Group();
		map.addObject(Geocercas.objectList);
		map.addObject(Geocercas.routeGroup);
		map.addObject(Geocercas.testGroup);
		map.addObject(Geocercas.routeMarkers);
		map.addObject(Geocercas.complexGroup);
		map.addObject(Geocercas.polyGroup);
		Geocercas._routePoints = new H.geo.LineString();
		Geocercas._complexPoints = new H.geo.LineString();
		
		Geocercas._polyGeoCircleProps = {
			radius : 	500,
			color:		"rgba(255,0,0,0.3)",
			colorHex:	"#FF0000",
			sides:		14
		};
		Geocercas._polyGeoSquareProps = {
			perimeter:	500,
			color:		"rgba(0,255,0,0.3)",
			colorHex:	"#00FF00"
		};
		Geocercas._polyComplexProps = {
			color:		"rgba(255,0,255,0.3)",
			colorHex:	"#FF00FF"
		};
    }
    /**
	 * Reinicia los arreglos de la Clase
	 */
    public ResetArrays(){
		Geocercas.Circles = null;
		Geocercas.Markers = null;
		Geocercas.Polygons = null;
		Geocercas.Circles = new Array();
		Geocercas.Markers = new Array();
		Geocercas.Polygons = new Array();
	}
	/**
	 * Obtiene la lista de Geocercas en la cuenta
	 * @param open_sidebar Abre el sidebar?
	 */
	public ConsultarGeocercas(open_sidebar : boolean = true){
		$("#sidebar").removeClass("emptylist");
		//Geocercas.polyGroup.removeAll();
		if(open_sidebar === true) {
			$(".geoShapes .list").html('');
			waitSidebar("<es>Cargando Georeferencias...</es><en>Loading Georeferences...</en>");
			this.OpenSidebar("geoShapes");
		}
		Geocercas.ShapeList = new Array();
		$.ajax({
            url:    geoUrl  + "ConsultarGeocercas",
            type:   "POST",
            timeout:10000,
            dataType:"json",
            data: {
                "token":  window["userData"]["token"]
            },
            success: function(data){
                console.log(data.data);
                try{
					window.geocercas = (data.data == null ? [] : data.data);
					if(window.geocercas.length == 0){
						$("#sidebar").addClass("emptylist");
						$("#sidebar").append('<div class="emptycar"></div>');
						$(".geoShapes .noContent").removeClass("hidden");
					}else{
						$("#sidebar").removeClass("emptylist");
						$("#sidebar .emptycar").remove();
						$(".geoShapes .noContent").addClass("hidden");
					}
					for(var i = 0;i<window.geocercas.length;i++){
						Geocercas.ShapeList.push(new GeoShape(geocercas[i].Tipo,
							geocercas[i].Nombre,
							null,
							"#"+geocercas[i].visual.Color.substr(2,8),
							{},
							(geocercas[i].Descripcion == undefined ? "" : geocercas[i].Descripcion),
							geocercas[i].CorreoElectronico,
							geocercas[i].Activada));
						Geocercas.ShapeInstance.push(new GeoShape(geocercas[i].Tipo,
							geocercas[i].Nombre,
							null,
							"#"+geocercas[i].visual.Color.substr(2,8),
							{},
							(geocercas[i].Descripcion == undefined ? "" : geocercas[i].Descripcion),
							geocercas[i].CorreoElectronico,
							geocercas[i].Activada));
						var tipo = "", icon = "", selected = "square";
						if(window.geocercas[i].Tipo == "GeoRuta"){
							tipo = '<es>Georuta</es><en>Geo Route</en>';
							icon = 'georuta';
						}
						if(window.geocercas[i].Tipo == "Poligonal"){
							tipo = '<es>Geocerca</es><en>Geofence</en>';
							icon = 'geocerca';
						}
						if (Geocercas.selectedShapes.indexOf(window.geocercas[i].GeocercaID) != -1) {
							selected = "check-square";
						}
						$(".geoShapes .list").append('<li id="geosel-'+window.geocercas[i].GeocercaID+'">'+
							'<div class="left-icon left-check" onclick="Geos.SelectShape(\''+window.geocercas[i].GeocercaID+'\','+i+')"><span class="check far fa-'+selected+'"></span></div>'+
							'<div class="left-icon" onclick="Geos.SelectShape(\''+window.geocercas[i].GeocercaID+'\','+i+')"><i class="zicon_'+icon+'" icon="pin"></i></div>'+
							'<div class="textBox" onclick="Geos.SelectShape(\''+window.geocercas[i].GeocercaID+'\','+i+')">'+
								'<div class="listText"><b>'+window.geocercas[i].Nombre+'</b></div>'+
								'<div class="listText listSubtext">'+tipo+'</div>'+
							'</div>'+/*
							(SesionEngine.Permisos("EdicionGeocercas") == true ? '<div class="chevron" onclick="Geos.Edit('+i+')"><span class="lsf lsf-icon" icon="right"></span></div>' : '')+*/
						'</li>');
						
						
					}
					closeWaitSidebar();
                }catch(e){
                    console.log(e);
                }
            },
            error: function(data){
				if($("#sidebar").is(":visible")){
					waitSidebar("<es>Reintentando...</es><en>Retrying...</en>");
					console.log("Retrying...");
					setTimeout(function(){
						Geos.ConsultarGeocercas();
					},3000);
				}
			}
        });
	}
	/**
	 * Selecciona una Geocerca a consultar de la lista, no mas de 3.
	 * @param shape Tipo de Geocerca
	 * @param id Identificador de la instancia de la Geocerca
	 */
	public SelectShape(shape : string,id : number) {
		if ($("#geosel-"+shape+" .check").hasClass("fa-check-square")){
			$("#geosel-"+shape+" .check").removeClass("fa-check-square").addClass("fa-square");
			Geocercas.selectedShapes.splice(Geocercas.selectedShapes.indexOf(shape),1);
			Geocercas.polyGroup.removeObject(Geocercas.ShapeInstance[id]._instance);
		}else{
			if(Geocercas.selectedShapes.length == 3) {
				toasty(lang('shapelimit'),"warn");
			}else{
				$("#geosel-"+shape+" .check").addClass("fa-check-square").removeClass("fa-square");
				Geocercas.selectedShapes.push(shape);
				Geos.ConsultarPuntosGeocerca(id,true);
			}
		}
	}
	/**
	 * Obtiene los puntos de una Geocerca
	 * @param index Indice de la Geocerca en el arreglo contenedor
	 * @param draw Si es true dibuja la Geocerca en el mapa
	 */
	public ConsultarPuntosGeocerca(index : number,draw: boolean) {
		var geoLista = new Array();
		waitSidebar("<es>Cargando Puntos...</es><en>Loading Coordinates...</en>");
		geoLista.push({
			"GeoCercaID":	geocercas[index].GeocercaID,
			"Nombre":		geocercas[index].Nombre,
			"Zona":			geocercas[index].Zona
		});
		$.ajax({
            url:    geoUrl  + "ConsultarPuntosGeocerca",
            type:   "POST",
            timeout:10000,
            dataType:"json",
            data: {
				"token":  window["userData"]["token"],
				"geoLista":JSON.stringify(geoLista)
            },
            success: function(data){
                console.log(data);
                try{
					window.puntosGeocercas = data.data;
					Geocercas.ShapeList[index].puntos = window.puntosGeocercas.Puntos;
					Geocercas.ShapeInstance[index].puntos = window.puntosGeocercas.Puntos;
					if(draw == true){
						Geos.DrawShape(index);
					}
					if(Geocercas._editing == true){
						$("#add-btn").removeClass("hidden").attr("onclick","Geos.SaveShapeChanges()");
					}
					closeWaitSidebar();
                }catch(e){
                    console.error(e);
                }
            },
            error: function(data){
				if($("#sidebar").is(":visible")){
					waitSidebar("<es>Reintentando...</es><en>Retrying...</en>");
					console.log("Retrying...");
			
					setTimeout(function(){
						Geos.ConsultarPuntosGeocerca(index,draw);
					},3000);
				}
            }
        });
	}
	/**
	 * Dibuja en el mapa la Geocerca indicada.
	 * @param index Indice de la instancia de la Geocerca
	 */
	public DrawShape(index: number){
		//if(objectIndex(Geocercas.ShapeInstance,"_titulo",geocercas[index].Nombre) < 0 ){
			/*var i = Geocercas.ShapeInstance.length;
			Geocercas.ShapeList[index]._puntos = puntosGeocercas[0].Puntos;
			map.setViewBounds(Geocercas.geoPolyMarker.getBounds(),false);*/
		//}
		try{
			Geocercas.polyGroup.removeObject(Geocercas.ShapeInstance[index].instance);
		}catch(e){}
		Geocercas.ShapeInstance[index].Load();
		map.setViewBounds(Geocercas.ShapeInstance[index]._instance.getBounds(),false);
	}
	/**
	 * Crea una Geocerca circular (localmente)
	 */
	public CreateGeoCircle() {
		if(Geocercas._CreatingShape == false){
			Geocercas._CreatingShape = true;
			var centerCoords = map.getCenter();
			Geocercas.Circles.push({"circle":new H.map.Circle(
				// The central point of the circle
				{lat:centerCoords.lat, lng:centerCoords.lng},
				// The radius of the circle in meters
				1000,
				{
					style: {
						strokeColor: 'rgba(255, 255, 255, 0.9)', // Color of the perimeter
						lineWidth: 2,
						fillColor: 'rgba(0, 0, 255, 0.4)'  // Color of the circle
					}
				}
			),"puntos":new Array()});
			Geocercas.testGroup.addObject(Geocercas.Circles[Geocercas.Circles.length-1].circle);
			Geocercas._CreatingShape = false;
		}
	}
	/**
	 * Inicia el proceso de creación de Geocerca Circular
	 */
	public CreateGeoPoly() {
		if(Geocercas._CreatingShape == false){
			$("#main .mainTop").addClass("no-logo");
			$("#main .mainTop .title").html("<es>Crear Georeferencia</es><en>New Geofence</en>");
			this.OpenSidebar("polyCircle");
			Geocercas._CreatingShape = true;
			$(".centerMarker .pin").css("background",Geocercas._polyGeoCircleProps.colorHex);
			$(".centerMarker").show();
			$(".delpolyCircle").hide();
			var coordenada = map.getCenter();
			map.setZoom(17);
			
            this.DrawCirclePoly(coordenada);
			this.StartSlider("polyCircle");
		}
	}
	/**
	 * Agrega una Geocerca Circular Poligonal al mapa y guarda instancia.
	 * @param coordenadas Objeto con coordenadas del centro de Geocerca Circular
	 */
	public PrintCirclePoly(coordenadas:object){
		Geocercas.NewPolyGeo.puntos = null;
		Geocercas.NewPolyGeo.puntos = new Array();
		var metros = calcMetros(coordenadas.lat, coordenadas.lng);
		var r1 = metros[0] * Geocercas._polyGeoCircleProps.radius;
		var r2 = metros[1] * Geocercas._polyGeoCircleProps.radius;
		var pi = Math.PI;
		
		var lineString = new H.geo.LineString();
		
		for (var n = 0; n < Geocercas._polyCircleSides; n++) {
			var x = parseFloat((Math.sin(n / Geocercas._polyGeoCircleProps.sides * 2 * pi) * r1).toFixed(6)) + coordenadas.lat;
			var y = parseFloat((Math.cos(n / Geocercas._polyGeoCircleProps.sides * 2 * pi) * r2).toFixed(6)) + coordenadas.lng;
			
			var punto = [x,  y, 0 ];
			//console.log("Coordenadas: " + punto[0] + "," + punto[1] + ", posicion: " + n);
			//Push a punto coordenada entre cada angulo de la geocerca
			Geocercas.NewPolyGeo.puntos.push(punto);
			//Dibuja el punto coordenada de la geocerca
			lineString.pushLatLngAlt(x,y,0);
		}
		Geocercas.geoPolyMarker = new H.map.Polygon(lineString, {
			style: {
				fillColor: Geocercas._polyGeoCircleProps.color,
				strokeColor: '#FFF',
				lineWidth: 3
			}
		});
	
		map.addObject(Geocercas.geoPolyMarker);
		map.setViewBounds(Geocercas.geoPolyMarker.getBounds(),false);
		Geocercas._repainting = false;
	}
	/**
	 * Dibuja en el mapa una Geocerca Circular Poligonal y la guarda
	 * @param coordenadas Coordenadas de la Geocerca Poligonal
	 */
	public DrawCirclePoly(coordenadas : object) {
		this.PrintCirclePoly(coordenadas);
		
		/*// LISTENERS
		// MAPVIEWCHANGE: Parecido al evento IDLE de GM, al iniciar el evento 
		// se remueve la ultima instancia de la polyGeocerca.
		map.addEventListener("mapviewchange", deletePolyInstance, false);
		Geocercas._geoEvents.push({
			"target":	"polyCircle",
			"type":		"mapviewchange",
			"function":	deletePolyInstance
		});*/
		
		// MAPVIEWCHANGEEND: Al terminar el evento MAPVIEWCHANGE se obtienen 
		// las coordenadas del centro del mapa, se crea otra instancia de 
		// Geocercas y se solicita repintar otra polyGeocerca.
		map.addEventListener("mapviewchangeend", repaintPolyInstance,false);
		Geocercas._geoEvents.push({
			"target":	"polyCircle",
			"type":		"mapviewchangeend",
			"function":	repaintPolyInstance
		});
	}
	/**
	 * Abre la vista para crear una Geocerca Cuadrada
	 */
	public CreateGeoSquare() {
		if(Geocercas._CreatingShape == false){
			$("#main .mainTop").addClass("no-logo");
			$("#main .mainTop .title").html("Crear Georeferencia");
			this.OpenSidebar("polySquare");
			Geocercas._CreatingShape = true;
			$(".centerMarker .pin").css("background",Geocercas._polyGeoSquareProps.colorHex);
			$(".centerMarker").show();
			$(".delpolySquare").hide();
			var coordenada = map.getCenter();
			map.setZoom(17);
			
			this.DrawSquarePoly(coordenada);
			this.StartSlider("polySquare");
			
		}
	}
	/**
	 * Dibuja en el mapa una Geocerca Cuadrada.
	 * @param coordenadas Objeto con coordenadas de la Geocerca Cuadrada
	 */
	public DrawSquarePoly(coordenadas : object) {
		this.PrintSquarePoly(coordenadas);
		
		/*// LISTENERS
		// MAPVIEWCHANGE: Parecido al evento IDLE de GM, al iniciar el evento 
		// se remueve la ultima instancia de la polyGeocerca.
		map.addEventListener("mapviewchange", deleteSquareInstance, false);
		Geocercas._geoEvents.push({
			"target":	"polySquare",
			"type":		"mapviewchange",
			"function":	deleteSquareInstance
		});
		*/
		// MAPVIEWCHANGEEND: Al terminar el evento MAPVIEWCHANGE se obtienen 
		// las coordenadas del centro del mapa, se crea otra instancia de 
		// Geocercas y se solicita repintar otra polyGeocerca.
		map.addEventListener("mapviewchangeend", repaintSquareInstance,false);
		Geocercas._geoEvents.push({
			"target":	"polySquare",
			"type":		"mapviewchangeend",
			"function":	repaintSquareInstance
		});
	}
	/**
	 * Agrega al mapa una Geocerca Cuadrada y guarda instancia.
	 * @param coordFiguras Coordenadas de los cuatro puntos de la Geocerca Cuadrada
	 */
	public PrintSquarePoly(coordFiguras : object) {
        Geocercas.NewPolyGeo.puntos = null;
		Geocercas.NewPolyGeo.puntos = new Array();
		var lineString = new H.geo.LineString();
		var distancia = calcMetros(coordFiguras.lat, coordFiguras.lng);
		var coordPrincipal = coordFiguras;
		//console.log("coordPrincipal",coordPrincipal);
		for (var i = 0; i < (Geocercas._polyGeoSquareProps.perimeter / 2) ; i++) {
			coordPrincipal.lat += distancia[0];
			coordPrincipal.lng -= distancia[1];
		}

		
		lineString.pushLatLngAlt(coordPrincipal.lat,coordPrincipal.lng,0);
		Geocercas.NewPolyGeo.puntos.push([coordPrincipal.lat,coordPrincipal.lng,0]);
	
		for (i = 0; i < (Geocercas._polyGeoSquareProps.perimeter) ; i++) {
			coordPrincipal.lng += distancia[1];
		}

		lineString.pushLatLngAlt(coordPrincipal.lat,coordPrincipal.lng,0);
		
		Geocercas.NewPolyGeo.puntos.push([coordPrincipal.lat,coordPrincipal.lng,0]);
	
		for (i = 0; i < (Geocercas._polyGeoSquareProps.perimeter) ; i++) {
			coordPrincipal.lat -= distancia[0];
		}

		lineString.pushLatLngAlt(coordPrincipal.lat,coordPrincipal.lng,0);
		Geocercas.NewPolyGeo.puntos.push([coordPrincipal.lat,coordPrincipal.lng,0]);
	
		for (i = 0; i < (Geocercas._polyGeoSquareProps.perimeter) ; i++) {
			coordPrincipal.lng -= distancia[1];
		}

		lineString.pushLatLngAlt(coordPrincipal.lat,coordPrincipal.lng,0);
		Geocercas.NewPolyGeo.puntos.push([coordPrincipal.lat,coordPrincipal.lng,0]);

		Geocercas.geoSquareMarker = new H.map.Polygon(lineString, {
			style: {
				fillColor: Geocercas._polyGeoSquareProps.color,
				strokeColor: '#FFF',
				lineWidth: 3
			}
		});
	
		map.addObject(Geocercas.geoSquareMarker);
		map.setViewBounds(Geocercas.geoSquareMarker.getBounds(),false);
		Geocercas._repainting = false;
	}
	/**
	 * Abre vista para crear Geocerca Compleja
	 */
	public CreateComplex() {
		if(Geocercas._CreatingShape == false){
			Geocercas._complexPointsArray = new Array();
			$("#main .mainTop").addClass("no-logo");
			$("#main .mainTop .title").html("Crear Georeferencia");
			this.OpenSidebar("polyComplex");
			Geocercas._CreatingShape = true;
			Geocercas._CreatingComplex = true;
			$(".delComplex").hide();
			Geocercas._inputMethod = 1;
			this.TogglePointInput();
			$(".otherMethod").removeClass("hidden");
			$(".undoButton").removeClass("hidden");
		}
	}
	/**
	 * Agrega el centro (punto inicial) de una Geocerca Compleja tomando
	 * el punto como el centro de la vista del mapa.
	 */
	public AddComplexPointCenter(){
		var coords = map.getCenter();
		if(Geocercas._editing == true){
			Geocercas.ShapeInstance[Geocercas._editShapeID].puntos.push({
				lat:coords.lat,
				lng:coords.lng
			});
			Geocercas.ShapeInstance[Geocercas._editShapeID].Reload();
		}else{
			this.AddComplexPoint(coords.lat,coords.lng);
		}
	}
	/**
	 * Agrega un punto mas a la Geocerca Compleja
	 * @param _lat Latitud del punto nuevo
	 * @param _lng Longitud del punto nuevo
	 */
	public AddComplexPoint(_lat:number,_lng:number){
		if(Geocercas._editing == true){
			Geocercas.ShapeInstance[Geocercas._editShapeID].puntos.push({
				lat:_lat,
				lng:_lng
			});
			Geocercas.ShapeInstance[Geocercas._editShapeID].Reload();
		}else{
			Geocercas._complexPointsArray.push({lat:_lat, lng:_lng});
			Geocercas._complexPoints.pushPoint(Geocercas._complexPointsArray[Geocercas._complexPointsArray.length-1]);

			if(Geocercas._complexPoints.getPointCount() > 1){
				Geocercas.complexMarker = null;
				this.UpdateComplexMaker();
			}
		}
		$(".undoButton").removeClass("disabled");
	}
	/**
	 * Actualiza la Geocerca Compleja en el mapa en creación
	 */
	public UpdateComplexMaker(){
        Geocercas.complexMarker = new H.map.Polygon(
            Geocercas._complexPoints, 
            { 
                style: { 
                    lineWidth: 6,
                    fillColor: hexToRgbA($("#color_polyComplex").val(),0.6),
                    strokeColor: '#FFF'
                 }
            }
		);

		Geocercas.complexGroup.removeAll();
		Geocercas.complexGroup.addObject(Geocercas.complexMarker);
		//map.setViewBounds(Geocercas.complexMarker.getBounds(),false);
		Geocercas._repainting = false;
	}
	/**
	 * CTRL+Z al ultimo punto de la Geocerca Compleja en creación
	 */
	public UndoLastComplexPoint() {
		try{
			if(Geocercas._editing == true){
				Geocercas.ShapeInstance[Geocercas._editShapeID].puntos.splice(Geocercas.ShapeInstance[Geocercas._editShapeID].puntos.length-1,1);
				if(Geocercas.ShapeInstance[Geocercas._editShapeID].puntos.length == 0){
					$(".undoPoligonal").addClass("disabled");
				}
				Geocercas.ShapeInstance[Geocercas._editShapeID].Reload();
				
			}else{
				if(Geocercas._complexPoints.getPointCount()>0){
					Geocercas._complexPoints.removePoint(Geocercas._complexPoints.getPointCount()-1);
					if(Geocercas._complexPoints.getPointCount() == 0){
						$(".undoButton").addClass("disabled");
					}
					Geocercas.complexGroup.removeAll();
					this.UpdateComplexMaker();
				}
			}
		}catch(e){}
	}
	/**
	 * Inicializa el Slider 
	 * @param section Seccion donde se localiza el Slider 
	 */
	public StartSlider(section : string){
		var startNumber = 500;
		switch(section){
			case "polySquare":
				if(Geocercas.polySquareSlider == null){
					if(Geocercas._editing == true){
						startNumber = Geocercas.ShapeInstance[Geocercas._editShapeID]._perimeter;
					}
					Geocercas.polySquareSlider = document.getElementById('slider_polySquare');
					noUiSlider.create(Geocercas.polySquareSlider, {
							start: startNumber,
							connect: [true, false],
							step: 50,
							orientation: 'horizontal', // 'horizontal' or 'vertical'
							behaviour: 'tap-drag', 
							range: {
							'min': 100,
							'max': 2500
						}
					});
					Geocercas.polySquareSlider.noUiSlider.on('update', function( values, handle ) {
						if(Geocercas._editing == true){
							Geocercas.ShapeInstance[Geocercas._editShapeID]._perimeter = parseInt(values[handle]);
							Geocercas.objectList.removeObject(Geocercas.ShapeInstance[Geocercas._editShapeID]._instance);
							Geocercas.ShapeInstance[Geocercas._editShapeID].Load();
							Geos.FocusShape(Geocercas._editShapeID);
						}else{
							Geocercas._polyGeoSquareProps.perimeter = parseInt(values[handle]);
							deleteSquareInstance();
							repaintSquareInstance();
						}
						$("#slider_polySquare_label .value").html(parseInt(values[handle]));
					});
				}else{
					if(Geocercas._editing == true){
						Geocercas.polySquareSlider.noUiSlider.set(Geocercas.ShapeInstance[Geocercas._editShapeID]._perimeter);
					}
				}
			break;
			case "polyCircle":
				if(Geocercas.polyCircleSlider == null){
					if(Geocercas._editing == true){
						startNumber = Geocercas.ShapeInstance[Geocercas._editShapeID]._radius;
					}
					Geocercas.polyCircleSlider = document.getElementById('slider_polyRadius');
					noUiSlider.create(Geocercas.polyCircleSlider, {
							start: startNumber,
							connect: [true, false],
							step: 50,
							orientation: 'horizontal', // 'horizontal' or 'vertical'
							behaviour: 'tap-drag', 
							range: {
							'min': 100,
							'max': 2500
						}
					});
					Geocercas.polyCircleSlider.noUiSlider.on('update', function( values, handle ) {
						if(Geocercas._editing == true){
							Geocercas.ShapeInstance[Geocercas._editShapeID]._radius = parseInt(values[handle]);
							Geocercas.objectList.removeObject(Geocercas.ShapeInstance[Geocercas._editShapeID]._instance);
							Geocercas.ShapeInstance[Geocercas._editShapeID].Load();
							Geos.FocusShape(Geocercas._editShapeID);
						}else{
							Geocercas._polyGeoCircleProps.radius = parseInt(values[handle]);
							deletePolyInstance();
							repaintPolyInstance();
						}
						$("#slider_polyRadius_label .value").html(parseInt(values[handle]));
					});
				}else{
					if(Geocercas._editing == true){
						Geocercas.polyCircleSlider.noUiSlider.set(Geocercas.ShapeInstance[Geocercas._editShapeID]._radius);
					}
				}
			break;
		}
	}
	/**
	 * Abre una seccion del Sidebar para Geocercas
	 * @param section Seccion del Sidebar
	 */
	public OpenSidebar(section : string){
		$(".mainTop .tool.right").hide();
		Geocercas._visibility = false;
		//$(".visible"+section+" i").html("visibility_off");
		//$(".visible"+section+" span").html("INVISIBLE");
		$("#sidebar .toHide").hide();
		$("#sidebar .geocercas").show();
		if(section != 'geoShapes'){
			$("#nombre_"+section).val("");
        	$("#add-btn").attr("onclick","Geos.SaveShape('"+section+"')");
			$("#add-btn").removeClass("hidden");
			$("#add-btn i").html("save");
		}
		Geocercas._creatingType = section;
		$("#sidebar,#close-btn").removeClass("hidden");
		$("#sidebar .geocercas > div").hide();
		$(".geocercas ."+section).show();
		$("#close-btn").attr("onclick","Geos.CloseSidebar('"+section+"')");
		views();
		this.ZoomObjects();
		Config.disableFullApp();
	}
	/**
	 * Cierra el Sidebar y apaga todo que tenga que ver con Geocercas
	 * @param section Seccion del Sidebar a cancelar
	 */
	public CloseSidebar(section: string){
		Geocercas._polyGeoSquareProps.perimeter = 500;
		Geocercas._polyGeoCircleProps.radius = 500;
		try{
			Geocercas.polyCircleSlider.noUiSlider.set(Geocercas._polyGeoCircleProps.radius);
		}catch(e){}
		try{
			Geocercas.polySquareSlider.noUiSlider.set(Geocercas._polyGeoSquareProps.perimeter);
		}catch(e){}
		$("#main .mainTop").removeClass("no-logo");
		$("#main .mainTop .title").html("");
		$("#sidebar,#add-btn,#close-btn,.otherMethod,.undoButton").addClass("hidden");
		$(".geocercas ."+section+",.centerAim").hide();
		this.RemovePolyCircle();
		this.RemovePolySquare();
		this.RemovePolyComplex();
		Geocercas.routeGroup.removeAll();
		Geocercas.routeMarkers.removeAll();
		Geocercas.complexGroup.removeAll();
		//Geocercas.polyGroup.removeAll();
		Geocercas.polyRoute = null;
		Geocercas._routePoints = null;
		Geocercas._complexPoints = null;
		Geocercas._CreatingRoute = false;
		Geocercas._CreatingComplex = false;
		Geocercas._routePoints = new H.geo.LineString();
		Geocercas._complexPoints = new H.geo.LineString();
		$(".undoButton").addClass("disabled");
		$(".mainTop .tool.right").show();
		views();
		Config.enableFullApp();
	}
	/**
	 * Guarda la Geocerca en plataforma.
	 * @param tipo Categoria de la Geocerca a guardar
	 */
    public SaveShape(tipo : string){
        var radius = 0;
        var perimeter = 0;
		var puntos;
		var finalType;
        if(tipo == "polyCircle"){
            radius = Geocercas._polyGeoCircleProps.radius;
			puntos = Geocercas.NewPolyGeo.puntos;
			finalType = "Poligonal";
        }else if(tipo == "polySquare"){
            perimeter = Geocercas._polyGeoCircleProps.perimeter;
			puntos = Geocercas.NewPolyGeo.puntos;
			finalType = "Poligonal";
        }else if(tipo == "polyComplex"){
			puntos = Geocercas._complexPointsArray;
			Geocercas.complexGroup.removeAll();
			finalType = "Poligonal";
		}else{
			puntos = Geocercas._routePointsArray;
			Geocercas.routeGroup.removeAll();
			Geocercas.routeMarkers.removeAll();
			finalType = "GeoRuta";
        }
        var titulo = $("#nombre_"+tipo).val();
        var color = $("#color_"+tipo).val();
		
		var coordenadas = convertPoints(Geocercas.NewPolyGeo.puntos,false);
	
			$.ajax({
				url:    geoUrl  + "GuardarPuntosGeocerca",
				type:   "POST",
				timeout:10000,
				dataType:"json",
				data: {
					"token":  	window["userData"]["token"],
					"accion":	0,
					"nombre":	titulo,
					"tipo":		finalType,
					"color":	color.substr(1,6),
					"puntos":	coordenadas,
					"puntosEditables":coordenadas
				},
				success: function(data){
					console.log(data);
					try{
						window.response= data.data.d;
					}catch(e){
						
					}
				},
				error: function(data){
					console.log(data);
				}
			});
		
		
		/*Geocercas.ShapeList.push(new GeoShape(tipo, 
            titulo, 
            map.getCenter(), 
            color, 
            puntos,
            radius,
			perimeter,
			Geocercas._visibility));
		
		Geocercas.ShapeInstance.push(new GeoShape(tipo, 
            titulo, 
            map.getCenter(), 
            color, 
            puntos,
            radius,
			perimeter,
			Geocercas._visibility));*/
		/*saveSet("shapeList",Geocercas.ShapeList);
        this.CloseSidebar(tipo);
		this.RemoveObjects();
		Geocercas.ShapeInstance[Geocercas.ShapeInstance.length-1].Load();
        this.LoadShapes();*/
	}
	/**
	 * Carga Geocercas locales.
	 */
    public LoadShapes(){
		Geocercas.objectList.removeAll();
		Geocercas.ShapeInstance = null;
		Geocercas.ShapeInstance = new Array();
        for(var i =0;i<Geocercas.ShapeList.length;i++){
            console.log("Loading "+Geocercas.ShapeList[i]._tipo);
            Geocercas.ShapeInstance.push(new GeoShape(Geocercas.ShapeList[i]._tipo,
                Geocercas.ShapeList[i]._titulo,
                Geocercas.ShapeList[i]._coords,
                Geocercas.ShapeList[i]._color,
                Geocercas.ShapeList[i]._puntos,
				Geocercas.ShapeList[i]._descripcion,
				Geocercas.ShapeList[i]._correos,
				Geocercas.ShapeList[i]._activada));
				Geocercas.ShapeInstance[i].Load();
        }
	}
	/**
	 * Carga una instancia de Geocerca.
	 * @param shapeID Identificador del a instancia de la Geocerca
	 */
	public LoadSingleShape(shapeID : number){
		Geocercas.objectList.removeAll();
		Geocercas.ShapeInstance = null;
		Geocercas.ShapeInstance = new Array();
		console.log("Loading "+Geocercas.ShapeList[shapeID]._tipo);
		Geocercas.ShapeInstance.push(new GeoShape(Geocercas.ShapeList[shapeID]._tipo,
			Geocercas.ShapeList[shapeID]._titulo,
			Geocercas.ShapeList[shapeID]._coords,
			Geocercas.ShapeList[shapeID]._color,
			Geocercas.ShapeList[shapeID]._puntos,
			Geocercas.ShapeList[shapeID]._descripcion,
			Geocercas.ShapeList[shapeID]._correos,
			Geocercas.ShapeList[shapeID]._activada));
			Geocercas.ShapeInstance[shapeID].Load();
	}/**
	 * Abre la vista de la lista de Geocercas.
	 */
	public ShowList() {
		selectMenu(".geos-menu");
		try{
            masterClose = ()=>{this.CloseSidebar('geoShapes');};
        }catch(e){}
		console.log("ShowList()");
		$("#main .mainTop").addClass("no-logo");
		$("#main .mainTop .title").html("<es>Georeferencias</es><en>Georeferences</en>");
		$(".geoShapes .list").html('');
		this.ConsultarGeocercas();
	}
	/**
	 * Inicializa la vista de edicion de Geocerca.
	 * @param shape Identificador de instancia de Geocerca
	 */
	public Edit(shape : number){
		if(SesionEngine.Permisos("EdicionGeocercas") == true) {
			if(Geocercas._editing == false){
				Geos.ConsultarPuntosGeocerca(shape,true);
				$(".GeoRuta h6.geoRuta_header").html("Color");
				$(".geoRuta_title").hide();
				$(".GeoRuta .undoGeoRuta,.otherMethod,.undoPoligonal").removeClass("hidden disabled");
				Geocercas._editShapeID = shape;
				var tipo = Geocercas.ShapeList[shape]._tipo;
				Geocercas._editing = true;
				$("#main .mainTop .title").html("<es>Editando Figura</es><en>Editing Shape</en>");
				$(".geocercas .geoConfig #nombreConfig").val(Geocercas.ShapeList[shape]._titulo);
				$(".geocercas .geoConfig #descConfig").val(Geocercas.ShapeList[shape]._descripcion);
				if(Geocercas.ShapeList[shape]._activada == true){
					$("#activadaConfig").attr("checked",true);
				}else{
					$("#activadaConfig").attr("checked",false);
				}
				$(".geocercas .geoConfig #nombreConfig").addClass("valid");
				$(".geocercas .geoConfig  label[for='nombreConfig']").addClass("active");
				$(".geocercas ."+tipo+",.configGeo,.editOnly").show();
				$(".geocercas ."+tipo).addClass("editando");
				//$("#add-btn").removeClass("hidden").attr("onclick","Geos.SaveShapeChanges()");
				$("#add-btn i").html("save");
				$(".geocercas .geoShapes").hide();
				$("#close-btn").attr("onclick","Geos.CloseEdit()");
				$("#color_"+tipo).val(Geocercas.ShapeList[shape]._color);
				$("#color_"+tipo+"_sel")[0].jscolor.fromString(Geocercas.ShapeList[shape]._color);
				$(".delGeoRuta,.delpolySquare,.delpolyCircle,.delpolyComplex").show();
				//$(".otherMethod,.otherMethodPut,.undoButton").addClass("hidden");
				
				try{
					var correos = Geocercas.ShapeList[shape]._correos.split(",");
					if(correos.length > 0){
						$(".geoMailField").html('');
					}
					$.each(correos,function(i,val){
						Geos.AddGeoConfigMail(val);
					});
					if(correos.length > 0){
						this.AddGeoConfigMail();
					}
				}catch(e){
					this.RestoreGeoConfigMail();
				}

				//Geocercas._visibility = Geocercas.ShapeList[shape]._activada;
				/*if(Geocercas._visibility  == false || Geocercas.ShapeList[shape]._activada == undefined ){
					Geocercas._visibility = false;
					$(".visible"+Geocercas.ShapeList[shape]._tipo+" i").html("visibility_off");
					$(".visible"+Geocercas.ShapeList[shape]._tipo+" span").html("INVISIBLE");
				}else{
					$(".visible"+Geocercas.ShapeList[shape]._tipo+" i").html("visibility_on");
					$(".visible"+Geocercas.ShapeList[shape]._tipo+" span").html("VISIBLE");
				}*/
				this.StartSlider(tipo);
				$("#sidebar").scrollTop(0);
			}
		}
	}
	/**
	 * Cierra vista de edicion de Geocerca.
	 */
	public CloseEdit(){
		console.log("CLOSING EDIT");
		$("#sidebar").scrollTop(0);
		if($(".geoNewTime").is(":visible")){
			$("#sidebar").css("overflow-y","auto");
			$(".geoNewTime").hide();
			$(".geoTimes").show();
			$("#add-btn").attr("onclick","Geos.SaveShapeChanges()").hide();
			$("#main .mainTop .title").html("<es>Horarios</es><en>Schedules</en>");
		}else if($(".geoTimes").is(":visible")){
			$(".geoTimes").hide();
			$("#add-btn").show();
			$("."+Geocercas.ShapeList[Geocercas._editShapeID]._tipo).show();
			$("#main .mainTop .title").html("<es>Editando Figura</es><en>Editing Shape</en>");
		}else if($(".geoNewNotif").is(":visible")){
			$(".geoNewNotif").hide();
			$(".geoNotifs").show();
			$("#add-btn").attr("onclick","Geos.SaveShapeChanges()").hide();
			$("#main .mainTop .title").html("<es>Notificaciones</es><en>Notifications</en>");
		}else if($(".geoNotifs").is(":visible")){
			$(".geoNotifs").hide();
			$("#add-btn").show();
			$("."+Geocercas.ShapeList[Geocercas._editShapeID]._tipo).show();
			$("#main .mainTop .title").html("<es>Editando Figura</es><en>Editing Shape</en>");
		}else if($(".geoConfig").is(":visible")){
			$(".geoConfig").hide();
			$("."+Geocercas.ShapeList[Geocercas._editShapeID]._tipo).show();
		}else{
			console.log("LAST STEP");
			Geocercas.routeMarkers.removeAll();
			$(".GeoRuta h6.geoRuta_header").html("<es>Título</es><en>Name</en>");
			$(".geoRuta_title").show();
			this.RestoreGeoConfigMail();
			if(Geocercas._editShapeID > -1){
				var tipo = Geocercas.ShapeList[Geocercas._editShapeID]._tipo;
				$(".geocercas ."+tipo).removeClass("editando");
				$(".geocercas ."+tipo).hide();	
				Geocercas.ShapeInstance[Geocercas._editShapeID].visible = Geocercas.ShapeList[Geocercas._editShapeID].visible;
				//Geocercas.ShapeInstance[Geocercas._editShapeID]._radius = Geocercas.ShapeList[Geocercas._editShapeID].radius;
				//Geocercas.ShapeInstance[Geocercas._editShapeID]._perimeter = Geocercas.ShapeList[Geocercas._editShapeID].perimeter;
				Geocercas.ShapeInstance[Geocercas._editShapeID].titulo = Geocercas.ShapeList[Geocercas._editShapeID].titulo;
				Geocercas.ShapeInstance[Geocercas._editShapeID].puntos = Geocercas.ShapeList[Geocercas._editShapeID].puntos;
				Geocercas.ShapeInstance[Geocercas._editShapeID].color = Geocercas.ShapeList[Geocercas._editShapeID].color;
				Geocercas.polyGroup.removeObject(Geocercas.ShapeInstance[Geocercas._editShapeID]._instance);
				if(Geocercas.selectedShapes.indexOf(window.geocercas[Geocercas._editShapeID].GeocercaID) != -1) {
					Geocercas.ShapeInstance[Geocercas._editShapeID].Load();
				}
			}
			$("#main .mainTop .title").html("<es>Georeferencias</es><en>Georeferences</en>");
			
			$("#add-btn").addClass("hidden");
			$(".geocercas .geoShapes").show();
			$("#close-btn").attr("onclick","Geos.CloseSidebar('geoShapes')");
			Geocercas._editing = false;
			this.ConsultarGeocercas();
			//this.LoadShapes();
			//this.ZoomObjects();
		}
	}
	/**
	 * Solicita la confirmacion de eliminacion de Geocerca
	 * que esta consultando.
	 */
	public EliminarGeocercas(){
		this.ConfirmDelPoly();
	}
	/**
	 * Actualiza una Geocerca con los cambios realizados
	 * a la misma.
	 */
	public SaveShapeChanges(){
		var tipo = Geocercas.ShapeList[Geocercas._editShapeID]._tipo;
		var finalType = "GeoRuta";
		var valor = 0;
		/*if(tipo == "polySquare"){
			valor = parseInt(Geocercas.polySquareSlider.noUiSlider.get());
			finalType = "Poligonal";
		}
		if(tipo == "polyCircle"){
			valor = parseInt(Geocercas.polyCircleSlider.noUiSlider.get());
			finalType = "Poligonal";
		}*/
		//Geocercas.ShapeList[Geocercas._editShapeID]._radius = valor;
		//Geocercas.ShapeList[Geocercas._editShapeID]._perimeter = valor;
		Geocercas.ShapeList[Geocercas._editShapeID].titulo = $(".geocercas .geoConfig #nombreConfig").val();
		Geocercas.ShapeList[Geocercas._editShapeID].puntos = Geocercas.ShapeInstance[Geocercas._editShapeID]._puntos;
		Geocercas.ShapeList[Geocercas._editShapeID].color = $("#color_"+tipo).val();
		Geocercas.ShapeList[Geocercas._editShapeID].visible = Geocercas.ShapeInstance[Geocercas._editShapeID]._activada;
		
		//Geocercas.ShapeInstance[Geocercas._editShapeID]._radius = Geocercas.ShapeList[Geocercas._editShapeID]._radius;
		//Geocercas.ShapeInstance[Geocercas._editShapeID]._perimeter = Geocercas.ShapeList[Geocercas._editShapeID]._perimeter;
		Geocercas.ShapeInstance[Geocercas._editShapeID].titulo = Geocercas.ShapeList[Geocercas._editShapeID]._titulo;
		Geocercas.ShapeInstance[Geocercas._editShapeID].color = Geocercas.ShapeList[Geocercas._editShapeID]._color;
		Geocercas.ShapeInstance[Geocercas._editShapeID].visible = Geocercas.ShapeList[Geocercas._editShapeID]._activada;
		Geocercas.polyGroup.removeObject(Geocercas.ShapeInstance[Geocercas._editShapeID]._instance);
		Geocercas.ShapeInstance[Geocercas._editShapeID].Load();

		var coordenadas = convertPoints(Geocercas.ShapeInstance[Geocercas._editShapeID]._puntos,true);
		var color = $("#color_"+tipo).val();
		var correos = new Array();
		$(".hasMail .correoConfig").each(function(i,val){
			correos.push($(this).val());
		});
		
		if($(".geoConfig").is(":visible")){ //GUARDA DATOS DE GEOCERCA
			waitSidebar(lang("updatinggeo"));
			$("#add-btn").addClass("hidden");
			$.ajax({
				url:    geoUrl  + "actualizarGeocerca",
				type:   "POST",
				timeout:10000,
				dataType:"json",
				data: {
					"token":  	window["userData"]["token"],
					"nombre":	Geocercas.ShapeList[Geocercas._editShapeID].titulo,
					"tipo":		geocercas[Geocercas._editShapeID].Tipo,
					"descripcion":$("#descConfig").val(),
					"id":		geocercas[Geocercas._editShapeID].GeocercaID,
					"zona":		geocercas[Geocercas._editShapeID].Zona,
					"correos":	correos,
					"activada":	$("#activadaConfig").is(":checked")
				},
				success: function(data){
					console.log(data);
					try{
						window.response = data.data.d;
						if(geocercas[Geocercas._editShapeID].Tipo == "GeoRuta"){
							toasty(lang("georouteupdated"));
						}else{
							toasty(lang("geofenceupdated"));
						}
					}catch(e){
						
					}
					$("#add-btn").removeClass("hidden");
					closeWaitSidebar();
				},
				error: function(data){
					console.log(data);
					closeWaitSidebar();
					toasty(lang("tryagain"));
					$("#add-btn").removeClass("hidden");
				}
			});
		}else{ //GUARDA SOLO PUNTOS DE LA GEOCERCA
			$.ajax({
				url:    geoUrl  + "GuardarPuntosGeocerca",
				type:   "POST",
				timeout:10000,
				dataType:"json",
				data: {
					"token":  	window["userData"]["token"],
					"accion":	2,
					"nombre":	$(".geocercas .geoConfig #nombreConfig").val(),
					"tipo":		finalType,
					"descripcion":$("#descConfig").val(),
					"color":	color.substr(1,6),
					"puntos":	coordenadas,
					"id":		geocercas[Geocercas._editShapeID].GeocercaID,
					"zona":		geocercas[Geocercas._editShapeID].Zona,
					"correos":	correos,
					"activada":	$("#activadaConfig").is(":checked")
				},
				success: function(data){
					console.log(data);
					try{
						window.response= data.data.d;
					}catch(e){
						
					}
				},
				error: function(data){
					console.log(data);
				}
			});
		}
		/*saveSet("shapeList",Geocercas.ShapeList);
		this.ShowList();
		this.CloseEdit();*/
	}
	/**
	 * Agrega un correo para vincularla con la Geocerca
	 * @param correo Correo a vincular con la Geocerca
	 */
	public AddGeoConfigMail(correo:string = ""){
		if(correo == "" && validateEmail($(".geoMailField .input-field.noMail .correoConfig").val()) == false){
			alerta("Error","Por favor, introduzca un correo electrónico válido.");
		}else{
			var id = $(".geoMailField .input-field").length;
			$(".geoMailField .input-field").each(function(i,val){
				$(this).children(".material-icons").attr("onclick","Geos.RemoveGeoConfigMail("+i+")").html('delete_forever');
				$(this).attr("id","geoMail-"+i).attr("mailId",i).addClass("hasMail").removeClass("noMail");
				$(this).children(".correoConfig").attr("disabled",true);
			});
			
			$(".geoMailField").append('<div class="input-field col s6 noMail" id="geoMail-'+id+'" mailId="'+id+'">'+
			'<i class="material-icons prefix" onclick="Geos.AddGeoConfigMail()">add</i>'+
			'<input placeholder=" "  en="Enter an e-mail address" es="Introduzca un correo electrónico" type="text" class="validate correoConfig hasPlaceholder" value="'+correo+'">'+
		'</div>');
		}
	}
	/**
	 * Reinicia el contenedor de correos de Geocerca
	 */
	public RestoreGeoConfigMail(){
		$(".geoMailField").html('<div class="input-field col s6 noMail" id="geoMail-0" mailId="0">'+
		'<i class="material-icons prefix" onclick="Geos.AddGeoConfigMail()">add</i>'+
		'<input placeholder=" "  en="Enter an e-mail address" es="Introduzca un correo electrónico" type="text" class="validate correoConfig hasPlaceholder">'+
	'</div>');
	}
	/**
	 * Retira el correo de la lista de correos ligados a una Geocerca
	 * @param id Identificador del correo en la lista
	 */
	public RemoveGeoConfigMail(id:number){
		$("#geoMail-"+id).remove();
	}
	/**
	 * Abre la vista de Configuracion de Geocerca
	 */
	public Configure() {
		$("#sidebar").scrollTop(0);
		$(".GeoRuta,.Poligonal").hide();
		$(".geoConfig").show();
	}
	/**
	 * Abre vista para Notificaciones de Geocercas
	 */
	public OpenGeoNotifDialog() {
		$("#sidebar").scrollTop(0);
		$("#main .mainTop .title").html("<es>Notificaciones</es><en>Notifications</en>");
		this.ConsultarRelaciones();
		$(".GeoRuta,.Poligonal").hide();
		$(".geoNotifs").show();
	}
	/**
	 * Abre vista para Horarios de Geocercas.
	 */
	public OpenGeoTimesDialog() {
		$("#sidebar").scrollTop(0);
		$("#main .mainTop .title").html("<es>Horarios</es><en>Schedules</en>");
		this.ConsultarHorarios();
		$(".GeoRuta,.Poligonal").hide();
		$(".geoTimes").show();
	}
	/**
	 * Abre vista para crear Notificaciones de Geocercas.
	 */
	public CreateGeoNotif(){
		$('.geoNewNotif input').toggle("slow",function(){
			$('input:checkbox').attr('checked','checked');
	  		$('input:radio').attr('checked','checked');
		},function(){
			$('input:checkbox').removeAttr('checked');
	 		$('input:radio').removeAttr('checked');    
		});
		$("#main .mainTop .title").html("<es>Crear Notificación</es><en>New Notification</en>");
		$(".geoNotifs").hide();
		$(".geoNewNotif").show();
		$(".notifCarList").html('');
		$("#add-btn").attr("onclick","Geos.SaveGeoNotif()").show();
		for(var i =0;i<window.vehiculos.length;i++){
			$(".notifCarList").append('<p>'+
				'<input type="checkbox" class="filled-in" id="notifCar-'+i+'" value="'+window.vehiculos[i].Unidad+'" />'+
				'<label for="notifCar-'+i+'">'+window.vehiculos[i].Descripcion+'</label>'+
		  '</p>');
		}
	}
	/**
	 * Abre vista para Crear Horario de Geocercas.
	 */
	public CreateGeoTime(){
		$("#main .mainTop .title").html("<es>Crear Horario</es><en>New Schedule</en>");
		$(".geoTimes").hide();
		$(".geoNewTime").show();
		$(".delGeoTime").hide();
		$("#add-btn").attr("onclick","Geos.SaveGeoTime()").show();
		$(".timepickerIni").val("08:00AM");
		$(".timepickerFin").val("09:00AM");
		$("#sidebar").css("overflow-y","visible");
	}
	/**
	 * Prepara UI para editar Horario de Geocerca.
	 * @param id Identificador de Horario
	 */
	public EditGeoTime(id:number){
		this.CreateGeoTime();
		$(".delGeoTime").show();
		$(".delGeoTime .btn-large").attr("onclick","Geos.ConfirmDelGeoNotif("+id+")");
		var index = objectIndex(geoTimes,"restriccionID",id);
		var hInicial = new moment(geoTimes[index].hInicial);
		var hFinal = new moment(geoTimes[index].hFinal);
		$(".timepickerIni").val(hInicial.format("hh:mmA"));
		$(".timepickerFin").val(hFinal.format("hh:mmA"));
		$("#add-btn").attr("onclick","Geos.SaveGeoTime("+id+")").show();
	}
	/**
	 * Guarda Relacion de Notificaciones para la Geocerca abierta.
	 */
	public SaveGeoNotif(){
		
		var relaciones = new Array();
		var tipo ="";
		var servicio = "";

		$(".notifCarList input:checked").each(function(i,val){
			relaciones.push($(this).val());
		});
		if($("#geoSystemNotif:checked").val() == "on" && $("#geoMailNotif:checked").val() == "on"){
			tipo = 12;
		}else if($("#geoSystemNotif:checked").val() == "on"){
			tipo = 1;
		}else if($("#geoMailNotif:checked").val() == "on"){
			tipo = 2;
		}else{
			tipo = -1;
		}
		
		servicio = $("input[name='geoAlert']:checked").val();
		if(tipo == -1){
			alerta(lang("error"),lang("alerttypeplz"));
		}else if(servicio == undefined){
			alerta(lang("error"),lang("serviceplz"));
		}else if(relaciones.length == 0){
			alerta(lang("error"),lang("relationsplz"));
		}else{
			$("#add-btn").hide();
			waitSidebar("<es>Creando Notificación...</es><en>Creating Notification...</en>");
			$.ajax({
				url:    geoUrl  + "NuevaRelacion",
				type:   "POST",
				timeout:10000,
				dataType:"json",
				data: {
					"token":  		window["userData"]["token"],
					"id":			geocercas[Geocercas._editShapeID].GeocercaID,
					"servicio":		servicio,
					"tipo":			tipo,
					"relaciones":	relaciones
				},
				success: function(data){
					closeWaitSidebar();
					console.log(data);
					window.response = data.d;
					Geos.CloseEdit();
					Geos.ConsultarRelaciones();
					toasty("<es>Se ha creado una Notificacion para esta Georeferencia.</es><en>A Notification has been created for this Georeference.</en>");
				},
				error:function(data){
					closeWaitSidebar();
					toasty("<es>Se ha producido un error, por favor intente de nuevo más tarde.</es><en>An error has occurred, please try again later.</en>");
				}
			});
		}
	}
	/**
	 * Crea o edita los Horarios de una Geocerca abierta.
	 * @param restID Identificador de restriccion de Horario, si es -1 es nuevo
	 * y se crea, de lo contrario se guarda.
	 */
	public SaveGeoTime(restID : number = -1){
		
		var d = new Date();
		if(timeVal($(".timepickerIni").val()) >=  timeVal($(".timepickerFin").val()) ){
			alerta(lang("error"),lang("endtimeissooner"));
		}else{
			$("#add-btn").hide();
			if(restID == -1){
				waitSidebar("<es>Creando Horario...</es><en>Creating Schedule...</en>");
				$.ajax({
					url:    geoUrl  + "GuardarRestriccion",
					type:   "POST",
					timeout:10000,
					dataType:"json",
					data: {
						"token":  		window["userData"]["token"],
						"id":			geocercas[Geocercas._editShapeID].GeocercaID,
						"hIni":			d.getFullYear()+"-"+fix(d.getMonth()+1)+"-"+fix(d.getDate())+" "+fixTime($(".timepickerIni").val()),
						"hFin":			d.getFullYear()+"-"+fix(d.getMonth()+1)+"-"+fix(d.getDate())+" "+fixTime($(".timepickerFin").val())
					},
					success: function(data){
						closeWaitSidebar();
						console.log(data);
						window.response = data.d;
						Geos.CloseEdit();
						Geos.ConsultarHorarios();
						toasty("<es>Se ha creado un Horario para esta Georeferencia.</es><en>A Schedule has been created for this Georeference.</en>");
					},
					error:function(data){
						closeWaitSidebar();
						toasty("<es>Se ha producido un error, por favor intente de nuevo más tarde.</es><en>An error has occurred, please try again later.</en>");
					}
				});
			}else{
				waitSidebar("<es>Guardando Horario...</es><en>Saving Schedule...</en>");
				$.ajax({
					url:    geoUrl  + "GuardarRestriccion",
					type:   "POST",
					timeout:10000,
					dataType:"json",
					data: {
						"token":  		window["userData"]["token"],
						"restID":		restID,
						"id":			geocercas[Geocercas._editShapeID].GeocercaID,
						"hIni":			d.getFullYear()+"-"+fix(d.getMonth()+1)+"-"+fix(d.getDate())+" "+fixTime($(".timepickerIni").val()),
						"hFin":			d.getFullYear()+"-"+fix(d.getMonth()+1)+"-"+fix(d.getDate())+" "+fixTime($(".timepickerFin").val())
					},
					success: function(data){
						closeWaitSidebar();
						console.log(data);
						window.response = data.d;
						Geos.CloseEdit();
						Geos.ConsultarHorarios();
						toasty("<es>Se han guardado los cambios en el Horario.</es><en>The changes have been saved for this Schedule.</en>");
					},
					error:function(data){
						closeWaitSidebar();
						toasty("<es>Se ha producido un error, por favor intente de nuevo más tarde.</es><en>An error has occurred, please try again later.</en>");
					}
				});
			}
		}
	}
	/**
	 * Obtiene Notificaciones de Georeferencias
	 * para la Geocerca abierta.
	 */
	public ConsultarRelaciones(){
		$("#add-btn").hide();
		$(".geoNotifLists").html('');
		waitSidebar("<es>Cargando Notificaciones de Georeferencia...</es><en>Loading Georeference Notifications...</en>");
		$.ajax({
			url:    geoUrl  + "ConsultarRelaciones",
			type:   "POST",
			timeout:10000,
			dataType:"json",
			data: {
				"token":  	window["userData"]["token"],
				"id":		geocercas[Geocercas._editShapeID].GeocercaID
			},
			success: function(data){
				console.log(data);
				try{
					window.geoNotifs = JSON.parse(data.data.d);
					if(window.geoNotifs.length == 0){
						$(".geoNotifs .noContent").html("<es>No hay Notificaciones programadas</es><en>No scheduled Notifications available</en>").removeClass("hidden");
					}else{
						$(".geoNotifs .noContent").addClass("hidden");
						for(var i=0;i<window.geoNotifs.length;i++){
							var servicio = "";
							var tipoNotif = "";
							switch(geoNotifs[i].ServicioSolicitado){
								case 0:
									servicio = "<es>Entrada/Salida</es><en>Enters &amp; Leaves</en>";
									icon ="swap_vertical_circle";
								break;
								case 1:
									servicio = "<es>Entrada</es><en>Enters</en>";
									icon = "radio_button_checked";
								break;
								case 2:
									servicio = "<es>Salida</es><en>Leaves</en>";
									icon = "all_out";
								break;
							}
							switch(geoNotifs[i].TipoNotificacion){
								case 12:
									tipoNotif = "<es>Sistema/Correo</es><en>System/Mail</en>";
								break;
								case 1:
									tipoNotif = "<es>Sistema</es><en>System</en>";
								break;
								case 2:
									tipoNotif = "<es>Correo</es><en>Mail</en>";
								break;
							}
							$(".geoNotifLists").append('<li class="no-effect" id="geoNotif-'+geoNotifs[i].UnidadSesionID+'">'+
								'<div class="left-icon"><i class="material-icons">'+icon+'</i></div>'+
								'<div class="textBox">'+
									'<div class="listText">'+geoNotifs[i].Descripcion+'</div>'+
									'<div class="listText listSubtext">'+servicio+'</div>'+
									'<div class="listText listSubtext">'+tipoNotif+'</div>'+
								'</div>'+
								'<div class="chevron" onclick="Geos.ConfirmDelGeoNotif('+geoNotifs[i].UnidadSesionID+')"><i class="material-icons">delete_forever</i></div>'+
							'</li>');
						}
					}
					closeWaitSidebar();
				}catch(e){
					
				}
			},
			error: function(data){
				console.log(data);
				if($(".geoNotifs").is(":visible")){
					waitSidebar("<es>Reintentando...</es><en>Retrying...</en>");
					console.log("Retrying...");
					setTimeout(function(){
						Geos.ConsultarRelaciones();
					},3000);
				}
			}
		});
	}
	/**
	 * Obtiene los Horarios de la Geocerca abierta.
	 */
	public ConsultarHorarios(){
		$("#add-btn").hide();
		$(".geoTimesList").html('');
		waitSidebar("<es>Cargando Horarios de Georeferencia...</es><en>Loading Georeference Schedules...</en>");
		$.ajax({
			url:    geoUrl  + "ConsultarRestricciones",
			type:   "POST",
			timeout:10000,
			dataType:"json",
			data: {
				"token":  	window["userData"]["token"],
				"id":		geocercas[Geocercas._editShapeID].GeocercaID
			},
			success: function(data){
				console.log(data);
				try{
					window.geoTimes = JSON.parse(data.data.d);
					if(window.geoTimes.length == 0){
						$(".geoTimes .noContent").html("<es>No hay Horarios programados</es><en>No Schedules available</en>").removeClass("hidden");
					}else{
						$(".geoTimes .noContent").addClass("hidden");
						for(var i=0;i<window.geoTimes.length;i++){
							var stage = " AM";
							var hInicial = new moment(window.geoTimes[i].hInicial);
							var hFinal = new moment(window.geoTimes[i].hFinal);

							$(".geoTimesList").append('<li class="no-effect" id="geoTime-'+geoTimes[i].restriccionID+'">'+
								'<div class="left-icon"><i class="material-icons">timer</i></div>'+
								'<div class="textBox">'+
									'<div class="listSubtext"><es>Hora Inicial</es><en>Start Time</en>: '+hInicial.format("hh:mm A")+'</div>'+
									'<div class="listSubtext"><es>Hora Final</es><en>End Time</en>: '+hFinal.format("hh:mm A")+'</div>'+
								'</div>'+
								'<div class="chevron" onclick="Geos.EditGeoTime('+geoTimes[i].restriccionID+')"><i class="material-icons">more_horiz</i></div>'+
							'</li>');
						}
					}
					closeWaitSidebar();
				}catch(e){
					console.log(e);
					closeWaitSidebar();
				}
			},
			error: function(data){
				console.log(data);
				if($(".geoTimes").is(":visible")){
					waitSidebar("<es>Reintentando...</es><en>Retrying...</en>");
					console.log("Retrying...");
					setTimeout(function(){
						Geos.ConsultarHorarios();
					},3000);
				}
			}
		});
	}
	/**
	 * Pide confirmacion al usuario para remover Notificacion de Geocerca.
	 * @param id Identificador de Notificacion de Geocerca
	 */
	public ConfirmDelGeoNotif(id:number){
		confirmar(lang("delnotif"),lang("delnotifsure"),lang("remove"),lang("cancel"),"Geos.DelGeoNotif("+id+")");
	}
	/**
	 * Elimina Notificacion de Geocerca de plataforma.
	 * @param id Identificador de Notificacion de Geocerca
	 */
	public DelGeoNotif(id:number){
		waitSidebar("<es>Eliminando Notificación...</es><en>Removing Notification...</en>");
		$.ajax({
			url:    geoUrl  + "EliminarRelacion",
			type:   "POST",
			timeout:10000,
			dataType:"json",
			data: {
				"token":  	window["userData"]["token"],
				"relacion":		id
			},
			success: function(data){
				console.log(data);
				$("#geoNotif-"+id).remove();
				closeWaitSidebar();
				if($(".geoNotifLists li").length == 0){
					$(".geoNotifs .noContent").html("<es>No hay Notificaciones programadas</es><en>No Notifications scheduled</en>").removeClass("hidden");
				}
			},
			error:function(data){

			}
		});
	}
	/**
	 * Pide confirmacion al usuario para eliminar Horario de Geocerca.
	 * @param id Identificador de Horario de Geocerca
	 */
	public ConfirmDelGeoTime(id:number){
		confirmar(lang("delschedule"),lang("delschedulesure"),lang("remove"),lang("cancel"),"Geos.DelGeoTime("+id+")");
	}
	/**
	 * Elimina Horario / Restriccion de Geocerca de plataforma.
	 * @param id Identificador de Horario de Geocerca
	 */
	public DelGeoTime(id:number){
		waitSidebar("<es>Eliminando Horario...</es><en>Removing Schedule...</en>");
		$.ajax({
			url:    geoUrl  + "EliminarRestricciones",
			type:   "POST",
			timeout:10000,
			dataType:"json",
			data: {
				"token":  	window["userData"]["token"],
				"time":		id
			},
			success: function(data){
				console.log(data);
				Geos.CloseEdit();
				$("#geoTime-"+id).remove();
				closeWaitSidebar();
				if($(".geoTimesList li").length == 0){
					$(".geoTimes .noContent").html("<es>No hay Horarios programados</es><en>No Schedules available</en>").removeClass("hidden");
				}
			},
			error:function(data){

			}
		});
	}
	/**
	 * Oculta instancias de Geocerca en el mapa.
	 */
	public HideObjects(){
		for(var i = 0;i<Geocercas.ShapeInstance.length;i++){
			Geocercas.ShapeInstance[i]._activada = false;
		}
		this.ReloadVisibility();
	}
	/**
	 * Establece visibilidad de las instancias de Geocerca
	 * segun su parametro _activada.
	 */
	public ReloadVisibility(){
		for(var i = 0;i<Geocercas.ShapeInstance.length;i++){
			Geocercas.ShapeInstance[i]._instance.setVisibility(Geocercas.ShapeInstance[i]._activada);
		}
	}
	/**
	 * Alterna visibilidad de Geocerca en general (no se aplica a la instancia, solo al UI).
	 */
	public ToggleEditVisibility(){
		if(Geocercas._editing == true){
			if(Geocercas._visibility == false) {
				Geocercas._visibility = true;
				$(".visible"+Geocercas.ShapeList[Geocercas._editShapeID]._tipo+" i").html("visibility_on");
				$(".visible"+Geocercas.ShapeList[Geocercas._editShapeID]._tipo+" span").html("VISIBLE");
			}else{
				Geocercas._visibility = false;
				$(".visible"+Geocercas.ShapeList[Geocercas._editShapeID]._tipo+" i").html("visibility_off");
				$(".visible"+Geocercas.ShapeList[Geocercas._editShapeID]._tipo+" span").html("INVISIBLE");
			}
		}else{
			if(Geocercas._visibility == false) {
				Geocercas._visibility = true;
				$(".visible"+Geocercas._creatingType+" i").html("visibility_on");
				$(".visible"+Geocercas._creatingType+" span").html("VISIBLE");
			}else{
				Geocercas._visibility = false;
				$(".visible"+Geocercas._creatingType+" i").html("visibility_off");
				$(".visible"+Geocercas._creatingType+" span").html("INVISIBLE");
			}
		}
	}
	/**
	 * Enfoca la vista del mapa en una Geocerca indicada.
	 * @param shape Identificador de instancia de Geocerca
	 */
	public FocusShape (shape : number){
		this.HideObjects();
		try{
			Geocercas.ShapeInstance[shape]._activada = true;
			//this.ReloadVisibility();
			if(Geocercas.ShapeList[shape]._tipo != "GeoRuta"){
				map.setCenter(Geocercas.ShapeList[shape]._coords);
				map.setViewBounds(Geocercas.ShapeInstance[shape]._instance.getBounds());
			}
			if(Geocercas.ShapeList[shape]._tipo == "GeoRuta"){
				map.setViewBounds(Geocercas.ShapeInstance[shape]._instance.getBounds());
			}
		}catch(e){}
	}
	/**
	 * Abre vista para creación de GeoRuta
	 */
	public CreateRoute() {
		if(Geocercas._CreatingShape == false){
			$("#main .mainTop .title").html("<es>Crear Ruta</es><en>New Route</en>");
			$("#main .mainTop").addClass("no-logo");
            Geocercas._routePointsArray = new Array();
			Geocercas._CreatingShape = true;
            Geocercas._CreatingRoute = true;
			this.OpenSidebar("GeoRuta");
			$(".editOnly").hide();
			$(".undoButton,.otherMethodPut,.otherMethod").removeClass("hidden");
			Geocercas._inputMethod = 1;
			this.TogglePointInput();
		}
	}
	/**
	 * Agrega un punto en GeoRuta basandose en
	 * la vista de mapa actual.
	 */
	public AddRoutePointCenter(){
		var coords = map.getCenter();
		if(Geocercas._editShapeID == -1){
			this.AddRoutePoint(coords.lat,coords.lng);
		}else{
			Geocercas.ShapeInstance[Geocercas._editShapeID].puntos.push({
				lat:coords.lat,
				lng:coords.lng
			});
			Geocercas.ShapeInstance[Geocercas._editShapeID].Reload();
			this.PlaceStartFinishMarkers();
		}
	}
	/**
	 * Alterna entre el metodo de creacion de punto:
	 * _inputMethod == 0 : Centro de mapa
	 * _inputMethod == 1 : Tap/Click
	 */
	public TogglePointInput(){
		if(Geocercas._inputMethod == 0){ //TAP a CENTER
			Geocercas._inputMethod = 1;
			$(".otherMethodPut").removeClass("hidden");
			$(".otherMethod i").html("touch_app");
			$(".centerAim").show();
		}else{ //CENTER a TAP
			Geocercas._inputMethod = 0; 
			$(".otherMethodPut").addClass("hidden");
			$(".otherMethod i").html("add_circle_outline");
			$(".centerAim").hide();
		}
	}
	/**
	 * Agrega un punto nuevo a una GeoRuta.
	 * @param _lat Latitud de punto nuevo en GeoRuta
	 * @param _lng Longitud de punto nuevo en GeoRuta
	 */
	public AddRoutePoint(_lat:number,_lng:number){
		if(Geocercas._editing == true){
			
			Geocercas.ShapeInstance[Geocercas._editShapeID].puntos.push({
				lat:_lat,
				lng:_lng
			});
			Geocercas.ShapeInstance[Geocercas._editShapeID].Reload();
			
			this.PlaceStartFinishMarkers();
		}else{
			Geocercas._routePointsArray.push({lat:_lat, lng:_lng});
			Geocercas._routePoints.pushPoint(Geocercas._routePointsArray[Geocercas._routePointsArray.length-1]);
			try{
				Geocercas.routeGroup.removeAll();
			}catch(e){}

			Geocercas.routeMarkers.removeAll();

			if(Geocercas._routePoints.getPointCount() == 1){
				//MARKER INICIO
				var markerInicio = new H.map.Marker(
					{
						lat:Geocercas._routePoints.extractPoint(0).lat, 
						lng:Geocercas._routePoints.extractPoint(0).lng
					},
					{
						icon: new H.map.Icon('img/marker-green.png')
					}
				);
				Geocercas.routeMarkers.addObject(markerInicio);
			}
			if(Geocercas._routePoints.getPointCount() > 1){
				Geocercas.polyRoute = null;
				this.UpdateRouteMaker();
			}
		}
		$(".undoGeoRuta").removeClass("disabled");
	}
	/**
	 * Actualiza la GeoRuta en el mapa
	 */
    public UpdateRouteMaker(){
		if(Geocercas._routePoints.getPointCount() > 1){
			Geocercas.polyRoute = new H.map.Polyline(
				Geocercas._routePoints, 
				{ 
					style: { 
						lineWidth: 6,
						fillColor: hexToRgbA($("#color_GeoRuta").val(),0.6),
						strokeColor: hexToRgbA($("#color_GeoRuta").val(),0.6)
					},
					arrows: { 
						fillColor: 'white', 
						frequency: 15, 
						width: 0.8, 
						length: 0.7 
					}
				}
			);
		}
		if(Geocercas._routePoints.getPointCount() > 0){
			//MARKER INICIO
			var markerInicio = new H.map.Marker(
				{
					lat:Geocercas._routePoints.extractPoint(0).lat, 
					lng:Geocercas._routePoints.extractPoint(0).lng
				},
				{
					icon: new H.map.Icon('img/marker-green.png')
				}
			);
			Geocercas.routeMarkers.addObject(markerInicio);
		}
		if(Geocercas._routePoints.getPointCount() > 1){
			//MARKER FINAL
			var markerFinal = new H.map.Marker(
				{
					lat:Geocercas._routePoints.extractPoint(Geocercas._routePoints.getPointCount()-1).lat, 
					lng:Geocercas._routePoints.extractPoint(Geocercas._routePoints.getPointCount()-1).lng
				},
				{
					icon: new H.map.Icon('img/marker-red.png')
				}
			);
			Geocercas.routeMarkers.addObject(markerFinal);
		}
		Geocercas.routeGroup.removeAll();
        Geocercas.routeGroup.addObject(Geocercas.polyRoute);
	}
	/**
	 * CTRL+Z al ultimo punto agregado a la GeoRuta.
	 */
	public UndoLastRoutePoint(){
		try{
			if(Geocercas._editShapeID == -1){
				if(Geocercas._routePoints.getPointCount()>0){
					Geocercas._routePoints.removePoint(Geocercas._routePoints.getPointCount()-1);
					if(Geocercas._routePoints.getPointCount() == 0){
						$(".undoGeoRuta").addClass("disabled");
					}
					Geocercas.routeMarkers.removeAll();
					Geocercas.routeGroup.removeAll();
					this.UpdateRouteMaker();
				}
			}else{
				Geocercas.ShapeInstance[Geocercas._editShapeID].puntos.splice(Geocercas.ShapeInstance[Geocercas._editShapeID].puntos.length-1,1);
				Geocercas.ShapeInstance[Geocercas._editShapeID].Reload();
				if(Geocercas.ShapeInstance[Geocercas._editShapeID].puntos.length == 0){
					$(".undoGeoRuta").addClass("disabled");
				}else{
					this.PlaceStartFinishMarkers();
				}
			}
		}catch(e){}
	}
	/**
	 * Coloca marcadores de inicio de ruta y fin de ruta
	 * en GeoRuta en el mapa.
	 */
	public PlaceStartFinishMarkers() {
		Geocercas.routeMarkers.removeAll();
		var markerInicio = new H.map.Marker(
			{
				lat:Geocercas.ShapeInstance[Geocercas._editShapeID].puntos[0].lat, 
				lng:Geocercas.ShapeInstance[Geocercas._editShapeID].puntos[0].lng
			},
			{
				icon: new H.map.Icon('img/marker-green.png')
			}
		);
		Geocercas.routeMarkers.addObject(markerInicio);
		if(Geocercas.ShapeInstance[Geocercas._editShapeID].puntos.length > 1){
			var markerFinal = new H.map.Marker(
				{
					lat:Geocercas.ShapeInstance[Geocercas._editShapeID].puntos[Geocercas.ShapeInstance[Geocercas._editShapeID].puntos.length-1].lat, 
					lng:Geocercas.ShapeInstance[Geocercas._editShapeID].puntos[Geocercas.ShapeInstance[Geocercas._editShapeID].puntos.length-1].lng
				},
				{
					icon: new H.map.Icon('img/marker-red.png')
				}
			);
			Geocercas.routeMarkers.addObject(markerFinal);
		}
	}
	/**
	 * Pide confirmacion al usuario para eliminar GeoRuta.
	 */
	public ConfirmDelRoute(){
		confirmar(lang("delroute"),lang("delroutesure"),lang("remove"),lang("cancel"),"Geos.DeletePoly()");
	}
	/**
	 * Elimina GeoRuta localmente.
	 */
	public DeleteRoute(){
		Geocercas.polyGroup.removeObject(Geocercas.ShapeInstance[Geocercas._editShapeID]._instance);
			Geocercas.ShapeInstance.splice(Geocercas._editShapeID,1);
			Geocercas.ShapeList.splice(Geocercas._editShapeID,1); 
			//saveSet("shapeList",Geocercas.ShapeList);
			$(".geocercas .GeoRuta").hide();	
			Geocercas._editShapeID = -1;
			this.ShowList();
			this.CloseEdit();
	}
	/**
	 * Pide confirmacion al usuario para eliminar Geocerca.
	 */
	public ConfirmDelPoly(){
		confirmar(lang("delgeo"),lang("delgeosure"),lang("remove"),lang("cancel"),"Geos.DeletePoly()");
	}
	/**
	 * Elimina una o varias Geocercas de plataforma.
	 */
	public DeletePoly(){
		var geoLista = new Array();
		geoLista.push({
			"GeocercaID":geocercas[Geocercas._editShapeID].GeocercaID,
			"Zona":		window.zona
		});
		$.ajax({
			url:    geoUrl  + "EliminarGeocercas",
			type:   "POST",
			timeout:10000,
			dataType:"json",
			data: {
				"token":  	window["userData"]["token"],
				"request":	JSON.stringify(geoLista)
			},
			success: function(data){
				console.log(data);
				try{
					window.response = data.data.d;
				}catch(e){
					
				}
			},
			error: function(data){
				console.log(data);
			}
		});
		/*Geocercas.objectList.removeObject(Geocercas.ShapeInstance[Geocercas._editShapeID]._instance);*/
		Geocercas.ShapeInstance.splice(Geocercas._editShapeID,1);
		Geocercas.ShapeList.splice(Geocercas._editShapeID,1); 
		//saveSet("shapeList",Geocercas.ShapeList);
		$(".geocercas .polySquare,.geocercas .polyCircle").hide();	
		Geocercas._editShapeID = -1;
		//this.ShowList();
		this.ShowList();
		this.CloseEdit();
	}
	public AddMarkerWithCoords(_lat:number,_lng:number){
		if(Geocercas._CreatingRoute == false){
			Geocercas.Markers.push({"marker":new H.map.Marker({lat:_lat, lng:_lng},
					{
						icon: new H.map.Icon(svgColorMarker
						.replace("__FILLCOLOR__", "#FF0000")
						.replace("__TEXT__", "")
						.replace("__FONTSIZE__", 14))
					}
				) 
			});
			//map.addObject(Geocercas.Markers[Geocercas.Markers.length-1].marker);
		
			Geocercas.testGroup.addObject(Geocercas.Markers[Geocercas.Markers.length-1].marker);
		}
	}
	/**
	 * Limpia el UI de la creacion de Geocerca Circular.
	 */
	public RemovePolyCircle() {
		try{
			map.removeObject(Geocercas.geoPolyMarker);
		}catch(e){}
		Geocercas._CreatingShape = false;
		for(var i = 0; i<Geocercas._geoEvents.length;i++){
			if(Geocercas._geoEvents[i].target == "polyCircle"){
				map.removeEventListener(Geocercas._geoEvents[i].type, Geocercas._geoEvents[i].function,false);
			}
		}
		$(".centerMarker").hide();
	}
	/**
	 * Limpia el UI de la creacion de Geocerca Cuadrada.
	 */
	public RemovePolySquare() {
		try{
			map.removeObject(Geocercas.geoSquareMarker);
		}catch(e){}
		Geocercas._CreatingShape = false;
		for(var i = 0; i<Geocercas._geoEvents.length;i++){
			if(Geocercas._geoEvents[i].target == "polySquare"){
				map.removeEventListener(Geocercas._geoEvents[i].type, Geocercas._geoEvents[i].function,false);
			}
		}
		$(".centerMarker").hide();
	}
	/**
	 * Limpia el UI de la creacion de Geocerca Compleja.
	 */
	public RemovePolyComplex() {
		try{
			map.removeObject(Geocercas.complexMarker);
		}catch(e){}
		Geocercas._CreatingShape = false;
		Geocercas._CreatingComplex = false;
		for(var i = 0; i<Geocercas._geoEvents.length;i++){
			if(Geocercas._geoEvents[i].target == "polyCircle"){
				map.removeEventListener(Geocercas._geoEvents[i].type, Geocercas._geoEvents[i].function,false);
			}
		}
		$(".centerMarker").hide();
	}
	/**
	 * Remueve objetos relacionados con Geocercas del mapa.
	 */
	public RemoveObjects(){
		//Geocercas._CreatingShape = false;
		//Geocercas._CreatingRoute = false;
		/*try{
			for(var i=0;i<Geocercas.Circles.length;i++){
				map.removeObject(Geocercas.Circles[i].circle);
			}
		}catch(e){}
		try{
			for(var i=0;i<Geocercas.Markers.length;i++){
				map.removeObject(Geocercas.Markers[i].marker);
			}
		}catch(e){}*/
		Geocercas._routePoints = null;
		Geocercas._routePoints = new H.geo.LineString();
		Geocercas.testGroup.removeAll();
		//Geocercas.routeGroup.removeAll();
		//this.ResetArrays();
		/*try{
			map.removeObject(Geocercas.geoPolyMarker);
		}catch(e){}
		try{
			map.removeObject(Geocercas.geoSquareMarker);
		}catch(e){}
		$(".centerMarker").hide();
		this.RemoveListeners();*/
	}
	/**
	 * Desvincula los Listeners de las Geocercas.
	 */
	public RemoveListeners() {
		for(var i=0;i<Geocercas._geoEvents.length;i++){
			map.removeEventListener(Geocercas._geoEvents[i].type, Geocercas._geoEvents[i].function,false);
		}
	}
	/**
	 * Centra y contiene la vista del mapa en 
	 * las Geocercas impresas en el mapa.
	 */
	public ZoomObjects() {
		try{
			map.setViewBounds(Geocercas.objectList.getBounds(),false);
		}catch(e){}
	}
	/**
	 * Establece el color del Pin indicador.
	 * @param hex Valor hexadecimal del color del Pin indicador (sin #)
	 */
	public SetCenterMarkerColor(hex : string){
		$(".pin").css("background","#"+hex);
		$(".pin:after").css("box-shadow","0 0 1px 2px #"+hex);
	}
	/**
	 *  Regresa el objeto con los Circulos de las Geocercas.
	 */
	public GetCircles() : object[]{
		return Geocercas.Circles;
	}
}