/**
 * GEOSHAPE
 * Esta clase maneja la instancia de una Georeferencia en la app
 */
class GeoShape{
    private _titulo : string = "";
    private _coords : any = null;
    private _puntos : any = null;
    private _color : string = "";
    private _radius : number = 200;
    private _perimeter : number = 200;
    private _descripcion : string = "";
    private _correos : string;
    private _tipo : string = "";
    private _fecha : any;
    private _activada = false;
    private _instance : any = null;
    private _edited = true;

    //GENERICO
    //Creamos una GeoForma si se requiere.
    constructor(tipo : string, titulo : string, coords : any, color : string, 
        puntos : any, descripcion : string, correos: string, activada : boolean) {
        this._titulo =  titulo;
        this._tipo =    tipo;
        this._coords =  coords;
        this._color =   color;
        this._puntos =  puntos;
        this._correos = correos;
        this._descripcion = descripcion;
        this._activada = activada;
        this._fecha =   Date.now();
    }

    /**
     * Regresa el titulo o nombre de la GeoForma
     */
    get titulo():string {
        return this._titulo;
    }
    /**
     * Asigna un titulo o nombre a la GeoForma
     */
    set titulo(titulo:string) {
        this._titulo = titulo;
        this._edited = true;
    }
    /**
     * Obtiene las coordenadas de la GeoForma
     */
    get coords():any {
        return this._coords;
    }
    /**
     * Asigna las coordenadas de la GeoForma
     */
    set coords(coords:any) {
        this._coords = coords;
        this._edited = true;
    }
    /**
     * Obtiene los puntos de la GeoForma (poligonal)
     */
    get puntos():any {
        return this._puntos;
    }
    /**
     * Asigna los puntos de la GeoForma (poligonal)
     */
    set puntos(puntos:any) {
        this._puntos = puntos;
        this._edited = true;
    }
    /**
     * Obtiene el color principal de la GeoForma
     */
    get color():string {
        return this._color;
    }
    /**
     * Asigna el color principal de la GeoForma
     */
    set color(color:string) {
        this._color = color;
        this._edited = true;
    }
    /**
     * Obtiene el radio de la GeoForma (circular)
     */
    get radius():number {
        return this._radius;
    }
    /**
     * Asigna el radio del a GeoForma (circular)
     */
    set radius(radius:number) {
        this._radius = radius;
        this._edited = true;
    }
    /**
     * Obtiene el perimetro de la GeoForma
     */
    get perimeter():number {
        return this._perimeter;
    }
    /**
     * Asigna el perimetro de la GeoForma
     */
    set perimeter(perimeter:number) {
        this._perimeter = perimeter;
        this._edited = true;
    }
    /**
     * Obtiene el tipo de la GeoForma (cuadrada, circular, poligonal, georuta)
     */
    get tipo():string {
        return this._tipo;
    }
    /**
     * Asigna el tipo de la GeoForma
     */
    set tipo(tipo:string) {
        this._tipo = tipo;
        this._edited = true;
    }
    /**
     * Obtiene la instancia de la GeoForma
     */
    get instance():any {
        return this._instance;
    }
    /**
     * Asigna la instancia de la GeoForma
     */
    set instance(instance:any) {
        this._instance = instance;
    }
    /**
     * Obtiene la bandera que indica si la GeoForma 
     * se está editando
     */
    get edited():boolean {
        return this._edited;
    }
    /**
     * Asigna la bandera a la instancia de la GeoForma
     * que indica si se está editando
     */
    set edited(edited:boolean) {
        this._edited = edited;
    }
    /**
     * Obtiene la descripcion de la GeoForma
     */
    get descripcion():string {
        return this._descripcion;
    }
    /**
     * Obtiene la descripcion de la GeoForma
     */
    set descripcion(descripcion:string) {
        this._descripcion = descripcion;
    }
    /**
     * Obtiene la bandera que indica si la GeoForma esta activada.
     * Si no lo está no se muestra en el mapa
     */
    get activada():boolean {
        return this._activada;
    }
    /**
     * Asigna a la instancia de la GeoForma si esta activada o no
     */
    set activada(activada:boolean) {
        this._activada = activada;
    }
    /**
     * Carga la instancia actual de la GeoForma
     */
    public Load(): any{
        //console.log("Loading shape..."+this._tipo);
        switch(this._tipo){
            case "polyCircle":
                //console.log("Loading Poly Circle...");
                return this.LoadPolyCircle();
            break;
            case "polySquare":
                //console.log("Loading Square...");
                return this.LoadSquare();
            break;
            case "geoRoute":
            case "GeoRuta":
                //console.log("Loading Route...");
                return this.LoadRoute();
            break;
            case "polyComplex":
            case "Poligonal":
                return this.LoadComplex();
            break;
        }
    }
    /**
     * [DEPRECATED]
     * Recarga la instancia de la GeoForma
     */
    private Reload(){
        Geocercas.polyGroup.removeObject(this._instance);
        this._instance = null;
        this._edited = true;
        this.Load();
    }
    /**
     * Carga una GeoForma Compleja (Poligonal) en el mapa
     */
    private LoadComplex(): any{
        if(this._edited == true){
            this._edited = false;
            var puntos = new H.geo.LineString();
            for(var i= 0;i<this._puntos.length;i++){
                puntos.pushPoint({lat:this.puntos[i].lat, lng:this.puntos[i].lng});
            }
            this._instance = new H.map.Polygon(
                puntos, 
                { 
                    style: { 
                        lineWidth: 2,
                        fillColor: hexToRgbA(this._color,0.3),
                        strokeColor: '#FFF'
                    }
                }
            );
            //this._instance.setVisibility(this._activada);
            Geocercas.polyGroup.addObject(this._instance);
        }
        return this._instance;
    }
    /**
     * Carga una instancia de GeoRuta en el mapa
     */
    private LoadRoute(): any{
        if(this._edited == true){
            this._edited = false;
            var puntos = new H.geo.LineString();
            for(var i= 0;i<this._puntos.length;i++){
            puntos.pushPoint({lat:this.puntos[i].lat, lng:this.puntos[i].lng});
            }

            this._instance = new H.map.Polyline(
                puntos, 
                { 
                    style: { 
                        lineWidth: 6,
                        strokeColor: hexToRgbA(this._color,0.6),
                        fillColor: hexToRgbA(this._color,0.6),
                        },
                    arrows: { 
                        fillColor: 'white', 
                        frequency: 15, 
                        width: 2, 
                        length: 1.8 
                    }
                }
            );
            //this._instance.setVisibility(this._activada);
            Geocercas.polyGroup.addObject(this._instance);
        }
        return this._instance;
    }
    /**
     * Carga una GeoForma Circular
     */
    private LoadPolyCircle(): any{
        var points = new Array();
        var metros = calcMetros(this._coords.lat, this._coords.lng);
		var r1 = metros[0] * this._radius;
		var r2 = metros[1] * this._radius;
		var pi = Math.PI;
		
		var lineString = new H.geo.LineString();
		
		for (n = 0; n < Geocercas._polyCircleSides; n++) {
			var x = parseFloat((Math.sin(n / Geocercas._polyGeoCircleProps.sides * 2 * pi) * r1).toFixed(6)) + this._coords.lat;
			var y = parseFloat((Math.cos(n / Geocercas._polyGeoCircleProps.sides * 2 * pi) * r2).toFixed(6)) + this._coords.lng;
			
            var punto = [x,  y, 0 ];
            points.push(punto);
			//console.log("Coordenadas: " + punto[0] + "," + punto[1] + ", posicion: " + n);
			//Push a punto coordenada entre cada angulo de la geocerca
			//Geocercas.NewPolyGeo.puntos.push(punto);
			//Dibuja el punto coordenada de la geocerca
			lineString.pushLatLngAlt(x,y,0);
		}
		this._instance = new H.map.Polygon(lineString, {
			style: {
				fillColor: hexToRgbA(this._color,0.3),
				strokeColor: '#FFF',
				lineWidth: 3
			}
		});
        this._puntos = points;
        this._instance.setVisibility(this._activada);
        Geocercas.objectList.addObject(this._instance);
        return this._instance;
    }
    /**
     * Carga una GeoForma cuadrada
     */
    private LoadSquare(): any{
       // console.log(this._coords.lat+", "+this._coords.lat);
        var lineString = new H.geo.LineString();
		var distancia = calcMetros(this._coords.lat, this._coords.lng);
		var coordPrincipal = {"lat":this._coords.lat,"lng":this._coords.lng};
		//console.log("coordPrincipal",coordPrincipal);
		for (i = 0; i < (this._perimeter / 2) ; i++) {
			coordPrincipal.lat += distancia[0];
			coordPrincipal.lng -= distancia[1];
		}

		
		lineString.pushLatLngAlt(coordPrincipal.lat,coordPrincipal.lng,0);
		//Geocercas.NewPolyGeo.puntos.push([coordPrincipal.lat,coordPrincipal.lng,0]);
	
		for (i = 0; i < (this._perimeter ) ; i++) {
			coordPrincipal.lng += distancia[1];
		}

		lineString.pushLatLngAlt(coordPrincipal.lat,coordPrincipal.lng,0);
		
		//Geocercas.NewPolyGeo.puntos.push([coordPrincipal.lat,coordPrincipal.lng,0]);
	
		for (i = 0; i < (this._perimeter ) ; i++) {
			coordPrincipal.lat -= distancia[0];
		}

		lineString.pushLatLngAlt(coordPrincipal.lat,coordPrincipal.lng,0);
		//Geocercas.NewPolyGeo.puntos.push([coordPrincipal.lat,coordPrincipal.lng,0]);
	
		for (i = 0; i < (this._perimeter ) ; i++) {
			coordPrincipal.lng -= distancia[1];
		}

		lineString.pushLatLngAlt(coordPrincipal.lat,coordPrincipal.lng,0);
		//Geocercas.NewPolyGeo.puntos.push([coordPrincipal.lat,coordPrincipal.lng,0]);

		this._instance = new H.map.Polygon(lineString, {
			style: {
				fillColor: hexToRgbA(this._color,0.3),
				strokeColor: '#FFF',
				lineWidth: 3
			}
		});
        this._instance.setVisibility(this._activada);
        Geocercas.objectList.addObject(this._instance);
        return this._instance;
    }
}