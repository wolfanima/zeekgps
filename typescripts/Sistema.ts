class Sistema {
    public gmaps_key:string = 'AIzaSyDA9imgQjqHXS9ux9S6Fpf-WQ-ozXhLxrc';
    public _gmaps_loaded:boolean = false;
    public sPath:any = [];

    /**
     * AJAX
     */
    public AJAX(type: string,url: string,data: object,success: any,error: any,_timeout: number = 10000,_datatype = "json"){
        console.log("AJAX Timeout: "+_timeout);
        $.ajax({
            url:		url,
            type:		type,
            timeout:	_timeout,
            dataType:	_datatype,
            data:		data,
            success:	success,
            error:		error
        });
    }

    /**
     * initGMaps
     */
    public initGMaps() {
        if(this._gmaps_loaded == false){
            $.getScript(`https://maps.googleapis.com/maps/api/js?v=3&key=${this.gmaps_key}&channel=zgps&language=es&region=MX&libraries=places,drawing,geometry`)
            .done(( script, textStatus ) =>{
                this._gmaps_loaded = true;
                console.log( "Google Maps Loaded" );
                this.initSnapPath();
            }).fail(( jqxhr, settings, exception ) =>{
                setTimeout(()=>{
                    this.initGMaps();
                },3000);
            });
        }
    }
    /**
     * initSnapPath
     */
    public initSnapPath() {
        this.sPath = [];
        try{
            this.sPath = new google.maps.MVCArray();
        }catch(e){
            console.error("No se inicializó Google Maps.");
        }
    }
    /**
     * getSnappedRoute
     */
    public getSnappedRoute(locData:any, locType:string = 'zgps') {
        this.initSnapPath();
        switch(locType){
            default:
            case "zgps":
                locData.forEach(coord => {
                    this.addToPath(coord.Latitud, coord.Longitud);
                });
                this.snapToRoad();
            break;
            case "teapp":
            break;
        }
    }
    /**
     * addToPath
     */
    public addToPath(lat,lng) {
        try{
            this.sPath.push(new google.maps.LatLng(lat,lng));
        }catch(e){
            console.error("No se inicializó Google Maps.");
        }
    }
    /**
     * snapToRoad
     */
    public snapToRoad() {
        var pathValues = [];
        try{
            for (var i = 0; i < this.sPath.getLength(); i++) {
                pathValues.push(this.sPath.getAt(i).toUrlValue());
            }
            $.get('https://roads.googleapis.com/v1/snapToRoads', {
                interpolate: true,
                key: this.gmaps_key,
                path: pathValues.join('|')
            }, function(data) {
                console.warn(data);
                window.snappedPoints = data.snappedPoints;
                Tracking._locationArray[0].locations = [];
                data.snappedPoints.forEach(sPoint => {
                    Tracking._locationArray[0].locations.push({
                        "Fecha":    "2021-05-26T22:33:15",
                        "Latitud":  sPoint.location.latitude,
                        "Longitud": sPoint.location.longitude
                    })
                });
                Track.DrawHistory(Tracking._locationArray[0].id);
            });
        }catch(e){
            console.error("No se inicializó Google Maps.");
        }
    }
    /**
	 * togglePass
	 */
	public togglePass(selector:string) {
		var sels = selector.split(",");
		if($(selector).is("[type='password']")) {
			$(selector).attr("type","text");
			$(sels).each((i,e)=>{
				$(`${e} + .show-pass .fas`).removeClass("fa-eye-slash").addClass("fa-eye");
			});
		}else{
			$(selector).attr("type","password");
			$(sels).each((i,e)=>{
				$(`${e} + .show-pass .fas`).removeClass("fa-eye").addClass("fa-eye-slash");
			});
		}
	}
}