/// <reference path ="jquery.d.ts"/> 

class TEAppClass {
    public apiUrl:string = 'https://apps.zeekgps.com/tp/api/';
    public apiToken:string = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEiLCJDbGllbnRlSWQiOiJNSUdVRUxPTkRSSVZJTkciLCJVc3VhcmlvSWQiOiJNSUdVRUxPTkRSSVZJTkciLCJuYmYiOjE2MTg4NjEyMzEsImV4cCI6MTYxOTQ2NjAzMSwiaWF0IjoxNjE4ODYxMjMxfQ.n-gu9WOxOXszOVftEMcEUShY-PfemhCUBU3ixbKwRVQ';
    public jwtTokenDecoded:any;
    public jwtToken:string;
    public nombreEmpresa:string = "Samsung Mexicana SA de CV";
    public nombreUnidad:string = "";
    public ruta: number = -1;
    public rateId:number = -1;
    public currentTrip: number = -1;
    public isDriver: boolean = false;
    public drawnRoute: boolean = false;
    public viajeEnCurso: boolean = false;
    public panicActive: boolean = false;
    public showingPanicPrompt: boolean = false;
    public panicSignalTimeout:any = null;
    public showingSuggestion: boolean = false;
    public unitMarker:any = null;
    public unitLabel:any = null;
    public routePolyLine;
    public routeMarkerGroup;
    public tripData: any;
    public tripStartTime:any;
    public tripEndTime:any;
    public regUserData:any;
    public regToken:string = "";
    public congrats = [
        "¡Excelente trabajo!",
        "¡Bien hecho!",
        "¡Super!",
        "¡Muy bien!",
        "¡Hasta la próxima!"
    ];
    public teAppMenuStruct = [
		{
			class: 		"tea-horario-menu",
			onclick: 	"resetMenu();TE.OpenHorario()",
			icon:		'fas fa-clock',
			title_es: 	"Horario",
			title_en: 	"Trip Times"
		},
		{
			class: 		"tea-viajes-menu",
			onclick: 	"resetMenu();TE.OpenViajes()",
			icon:		'fas fa-route',
			title_es: 	"Mis Viajes",
			title_en: 	"Past Trips"
		},
		{
			class: 		"tea-checkin-menu",
			onclick: 	"TE.CheckIn()",
			icon:		'fas fa-user-check',
			title_es: 	"Abordaje",
			title_en: 	"Check-in"
		},
		{
			class: 		"settings-menu",
			onclick: 	"resetMenu();Config.OpenSettings();",
			icon:		'zicon_config',
			title_es: 	"Configuración",
			title_en: 	"Settings"
		},
		{
			class: 		"help-menu",
			onclick: 	"resetMenu();Config.OpenHelp();",
			icon:		'zicon_ayuda',
			title_es: 	"Ayuda",
			title_en: 	"Help"
		},
		{
			divider:	true
		},
		{
			class: 		"debug console-menu hidden",
			onclick: 	"resetMenu();Config.OpenConsole()",
			icon:		'fas fa-terminal',
			title_es: 	"Consola",
			title_en: 	"Console"
		},
		{
			class: 		"",
			onclick: 	"resetMenu();SesionEngine.CerrarSesion();",
			icon:		'zicon_cerrar-sesion',
			title_es: 	"Cerrar Sesión",
			title_en: 	"Logout"
		}
    ];
    //MENU CONDUCTOR
    public teAppMenuStructDriver = [
        {
			class: 		"tea-horario-menu",
			onclick: 	"resetMenu();TE.OpenHorario()",
			icon:		'fas fa-clock',
			title_es: 	"Horario de Viajes",
			title_en: 	"Trip Times"
        },
        {
			class: 		"tea-viajes-menu",
			onclick: 	"resetMenu();TE.OpenViajes()",
			icon:		'fas fa-route',
			title_es: 	"Mis Viajes Pasados",
			title_en: 	"Past Trips"
        },
        {
			class: 		"settings-menu",
			onclick: 	"resetMenu();Config.OpenSettings();",
			icon:		'zicon_config',
			title_es: 	"Configuración",
			title_en: 	"Settings"
		},
		{
			class: 		"help-menu",
			onclick: 	"resetMenu();Config.OpenHelp();",
			icon:		'zicon_ayuda',
			title_es: 	"Ayuda",
			title_en: 	"Help"
		},
		{
			divider:	true
		},
		{
			class: 		"debug console-menu hidden",
			onclick: 	"resetMenu();Config.OpenConsole()",
			icon:		'fas fa-terminal',
			title_es: 	"Consola",
			title_en: 	"Console"
		},
		{
			class: 		"",
			onclick: 	"resetMenu();SesionEngine.CerrarSesion();",
			icon:		'zicon_cerrar-sesion',
			title_es: 	"Cerrar Sesión",
			title_en: 	"Logout"
		}
    ];
    public reportSubjects = [
        "Comportamiento del conductor",
        "Conducción",
        "Ruta inconveniente",
        "Otra razón"
    ];

    public tripList: any = [];

    constructor (){
        this.routeMarkerGroup = new H.map.Group();
        map.addObject(this.routeMarkerGroup);
        checkBuild();
    }

    /**
     * Sesion
     */
    public Sesion(usuario:string, contrasena:string) {
        this.IniciarSesion(usuario,contrasena);
    }
    /**
     * IniciarSesion
     */
    public IniciarSesion(usuario:string, contrasena:string) {
        wait("Iniciando Sesión","Por favor, espere...");
        this.ajaxAuth("POST",this.apiUrl+'passenger/Login/authenticate', {
            username:   usuario,
            password:   contrasena
        }, 
        (data,response,xhr)=>{
            if(xhr.status == 200){
                window.justLogged = true;
                setKey("zeekgpsUser",usuario);
                setKey("zeekgpsPass",contrasena);
                this.jwtToken = data.token;
                this.jwtTokenDecoded = this.decodeJWT(this.jwtToken);
                window.userData = new Object();
                userData.username = usuario;
                this.processActiveTrip(data.viaje);
                this.extractUserData();
                SesionEngine.Startup();
            }else{
                $("#loginPage").removeClass("hidden");
                toasty("Lo sentimos, ha ocurrido un error al autenticar, por favor intente más tarde (-3).","error");
                closeWait();
            }
        },
        (err)=>{
            if(err.status == 400){
                toasty("Lo sentimos, sus datos son incorrectos, por favor corríjalos.","error");
            }else{
                toasty("Lo sentimos, ha ocurrido un error al autenticar, por favor intente más tarde (-2).","error");
            }
            console.error(err);
            $("#loginPage").removeClass("hidden");
            closeWait();
            
        });
    }
    /**
     * processActiveTrip
     */
    public processActiveTrip(data) {
        if(data != null){
            this.viajeEnCurso = true;
            setTimeout(()=>{
                this.tripData = this.constructTripData(data);
                this.ruta = this.tripData.id;
                this.nombreUnidad = this.tripData.nombreUnidad;
                this.displayCurrentLiveTrip(this.ruta);
                this.RastreoContinuo();
            },0)
           
        }
    }
    /**
     * extractUserData
     */
    public extractUserData() {
        userData.userID = parseInt(this.jwtTokenDecoded.UsuarioId);
        userData.Rol = this.jwtTokenDecoded.Rol;
        userData.ClienteId = this.jwtTokenDecoded.ClienteId;
        userData.tipo = "light";
        userData.nombreUsuario = this.jwtTokenDecoded.Nombre;
        userData.nombreEmpresa = this.jwtTokenDecoded.Empresa;
        if(userData.Rol == "Driver") {
            this.isDriver = true;
        }else{
            this.isDriver = false;
        }
        saveSet("zeekUserdata",userData);
        setTimeout(()=>{
            this.ShowSuggestion();
        },1000);
    }
    /**
     * ajaxAuth
     */
    public ajaxAuth(method:string = "POST",url:string,data:any,successCB:any,errorCB:any,_timeout:number = 30000) {
        var ajaxOptions = {
            'url':		    url,
            'method':		method,
            'headers': {
                'Authorization':    `Bearer ${this.apiToken}`,
                'Content-Type':     "application/json",
            },
            'timeout':	    _timeout,
            'data':		    JSON.stringify(data),
            'success':	    successCB,
            'error':		errorCB,
            'cache': 		false
        };
        $.ajax(ajaxOptions);
    }
    /**
     * ajax
     */
     public ajax(method:string = "POST",url:string,data:any,successCB:any,errorCB:any,_timeout:number = 30000) {
        var ajaxOptions = {
            'url':		    url,
            'method':		method,
            'headers': {
                'Authorization':    `Bearer ${this.jwtToken}`,
                'Content-Type':     "application/json",
            },
            'timeout':	    _timeout,
            'success':	    successCB,
            'error':		errorCB,
            'cache': 		false
        };
        if(data != null) {
            ajaxOptions.data = JSON.stringify(data);
        }
        $.ajax(ajaxOptions);
    }
    /**
     * CheckIn
     */
    public CheckIn() {
        if(this.viajeEnCurso){
            alerta("Viaje Activo","Ya se encuentra a bordo con viaje en ruta, para volver abordar una unidad antes debe confirmar que bajó de la unidad.");
        }else{
            this.HideSuggestion();
            resetMenu();
            selectMenu(".tea-checkin-menu");
            $("#manual_qr").val('');
            hideActionCircle();
            $("#checkin").removeClass("hidden");
            masterClose = ()=>{this.CloseCheckIn();};
            this.QRPrep();
        }
    }
    /**
     * CancelCheckIn
     */
    public CancelCheckIn() {
        this.cleanRoute();
        this.destroyCheckIn();
        this.ruta = -1;
        this.destroyQR();
        $("html").removeClass("scanning-qr");
    }
    /**
     * destroyCheckIn
     */
    public destroyCheckIn() {
        $("#main .mainTop").removeClass("no-logo");
        $("#main .mainTop .title").html("");
        $("#close-btn").addClass("hidden");
        $("#checkin").removeClass("hidden");
        $(".checkin_success,.checkin_manual").addClass("hidden");
        $(".checkin_process").removeClass("hidden");
        if(this.viajeEnCurso == false) {
            $("#map .trip_card").removeClass("current_trip");
            $("#map .trip_card").html('').addClass("hidden");
        }
    }
    /**
     * CloseCheckIn
     */
    public CloseCheckIn() {
        if($("#map .trip_card").hasClass("current_trip") && this.viajeEnCurso == false){ //CHECK-IN
            this.CancelCheckIn();
        }else{
            $("#checkin").addClass("hidden");
            resetBack();
            if(this.viajeEnCurso == false){
                this.ShowSuggestion();
            }
        }
    }
    /**
     * ManualQR
     */
    public ManualQR() {
        $(".checkin_process").addClass("hidden");
        $(".checkin_manual").removeClass("hidden");
        $("#manual_qr").val('');
        $("#manual_qr").focus();
    }
    /**
     * ManualQRConf
     */
    public ManualQRConf() {

        this.HideSuggestion();
        var rutaId = trim($("#manual_qr").val().toUpperCase());
        if(rutaId == ""){
            alerta("Error","Por favor, introduzca un código de abordaje.");
        }else{
            $("#manual_qr").blur();
            this.verificarCodigoQR(rutaId);
        }
    }
    /**
     * verificarCodigoQR
     */
    public verificarCodigoQR(codigoQR:string) {
        wait("Verificando código...","Por favor, espere.");  
        this.ajax("POST",this.apiUrl+'passenger/Trips/preview',codigoQR,
        (data,resp,xhr) =>{
            switch(parseInt(xhr.status)) {
                case 200:
                        if(this.jwtTokenDecoded.Empresa == data.nombreEmpresa) {
                            this.tripData = this.constructTripData(data);
                            this.processQRTrip(codigoQR);
                        }else{
                            toasty("Lo sentimos, no puedes tomar el viaje de otra empresa (-3).","warn");
                            closeWait();
                        }
                    break;
                default:
                    toasty("Lo sentimos, no ha sido posible verificar este código QR, ha sido eliminado o no es válido (-1).","error");
                    break;
            }
        },
        (err)=>{
            switch(err.status) {
                case 404:
                    toasty("Lo sentimos, este código QR no existe o ha sido eliminado (-4).","error");
                    break;
                case 400:
                    toasty("Lo sentimos, este código QR no es válido, intenta escanearlo o escribirlo nuevamente (-3).","error");
                    break;
                default:
                    toasty("Lo sentimos, no ha sido posible verificar este código QR, revise su conexión e intente de nuevo (-2).","error");
                    break;
            }
            closeWait();
        });
    }
    public QRPrep() {
        this.CancelCheckIn();
        try{
            if (device.platform.toLowerCase() == "android") {
                var permissions = cordova.plugins.permissions;
                permissions.checkPermission(permissions.CAMERA, checkPermissionCallback, null);
            }else{
                this.readQRCode();
            }
        }catch(e){
            
        }
    }
    /**
     * readQRCode
     */
    public readQRCode() {
        //console.warn("READING QR CODE");
        window.readingCode = true;
        window.masterBackup = masterClose;
        window.barCodeText = "";
        masterClose = function(){};
        this.startScanner();
        setTimeout(()=>{
            if($("html").hasClass("scanning-qr")){
                this.startScanner();
            }
        },500);
    }
    /**
     * startScanner
     */
    public startScanner() {
        try{
            QRScanner.prepare((err,status)=>{
                if(err){
                    console.error("QRScanner Prep Error",err._message);
                }else{
                    //console.log('QRScanner is initialized. Status:');
                    //console.log(status);
                    QRScanner.scan(processQRScan);
                    QRScanner.show(function(status){
                        $("html").addClass("scanning-qr");
                        console.warn("QRScanner Show",status);
                    });
                    QRScanner.getStatus((status)=>{
                        if(status.canEnableLight == true){
                            this.showTorchButton();
                        }
                    });
                }
            });
        }catch(e){
            $("html").removeClass("scanning-qr");
            console.error("QRScanner Error",e);
        }
    }
    /**
     * cancelScan
     */
    public cancelScan() {
        $("html").removeClass("scanning-qr");
        QRScanner.cancelScan();
    }
    /**
     * showTorchButton
     */
    public showTorchButton() {
        $("#scanUI .scan-torch").removeClass("hidden");
    }
    /**
     * toggleTorch
     */
    public toggleTorch() {
        QRScanner.getStatus(function(status){
            if(status.lightEnabled == true){
                $(".scan-torch").html('<i class="fal fa-bolt"></i>').removeClass("torch-enabled");
                QRScanner.disableLight(function(err, status){});
            }else{
                $(".scan-torch").html('<i class="fas fa-bolt"></i>').addClass("torch-enabled");
                QRScanner.enableLight(function(err, status){});
            }
        });
    }
    /**
     * destroyQR
     */
    public destroyQR() {
        $("html").removeClass("scanning-qr");
        try{
            QRScanner.getStatus(function(status){
                if(status.lightEnabled == true){
                    QRScanner.disableLight(function(err, status){
                        $(".scan-torch").html('<i class="fal fa-bolt"></i>').removeClass("torch-enabled");
                        setTimeout(()=>{
                            QRScanner.destroy();
                        },0);
                    });
                }else{
                    setTimeout(()=>{
                        QRScanner.destroy();
                    },0);
                }
            });
        }catch(e){}
    }
    /**
     * processQRTrip
     */
    public processQRTrip(liveId: any) {
        var viaje = this.tripData;
        var f_inicio = UTC2Local(viaje.fechaInicio);
        this.currentTrip = liveId;
        this.nombreUnidad = viaje.nombreUnidad;
        $("#checkin").addClass("hidden");
        $("#map .trip_card,#close-btn").removeClass("hidden");
        $("#close-btn").attr("onclick","TE.CancelCheckIn()");
        $("#main .mainTop").addClass("no-logo");
        $("#map .trip_card").addClass("current_trip");
        this.AdjustTitle("Confirmar Abordaje");
        viaje.conductor = this.constructDriverData(viaje.conductor);
        $(".checkin_process,.checkin_manual").addClass("hidden");
        
        $("#map .trip_card").html(`
            <div class="carta">
                <div class="trip-container">
                    <div class="trip-basic-data">
                        <div class="trip-info">
                            <div class="trip-place trip-start">
                                <div class="trip-location"><div class="trip-icon green"></div> <span>${viaje.direccionInicio}</span></div>
                            </div>
                            <div class="trip-divider"></div>
                            <div class="trip-place trip-end">
                                <div class="trip-location"><div class="trip-icon red"></div> <span>${viaje.direccionDestino}</span></div>
                            </div>
                        </div>
                        <div class="trip-data">
                            <div class="trip-route-unit">
                                <div class="trip-route trip-box">
                                    <div class="trip-title">Ruta</div>
                                    <span>${viaje.nombreRuta}</span>
                                </div>
                                <div class="trip-unit trip-box">
                                    <div class="trip-title">Unidad</div>
                                    <span>${viaje.nombreUnidad}</span>
                                </div>
                            </div>
                            <div class="trip-driver">
                                <div class="trip-start-time trip-box">
                                    <div class="trip-title">Hora de Partida</div>
                                    <span>${f_inicio.format("hh:mm A")}</span>
                                </div>
                                ${this.isDriver == false ? `
                                <div class="trip-driver-mugshot trip-box">
                                    <div class="trip-title">Conductor</div>
                                    <div class="trip-driver-info">
                                        <div class="trip-driver-image" style="background:url('${viaje.conductor.photo_url}') no-repeat center center;background-size:cover;"></div>
                                        <div class="trip-driver-name">
                                            <span>${viaje.conductor.nombre}</span>
                                            <div><span><i class="fas fa-star"></i> ${viaje.conductor.estrellas.toFixed(2)}</span></div>
                                        </div>
                                    </div>
                                </div>` : 
                                `<div class="trip-route-time trip-box">
                                    <div class="trip-title">Distancia</div>
                                    <span>${this.calcRouteLength(viaje.ruta.puntos)} km</span>
                                </div>`
                            }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="buttons_checkin center-align">
                <button class="btn waves-effect waves-light" onclick="TE.ConfirmCheckIn()">
                    Confirmar
                </button>
                <button class="btn waves-effect waves-light red" onclick="TE.CancelCheckIn()">
                    Cancelar
                </button>
            </div>
        `);
        this.drawTripRoute(viaje.ruta.puntos);
        this.routeMapPadding();
        
        closeWait();
        
    }
    /**
     * DriverStartTrip
     */
    public DriverStartTrip(tripId:number) {
        confirmar("Iniciar Viaje","<b>¿Está seguro/a de iniciar este viaje seleccionado?</b>","Confirmar","Cancelar",`TE.ConfirmDriverStartTrip(${tripId})`);

    }
    /**
     * ConfirmDriverStartTrip
     */
    public ConfirmDriverStartTrip(tripId:number) {
        wait("Confirmando inicio de viaje...","Por favor, espere.");
        this.ajax("POST",`${this.apiUrl}driver/Trips/start`, tripId,
        (data,resp,xhr)=>{
            if(xhr.status == 200){
                var tripIndex = objectIndex(this.tripList,"id",tripId);
                $(".teapp_label_endtrip").addClass("driver_end");
                $("#map .trip_card").removeClass("current_trip");
                $("#map .trip_card").html('').addClass("hidden");
                this.ruta = ('idViajeConductor' in data? data.idViajeConductor : -1);
                this.currentTrip = tripId;
                this.tripData = this.tripList[tripIndex];
                this.viajeEnCurso = true;
                this.nombreUnidad = this.tripData.nombreUnidad;
                this.destroyTripTimes();
                this.CloseHorario();
                this.tripStartTime = new moment();
                this.RastreoContinuo();
            }else{
                alerta("Error de Confirmación","Lo sentimos, no ha sido posible confirmar el inicio del viaje (-1).");
            }
            closeWait();
        },
        (err)=>{
            if(err.status == 404){
                alerta("Error de Confirmación","Lo sentimos, este viaje fue eliminado o no existe (-4).");
            }else{
                alerta("Error de Confirmación","Lo sentimos, no ha sido posible confirmar el inicio del viaje (-2).");
            }
            closeWait();
        });
    }
    /**
     * destroyTripTimes
     */
    public destroyTripTimes() {
        $("#main .mainTop").removeClass("no-logo");
        $("#main .mainTop .title").html("");
        $("#close-btn").addClass("hidden");
        $("#te_horario").removeClass("hidden");
        if(this.viajeEnCurso == false) {
            $("#map .trip_card").removeClass("current_trip");
            $("#map .trip_card").html('').addClass("hidden");
        }
    }
    /**
     * ConfirmCheckIn
     */
    public ConfirmCheckIn() {
        if(this.isDriver){
            $(".teapp_label_endtrip").addClass("driver_end");
            wait("Confirmando abordaje de pasajero...","Por favor, espere.");
        }else{
            wait("Confirmando abordaje...","Por favor, espere.");
        }
        this.ajax("POST",`${this.apiUrl}passenger/Trips/preview/start`, this.currentTrip,
        (data,resp,xhr)=>{
            switch(xhr.status){
                case 200:
                    this.ruta = ('idViajePasajero' in data ? data.idViajePasajero : -1);
                    this.viajeEnCurso = true;
                    this.destroyCheckIn();
                    this.CloseCheckIn();
                    
                    this.displayCurrentLiveTrip(this.currentTrip);
                    this.RastreoContinuo();
                    break;
                default:
                    alerta("Error de Abordaje","Lo sentimos, no ha sido posible confirmar el abordaje; aun asi aborde el transporte (-1).");
                    break;
            }
            closeWait();
        },
        (err)=>{
            switch(err.status) {
                case 404:
                    alerta("Error de Abordaje","Lo sentimos, este viaje no existe o ya concluyó (-4).");
                    break;
                default:
                    alerta("Error de Abordaje","Lo sentimos, no ha sido posible confirmar el abordaje; aun asi aborda el transporte (-2).");
                    break;
            }
            closeWait();
        });
    }
    /**
     * displayCurrentLiveTrip
     */
    public displayCurrentLiveTrip(tripId) {
        this.processQRTrip(tripId);
        this.AdjustTitle("");
        $("#close-btn").addClass("hidden");
        $("#main .mainTop").removeClass("no-logo");
       
        $("#map .trip_card .buttons_checkin").html(`
            <button class="btn waves-effect waves-light zeekColor current-trip-options" onclick="TE.CurrentTripOptions()"></button>
            <button class="btn waves-effect waves-light red just-icon" onclick="TE.PanicButton()"><i class="fas fa-exclamation-triangle"></i></button>
        `).addClass("current-trip-buttons");
       
    }
    /**
     * CurrentTripOptions
     */
    public CurrentTripOptions() {
        $("#trip_options").removeClass("hidden");
    }
    /**
     * CloseTripOptions
     */
    public CloseTripOptions() {
        $("#trip_options").addClass("hidden");
    }
    /**
     * CurrentTripSecurity
     */
    public CurrentTripSecurity() {
        $("#trip_security").removeClass("hidden");
    }
    /**
     * CloseSecurityOptions
     */
    public CloseSecurityOptions() {
        if($("#trip_report").is(":visible")){
            $("#trip_report").addClass("hidden");
            $("#trip_security").removeClass("hidden");
        }else{
            $("#trip_security").addClass("hidden");
        }
    }
    /**
     * ReportTrip
     */
    public ReportTrip() {
        $("#reportBody").val('');
        $("#reportBody,#reportReason").attr("disabled",false);
        M.textareaAutoResize($('#reportBody'));
        $("#reportReason").html('<option selected disabled>Elija una razón</option>');
        this.reportSubjects.forEach(razon => {
            $("#reportReason").append(`<option>${razon}</option>`);
        });
        
        $("#trip_security").addClass("hidden");
        $("#trip_report").removeClass("hidden");
        
    }
    /**
     * OpenViajes
     */
    public OpenViajes() {
        
        selectMenu(".tea-viajes-menu");
        hideActionCircle();
        masterClose = ()=>{this.CloseViajes();};
        $("#te_viajes").removeClass("hidden");
        this.requestTrips();
        $("#te_viajes .subVista").scrollTop(0);
    }
    /**
     * CloseViajes
     */
    public CloseViajes(forceClose:boolean = false) {
        if($("#map .trip_card").is(":visible") && $("#te_viajes").is(":visible") == false && forceClose == false){
            this.CloseTrip();
        }else{
            $("#te_viajes .tripList").html('');
            $("#te_viajes").addClass("hidden");
            resetBack();
            if(this.viajeEnCurso) {
                this.displayCurrentLiveTrip(this.currentTrip);
            }else{
                this.ShowSuggestion();
            }
        }
    }
    /**
     * requestTrips
     */
    public requestTrips() {
        this.tripList = [];
        wait("Obteniendo viajes...","Por favor espere");
        this.ajax("GET",this.apiUrl+`${this.isDriver ? 'driver' : 'passenger'}/Trips/fulfilled`,null,
        (data,resp,xhr)=>{
            if(xhr.status == 200) {
                this.tripList = this.constructTripData(data);
            }else{
                toasty("Lo sentimos, no fue posible obtener tus viajes pasados (-1).","error");
            }
            this.loadTrips();
        },
        (err)=>{
            this.loadTrips();
            toasty("Lo sentimos, no fue posible obtener tus viajes pasados (-2).","error");
        });
    }
    /**
     * loadTrips
     */
    public loadTrips() {
        var cont = 1;
        $("#te_viajes .tripList").html('');
        var anio = new Date().getFullYear();
        this.tripList.sort((a,b)=>new Date(b.fechaInicio) - new Date(a.fechaInicio));
        var pastDate = "",viajes=0;
        this.tripList.forEach((viaje,tripId) => {
            var f_inicio = UTC2Local(viaje.fechaInicio),
                f_destino = UTC2Local(viaje.fechaDestino);
            if(this.isDriver == false){
                viaje.conductor = this.constructDriverData(viaje.conductor);
            }
        
            viajes++;
            if(pastDate != f_inicio.format("MM-DD-YYYY")) {
                $("#te_viajes .tripList").append(`
                    <div class="horario_date"><div>${f_inicio.format((f_inicio.format("Y") != anio ? "DD [de] MMMM [de] YYYY" : "DD [de] MMMM"))}</div></div>
                `);
                pastDate = f_inicio.format("MM-DD-YYYY");
            }
            $("#te_viajes .tripList").append(`
                <div class="carta" onclick="TE.OpenTrip(${viaje.id})">
                    <div class="trip-route-img tripid-${viaje.id}"></div>
                    <div class="trip-container">
                        <div class="trip-basic-data">
                            <div class="trip-info">
                                <div class="trip-place trip-start">
                                    <div class="trip-location"><div class="trip-icon green"></div> <span>${viaje.direccionInicio}</span></div>
                                    <div class="trip-date"><div class="trip-divider"></div><span>${f_inicio.format("hh:mm A")}</span></div>
                                </div>
                                <div class="trip-divider"></div>
                                <div class="trip-place trip-end">
                                    <div class="trip-location"><div class="trip-icon red"></div> <span>${viaje.direccionDestino}</span></div>
                                    <div class="trip-date">${f_destino.format("hh:mm A")}</div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-chevron">
                            <i class="fas fa-chevron-right"></i>
                        </div>
                    </div>
                </div>
            `);
            
            setTimeout(()=>{
                this.renderRouteMap(viaje.id);
            },10*cont);
            cont++;
        });
        if(viajes==0){
            $("#te_viajes .noContent").removeClass("hidden");
        }else{
            $("#te_viajes .noContent").addClass("hidden");
        }
        closeWait();
    }
    /**
     * renderRouteMap
     */
    public renderRouteMap(tripId:number) {
        var tripLocation  = objectIndex(this.tripList,"id",tripId);
        var url = "https://image.maps.ls.hereapi.com/mia/1.6/route?apiKey="+hereApiKey+this.constructRouteMap(tripId,this.tripList[tripLocation].ruta.puntos);
        //console.warn("Route IMG",tripId,url);

        cacheImg(url,tripId,true).then((mapUrl)=>{
            $(".tripid-"+tripId).css({ "background": "url(" + mapUrl + ") no-repeat center center", "background-size": "cover" });
        });
    }
    /**
     * shortRoute
     */
    public shortRoute(tripId:number) {
        var tripLocation  = objectIndex(this.tripList,"id",tripId);
        var origRoute = JSON.parse(JSON.stringify(this.tripList[tripLocation].ruta));
        var shortenedRoute = [];
        shortenedRoute.push(origRoute[0]); //PUNTO INICIAL
        for(var shIndex=1;shIndex<origRoute.length;shIndex++) {
            if(shIndex%2 != 0){
                shortenedRoute.push(origRoute[shIndex]);
            }
        }
        var url = "https://image.maps.ls.hereapi.com/mia/1.6/route?apiKey="+hereApiKey+this.constructRouteMap(tripId,shortenedRoute);
        //console.warn("Route IMG",tripId,url);

        cacheImg(url,tripId,false).then((mapUrl)=>{
            $(".tripid-"+tripId).css({ "background": "url(" + mapUrl + ") no-repeat center center", "background-size": "cover" });
        });
    }
    /**
     * OpenTrip
     */
    public OpenTrip(tripId:number) {
        this.HideSuggestion();
        this.cleanRoute(true);
        $("#te_viajes").addClass("hidden");
        $("#map .trip_card,#close-btn").removeClass("hidden");
        $("#close-btn").attr("onclick","TE.CloseViajes()");
        $("#main .mainTop").addClass("no-logo");
        this.AdjustTitle("Información de Viaje");
        $(".mainTop .fade,#map .fade").hide();
        
        var anio = new Date().getFullYear();
        var tripIndex = objectIndex(this.tripList,"id",tripId);
        var viaje = this.tripList[tripIndex];
        this.drawTripRoute(viaje.ruta.puntos);
        if(this.isDriver == false){
            viaje.conductor = this.constructDriverData(viaje.conductor);
        }
        var f_inicio = UTC2Local(viaje.fechaInicio);
        var f_destino = UTC2Local(viaje.fechaDestino);

            $("#map .trip_card").html(`
                <div class="carta">
                    <div class="trip-container">
                        <div class="trip-basic-data">
                            <div class="trip-info">
                                <div class="trip-place trip-start">
                                    <div class="trip-location"><div class="trip-icon green"></div> <span>${viaje.direccionInicio}</span></div>
                                    <div class="trip-date"><div class="trip-divider"></div><span>${f_inicio.format((f_inicio.format("Y") != anio ? "MMM DD YYYY, hh:mm A" : "MMM DD, hh:mm A"))}</span></div>
                                </div>
                                <div class="trip-divider"></div>
                                <div class="trip-place trip-end">
                                    <div class="trip-location"><div class="trip-icon red"></div> <span>${viaje.direccionDestino}</span></div>
                                    <div class="trip-date">${f_destino.format((f_destino.format("Y") != anio ? "MMM DD YYYY, hh:mm A" : "MMM DD, hh:mm A"))}</div>
                                </div>
                            </div>
                            <div class="trip-data">
                                <div class="trip-route-unit">
                                    <div class="trip-route trip-box">
                                        <div class="trip-title">Ruta</div>
                                        <span>${viaje.nombreRuta}</span>
                                    </div>
                                    <div class="trip-unit trip-box">
                                        <div class="trip-title">Unidad</div>
                                        <span>${viaje.nombreUnidad}</span>
                                    </div>
                                </div>
                                ${this.isDriver == false ? `
                                <div class="trip-driver">
                                    <div class="trip-driver-mugshot trip-box">
                                        <div class="trip-title">Conductor</div>
                                        <div class="trip-driver-info">
                                            <div class="trip-driver-image" style="background:url('${viaje.conductor.photo_url}') no-repeat center center;background-size:cover;"></div>
                                            <div class="trip-driver-name">
                                                <span>${viaje.conductor.nombre}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="trip-driver-stars trip-box">
                                        <div class="trip-title">Calificación</div>
                                        <span>${this.renderStars(viaje.calificacion)}</span>
                                    </div>
                                </div>` : ``}
                            </div>
                        </div>
                    </div>
                </div>
            `);
        this.routeMapPadding();
    }
    /**
     * routeMapPadding
     */
    public routeMapPadding() {
        if(this.drawnRoute == true){
            if(isLandscape){
                map.getViewPort().setPadding(0, $("#map .trip_card").outerWidth(), 0, 0);
            }else{
                map.getViewPort().setPadding(0, 0, $("#map .trip_card").outerHeight(), 0);
            }
           
            map.setViewBounds(this.routeMarkerGroup.getBounds(),false);   
        } 
        this.AdjustPanicPill();
    }
    /**
     * CloseTrip
     */
    public CloseTrip() {
        this.drawnRoute = false;
        this.cleanRoute(true);
        $("#main .mainTop").removeClass("no-logo");
        $("#main .mainTop .title").html("");
        $("#map .trip_card").html('').addClass("hidden");
        $("#close-btn").addClass("hidden");
        $("#te_viajes").removeClass("hidden");
        try{
            masterClose = ()=>{this.CloseViajes();};
        }catch(e){}
    }
    /**
     * cleanRoute
     */
    public cleanRoute(forceClean:boolean = false) {
        if(this.viajeEnCurso == false || forceClean == true){
            this.routeMarkerGroup.removeAll();
            this.drawnRoute = false;
            this.unitMarker = null;
            this.unitLabel = null;
        }
    }
    /**
     * logoutClean
     */
    public logoutClean() {
        this.viajeEnCurso = false;
        this.cleanRoute(true);
        $("#map .trip_card").removeClass("current_trip");
        $("#map .trip_card").html('').addClass("hidden");
    }
    /**
     * drawTripRoute
     */
    public drawTripRoute(tripLocations:any) {
        this.drawnRoute = true;
        var viaje = tripLocations;

        console.log("DRAWING TRIP ROUTE");
        
        var puntos = new H.geo.LineString();
        for(var j =0;j<viaje.length;j++){
            puntos.pushPoint({lat: viaje[j].latitud, lng:  viaje[j].longitud});
        }
        //var color = ColorLuminance(getRandomColor(),-0.4);
        var color = "#4a62bc";
        this.routePolyLine = new H.map.Polyline(
            puntos, 
            { 
                style: { 
                    lineWidth: 6,
                    strokeColor: hexToRgbA(color,0.85),
                    fillColor: hexToRgbA(color,0.85),
                    },
                arrows: { 
                    fillColor: 'white', 
                    frequency: 15, 
                    width: 2, 
                    length: 1.8 
                }
            }
        );
        
        this.routeMarkerGroup.addObject(this.routePolyLine);
        //PIN DE INICIO
        var iconElem = this.CreateDomIcon("img/iconos/pin-start.png","startend");
        var startMarker =  new H.map.DomMarker(
            {lat: viaje[0].latitud , lng: viaje[0].longitud},
            {
                icon: new H.map.DomIcon(iconElem)
            }
        );
        var labelElem = this.CreateDomLabel(lang("route_start"),"point");
        var startLabel = new H.map.DomMarker(
            {lat:viaje[0].latitud , lng: viaje[0].longitud},
            {
                min: 11,
                icon: new H.map.DomIcon(labelElem)
            }
        );
        startMarker.setZIndex(0);
        startLabel.setZIndex(0);
        this.routeMarkerGroup.addObject(startMarker);
        this.routeMarkerGroup.addObject(startLabel);
                    
        //PIN DE META
        var iconElem = this.CreateDomIcon("img/iconos/pin-end.png","startend");
        var endMarker =  new H.map.DomMarker(
            {lat: viaje[viaje.length-1].latitud , lng: viaje[viaje.length-1].longitud },
            {
                icon:  new H.map.DomIcon(iconElem)
            }
        );
        var labelElem = this.CreateDomLabel(lang("route_end"),"point");
        var endLabel = new H.map.DomMarker(
            {lat:viaje[viaje.length-1].latitud , lng: viaje[viaje.length-1].longitud},
            {
                min: 11,
                icon: new H.map.DomIcon(labelElem)
            }
        );
        endMarker.setZIndex(0);
        endLabel.setZIndex(0);
        this.routeMarkerGroup.addObject(endMarker);
        this.routeMarkerGroup.addObject(endLabel);
                    
        map.setViewBounds(this.routeMarkerGroup.getBounds(),false);   
        try{
            this.routeMarkerGroup.removeEventListener('tap',TE.FocusOnMarker,false);
        }catch(e){}
        this.routeMarkerGroup.addEventListener('tap',TE.FocusOnMarker,false);   
        //this.RenderTrackingMarker(); 
    }
    /**
     * FocusOnMarker
     */
    public FocusOnMarker(evt:any) {
        map.setCenter(evt.target.getPosition());
        map.setZoom(17);
    }
    /**
     * EndLiveTrip
     */
    public EndLiveTrip() {
        if(this.isDriver){
            confirmar("Terminar Viaje","Al confirmar que desea terminar el viaje acepta que ha llegado a su punto de destino y todos los pasajeros han bajado de la unidad, <b><u>¿está seguro/a de hacerlo?</u></b>","Confirmar","Cancelar","TE.ConfirmEndTrip()");
        }else{
            confirmar("Terminar Viaje","Al confirmar que desea terminar el viaje acepta que bajará o bajó de la unidad, <b><u>¿está seguro/a de hacerlo?</u></b>","Confirmar","Cancelar","TE.ConfirmEndTrip()");
        }
    }
    /**
     * ConfirmEndTrip
     */
    public ConfirmEndTrip() {
        wait("Terminando viaje...","Por favor, espere.");
        this.ajax("POST",`${this.apiUrl}${this.isDriver ? 'driver' : 'passenger'}/Trips/finish`,this.ruta,
        (data,resp,xhr)=>{
            if(xhr.status == 200){
                this.tripStartTime =  UTC2Local(data.fechaInicio);
                this.tripEndTime =  UTC2Local(data.fechaDestino);
                if(this.isDriver == false) {
                    this.rateId = ('idViajePasajeroHistorial' in data ? data.idViajePasajeroHistorial : -1);
                }else{
                    this.rateId = data.idViajeConductorHistorial;
                }
                this.CloseTripOptions();
                this.viajeEnCurso = false;
                $("#map .trip_card").removeClass("current_trip");
                $("#map .trip_card").html('').addClass("hidden");
                this.cleanRoute();
                $("#trip_summary").removeClass("hidden");
                this.FillTripSummary();
                this.DeactivatePanicSignal();
            }else{
                if(this.isDriver){
                    alerta("Lo sentimos, no ha sido posible terminar el viaje. Vuelva a intentar de nuevo, si sigue con el problema por favor contacte a la Administración de la plataforma (-1).");
                }else{
                    alerta("Lo sentimos, no ha sido posible terminar el viaje. Baje de la unidad y vuelva a intentar de nuevo (-1).");
                }
            }
            closeWait();
        },
        (err)=>{
            toasty("Lo sentimos, no ha sido posible terminar el viaje, revise su conexión e intente de nuevo (-2).","error");
            closeWait();
        });
    }
    /**
     * FillTripSummary
     */
    public FillTripSummary() {
        var viaje = this.tripData;
        if(this.isDriver){
            var tiempo = msToTime(this.tripEndTime.diff(this.tripStartTime));
            var distancia = this.calcRouteLength(viaje.ruta.puntos);
            viaje.conductor = this.constructDriverData(viaje.conductor);
            $("#trip_summary .teapp_box_options").html(`
                <div class="trip_end_driver_message">${this.getDriverEndCongrats()}</div>
                <div class="trip_end_driver_info">
                    <div class="trip_end_info_row">
                        <div class="trip_end_info_label">Tiempo</div>
                        <div class="trip_end_info_value">${tiempo}</div>
                    </div>
                    <div class="trip_end_info_row">
                        <div class="trip_end_info_label">Distancia</div>
                        <div class="trip_end_info_value">${distancia} km</div>
                    </div>
                </div>
                <div class="trip_end_report_label" onclick="TE.ToggleReportModule()">Agregar Reporte de Viaje<span><i class="fas fa-chevron-right"></i></span></div>
                <div class="row trip_end_report_box hidden">
                    <div class="col input-field">
                        <textarea id="trip_end_report_details" class="materialize-textarea"></textarea>
                        <label for="trip_end_report_details">Descripción de Reporte</label>
                        <button class="btn waves-effect waves-light zeekColor full-btn send-driver-report-btn" onclick="TE.SendDriverReport()">Enviar</button>
                    </div>
                </div>
                <button class="btn waves-effect waves-light zeekColor close-trip-summary-btn" onclick="TE.CloseTripSummary()">Cerrar</button>
            `);
       }else{
            $("#trip_summary .teapp_box_options").html(`
                <div class="trip_end_avatar" style="background:url('${viaje.conductor.photo_url}') no-repeat center center;background-size:cover;"></div>
                <div class="trip_end_drivername">${viaje.conductor.nombre}</div>
                <div class="trip_end_rating">
                    <div class="rate_message">Otorga una calificación al Conductor</div>
                    <div class="rate_stars">
                        <div class="rate_star" star="1" message="Pésimo"></div>
                        <div class="rate_star" star="2" message="Malo"></div>
                        <div class="rate_star" star="3" message="Regular"></div>
                        <div class="rate_star" star="4" message="Bueno"></div>
                        <div class="rate_star" star="5" message="Excelente"></div>
                    </div>
                </div>
                <div class="trip_end_report_label" onclick="TE.ToggleReportModule()">Agregar Comentario<span><i class="fas fa-chevron-right"></i></span></div>
                <div class="row trip_end_report_box hidden">
                    <div class="col input-field">
                        <textarea id="trip_end_report_passenger_details" class="materialize-textarea"></textarea>
                        <label for="trip_end_report_passenger_details">Introduzca aquí su Comentario</label>
                    </div>
                </div>
                <button class="btn waves-effect waves-light zeekColor disabled rating-btn" onclick="TE.SendRating()">Enviar</button>
            `);
        }
    }
    /**
     * getDriverEndCongrats
     */
    public getDriverEndCongrats() {
        return this.congrats[rand(0,this.congrats.length-1)];
    }
    /**
     * SendDriverReport
     */
    public SendDriverReport() {
        var reportBody = trim($("#trip_end_report_details").val());
        if(reportBody == ""){
            toasty("Por favor escriba la descripción del reporte de viaje a enviar.","warn");
        }else{
            toasty("Enviando reporte, por favor espere...");
            $(".send-driver-report-btn,#trip_end_report_details").addClass("disabled");
            this.ajax("POST",this.apiUrl+'driver/trips/report', {
                id:         this.rateId,
                report:     reportBody 
            },
            (data,resp,xhr)=>{
                if(xhr.status == 200) {
                    $(".trip_end_report_label")
                    .html(`<div class="trip_report_sent">Reporte de Viaje enviado:</div><div class="trip_report_body">${reportBody}</div>`)
                    .attr("onclick","")
                    .removeClass("report-open")
                    .addClass("report-sent");
                    this.ToggleReportModule(true);
                    toasty("Reporte de Fin de Viaje enviado con éxito.","success");
                }else{
                    toasty("Lo sentimos, el reporte no ha podido ser enviado (-1).","error");
                }
                $(".send-driver-report-btn,#trip_end_report_details").removeClass("disabled");
            },
            (err)=>{
                toasty("Lo sentimos, el reporte no ha podido ser enviado, revise su conexión e intente de nuevo (-2).","error");
                $(".send-driver-report-btn,#trip_end_report_details").removeClass("disabled");
            });
        }
    }
    /**
     * ToggleReportModule
     */
    public ToggleReportModule(forceHide:boolean = false) {
        if($(".trip_end_report_box").is(":visible") || forceHide == true) {
            $(".trip_end_report_label").removeClass("report-open");
            $(".trip_end_report_label span").html('<i class="fas fa-chevron-right"></i>');
            $(".close-trip-summary-btn").removeClass("hidden");
            $(".trip_end_report_box").addClass("hidden");
        }else{
            $(".trip_end_report_label").addClass("report-open");
            $(".trip_end_report_label span").html('<i class="fas fa-chevron-down"></i>')
            $(".close-trip-summary-btn").addClass("hidden");
            $(".trip_end_report_box").removeClass("hidden");
        }
    }
    /**
     * CloseTripSummary
     */
    public CloseTripSummary() {
        $("#trip_summary").addClass("hidden"); 
        setTimeout(()=>{
            this.ShowSuggestion();
        },3000);
    }
    /**
     * SendRating
     */
    public SendRating() {
        toasty("Enviando calificación...");
        $(".rating-btn").addClass("disabled");
        this.ajax("POST",this.apiUrl+"passenger/Trips/rate",{
            id:     this.rateId,
            rating: $(".rate_star.pos").length,
            report: trim($("#trip_end_report_passenger_details").val())
        },
        (data,resp,xhr)=>{
            if(xhr.status == 200) {
                toasty("Calificación enviada con éxito.","success");
                $("#trip_summary").addClass("hidden");    
                $("#trip_summary .teapp_box_options").html('');   
                try{
                    map.setViewBounds(Tracking._markerGroup.getBounds());
                }catch(e){}
                setTimeout(()=>{
                    this.ShowSuggestion();
                },3000);
            }else{
                toasty("Lo sentimos, no ha sido posible enviar la calificación/reporte. Por favor, vuelva a intentar después (-1).","error");
                $(".rating-btn").removeClass("disabled");
            }
            
        },
        (err)=>{
            $(".rating-btn").removeClass("disabled");
            toasty("Lo sentimos, no ha sido posible enviar la calificación/reporte. Por favor, revise su conexión y vuelva a intentar (-2).","error");
        });
    }
    /**
     * RenderTrackingMarker
     */
    public RenderTrackingMarker(nombreUnidad,_lat,_lng) {
        if(this.viajeEnCurso){
            if(this.unitMarker == null){
                var iconElem = Track.CreateDomIcon("img/teapp/teapp_unit.png","teapp_unit");
                this.unitMarker =   new H.map.DomMarker(
                    {lat:_lat , lng:_lng },
                    {
                        icon:   new H.map.DomIcon(iconElem),
                        zIndex:1
                    }
                );
                this.routeMarkerGroup.addObject(this.unitMarker);
                
                var iconLabel = this.CreateDomLabel(nombreUnidad,'icon');
                this.unitLabel =  new H.map.DomMarker(
                    {lat:_lat, lng:_lng},
                    {
                        zIndex:1,
                        icon: new H.map.DomIcon(iconLabel)
                    }
                );
                this.routeMarkerGroup.addObject(this.unitLabel);
            }else{
                this.unitMarker.setPosition({lat:_lat , lng:_lng });
                this.unitLabel.setPosition({lat:_lat , lng:_lng });
            }
        }
    }
    /**
     * RastreoContinuo
     */
    public RastreoContinuo() {
        if(this.viajeEnCurso){
            this.ajax("POST",`${this.apiUrl}${this.isDriver ? 'driver' : 'passenger'}/Units/track`,this.ruta,
            (data,resp,xhr)=>{
                if(xhr.status == 200){
                    this.RenderTrackingMarker(this.nombreUnidad,data.ubication.latitude,data.ubication.longitude);
                }
                setTimeout(()=>{
                    this.RastreoContinuo();
                },10000);
            },
            (err)=>{
                setTimeout(()=>{
                    this.RastreoContinuo();
                },3000);
            });
        }
    }
    /**
     * Abre el Sidebar
     * @param section Seccion en especifico del Sidebar a abrir
     */
    public OpenSidebar(section : string){
        $("#sidebar .toHide").hide();
		$("#sidebar .teappBlock").show();
        $("#sidebar,#close-btn").removeClass("hidden");
		$("#sidebar .teappBlock > div").hide();
		$(".teappBlock ."+section).show();
        $("#close-btn").attr("onclick","TE.CloseSidebar('"+section+"')");
       
        $(".mainTop .tool.right").hide();
        views();
        Config.disableFullApp();
    }
    /**
     * Cierra el Sidebar y cancela la seccion en la que se encontraba
     * @param section Seccion del Sidebar a cancelar
     */
    public CloseSidebar(section : string){
        $("#te_viajes").removeClass("hidden");
        $("#main .mainTop").removeClass("no-logo");
        $("#main .mainTop .title").html("");
        $("#sidebar,#add-btn,#close-btn").addClass("hidden");
        $(".teappBlock ."+section).hide();
        $(".undoButton").addClass("disabled");
        
        $("#sidebar").removeClass("emptylist");
        
        views();
        Config.enableFullApp();
    }
    /**
     * Cambia el titulo del top y del showcase
     * dependiendo del modulo abierto
     */
    public AdjustTitle(title:string) {
        $("#main .mainTop").addClass("no-logo");
        $("#main .mainTop.no-logo .title,.tap-title,.showcase-tap-title").html(title);
    }
    /**
     * constructRouteMap
     */
    public constructRouteMap(tripId:number,ruta:any): string {
        var jsonQuery = {
            r:      "",
            lc:     "ff4a62bc",
            sc:     "ff4a62bc",
            lw:     4,
            h:      150,
            w:     parseInt($(".tripid-"+tripId).width()),
            noicon: true,
            style:  "mini",
            ppi:    512,
            poithm: 1,
            t:      10,
            q:      100,
            nocp:   true
        };

        var tripLen = ruta.length-1;
        var posArray = [];
        ruta.forEach(pos => {
            if('lat' in pos && 'lng' in pos) {
                posArray.push(pos.lat);
                posArray.push(pos.lng);
            }
            if('latitud' in pos && 'longitud' in pos) {
                posArray.push(pos.latitud);
                posArray.push(pos.longitud);
            }
        });
        jsonQuery.r = posArray.join(",");
        if('lat' in ruta[0] && 'lng' in ruta[0]) {
            jsonQuery.m = `${ruta[0].lat},${ruta[0].lng},${ruta[tripLen].lat},${ruta[tripLen].lng}`;
            jsonQuery.poix0 = `${ruta[0].lat},${ruta[0].lng};ff00cc00;;;•`;
            jsonQuery.poix1 = `${ruta[tripLen].lat},${ruta[tripLen].lng};ffdd0000;;;•`;
        }
        if('latitud' in ruta[0] && 'longitud' in ruta[0]) {
            jsonQuery.m = `${ruta[0].latitud},${ruta[0].longitud},${ruta[tripLen].latitud},${ruta[tripLen].longitud}`;
            jsonQuery.poix0 = `${ruta[0].latitud},${ruta[0].longitud};ff00cc00;;;•`;
            jsonQuery.poix1 = `${ruta[tripLen].latitud},${ruta[tripLen].longitud};ffdd0000;;;•`;
        }
        
        return "&"+$.param(jsonQuery);
    }
    /**
     * renderStars
     */
    public renderStars(stars:number) {
        var percent = (stars/5)*100;
        return `<div class="trip-stars"><div class="trip-stars-over" style="width:${percent}%"></div></div>`;
    }
    /**
     * Crea icono en el DOM que puede personalizarse
     * @param carIcon Nombre del icono
     * @param auxClass Clase auxiliar para el icono
     */
    public CreateDomIcon(carIcon: string, auxClass: string = ""): any {
        var icon = document.createElement('div');
        icon.className = "domIcon "+auxClass;
        icon.style.background = `url(${carIcon}) no-repeat center center`;
        return icon;
    }
    /**
     * Crea un Label en el DOM que puede personalizarse
     * @param text Texto del Label
     * @param type Tipo del Label
     */
    public CreateDomLabel(text: string = "Test", type: string = "icon"): any {
        var _length = getLabelWidth(text, type);
        var label = document.createElement('div');
        label.className = "domLabel_"+type;
        label.style.left = ((_length / 2)*-1)+"px";
        label.innerHTML = text;
        return label;
    }
    /**
     * getRouteLength
     */
    public getRouteLength(routePoly:any) { //En KM
        var geometry = routePoly.getGeometry();
        let distance = 0;
        let last = geometry.extractPoint(0);
        for (let i=1; i < geometry.getPointCount(); i++) {
            var point = geometry.extractPoint(i);
            distance += last.distance(point);
            last = point;
        }
        if (routePoly.isClosed()) {
            distance += last.distance(geometry.extractPoint(0));
        }
        return parseFloat((distance/1000).toFixed(1));
    }
    /**
     * calcRouteLength
     */
    public calcRouteLength(ruta:any) {
        var puntos = new H.geo.LineString();
        for(var j =0;j<ruta.length;j++){
            puntos.pushPoint({lat: ruta[j].latitud, lng:  ruta[j].longitud});
        }

        var routePoly = new H.map.Polyline(puntos);
        return this.getRouteLength(routePoly);
    }  
    /**
     * OpenHorario
     */
    public OpenHorario() {
        
        selectMenu(".tea-horario-menu");
        hideActionCircle();
        masterClose = ()=>{this.CloseHorario();};
        $("#te_horario").removeClass("hidden");
        this.requestTimes();
        $("#te_horario .subVista").scrollTop(0);
    }
    /**
     * CloseHorario
     */
    public CloseHorario(forceClose:boolean = false) {
        if($("#map .trip_card").is(":visible") &&  $("#te_horario").is(":visible") == false && forceClose == false){
            this.cleanRoute(true);
            $("#main .mainTop").removeClass("no-logo");
            $("#main .mainTop .title").html("");
            $("#close-btn").addClass("hidden");
            $("#map .trip_card").removeClass("current_trip");
            $("#map .trip_card").html('').addClass("hidden");
            $("#te_horario").removeClass("hidden");
        }else{
            $("#te_horario .tripList").html('');
            $("#te_horario").addClass("hidden");
            resetBack();
            if(this.viajeEnCurso) {
                this.displayCurrentLiveTrip(this.currentTrip);
            }else{
                this.ShowSuggestion();
            }
        }
    }
    /**
     * ShowSuggestion
     */
    public ShowSuggestion() {
        if(this.showingSuggestion == false && this.viajeEnCurso == false){
            this.showingSuggestion = true;
            if(this.isDriver) {
                $(".trip_suggestion_passenger").addClass("hidden");
                $(".trip_suggestion_driver").removeClass("hidden");
                $(".trip_suggestion_passenger .teapp_box").fadeOut(0);
                $(".trip_suggestion_driver .teapp_box").fadeOut(0).fadeIn(()=>{
                    this.showingSuggestion = false;
                });
            }else{
                $(".trip_suggestion_driver .teapp_box").fadeOut(0);
                $(".trip_suggestion_passenger .teapp_box").fadeOut(0).fadeIn(()=>{
                    this.showingSuggestion = false;
                });
                $(".trip_suggestion_driver").addClass("hidden");
                $(".trip_suggestion_passenger").removeClass("hidden");
            }
        }
    }
    /**
     * HideSuggestion
     */
    public HideSuggestion() {
        $(".trip_suggestion_passenger,.trip_suggestion_driver").addClass("hidden");
    }
    /**
     * requestTimes
     */
    public requestTimes() {
        this.tripList = [];
        wait("Obteniendo Horario...","Por favor espere");
        this.ajax("GET", `${this.apiUrl}${this.isDriver ? 'driver' : 'passenger'}/Trips/${this.isDriver ? 'assigned' : 'available'}`, 
        (this.isDriver ? null : ''),
        (data,resp,xhr)=>{
            if(xhr.status == 200){
                this.tripList = this.constructTripData(data);
            }else{
                toasty("Lo sentimos, no ha sido posible obtener la lista de viajes.","error");
            }
            this.loadTimes();
        },
        (err)=>{
            toasty("Lo sentimos, no ha sido posible obtener la lista de viajes.","error");
            closeWait();
            this.loadTimes();
        });
    }
    /**
     * loadTimes
     */
    public loadTimes() {
        $("#te_horario .tripList").html('');
        var anio = new Date().getFullYear(),
            pastDate = "",
            viajes = 0;
        this.tripList.sort((b,a)=>new Date(b.fechaInicio) - new Date(a.fechaInicio));
        this.tripList.forEach((viaje,i)=> {
            var f_inicio = UTC2Local(viaje.fechaInicio);
            var f_destino = UTC2Local(viaje.fechaDestino);
            /*if( moment(f_inicio).isAfter() && 
                (this.isDriver == false || 
                    (this.isDriver && userData.userID == viaje.idConductor)
                )
            ){*/
                
                
                if(f_inicio.isSame(new Date(),'day') ){
                    /*if(pastDate != f_inicio.format("MM-DD-YYYY") && this.isDriver == true) {
                        $("#te_horario .tripList").append(`
                            <div class="horario_date"><div>${f_inicio.format((f_inicio.format("Y") != anio ? "DD [de] MMMM [de] YYYY" : "DD [de] MMMM"))}</div></div>
                        `);
                        pastDate = f_inicio.format("MM-DD-YYYY");
                    }*/
                    viajes++;
                    $("#te_horario .tripList").append(`
                        <div class="carta" onclick="TE.PreviewTrip(${viaje.id})">
                            <div class="trip-container">
                                <div class="trip-basic-data">
                                    <div class="trip-info">
                                        <div class="trip-place trip-start">
                                            <div class="trip-location"><div class="trip-icon green"></div> <span>${viaje.direccionInicio}</span></div>
                                            <!--<div class="trip-date"><div class="trip-divider"></div><span>${f_inicio.format((f_inicio.format("Y") != anio ? "MMM DD YYYY, hh:mm A" : "MMM DD, hh:mm A"))}</span></div>-->
                                        </div>
                                        <div class="trip-divider"></div>
                                        <div class="trip-place trip-end">
                                            <div class="trip-location"><div class="trip-icon red"></div> <span>${viaje.direccionDestino}</span></div>
                                            <!--<div class="trip-date">${f_destino.format((f_destino.format("Y") != anio ? "MMM DD YYYY, hh:mm A" : "MMM DD, hh:mm A"))}</div>-->
                                        </div>
                                    </div>
                                    <div class="trip-data">
                                        <div class="trip-route-unit">
                                            <div class="trip-route trip-box">
                                                <div class="trip-title">Hora de Partida</div>
                                                <span>${f_inicio.format("hh:mm A")}</span>
                                            </div>
                                            <div class="trip-unit trip-box">
                                                <div class="trip-title">Distancia</div>
                                                <span>${this.calcRouteLength(viaje.ruta.puntos)} km</span>
                                            </div>
                                        </div>
                                        <div class="trip-route-unit">
                                            <div class="trip-route trip-box">
                                                <div class="trip-title">Ruta</div>
                                                <span>${viaje.nombreRuta}</span>
                                            </div>
                                            <div class="trip-unit trip-box">
                                                <div class="trip-title">Unidad</div>
                                                <span>${viaje.nombreUnidad}</span>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="trip-chevron">
                                    <i class="fas fa-chevron-right"></i>
                                </div>
                            </div>
                        </div>
                    `);
                }
            //}
        });
        if(viajes==0){
            $("#te_horario .noContent").removeClass("hidden");
        }else{
            $("#te_horario .noContent").addClass("hidden");
        }
        closeWait();
    }
    /**
     * PreviewTrip
     */
    public PreviewTrip(tripId:number) {
        var teId = objectIndex(this.tripList,"id",tripId);
        if(teId >=0){ //VIAJE VALIDO
            this.HideSuggestion();
            this.cleanRoute(true);
            $("#te_horario").addClass("hidden");
            $("#map .trip_card,#close-btn").removeClass("hidden");
            $("#close-btn").attr("onclick","TE.CloseHorario()");
            $("#main .mainTop").addClass("no-logo");
            $("#map .trip_card").addClass("current_trip");
            this.AdjustTitle("Vista Previa de Ruta");
            var viaje = this.tripList[teId];
            if(this.isDriver == false){
                viaje.conductor = this.constructDriverData(viaje.conductor);
            }
            var f_inicio = UTC2Local(viaje.fechaInicio);
            $("#map .trip_card").html(`
                <div class="carta">
                    <div class="trip-container">
                        <div class="trip-basic-data">
                            <div class="trip-info">
                                <div class="trip-place trip-start">
                                    <div class="trip-location"><div class="trip-icon green"></div> <span>${viaje.direccionInicio}</span></div>
                                </div>
                                <div class="trip-divider"></div>
                                <div class="trip-place trip-end">
                                    <div class="trip-location"><div class="trip-icon red"></div> <span>${viaje.direccionDestino}</span></div>
                                </div>
                            </div>
                            <div class="trip-data">
                                <div class="trip-route-unit">
                                    <div class="trip-route trip-box">
                                        <div class="trip-title">Ruta</div>
                                        <span>${viaje.nombreRuta}</span>
                                    </div>
                                    <div class="trip-unit trip-box">
                                        <div class="trip-title">Unidad</div>
                                        <span>${viaje.nombreUnidad}</span>
                                    </div>
                                </div>
                                <div class="trip-driver">
                                    <div class="trip-start-time trip-box">
                                        <div class="trip-title">Hora de Partida</div>
                                        <span>${f_inicio.format("hh:mm A")}</span>
                                    </div>
                                    ${this.isDriver ? 
                                        `<div class="trip-route-distance trip-box">
                                            <div class="trip-title">Distancia</div>
                                            <div class="trip-driver-info">
                                                <span>${this.calcRouteLength(viaje.ruta.puntos)} km</span>
                                            </div>
                                        </div>` : 
                                        `<div class="trip-driver-mugshot trip-box">
                                            <div class="trip-title">Conductor</div>
                                            <div class="trip-driver-info">
                                                <div class="trip-driver-image" style="background:url('${viaje.conductor.photo_url}') no-repeat center center;background-size:cover;"></div>
                                                <div class="trip-driver-name">
                                                    <span>${viaje.conductor.nombre}</span>
                                                    <div><span><i class="fas fa-star"></i> ${viaje.conductor.estrellas.toFixed(2)}</span></div>
                                                </div>
                                            </div>
                                        </div>`
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                ${this.isDriver ? `
                <div class="buttons_checkin center-align">
                    <button class="btn waves-effect waves-light full-btn driver_starttrip_btn ${this.viajeEnCurso ? `disabled` : ``}" onclick="${this.viajeEnCurso ? `` : `TE.DriverStartTrip(${tripId})`}"></button>
                </div>` : ''}
                
            `);
            this.drawTripRoute(viaje.ruta.puntos);
            this.routeMapPadding();
        }else{
            //VIAJE INVALIDO
        }
    }
    /**
     * constructDriverData
     */
    public constructDriverData(data) {
        var resp = new Object();
        resp.id = ('id' in data ? data.id : -1);
        resp.nombre = `${data.nombre} ${'apellido' in data ? data.apellido : ''}`;
        resp.nombre = ('nombreConductor' in data ? data.nombreConductor : resp.nombre);
        if('photo_url' in data) {
            resp.photo_url = (data.photo_url == '' ? 'img/teapp/user_avatar.png' : data.photo_url);
        }else{
            resp.photo_url = 'img/teapp/user_avatar.png';
        }
        if('rutaImagenPerfil' in data) {
            resp.photo_url = (data.rutaImagenPerfil == '' ? 'img/teapp/user_avatar.png' : data.rutaImagenPerfil);
        }else{
            resp.photo_url = 'img/teapp/user_avatar.png';
        }
        
        resp.estrellas = (data.calificacionPromedio == null || data.numeroCalificaciones == 0 ? 5 : data.calificacionPromedio);
        resp.estrellas = ('calificacion' in data ? data.calificacion : resp.estrellas);
        return resp;
    }
    /**
     * constructTripData
     */
    public constructTripData(data) {
        if(Array.isArray(data) == false) {
            if(data.nombreRuta == null){
                data.nombreRuta = '---';
            }
            if('idConductorNavigation' in data) {
                data.conductor = data.idConductorNavigation;
            }
            if(this.isDriver) {
                if('idViajeConductor' in data) {
                    data.id = data.idViajeConductor;
                }
                if('idViajeConductorHistorial' in data) {
                    data.id = data.idViajeConductorHistorial;
                }
            }else{
                if('idViajeConductor' in data) {
                    data.id = data.idViajeConductor;
                }
                if('idViajePasajero' in data){
                    data.id = data.idViajePasajero;
                }
                if('idViajePasajeroHistorial' in data) {
                    data.id = data.idViajePasajeroHistorial;
                }
            }
            if('idRuta' in data == false) {
                data.idRuta = data.id;
            }
            if('ruta' in data) {
                if('tpParada' in data.ruta) {
                    data.paradas = data.tpParada;
                }
                if('tpPuntosRuta' in data.ruta) {
                    data.puntos = data.tpPuntosRuta;
                }
                
            }else{
                data.ruta = new Object();
                data.ruta.puntos = data.puntos;
                data.ruta.paradas = data.paradas;
                if('tpPuntosRutaHistorials' in data) {
                    data.ruta.puntos = data.tpPuntosRutaHistorials;
                }
                if('tpParadasHistorials' in data) {
                    data.ruta.paradas = data.tpParadasHistorials;
                }
            }
        }else{
           
            data.forEach((trip,i) => {
                if(trip.nombreRuta == null){
                    trip.nombreRuta = '---';
                }
                if('idConductorNavigation' in trip) {
                    trip.conductor = trip.idConductorNavigation;
                }
                if(this.isDriver) {
                    if('idViajeConductor' in trip) {
                        trip.id = trip.idViajeConductor;
                    }
                    if('idViajeConductorHistorial' in trip) {
                        trip.id = trip.idViajeConductorHistorial;
                    }
                }else{
                    if('idViajeConductor' in trip) {
                        trip.id = trip.idViajeConductor;
                    }
                    if('idViajePasajero' in trip){
                        trip.id = trip.idViajePasajero;
                    }
                    if('idViajePasajeroHistorial' in trip) {
                        trip.id = trip.idViajePasajeroHistorial;
                    }
                }
                if('idRuta' in trip == false) {
                    trip.idRuta = trip.id;
                }
                if('ruta' in trip) {
                    if('tpParada' in trip.ruta) {
                        trip.ruta.paradas = trip.ruta.tpParada;
                    }
                    if('tpPuntosRuta' in trip.ruta) {
                        trip.ruta.puntos = trip.ruta.tpPuntosRuta;
                    }
                    
                }else{
                    trip.ruta = new Object();
                    trip.ruta.puntos = trip.puntos;
                    trip.ruta.paradas = trip.paradas;
                    if('tpPuntosRutaHistorials' in trip) {
                        trip.ruta.puntos = trip.tpPuntosRutaHistorials;
                    }
                    if('tpParadasHistorials' in trip) {
                        trip.ruta.paradas = trip.tpParadasHistorials;
                    }
                }
            });
        }
       
        return data;
    }
    /**
     * fixUnitName
     */
    public fixUnitName(name:string) {
        return name.replace(/st/g,"");
    }
    /**
     * PanicButton
     */
    public PanicButton() {
        if(this.panicActive){
            this.CloseSecurityOptions();
            this.ShowPanicBubble(0);
        }else{
            if(this.showingPanicPrompt == false){
                this.showingPanicPrompt = true;
                confirmar("Activar Botón de Pánico","<b>¿Está seguro/a de activar la alerta?</b> Su alerta será notificada y su ubicación actual se compartirá con el centro de seguridad.","Activar","Cancelar","TE.ConfirmPanicButton()","TE.CancelPanicButtonActivate()");
            }
        }
    }
    /**
     * CancelPanicButtonActivate
     */
    public CancelPanicButtonActivate() {
        this.showingPanicPrompt = false;
    }
    /**
     * ConfirmPanicButton
     */
    public ConfirmPanicButton() {
        if(this.panicActive){
            this.ShowPanicBubble(0);
        }else{
           this.ActivatePanicSignal();
        }
    }
    /**
     * ActivatePanicSignal
     */
    public ActivatePanicSignal(repeatedSignal:boolean = false) {
        wait("Activando...","Por favor espere.");
        this.sendPanicData(this.generatePanicData(),
            (data,resp,xhr)=>{
            if(xhr.status == 200){
                watchCurrentPosition(true);
                this.panicActive = true;
                this.ShowPanicPill();
                this.panicSignalTimeout = setTimeout(()=>{
                    this.sendPanicSignal();
                },10000);
            }else{
                toasty("Lo sentimos, ha ocurrido un error al activar el botón de pánico, por favor intente de nuevo.","error");
            }
            closeWait();
        },
        (err)=>{
            this.showingPanicPrompt = false;
            closeWait();
            toasty("Lo sentimos, ha ocurrido un error al activar el botón de pánico, revise su conexión e intente de nuevo.","error");
        });
    }
    /**
     * sendPanicData
     */
    public sendPanicData(panicData,successCB,errorCB) {
        this.ajax("POST",this.apiUrl+`${this.isDriver ? 'driver' : 'passenger'}/panic/activate`, 
            panicData,
            successCB,
            errorCB
        );
    }
    /**
     * sendPanicSignal
     */
    public sendPanicSignal() {
        this.sendPanicData(this.generatePanicData(),  
            (data,resp,xhr)=>{
                this.panicSignalTimeout = setTimeout(()=>{
                    if(this.panicActive){
                        this.sendPanicSignal();
                    }
                },10000);
             },
            (err)=>{
                this.panicSignalTimeout = setTimeout(()=>{
                    if(this.panicActive){
                        this.sendPanicSignal();
                    }
                },5000);
            }
        );
    }
    /**
     * DeactivatePanicSignal
     */
    public DeactivatePanicSignal() {
        if(this.panicActive){
            this.showingPanicPrompt = false;
            this.ajax("POST",this.apiUrl+`${this.isDriver ? 'driver' : 'passenger'}/panic/deactivate`,
                this.generatePanicData(),
                (data,resp,xhr)=>{
                    if(xhr.status == 200) {
                        toasty("Alerta de Pánico deshabilitada.","warn");
                        stopWatchingPosition();
                    }
                },
                (err)=>{

                }
            );
            this.panicActive = false;
            $("#panicUI").removeClass("show_panic");
            $("body").removeClass("panic-active");
            this.HidePanicBubble();
            clearTimeout(this.panicSignalTimeout);
        }
    }
    /**
     * generatePanicData
     */
    public generatePanicData() {
        var panicData = {
            latitud:    (typeof window.myPosition == "undefined" ? null : window.myPosition.latitude) ,
            longitud:   (typeof window.myPosition == "undefined" ? null :  window.myPosition.longitude)
        };
        if(this.isDriver){
            panicData.idViajeConductor = this.ruta; 
        }else{
            panicData.idViajePasajero = this.ruta;
        }
        return panicData;
    }
    /**
     * ShowPanicPill
     */
    public ShowPanicPill() {
        $("body").addClass("panic-active");
        this.AdjustPanicPill();
        $("#panicUI").addClass("show_panic");
        this.ShowPanicBubble();
    }
    /**
     * ShowPanicBubble
     */
    public ShowPanicBubble(delay:number = 1500) {
        setTimeout(()=>{
            $(".panic_bubble").addClass("show_bubble");
        },delay);
    }
    /**
     * AdjustPanicPill
     */
    public AdjustPanicPill() {
        if($(window).width() > $("html").height()) { //LANDSCAPE
            $("body.panic-active #panicUI").css("bottom",10);
        }else{
            $("body.panic-active #panicUI").css("bottom",$(".trip_card.current_trip").outerHeight()-5);
            setTimeout(()=>{
                $("body.panic-active #panicUI").css("bottom",$(".trip_card.current_trip").outerHeight()-5);
            },250);
        }
    }
    /**
     * HidePanicBubble
     */
    public HidePanicBubble() {
        $(".panic_bubble").removeClass("show_bubble");
    }
    /**
     * SendTripReport
     */
    public SendTripReport() {
        toasty("Enviando reporte...");
        $("#trip_report .btn").addClass("disabled");
        $("#reportBody,#reportReason").attr("disabled",true);
        
    }
    /**
     * decodeJWT
     */
    public decodeJWT(jwt) {
        var tokens = jwt.split(".");
        return JSON.parse(atob(tokens[1]));
    }
    
    /**
     * EnterRegToken
     */
    public EnterRegToken() {
        masterClose = ()=>{this.ExitRegister();};
        $("#reg_token").val('');
        $("#teapp_regtoken").removeClass("hidden");
    }
    /**
     * closeRegisterForm
     */
    public closeRegisterForm() {
        $("#teapp_register input").removeClass("valid invalid").val('');
        M.updateTextFields();
        $("#teapp_register,.teapp_reg_errors").addClass("hidden");
        $("#teapp_regtoken").removeClass("hidden");
        $("#teapp_register label").removeClass("active");
    }
    /**
     * ExitRegister
     */
    public ExitRegister(forceClose:boolean = false) {
        if(forceClose) {
            this.closeRegisterForm();
        }
        if($("#teapp_register").is(":visible")) {
           this.closeRegisterForm();
        }else{
            masterClose = ()=>{closeApp();};
            $("#teapp_regtoken").addClass("hidden");
        }
    }
    /**
     * VerifyRegToken
     */
    public VerifyRegToken() {
        this.regToken = trim($("#reg_token").val());
        if(this.regToken == '') {
            toasty("Por favor introduzca un código de registro.","error");
        }else{
            wait("Verificando código...","Por favor, espere.");
            this.ajax("POST",this.apiUrl+`login/verify`,this.regToken,
                (data,resp,xhr)=>{
                    closeWait();
                    if(xhr.status == 200) {
                        this.regUserData = data;
                        this.StartRegister();
                    }else{
                        toasty("No se pudo validar el código, por favor intente más tarde.");
                    }
                },
                (err)=>{
                    closeWait();
                    switch(err.status) {
                        case 404:
                            toasty("No se encontró el código. Por favor introduzca un código de registro válido.");
                        break;
                        default:
                            toasty("No se pudo validar el código, por favor intente más tarde (-2).");
                        break;
                    }
                }
            );
        }
      
    }
    /**
     * StartRegister
     */
    public StartRegister() {
        $(".teapp_company").html(this.regUserData.nombre.toUpperCase());
        $("#teapp_regtoken").addClass("hidden");
        $("#teapp_register").removeClass("hidden");
    }

    /**
     * RegisterUser
     */
    public RegisterUser() {
        var errors = [],
            nombre =    trim($("#teapp_nombre").val()),
            apellido_materno = trim($("#teapp_apellido_materno").val()),
            apellido_paterno = trim($("#teapp_apellido_paterno").val()),
            correo =    trim($("#teapp_correo").val()),
            /*conf_correo=trim($("#teapp_correo_conf").val()),*/
            pass =      trim($("#teapp_pass").val()),
            pass_conf=  trim($("#teapp_pass_conf").val());
        $("#teapp_register input").removeClass("invalid");

        if(nombre == '') {
            errors.push('Introduzca un nombre válido.');
            $("#teapp_nombre").addClass("invalid");
        }
        if(apellido_materno == '') {
            errors.push('Introduzca su apellido materno.');
            $("#teapp_apellido_materno").addClass("invalid");
        }
        if(apellido_paterno == '') {
            errors.push('Introduzca su apellido paterno.');
            $("#teapp_apellido_paterno").addClass("invalid");
        }
        if(correo == '') {
            //errors.push('Introduzca un correo.');
            errors.push('Introduzca un nombre de usuario');
            $("#teapp_correo").addClass("invalid");
        }
        if(correo.match(/^[a-zA-Z0-9._]{5,20}$/gi) == null) {
            errors.push('Introduzca un nombre de usuario válido con un mínimo de 5 caracteres (solo se admiten letras y números, sin espacios, caracteres permitidos: . y _).');
            $("#teapp_correo").addClass("invalid");
        }
        /*if(conf_correo == '') {
            errors.push('Confirme su correo.');
            $("#teapp_correo_conf").addClass("invalid");
        }
        if(validateEmail(correo) == false) {
            errors.push('Introduzca un correo válido.');
            $("#teapp_correo").addClass("invalid");
        }
        if(validateEmail(conf_correo) == false) {
            errors.push('Utilice un correo válido para confirmarlo.');
            $("#teapp_correo_conf").addClass("invalid");
        }
        if(correo != conf_correo) {
            errors.push('Los correos no coinciden.');
            $("#teapp_correo").addClass("invalid");
            $("#teapp_correo_conf").addClass("invalid");
        }*/
        if(pass == '') {
            errors.push('Introduzca una contraseña.');
            $("#teapp_pass").addClass("invalid");
        }
        if(pass.length < 6){
            errors.push('La contraseña debe contener 6 caracteres como mínimo.');
            $("#teapp_pass").addClass("invalid");
        }
        if(pass != pass_conf) {
            errors.push('Las contraseñas no coinciden.');
            $("#teapp_pass").addClass("invalid");
            $("#teapp_pass_conf").addClass("invalid");
        }
        if(errors.length > 0){
            $(".teapp_reg_errors").removeClass('hidden');
            $(".teapp_error_list").html("");
            errors.forEach(err =>{
                $(".teapp_error_list").append(`<li>${err}</li>`);
            });
            location.href='#reg_errors';
        }else{
            wait("Registrando cuenta...","Por favor espere.");
            this.ajax("POST",this.apiUrl+`login/register`, {
                nombre:             nombre,
                apellidoPaterno:    apellido_paterno,
                apellidoMaterno:    apellido_materno,
                rol:                this.regUserData.rol,
                idEmpresa:          this.regUserData.idEmpresa,
                usuario:            correo,
                contrasena:         pass,
                token:              this.regToken
            },
                (data,resp,xhr)=>{
                    closeWait();
                    if(xhr.status == 200) {
                        this.regToken = '';
                        $("#loginUser").val(correo);
                        alerta("Cuenta Registrada",`Su cuenta ha sido registrada en la empresa <b>${this.regUserData.nombre.toUpperCase()}</b>, ya puede iniciar sesión con sus datos.`);
                        this.ExitRegister(true);
                    }else{
                        toasty("Lo sentimos, no se pudo registrar su cuenta, por favor intente de nuevo más tarde (-1).");
                    }
                },
                (err)=>{
                    closeWait();
                    switch(err.status){
                        case 404:
                            toasty("Lo sentimos, este nombre de usuario ya se encuentra registrado, por favor introduzca otro (-4).");
                            break;
                        default:
                            toasty("Lo sentimos, no se pudo registrar su cuenta, por favor intente de nuevo más tarde (-2).");
                            break;
                    }
                    
                }
            );
        }
    }
}