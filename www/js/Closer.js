var CloserClass = /** @class */ (function () {
    function CloserClass() {
        this.closingAll = false;
        this.closeData = [
            {
                f: "Track.CloseSidebar()",
                t: 1
            },
            {
                f: "Track.CloseTrackingSelect()",
                t: 1
            },
            {
                f: "Track.CloseTrackingSelect()",
                t: 1
            },
            {
                f: "Track.CloseHistory()",
                t: 1
            },
            {
                f: "Geos.CloseSidebar('geoShapes')",
                t: 1
            },
            {
                f: "Checkpoints.CloseSidebar('groupsList')",
                t: 1
            },
            {
                f: "Checkpoints.CloseSidebar('pointsList')",
                t: 1
            },
            {
                f: "Fuel.Close()",
                t: 2
            },
            {
                f: "Cars.CloseCars()",
                t: 2
            },
            {
                f: "Share.CloseShare()",
                t: 1
            },
            {
                f: "PushNotifs.Close()",
                t: 3
            },
            {
                f: "Config.CloseSettings()",
                t: 1
            },
            {
                f: "Config.CloseConsole()",
                t: 1
            },
            {
                f: "Config.CloseHelp()",
                t: 2
            },
            {
                f: "TE.CloseCheckIn()",
                t: 2
            },
            {
                f: "TE.CloseHorario(true)",
                t: 2
            },
            {
                f: "TE.CloseViajes(true)",
                t: 2
            },
            {
                f: "Interface.CloseApagadoMotor()",
                t: 1
            }
        ];
    }
    /**
     * CloseAll
     */
    CloserClass.prototype.CloseAll = function () {
        this.closingAll = true;
        if ($(".fullApp").hasClass("hidden") == false && Config._fullApp == true) {
            for (var c = 0; c < this.closeData.length; c++) {
                for (var t = 0; t < this.closeData[c].t; t++) {
                    eval(this.closeData[c].f);
                }
            }
        }
        this.closingAll = false;
    };
    return CloserClass;
}());
