var animSpeed = 200, masterClose = null;
var Menu = /** @class */ (function () {
    function Menu() {
    }
    //Abre el menu
    Menu.prototype.Open = function () {
        $(".fade").fadeTo(animSpeed, 1);
        //El menu ya esta abierto?
        if ($("#main").is(":visible")) {
            if (Menu._isOpen == false) {
                $(".menu").fadeIn('fast', function () {
                    //Guardamos el nuevo estado del menu
                    //FUNCIONA LA ANIMACION PERO NO GUARDA EL ESTADO
                    Menu._isOpen = true;
                    masterClose = function () { this.Hide(); };
                });
                masterClose = function () { this.Hide(); };
            }
            else {
                //Si el menu ya esta abierto, cierralo papu
                this.Hide();
            }
        }
    };
    //Oculta el menu
    Menu.prototype.Hide = function () {
        //Esta abierto el menu?
        if (Menu._isOpen == true) {
            //Animaciones chidoris
            $(".fade").fadeTo(animSpeed, 0, function () { $(this).hide(); });
            $(".menu").fadeOut(animSpeed, function () {
                Menu._isOpen = false;
                try {
                    masterClose = function () { navigator.app.exitApp(); };
                }
                catch (e) { }
            });
        }
    };
    //Obtiene el estado del menu
    Menu.prototype.GetStatus = function () {
        return Menu._isOpen;
    };
    //Establece el estado del menu
    Menu.prototype.SetStatus = function (openStatus) {
        Menu._isOpen = openStatus;
    };
    //Indica el estado del menu
    //True = Abierto
    //False = Cerrado [Default]
    Menu._isOpen = false;
    return Menu;
}());
var MapControls = /** @class */ (function () {
    function MapControls() {
        this.ResetArrays();
        MapControls.objectList = new H.map.Group();
        MapControls.routeGroup = new H.map.Group();
        map.addObject(MapControls.objectList);
        map.addObject(MapControls.routeGroup);
        MapControls._routePoints = new H.geo.LineString();
    }
    MapControls.prototype.ResetArrays = function () {
        MapControls.Circles = null;
        MapControls.Markers = null;
        MapControls.Polygons = null;
        MapControls.Circles = new Array();
        MapControls.Markers = new Array();
        MapControls.Polygons = new Array();
    };
    MapControls.prototype.CreateGeoCircle = function () {
        if (MapControls._CreatingShape == false) {
            MapControls._CreatingShape = true;
            var centerCoords = map.getCenter();
            MapControls.Circles.push({ "circle": new H.map.Circle(
                // The central point of the circle
                { lat: centerCoords.lat, lng: centerCoords.lng }, 
                // The radius of the circle in meters
                1000, {
                    style: {
                        strokeColor: 'rgba(55, 85, 170, 0.6)',
                        lineWidth: 2,
                        fillColor: 'rgba(0, 128, 0, 0.7)' // Color of the circle
                    }
                }), "puntos": new Array() });
            map.addObject(MapControls.Circles[MapControls.Circles.length - 1].circle);
        }
    };
    MapControls.prototype.CreateGeoPoly = function () {
        if (MapControls._CreatingShape == false) {
            MapControls._CreatingShape = true;
            $(".centerMarker").show();
            var coordenada = map.getCenter();
            map.setZoom(15);
            this.DrawCirclePoly(coordenada);
        }
    };
    MapControls.prototype.PrintCirclePoly = function (coordenadas) {
        MapControls.NewPolyGeo.puntos = null;
        MapControls.NewPolyGeo.puntos = new Array();
        var metros = calcMetros(coordenadas.lat, coordenadas.lng);
        var r1 = metros[0] * MapControls._polyRadius;
        var r2 = metros[1] * MapControls._polyRadius;
        var pi = Math.PI;
        var lineString = new H.geo.LineString();
        for (n = 0; n < MapControls._polyCircleSides; n++) {
            var x = parseFloat((Math.sin(n / MapControls._polyCircleSides * 2 * pi) * r1).toFixed(6)) + coordenadas.lat;
            var y = parseFloat((Math.cos(n / MapControls._polyCircleSides * 2 * pi) * r2).toFixed(6)) + coordenadas.lng;
            var punto = [x, y, 0];
            console.log("Coordenadas: " + punto[0] + "," + punto[1] + ", posicion: " + n);
            //Push a punto coordenada entre cada angulo de la geocerca
            MapControls.NewPolyGeo.puntos.push(punto);
            //Dibuja el punto coordenada de la geocerca
            lineString.pushLatLngAlt(x, y, 0);
        }
        MapControls.geoPolyMarker = new H.map.Polygon(lineString, {
            style: {
                fillColor: '#FF000033',
                strokeColor: '#FFF',
                lineWidth: 3
            }
        });
        map.addObject(MapControls.geoPolyMarker);
    };
    MapControls.prototype.DrawCirclePoly = function (coordenadas) {
        this.PrintCirclePoly(coordenadas);
        // LISTENERS
        // MAPVIEWCHANGE: Parecido al evento IDLE de GM, al iniciar el evento 
        // se remueve la ultima instancia de la polyGeocerca.
        map.addEventListener("mapviewchange", deletePolyInstance, false);
        MapControls._geoEvents.push({
            "target": "polyCircle",
            "type": "mapviewchange",
            "function": deletePolyInstance
        });
        // MAPVIEWCHANGEEND: Al terminar el evento MAPVIEWCHANGE se obtienen 
        // las coordenadas del centro del mapa, se crea otra instancia de 
        // MapControls y se solicita repintar otra polyGeocerca.
        map.addEventListener("mapviewchangeend", repaintPolyInstance, false);
        MapControls._geoEvents.push({
            "target": "polyCircle",
            "type": "mapviewchangeend",
            "function": repaintPolyInstance
        });
    };
    MapControls.prototype.CreateGeoSquare = function () {
        if (MapControls._CreatingShape == false) {
            MapControls._CreatingShape = true;
            $(".centerMarker").show();
            var coordenada = map.getCenter();
            map.setZoom(15);
            this.DrawSquarePoly(coordenada);
        }
    };
    MapControls.prototype.DrawSquarePoly = function (coordenadas) {
        this.PrintSquarePoly(coordenadas);
        // LISTENERS
        // MAPVIEWCHANGE: Parecido al evento IDLE de GM, al iniciar el evento 
        // se remueve la ultima instancia de la polyGeocerca.
        map.addEventListener("mapviewchange", deleteSquareInstance, false);
        MapControls._geoEvents.push({
            "target": "polySquare",
            "type": "mapviewchange",
            "function": deleteSquareInstance
        });
        // MAPVIEWCHANGEEND: Al terminar el evento MAPVIEWCHANGE se obtienen 
        // las coordenadas del centro del mapa, se crea otra instancia de 
        // MapControls y se solicita repintar otra polyGeocerca.
        map.addEventListener("mapviewchangeend", repaintSquareInstance, false);
        MapControls._geoEvents.push({
            "target": "polySquare",
            "type": "mapviewchangeend",
            "function": repaintSquareInstance
        });
    };
    MapControls.prototype.PrintSquarePoly = function (coordFiguras) {
        var lineString = new H.geo.LineString();
        var distancia = calcMetros(coordFiguras.lat, coordFiguras.lng);
        var coordPrincipal = coordFiguras;
        console.log("coordPrincipal", coordPrincipal);
        for (i = 0; i < (MapControls._squarePerimeter / 2); i++) {
            coordPrincipal.lat += distancia[0];
            coordPrincipal.lng -= distancia[1];
        }
        lineString.pushLatLngAlt(coordPrincipal.lat, coordPrincipal.lng, 0);
        //Geocerca.Puntos.push(coordenada);
        for (i = 0; i < (MapControls._squarePerimeter); i++) {
            coordPrincipal.lng += distancia[1];
        }
        lineString.pushLatLngAlt(coordPrincipal.lat, coordPrincipal.lng, 0);
        //Geocerca.Puntos.push(coordenada);
        for (i = 0; i < (MapControls._squarePerimeter); i++) {
            coordPrincipal.lat -= distancia[0];
        }
        lineString.pushLatLngAlt(coordPrincipal.lat, coordPrincipal.lng, 0);
        //Geocerca.Puntos.push(coordenada);
        for (i = 0; i < (MapControls._squarePerimeter); i++) {
            coordPrincipal.lng -= distancia[1];
        }
        lineString.pushLatLngAlt(coordPrincipal.lat, coordPrincipal.lng, 0);
        //Geocerca.Puntos.push(coordenada);
        MapControls.geoSquareMarker = new H.map.Polygon(lineString, {
            style: {
                fillColor: '#FF000033',
                strokeColor: '#FFF',
                lineWidth: 3
            }
        });
        map.addObject(MapControls.geoSquareMarker);
    };
    MapControls.prototype.CreateRoute = function () {
        if (MapControls._CreatingShape == false) {
            MapControls._CreatingShape = true;
            MapControls._CreatingRoute = true;
        }
    };
    MapControls.prototype.AddRoutePoint = function (_lat, _lng) {
        MapControls._routePoints.pushPoint({ lat: _lat, lng: _lng });
        try {
            MapControls.routeGroup.removeAll();
        }
        catch (e) { }
        if (MapControls._routePoints.getPointCount() > 1) {
            MapControls.polyRoute = null;
            MapControls.polyRoute = new H.map.Polyline(MapControls._routePoints, {
                style: { lineWidth: 6 },
                arrows: {
                    fillColor: 'white',
                    frequency: 15,
                    width: 0.8,
                    length: 0.7
                }
            });
            MapControls.routeGroup.addObject(MapControls.polyRoute);
        }
    };
    MapControls.prototype.AddMarkerWithCoords = function (_lat, _lng) {
        if (MapControls._CreatingRoute == false) {
            MapControls.Markers.push({ "marker": new H.map.Marker({ lat: _lat, lng: _lng }) });
            //map.addObject(MapControls.Markers[MapControls.Markers.length-1].marker);
            MapControls.objectList.addObject(MapControls.Markers[MapControls.Markers.length - 1].marker);
        }
    };
    MapControls.prototype.RemoveObjects = function () {
        MapControls._CreatingShape = false;
        MapControls._CreatingRoute = false;
        /*try{
            for(var i=0;i<MapControls.Circles.length;i++){
                map.removeObject(MapControls.Circles[i].circle);
            }
        }catch(e){}
        try{
            for(var i=0;i<MapControls.Markers.length;i++){
                map.removeObject(MapControls.Markers[i].marker);
            }
        }catch(e){}*/
        MapControls._routePoints = null;
        MapControls._routePoints = new H.geo.LineString();
        MapControls.objectList.removeAll();
        MapControls.routeGroup.removeAll();
        this.ResetArrays();
        try {
            map.removeObject(MapControls.geoPolyMarker);
        }
        catch (e) { }
        try {
            map.removeObject(MapControls.geoSquareMarker);
        }
        catch (e) { }
        $(".centerMarker").hide();
        this.RemoveListeners();
    };
    MapControls.prototype.RemoveListeners = function () {
        for (var i = 0; i < MapControls._geoEvents.length; i++) {
            map.removeEventListener(MapControls._geoEvents[i].type, MapControls._geoEvents[i].function, false);
        }
    };
    MapControls.prototype.ZoomObjects = function () {
        try {
            map.setViewBounds(MapControls.objectList.getBounds(), false);
        }
        catch (e) { }
    };
    MapControls.prototype.SetCenterMarkerColor = function (hex) {
        $(".pin").css("background", "#" + hex);
        $(".pin:after").css("box-shadow", "0 0 1px 2px #" + hex);
    };
    MapControls.prototype.GetCircles = function () {
        return MapControls.Circles;
    };
    MapControls._CreatingShape = false;
    MapControls._CreatingRoute = false;
    MapControls._polyCircleSides = 15;
    MapControls.objectList = null;
    MapControls.routeGroup = null;
    MapControls.Circles = new Array();
    MapControls.Markers = new Array();
    MapControls.Polygons = new Array();
    MapControls.NewPolyGeo = new Object();
    MapControls.geoPolyMarker = null;
    MapControls.geoSquareMarker = null;
    MapControls.polyRoute = null;
    MapControls._geoEvents = new Array();
    MapControls._routePoints = null;
    MapControls._polyRadius = 500; //Radio default de la PolyGeocerca
    MapControls._squarePerimeter = 500; //Perimetro default de la Geocerca Cauadrada
    return MapControls;
}());
//# sourceMappingURL=Classes.js.map