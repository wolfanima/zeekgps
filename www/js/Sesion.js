/// <reference path ="jquery.d.ts"/> 
/**
 * SESION
 * Clase que maneja todo lo que tenga que ver con Sesiones
 * (Login, Logout, Datos de Usuario, etc)
 */
var Sesion = /** @class */ (function () {
    function Sesion() {
        this._usuario = "";
        this._contrasena = "";
        this._isLogged = false;
        this._sessionStrikes = 0;
    }
    /**
     * Regresa el nombre de Usuario
     */
    Sesion.prototype.GetUsername = function () {
        return this._usuario;
    };
    /**
     * [CONSTRUCTOR]
     * Manda llamar la funcion para iniciar la sesion en plataforma
     * @param usuario Nombre de usuario del cliente
     * @param contrasena Contraseña del cliente
     */
    Sesion.prototype.Sesion = function (usuario, contrasena) {
        if (isTEApp) {
            TE.Sesion(usuario, contrasena);
        }
        else {
            this.IniciarSesion(usuario, contrasena);
        }
    };
    /**
     * Inicia sesion en plataforma y obtiene datos del cliente
     * @param usuario Nombre de usuario del cliente
     * @param contrasena Contraseña del cliente
     */
    Sesion.prototype.IniciarSesion = function (usuario, contrasena) {
        var _this = this;
        this._usuario = usuario;
        this._contrasena = contrasena;
        if (this._usuario == "" || this._contrasena == "") {
            alerta(lang("sesionerror"), lang("sesiondata"));
            closeWait();
            return;
        }
        $(".abortConnection").show();
        wait("<es>Iniciando Sesión</es><en>Logging On</en>", "<es>Por favor, espere...</es><en>Please wait...</en>");
        $("#loginPass").prop("type", "password");
        $(".show-pass i").removeClass("fa-eye").addClass("fa-eye-slash");
        window.sesionAjax = ajax("POST", authUrl + "IniciaSesion", {
            "user": this._usuario,
            "pass": this._contrasena
        }, function (data, status, xhr) {
            //console.warn("LOGIN",data);
            if (data.token == null) {
                if (data.user.Status == 2) {
                    alerta(lang("incorrectdata"), lang("sesiondata"));
                }
                else if (data.user.Status == 4) {
                    alerta(lang("sesionerror"), lang("denied_enter"));
                }
                else if (data.user.Status == 0) {
                    alerta(lang("sesionerror"), lang("app_blocked"));
                }
                else if (data.status == "DENIED") {
                    alerta(lang("sesionerror"), lang("denied_enter"));
                }
                else {
                    alerta(lang("sesionerror"), lang("tryagain"));
                }
                unsubscribePush();
                closeWait();
            }
            else {
                if (data.user.Status == 1 ||
                    data.user.Status == 6) {
                    if (data.user.Status == 6) {
                        alerta("Aviso Zeek GPS", lang("factura_vencer"));
                    }
                    $(".abortConnection").hide();
                    $("#loginPass").val('');
                    $(".user-view .name").html(data.user.Nombre);
                    setKey("zeekgpsUser", _this._usuario);
                    setKey("zeekgpsPass", _this._contrasena);
                    window.userData = data;
                    window.userData.token = encodeURI(window.userData.token);
                    window.justLogged = true;
                    window.vehiculos = data.vehiculos.sort(sortDescripcion);
                    window.userData.soporte = JSON.parse(data.soporte);
                    $(".user-view .email").html(window.vehiculos.length + " <en>vehicles</en><es>vehículos</es>");
                    Cars.ExtractTags();
                    window.serverTime = data.serverTime;
                    window.plataforma = data.tipo;
                    var m = new moment(serverTime);
                    userData.username = SesionEngine.GetUsername();
                    window.zona = parseInt(m.format("Z").substr(0, 3));
                    userData.permisos = data.permisos;
                    userData.zona = window.zona;
                    userData.serverTime = data.serverTime;
                    userData.tipo = data.tipo;
                    userData.user_id = getParameterByName("UsuarioID", "&" + userData.token.substr(8, userData.token.length));
                    userData.clienteID = getParameterByName("Cliente", userData.token);
                    userData.clientToken = getParameterByName("Token", userData.token);
                    saveSet("zeekUserdata", window.userData);
                    console.warn("Cliente ID", userData.clienteID);
                    console.warn("Usuario ID", userData.user_id);
                    _this.Startup();
                }
                else {
                    unsubscribePush();
                    alerta(lang("sesionerror"), lang("sesiondata"));
                    closeWait();
                }
            }
        }, function (data) {
            unsubscribePush();
            if (data.statusText != "abort") {
                alerta(lang("sesionerror"), lang("tryagain"));
            }
            closeWait();
        }, 90000);
    };
    /**
     * Obtiene los datos basicos de la cuenta
     */
    Sesion.prototype.GetUserData = function () {
        var _this = this;
        console.warn("GetUserData exec");
        $(".abortConnection").show();
        window.sesionAjax = ajax("POST", authUrl + "getUserData", {
            "token": window["userData"]["token"],
        }, function (data, status, xhr) {
            //console.warn("GETUSERDATA",data);
            window.vehiculos = data.vehiculos;
            window.justLogged = true;
            if (data.user.Status == 0) {
                alerta(lang("sesionerror"), lang("app_blocked"));
                window.justLogged = false;
                $("#loginPage").removeClass("hidden");
                closeWait();
                unsubscribePush();
            }
            else {
                if (data.tipo == null) {
                    window.justLogged = false;
                    $("#loginPage").removeClass("hidden");
                    closeWait();
                    alerta(lang("error"), lang("expiredsesion"));
                    localStorage.removeItem("zeekUserdata");
                    unsubscribePush();
                    _this.ResetTheme();
                }
                else {
                    $(".abortConnection").hide();
                    $(".user-view .email").html(window.vehiculos.length + " <en>vehicles</en><es>vehículos</es>");
                    Cars.ExtractTags();
                    window.serverTime = data.serverTime;
                    window.plataforma = data.tipo;
                    var m = new moment(serverTime);
                    window.zona = parseInt(m.format("Z").substr(0, 3));
                    $(".user-view .name").html(userData.user.Nombre);
                    userData.permisos = data.permisos;
                    userData.zona = window.zona;
                    userData.serverTime = data.serverTime;
                    userData.vehiculos = data.vehiculos.sort(sortDescripcion);
                    userData.soporte = JSON.parse(data.soporte);
                    userData.tipo = data.tipo;
                    userData.user_id = getParameterByName("UsuarioID", "&" + userData.token.substr(8, userData.token.length));
                    userData.clienteID = getParameterByName("Cliente", userData.token);
                    userData.clientToken = getParameterByName("Token", userData.token);
                    saveSet("zeekUserdata", window.userData);
                    console.warn("Cliente ID", userData.clienteID);
                    console.warn("Usuario ID", userData.user_id);
                    window.distMetric = capitalizeFirstLetter(getKey("zeekgpsDistMetric"));
                    window.fluidMetric = capitalizeFirstLetter(getKey("zeekgpsFluidMetric"));
                    window.tempMetric = getKey("zeekgpsTempMetric");
                    _this.Startup();
                }
            }
        }, function (data) {
            userData.tipo = "transporte";
            _this.PrepUser();
            window.sessionError = true;
            if (data.statusText != "abort") {
                if (!!window.cordova == true) {
                    alerta(lang("sesionerror"), lang("tryagain"));
                }
                else {
                    toasty(lang("tryagain"));
                }
            }
            unsubscribePush();
            $("#loginPage").removeClass("hidden");
            closeWait();
        }, 90000);
    };
    /**
     * Metodo que inicializa varios metodos que requiere una sesion inicializada
     */
    Sesion.prototype.Startup = function () {
        //userData.permisos.SoloRastreo = true;
        console.warn("Startup executed");
        window.justLogged = false;
        this._isLogged = true;
        userData.AppVersion = version;
        $(".logged").removeClass("hidden");
        $("#loginPage").addClass("hidden"); //Elementos que requieren una sesion ahora son visibles
        this.PrepUser();
        if (getKey("zeekgpsBanner-" + userData.username) == null) {
            setKey("zeekgpsBanner-" + userData.username, 1);
            $(".bannerSwitch input").prop("checked", true);
        }
        else if (parseInt(getKey("zeekgpsBanner-" + userData.username)) == 1) {
            $(".bannerSwitch input").prop("checked", true);
        }
        else {
            $(".bannerSwitch input").prop("checked", false);
        }
        if (getKey("zeekgpsFull-" + userData.username) == null) {
            setKey("zeekgpsFull-" + userData.username, 1);
            $(".fullSwitch input").prop("checked", true);
            Config.setFullApp(true);
        }
        else if (parseInt(getKey("zeekgpsFull-" + userData.username)) == 1) {
            $(".fullSwitch input").prop("checked", true);
            Config.setFullApp(true);
        }
        else {
            $(".fullSwitch input").prop("checked", false);
            Config.setFullApp(false);
        }
        this.FillOnlyRC();
        if (this.Permisos("SoloRastreo") == true && isTEApp == false) {
            Track.SoloRastreo(true);
        }
        if (this.Permisos("Fuel") == true) {
            $(".fuel-menu").removeClass("hidden");
        }
        else {
            $(".fuel-menu").addClass("hidden");
        }
        console.log("Saved Push", Push._savedPush);
        console.warn("zeekgpsPush", getKey("zeekgpsPush"));
        if (isTEApp == false) {
            if (parseInt(getKey("zeekgpsPush")) == 1 || getKey("zeekgpsPush") == null) {
                $(".pushSwitch input").prop("checked", true);
                try {
                    PushNotifs.Activate(true);
                }
                catch (e) {
                    console.error("pushNotifs Activate Startup", e);
                }
            }
        }
        MenuCtrl.Construct();
        if (isTEApp == false) {
            PushNotifs.CreatePushChannels();
            setTimeout(function () {
                PushNotifs.DisplayPush();
            }, 1000);
            PushNotifs.loadHuaweiPush();
            this.AutoRCInit();
            this.checkSesion();
        }
        /* INSOMNIA */
        this.InsomniaInit();
        closeWait();
        views();
        Support.InitRemoteConsole();
        ;
    };
    /**
     * ResetTheme
     */
    Sesion.prototype.ResetTheme = function () {
        var barcolor = "#d9400e";
        if (isTEApp) {
            $("body").removeClass("transporte flotillas");
            $("body").addClass("light");
            barcolor = "#4a62bc";
        }
        else {
            $("body").removeClass("transporte flotillas light");
            $("body").addClass("transporte");
            $(".theme").attr("href", "css/transporte.css");
        }
        $(".theme-color-meta").attr("content", barcolor);
        try {
            StatusBar.backgroundColorByHexString(barcolor);
            StatusBar.overlaysWebView(false);
            StatusBar.show();
        }
        catch (e) { }
    };
    /**
     * En ciertas cuentas es necesario mostrar solo el Rastreo Continuo,
     * este metodo inicializa la app como tal (requiere permiso SoloRastreo)
     */
    Sesion.prototype.FillOnlyRC = function () {
        $("#soloRastreo .list").html("");
        if (vehiculos.length == 0) {
            $("#soloRastreo .noContent").removeClass("hidden");
        }
        else {
            for (var i = 0; i < vehiculos.length; i++) {
                if (vehiculos[i].Estado == -1) {
                    $("#soloRastreo .list").append("\n                        <li onclick=\"Track.TrackUnit('" + vehiculos[i].Unidad + "')\">\n                            <div class=\"left-icon car-icon\">\n                                <span style=\"background-image:url('" + getCarIconUri(vehiculos[i].IconoID, true) + "')\"></span>\n                            </div>\n                            <div class=\"textBox\">\n                                <div class=\"listText infoCarname\">" + vehiculos[i].Descripcion + "</div>\n                                <div class=\"listText listSubtext\"><es>Chofer</es><en>Driver</en>: " + na(vehiculos[i].Chofer) + "</div>\n                            </div>\n                            <div class=\"chevron\">\n                                <span class=\"lsf lsf-icon\" icon=\"right\"></span>\n                            </div>\n                        </li>\n                    ");
                }
            }
        }
    };
    /**
     * Cierra la sesion del usuario en la app
     */
    Sesion.prototype.CerrarSesion = function () {
        wait("Cerrando sesión...", "Por favor, espere.");
        TE.HideSuggestion();
        if (isTEApp) {
            logoutEvents();
            return;
        }
        if (!!window.cordova == true) { //APP MOVIL
            logoutPush();
        }
        else { //WEB o LOCAL
            logoutEvents();
        }
    };
    /**
     * Regresa si el usuario tiene una sesion abierta en la app
     */
    Sesion.prototype.isLogged = function () {
        return this._isLogged;
    };
    /**
     * Obtiene el tiempo actual en plataforma
     */
    Sesion.prototype.getServerTime = function () {
        ajax("POST", authUrl + "getServerTime", {
            token: window["userData"]["token"]
        }, function (data) {
            window.serverTime = data.time;
            var m = new moment(serverTime);
            window.zona = parseInt(m.format("Z").substr(0, 3));
        }, function () {
        }, 30000);
    };
    /**
     * AutoRCInit
     */
    Sesion.prototype.AutoRCInit = function () {
        if (isTEApp) {
            setKey("zeekgpsAutoRC-" + userData.username, 1);
        }
        if (loadSet("zeekgpsAutoRCList-" + userData.username, true) == null) {
            vehiculos.forEach(function (elem) {
                if (elem.Estado == -1) {
                    Tracking._tempUnidades.push(elem.Unidad);
                }
            });
            Config._autoRCList = Tracking._tempUnidades;
            saveSet("zeekgpsAutoRCList-" + userData.username, Tracking._tempUnidades);
        }
        if (parseInt(getKey("zeekgpsAutoRC-" + userData.username)) == 1) {
            $(".autoRCSwitch input").attr("checked", true);
            Tracking._tempUnidades = loadSet("zeekgpsAutoRCList-" + userData.username);
            Config._autoRCList = Tracking._tempUnidades;
            Track.StartTracking("", true, true);
        }
    };
    /**
     * Inicializa el plugin de Insomnia
     * (evita que la pantalla del dispositivo se apague)
     */
    Sesion.prototype.InsomniaInit = function () {
        try {
            if (getKey("zeekgpsScreen-" + userData.username) != null) {
                if (parseInt(getKey("zeekgpsScreen-" + userData.username)) == 1) {
                    //ACTIVAR
                    $(".screenSwitch input").attr("checked", true);
                    try {
                        window.plugins.insomnia.keepAwake();
                    }
                    catch (e) {
                        if (!!window.cordova) {
                            console.error("Insomnia with true: " + e.message);
                        }
                    }
                }
                else {
                    //DESACTIVAR
                    $(".screenSwitch input").attr("checked", false);
                    try {
                        window.plugins.insomnia.allowSleepAgain();
                    }
                    catch (e) {
                        if (!!window.cordova) {
                            console.error("Insomnia with false: " + e.message);
                        }
                    }
                }
            }
            else {
                setKey("zeekgpsScreen-" + userData.username, 1);
                //ACTIVAR
                $(".screenSwitch input").attr("checked", true);
                try {
                    window.plugins.insomnia.keepAwake();
                }
                catch (e) {
                    console.error("Insomnia with null: " + e.message);
                }
            }
        }
        catch (e) {
            console.error("Insomnia: " + e.message);
        }
    };
    /**
     * processAppUI
     */
    Sesion.prototype.processAppUI = function () {
        if (isTEApp) {
            $(".user-view .name").html(userData.nombreUsuario);
            $(".user-view .email").html(userData.nombreEmpresa);
            $(".trip_suggestion_" + (TE.isDriver ? 'driver' : 'passenger') + " .teapp_box_title").html("\u00A1Bienvenido/a, " + userData.nombreUsuario + "!");
            $(".actioncircle").removeClass("hidden");
            $(".pushOption").removeClass("onlyRC");
            $(".mainTop .tool.right,.onlyRC").addClass("hidden");
            try {
                StatusBar.backgroundColorByHexString("#4a62bc");
                StatusBar.overlaysWebView(false);
                StatusBar.show();
            }
            catch (e) { }
            $(".user-view .background").css({ "background": "", "background-size": "cover" });
        }
        else {
            $(".user-view .servicelogo").prop("src", "img/zeek_logo_" + userData.tipo + ".png");
            $(".user-view .background").css({ "background": "url(./img/" + userData.tipo + "_bg.png) no-repeat left bottom", "background-size": "cover" });
            $("#favicon").prop("href", "img/fav_" + userData.tipo + ".ico");
            $(".theme").attr("href", "css/" + userData.tipo + ".css");
            $("title").html('Zeek GPS App');
        }
    };
    /**
     * Prepara la personalizacion de la app acorde a los datos del usuario
     */
    Sesion.prototype.PrepUser = function () {
        checkBuild();
        console.warn("Prepping user...");
        if (isTEApp) {
            $("body").removeClass("transporte flotillas").addClass("light");
        }
        else {
            $("body").removeClass("transporte flotillas light").addClass(userData.tipo);
        }
        $("#accTypeSelect").val(userData.tipo);
        $(".accTypeLabel").html($("#accTypeSelect option:selected").text());
        try {
            window.userData.DeviceModel = device.model;
            window.userData.DevicePlatform = device.platform;
            window.userData.DeviceVersion = device.version;
        }
        catch (e) { }
        this.changeStatusBar();
        if (isTEApp == false) {
            this.CheckPermits();
        }
        if (isWL == false) {
            this.processAppUI();
        }
        else {
            $(".user-view .servicelogo").prop("src", "img/wl_logo_small.png");
            $(".user-view .background").css({ "background": "url(./img/wl_bg.png) no-repeat left bottom", "background-size": "cover" });
            $(".top").css({ "background": "#333 url(img/wl_logo_small.png) no-repeat center center", "background-size": "95px" });
        }
    };
    Sesion.prototype.changeStatusBar = function () {
        try {
            StatusBar.overlaysWebView(false);
            StatusBar.show();
            var barcolor = "#d9400e";
            if (isWL == false) {
                if (isTEApp) {
                    barcolor = "#4a62bc";
                    StatusBar.backgroundColorByHexString(barcolor);
                    StatusBar.styleLightContent();
                }
                else {
                    if (userData.tipo == "light") {
                        barcolor = "#15598f";
                        StatusBar.backgroundColorByHexString(barcolor);
                        StatusBar.styleLightContent();
                    }
                    if (userData.tipo == "flotillas") {
                        barcolor = "#e5b604";
                        StatusBar.backgroundColorByHexString(barcolor);
                        StatusBar.styleDefault();
                    }
                    else {
                        StatusBar.backgroundColorByHexString(barcolor);
                        StatusBar.styleLightContent();
                    }
                    $(".user-view .background").css({ "background": barcolor + " url(./img/" + userData.tipo + "_bg.png) no-repeat left bottom", "background-size": "cover" });
                }
            }
            else {
                barcolor = "#295811";
            }
            $(".theme-color-meta").attr("content", barcolor);
            views();
        }
        catch (e) {
            //console.error("changeStatusBar",e);
        }
    };
    /**
     * Verifica los permisos de la cuenta del usuario
     * y oculta o muestra secciones acorde a estos
     */
    Sesion.prototype.CheckPermits = function () {
        if (this.Permisos("ConsultaGeocercas") == true) {
            $(".ConsultaGeocercas").show();
        }
        else {
            $(".ConsultaGeocercas").hide();
        }
        if (this.Permisos("ConsultaOrdenesTrabajo") == true) {
            $(".ConsultaOrdenesTrabajo").show();
        }
        else {
            $(".ConsultaOrdenesTrabajo").hide();
        }
        if (this.Permisos("ConsultaPuntos") == true) {
            $(".ConsultaPuntos").show();
        }
        else {
            $(".ConsultaPuntos").hide();
        }
        if (this.Permisos("ConsultaVehiculos") == true && this.Permisos("SoloRastreo") == false) {
            $(".ConsultaVehiculos").show();
        }
        else {
            $(".ConsultaVehiculos").hide();
        }
        if (this.Permisos("UltimaPosicion") == true && this.Permisos("SoloRastreo") == false) {
            $(".UltimaPosicion").show();
        }
        else {
            $(".UltimaPosicion").hide();
        }
        if (this.Permisos("RastreoContinuo") == true || this.Permisos("SoloRastreo") == true) {
            $(".RastreoContinuo").show();
        }
        else {
            $(".RastreoContinuo").hide();
        }
        if (this.Permisos("Historico") == true || this.Permisos("Historico") > 0) {
            $(".Historico").show();
        }
        else {
            $(".Historico").hide();
        }
        if (this.Permisos("HistoricoConPuntos") == true) {
            $(".HistoricoConPuntos").show();
        }
        else {
            $(".HistoricoConPuntos").hide();
        }
        if ((this.Permisos("Historico") > 0 ||
            this.Permisos("RastreoContinuo") == true ||
            this.Permisos("ConsultaPuntos") == true ||
            this.Permisos("ConsultaGeocercas") == true) &&
            this.Permisos("SoloRastreo") == false) {
            $(".limpiarMapa").show();
        }
        else {
            $(".limpiarMapa").hide();
        }
        if ((this.Permisos("ConsultaGeocercas") == false &&
            this.Permisos("ConsultaPuntos") == false) ||
            this.Permisos("SoloRastreo") == true) {
            $(".collapsible-header.georeferencias").hide();
        }
        if ((this.Permisos("ConsultaGeocercas") == true ||
            this.Permisos("ConsultaPuntos") == true) &&
            this.Permisos("SoloRastreo") == false) {
            $(".collapsible-header.georeferencias").show();
        }
        if (this.Permisos("SoloRastreo") == true && isTEApp == false) {
            $(".onlyRC").addClass("hidden");
            $(".SoloRastreo").removeClass("hidden");
            $(".collapsible-header.rastreo").hide();
        }
        else {
            $(".SoloRastreo").addClass("hidden");
            $(".onlyRC").removeClass("hidden");
            if (this.Permisos("RastreoContinuo") == true ||
                this.Permisos("UltimaPosicion") == true ||
                this.Permisos("Historico") > 0) {
                $(".collapsible-header.rastreo").show();
            }
        }
    };
    /**
     * Verifica si un Permiso existe para el usuario, si no llegara a existir
     * entonces siempre lo devolverá como False.
     * @param permit String del permiso a verificar si existe
     */
    Sesion.prototype.Permisos = function (permit) {
        try {
            return (typeof window.userData.permisos[permit] == "undefined" ? false : window.userData.permisos[permit]);
        }
        catch (e) {
            return false;
        }
    };
    /**
     * checkSession
     */
    Sesion.prototype.checkSesion = function () {
        var _this = this;
        window.sessionCheck = setInterval(function () {
            ajax("POST", "https://auto.zeekgps.com/zeekgpsapp/Sesion/checkSesion", {
                "user": getKey("zeekgpsUser"),
                "pass": getKey("zeekgpsPass")
            }, function (data, status, xhr) {
                //console.log(data);
                if (data.token == null) {
                    _this._sessionStrikes++;
                    if (_this._sessionStrikes >= 3) {
                        if (data.user.Status == 2) {
                            alerta(lang("incorrectdata"), lang("sesiondata"));
                            _this.CerrarSesion();
                        }
                        else if (data.user.Status == 4) {
                            alerta(lang("sesionerror"), lang("denied_enter"));
                            _this.CerrarSesion();
                        }
                        else if (data.user.Status == 0) {
                            alerta(lang("sesionerror"), lang("app_blocked"));
                            _this.CerrarSesion();
                        }
                        else if (data.status == "DENIED") {
                            alerta(lang("sesionerror"), lang("denied_enter"));
                            _this.CerrarSesion();
                        }
                        _this._sessionStrikes = 0;
                    }
                }
            }, function (err) {
            });
        }, 30000);
    };
    /**
     * stopCheckSession
     */
    Sesion.prototype.stopCheckSession = function () {
        try {
            clearInterval(window.sessionCheck);
            this._sessionStrikes = 0;
        }
        catch (e) { }
    };
    Sesion._appColor = "";
    return Sesion;
}());
