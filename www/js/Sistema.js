var Sistema = /** @class */ (function () {
    function Sistema() {
        this.gmaps_key = 'AIzaSyDA9imgQjqHXS9ux9S6Fpf-WQ-ozXhLxrc';
        this._gmaps_loaded = false;
        this.sPath = [];
    }
    /**
     * AJAX
     */
    Sistema.prototype.AJAX = function (type, url, data, success, error, _timeout, _datatype) {
        if (_timeout === void 0) { _timeout = 10000; }
        if (_datatype === void 0) { _datatype = "json"; }
        console.log("AJAX Timeout: " + _timeout);
        $.ajax({
            url: url,
            type: type,
            timeout: _timeout,
            dataType: _datatype,
            data: data,
            success: success,
            error: error
        });
    };
    /**
     * initGMaps
     */
    Sistema.prototype.initGMaps = function () {
        var _this = this;
        if (this._gmaps_loaded == false) {
            $.getScript("https://maps.googleapis.com/maps/api/js?v=3&key=" + this.gmaps_key + "&channel=zgps&language=es&region=MX&libraries=places,drawing,geometry")
                .done(function (script, textStatus) {
                _this._gmaps_loaded = true;
                console.log("Google Maps Loaded");
                _this.initSnapPath();
            }).fail(function (jqxhr, settings, exception) {
                setTimeout(function () {
                    _this.initGMaps();
                }, 3000);
            });
        }
    };
    /**
     * initSnapPath
     */
    Sistema.prototype.initSnapPath = function () {
        this.sPath = [];
        try {
            this.sPath = new google.maps.MVCArray();
        }
        catch (e) {
            console.error("No se inicializó Google Maps.");
        }
    };
    /**
     * getSnappedRoute
     */
    Sistema.prototype.getSnappedRoute = function (locData, locType) {
        var _this = this;
        if (locType === void 0) { locType = 'zgps'; }
        this.initSnapPath();
        switch (locType) {
            default:
            case "zgps":
                locData.forEach(function (coord) {
                    _this.addToPath(coord.Latitud, coord.Longitud);
                });
                this.snapToRoad();
                break;
            case "teapp":
                break;
        }
    };
    /**
     * addToPath
     */
    Sistema.prototype.addToPath = function (lat, lng) {
        try {
            this.sPath.push(new google.maps.LatLng(lat, lng));
        }
        catch (e) {
            console.error("No se inicializó Google Maps.");
        }
    };
    /**
     * snapToRoad
     */
    Sistema.prototype.snapToRoad = function () {
        var pathValues = [];
        try {
            for (var i = 0; i < this.sPath.getLength(); i++) {
                pathValues.push(this.sPath.getAt(i).toUrlValue());
            }
            $.get('https://roads.googleapis.com/v1/snapToRoads', {
                interpolate: true,
                key: this.gmaps_key,
                path: pathValues.join('|')
            }, function (data) {
                console.warn(data);
                window.snappedPoints = data.snappedPoints;
                Tracking._locationArray[0].locations = [];
                data.snappedPoints.forEach(function (sPoint) {
                    Tracking._locationArray[0].locations.push({
                        "Fecha": "2021-05-26T22:33:15",
                        "Latitud": sPoint.location.latitude,
                        "Longitud": sPoint.location.longitude
                    });
                });
                Track.DrawHistory(Tracking._locationArray[0].id);
            });
        }
        catch (e) {
            console.error("No se inicializó Google Maps.");
        }
    };
    /**
     * togglePass
     */
    Sistema.prototype.togglePass = function (selector) {
        var sels = selector.split(",");
        if ($(selector).is("[type='password']")) {
            $(selector).attr("type", "text");
            $(sels).each(function (i, e) {
                $(e + " + .show-pass .fas").removeClass("fa-eye-slash").addClass("fa-eye");
            });
        }
        else {
            $(selector).attr("type", "password");
            $(sels).each(function (i, e) {
                $(e + " + .show-pass .fas").removeClass("fa-eye").addClass("fa-eye-slash");
            });
        }
    };
    return Sistema;
}());
