var changelog = new Array();

function showChangelog() {
	changelog = new Array();
	if(isTEApp == true){
		changelog.push({
			"v":"1.0.0",
			"changes_es": {
				"all":[
					"Lanzamiento inicial"
				],
				"android":[],
				"ios":[]
			},
			"changes_en": {
				"all":[
					"Initial launch"
				],
				"android":[],
				"ios":[]
			}
		});
	}else{
		changelog.push({
			"v":"1.4.0 - 1.4.3",
			"changes_es": {
				"all":[
					"(1.4.2 - 1.4.3) Se agregaron nuevos íconos de unidades.",
					"(1.4.2) Se agregaron nuevos eventos para cámara de fatiga.",
					"(1.4.1) Se agregó estado de Motor en burbujas de información en unidades.",
					"Se corrigió detalle al consultar listas extensas de eventos en Histórico.",
					"Ahora se puede iniciar en Rastreo Continuo al iniciar sesión (se puede habilitar o deshabilitar en Configuración).",
					"Se agregó estado de batería en dispositivos compatibles.",
					"En Apagado de Motor ahora tiene acceso a ambas opciones de interface (Habilitar/Deshabilitar).",
					"Algunos detalles corregidos."
				],
				"android":[],
				"ios":[]
			},
			"changes_en": {
				"all":[
					"(1.4.2) New icons for units were added.",
					"(1.4.2) New events for fatigue camera were added.",
					"(1.4.1) Engine status was added to information bubbles on units.",
					"A bug was fixed when consulting large events list on History.",
					"Now you can enter automatically in Real Time Tracking mode when you open the app (it can be configured on Settings).",
					"GPS Battery status has been added to compatible devices.",
					"On Engine Shutdown module you now have access to both interface options (Enable/Disable).",
					"Some bugs fixed."
				],
				"android":[
					
				],
				"ios":[]
			}
		});
		changelog.push({
			"v":"1.3.0",
			"changes_es": {
				"all":[
					"(Solo Fuel) Ahora se pueden ver los datos de los tanques de combustible en Rastreo Continuo al tocar las unidades con Fuel habilitado.",
					"(Solo Fuel) Ahora pueden consultarse gráficas de Fuel e información básica.",
					"Algunos detalles corregidos."
				],
				"android":[],
				"ios":[]
			},
			"changes_en": {
				"all":[
					"(Only Fuel) Now you can see fuel tank data on Real Time Tracking when you touch a Fuel-enabled vehicle on the map.",
					"(Only Fuel) Now you can consult Fuel data graphs and basic information.",
					"Some bugs fixed."
				],
				"android":[
					
				],
				"ios":[]
			}
		});
		changelog.push({
			"v":"1.2.0",
			"changes_es": {
				"all":[
					"Ahora se puede compartir el rastreo de unidades con otras personas.",
					"Ahora se mostrará el último evento a un lado del ícono de campana en Rastreo Continuo, se alternará entre el número de eventos y el último evento ocurrido.",
					"Se homologaron unidades deshabilitadas para Rastreo Continuo con la plataforma."
				],
				"android":[],
				"ios":[
					"Se corrigieron detalles en dispositivos iPhone."
				]
			},
			"changes_en": {
				"all":[
					"Now you can share the tracking with just a link.",
					"Now on Real Time Tracking the events number will also show the last event ocurred on that vehicle.",
					"Fix for vehicles that are disabled on platform didn't show up correctly on the app."
				],
				"android":[
					
				],
				"ios":[
					"Some fixes on iPhone devices."
				]
			}
		});
		changelog.push({
			"v":"1.1.1 - 1.1.8",
			"changes_es": {
				"all":[
					"Corregido error en Histórico al tratar de seleccionar Horas.",
					"Corregido mensaje de respuesta en Apagado de Motor para ciertos dispositivos.",
					"Corregido detalle de subcuentas que no podian iniciar sesión.",
					"Corregido detalle estético en opción de menú para Georeferencias cuando ninguna sub opción se encuentra disponible.",
					"Corregido detalle de iconos de unidades que no reportaban mas de 4 horas.",
					"Corregidos detalles en cuentas de Solo Rastreo que les aparecía la barra de eventos y cuadro de información de funciones de app."
				],
				"android":[
					"Ahora la app no se minimza al pasarlo a segundo plano, esto para reducir el consumo de batería.",
					"Corregido detalle con Push Notifications que no llegaban."
				],
				"ios":[]
			},
			"changes_en": {
				"all":[
					"Fixed bug when trying to select Hours on History.",
					"Fixed response for Engine Shutdown.",
					"Fixed bug that denied access for sub accounts.",
					"Fixed UI bug that showed the menu header for Georeferences even when no sub options where available.",
					"Fixed icons for vehicles that didn't respond after 4 hours.",
					"Fixed bugs where the bottom event bar and the floating showcase box appeared on Tracking Only accounts."
				],
				"android":[
					"Now the app won't minimize when put on the background, this measure was taken to reduce battery consumption.",
					"Fixed bug for Push Notifications."
				],
				"ios":[]
			}
		});

		changelog.push({
			"v":"1.1.0",
			"changes_es": {
				"all":[
					"Las unidades ahora se ordenan alfabéticamente por predeterminado en las listas donde se muestren.",
					"Ahora se mostrará una barra inferior con los eventos en vivo cuando se reciban Notificaciones Push con la app abierta, o cuando reciba eventos en Rastreo Continuo.",
					"Ahora la app le notificará cuando una unidad no respondió al Apagado de Motor.",
					"Varios errores corregidos."
				],
				"android":[
					"Se aumentó la calidad del mapa y elementos."
				],
				"ios":[]
			},
			"changes_en": {
				"all":[
					"Any vehicle list now will order alphabetically by default.",
					"An event banner now will appear on the screen below when you receive Push Notifications or if you receive events while on Real Time Tracking.",
					"Now the app will notify you when a vehicle doesn't respond to the Engine Shutdown command.",
					"Several bug fixes."
				],
				"android":[
					"Quality of map and elements on it were fixed."
				],
				"ios":[]
			}
		});
		changelog.push({
			"v":"1.0.1",
			"changes_es": {
				"all":[
					"Lanzamiento inicial"
				],
				"android":[],
				"ios":[]
			},
			"changes_en": {
				"all":[
					"Initial launch"
				],
				"android":[],
				"ios":[]
			}
		});
	}
	$("#changelog").modal({
		inDuration: 0,
		outDuration:0
	});
	if(getKey("zeekgpsChangelog") != version && version != "1.0.0") {
		displayChangelog();
		setKey("zeekgpsChangelog",version); 
	}
}
function fillChangelog() {
	var parsedChangelog ="";
	for(var i = 0; i < changelog.length;i++) {
		parsedChangelog += "<div class='changeVersion'>"+changelog[i].v+"</div>";
		try{
			if(device.platform.toLowerCase() === "android") {
				parsedChangelog += byDeviceLog(i,"android");
			}
		}catch(e){
			parsedChangelog += byDeviceLog(i,"android");
		}
		try{
			if(device.platform.toLowerCase() === "ios") {
				parsedChangelog += byDeviceLog(i,"ios");
			}
		}catch(e) {
			parsedChangelog += byDeviceLog(i,"ios");
		}
		for(var j = 0; j < changelog[i]["changes_"+window.idioma].all.length;j++) {
			parsedChangelog += "<div class='changeText'>&bull; "+changelog[i]["changes_"+window.idioma].all[j]+"</div>";
		}
		
		parsedChangelog+= "<div class='divider'></div>";
	}
	$("p.changelog").html(parsedChangelog);
}
function displayChangelog() {
	fillChangelog();
	$("#changelog").modal("open");
}
function byDeviceLog(i,_device) {
	var text = "";
	for(var j = 0; j < changelog[i]["changes_"+window.idioma][_device].length;j++) {
		text += "<div class='changeText'>&bull; "+changelog[i]["changes_"+window.idioma][_device][j]+"</div>";
	}
	return text;
}