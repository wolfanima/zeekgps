/// <reference path ="jquery.d.ts"/> 
/**
 * PUNTOGRUPO
 * Clase que engloba la vista de PUNTOS y sus GRUPOS.
 */
var PuntoGrupo = /** @class */ (function () {
    function PuntoGrupo() {
        window.ibubble = null;
        PuntoGrupo.pointsGroup = new H.map.Group();
        map.addObject(PuntoGrupo.pointsGroup);
    }
    /**
     * Obtiene los Puntos y sus Grupos.
     * @param drawPoints TRUE para dibujarlos
     */
    PuntoGrupo.prototype.ObtenerPuntosConGrupos = function (drawPoints) {
        if (drawPoints === void 0) { drawPoints = false; }
        if ($(".groupsList").is(":visible")) {
            $("#add-btn").addClass("hidden");
        }
        waitSidebar(lang("loadingpoints"));
        $.ajax({
            url: pointUrl + "ObtenerPuntosConGrupos",
            type: "POST",
            timeout: 10000,
            data: {
                "token": window["userData"]["token"]
            },
            success: function (data) {
                console.log(data);
                window.listaPuntos = data.data;
                Checkpoints.ObtenerGrupos(drawPoints);
            },
            error: function (data) {
                console.log(data);
                if ($(".checkpoints").is(":visible")) {
                    waitSidebar("<es>Reintentando...</es><en>Retrying...</en>");
                    console.log("Retrying...");
                    setTimeout(function () {
                        Checkpoints.ObtenerPuntosConGrupos(drawPoints);
                    }, 3000);
                }
            }
        });
    };
    /**
     * Obtiene los Puntos, y solo los Puntos.
     * @param drawPoints TRUE para dibujarlos
     */
    PuntoGrupo.prototype.ObtenerPuntos = function (drawPoints) {
        if (drawPoints === void 0) { drawPoints = false; }
        if ($(".groupsList").is(":visible")) {
            $("#add-btn").addClass("hidden");
        }
        waitSidebar(lang("loadingpoints"));
        $.ajax({
            url: pointUrl + "ObtenerPuntosConGrupos",
            type: "POST",
            timeout: 10000,
            data: {
                "token": window["userData"]["token"]
            },
            success: function (data) {
                console.log(data);
                window.listaPuntos = data.data;
                if (drawPoints) {
                    Checkpoints.ShowCheckpointsOnMap();
                }
            },
            error: function (data) {
                console.log(data);
                if ($(".checkpoints").is(":visible")) {
                    waitSidebar("<es>Reintentando...</es><en>Retrying...</en>");
                    console.log("Retrying...");
                    setTimeout(function () {
                        Checkpoints.ObtenerPuntos(drawPoints);
                    }, 3000);
                }
            }
        });
    };
    /**
     * Obtiene los Grupos y solo los Grupos
     * @param drawPoints TRUE para dibujar los puntos relacionados a los Grupos
     */
    PuntoGrupo.prototype.ObtenerGrupos = function (drawPoints) {
        if (drawPoints === void 0) { drawPoints = false; }
        if ($(".groupsList").is(":visible")) {
            $("#add-btn").addClass("hidden");
        }
        waitSidebar(lang("loadinggroups"));
        $.ajax({
            url: pointUrl + "ObtenerGrupos",
            type: "POST",
            timeout: 10000,
            data: {
                "token": window["userData"]["token"]
            },
            success: function (data) {
                console.log(data);
                window.gruposPuntos = data.data;
                if (drawPoints) {
                    Checkpoints.ShowCheckpointsOnMap();
                }
                closeWaitSidebar();
                if ($(".pointsList").is(":visible") || $(".pointPage").is(":visible")) {
                    if (PuntoGrupo._creatingPoint == true) {
                        for (var i = 0; i < window.gruposPuntos.length; i++) {
                            $(".pointPage .pointGroupList").append('<p>' +
                                '<input type="checkbox" class="filled-in"  id="group-' + window.gruposPuntos[i].GrupoID + '" value="' + window.gruposPuntos[i].GrupoID + '" />' +
                                '<label for="group-' + window.gruposPuntos[i].GrupoID + '">' + window.gruposPuntos[i].Nombre + '</label>' +
                                '</p>');
                        }
                    }
                    Checkpoints.FillPointList();
                }
                if ($(".pointsList").is(":visible")) {
                    if (listaPuntos.length == 0) {
                        $("#sidebar").addClass("emptylist");
                        $("#sidebar").append('<div class="emptycar"></div>');
                        $(".pointsList .noContent").removeClass("hidden");
                        $(".pointsList .search-input").addClass("hide");
                    }
                    else {
                        $("#sidebar").removeClass("emptylist");
                        $("#sidebar .emptycar").remove();
                        $(".pointsList .noContent").addClass("hidden");
                        $(".pointsList .search-input").removeClass("hide");
                    }
                }
                $(".checkpoints #group-filter").html('<option selected value="all">' + lang("allpoints") + '</option>');
                $(".checkpoints #group-filter").append('<option disabled>' + lang("groupfilter") + '</option>');
                for (var i = 0; i < window.gruposPuntos.length; i++) {
                    $(".checkpoints #group-filter").append('<option value="' + window.gruposPuntos[i].GrupoID + '">' + window.gruposPuntos[i].Nombre + '</option>');
                }
                formSelect();
                if ($(".groupsList").is(":visible")) {
                    Checkpoints.FillGroupList();
                    //$("#add-btn").attr("onclick","Checkpoints.CreateGroup()").removeClass("hidden");
                }
                if (!$("#historial").hasClass("hidden")) {
                    Track.FillPointGroup();
                    closeWait();
                }
                $("#sidebar").scrollTop(0);
            },
            error: function (data) {
                console.log(data);
                if ($(".checkpoints").is(":visible")) {
                    waitSidebar("<es>Reintentando...</es><en>Retrying...</en>");
                    console.log("Retrying...");
                    setTimeout(function () {
                        Checkpoints.ObtenerGrupos();
                    }, 3000);
                }
            }
        });
    };
    /**
     * Llena la lista del UI de Puntos
     */
    PuntoGrupo.prototype.FillPointList = function () {
        $(".checkpointList").html('');
        for (var i = 0; i < window.listaPuntos.length; i++) {
            var pointGroupColor = "000000";
            /*if( $(".pointPage").is(":visible")){
                if(window.listaPuntos[i].PuntoID == PuntoGrupo._editPoint){
                    var pid = objectIndex(listaPuntos,"PuntoID",PuntoGrupo._editPoint);
                    map.setCenter({lat:listaPuntos[i].x, lng:listaPuntos[i].y});
                    map.setZoom(15);
                }
            }*/
            if (listaPuntos[i].Grupos.length > 0) {
                pointGroupColor = ("000000" + parseInt(listaPuntos[i].Grupos[0].Color).toString(16)).substr(-6);
            }
            var selected = "square";
            if (PuntoGrupo.selectedPoints.indexOf(window.listaPuntos[i].PuntoID) >= 0) {
                selected = "check-square";
            }
            var groupClasses = "";
            for (var g = 0; g < listaPuntos[i].Grupos.length; g++) {
                groupClasses += " group-" + listaPuntos[i].Grupos[g].GrupoID;
            }
            if ($("#sidebar").is(":visible")) {
                $(".checkpointList").append('<li class="' + groupClasses + '" id="puntosel-' + listaPuntos[i].PuntoID + '">' +
                    '<div class="left-icon left-check" onclick="Checkpoints.SelectPoint(' + listaPuntos[i].PuntoID + ',' + i + ')"><span class="check far fa-' + selected + '"></span></div>' +
                    '<div class="left-icon" onclick="Checkpoints.SelectPoint(' + listaPuntos[i].PuntoID + ',' + i + ')" ><i class="zicon_puntos" style="color:#' + pointGroupColor + '"></i></div>' +
                    '<div class="textBox" onclick="Checkpoints.SelectPoint(' + listaPuntos[i].PuntoID + ',' + i + ')">' +
                    '<div class="listText">' + listaPuntos[i].Nombre + '</div>' +
                    '</div>' +
                    ("<div class=\"chevron\" onclick=\"Checkpoints.FocusCoord(" + listaPuntos[i].PuntoID + ")\"><span class=\"lsf lsf-icon\">geo</span></div>") +
                    /*(SesionEngine.Permisos("EdicionPuntos") == true ? '<div class="chevron" onclick="Checkpoints.OpenPoint('+listaPuntos[i].PuntoID+')"><span class="lsf lsf-icon" icon="right"></span></div>' : '')+*/
                    '</li>');
            }
        }
        //this.ShowCheckpointsOnMap();
    };
    /**
     * Enfoca y centra el mapa en un Punto determinado
     * @param pid Identificador de la instancia del Punto
     */
    PuntoGrupo.prototype.FocusCoord = function (pid) {
        var id = objectIndex(listaPuntos, "PuntoID", pid);
        map.setCenter({ lat: listaPuntos[id].x, lng: listaPuntos[id].y });
        map.setZoom(15);
        if (objectIndex(PuntoGrupo.Points, "id", pid) == -1 || objectIndex(PuntoGrupo.selectedPoints, "id", pid) == -1) {
            window.focusingPoint = true;
            $(".centerLabel .label").html(listaPuntos[id].Nombre);
            var pointGroupColor = "000000";
            if (listaPuntos[id].Grupos.length > 0) {
                pointGroupColor = ("000000" + parseInt(listaPuntos[id].Grupos[0].Color).toString(16)).substr(-6);
            }
            $(".centerMarker").css({
                "background": "url('data:image/svg+xml;utf8," + svgCenterMarker.replace("__FILLCOLOR__", "#" + pointGroupColor) + "') no-repeat center center",
                "background-position": "50% calc(50% - 17px)"
            }).show();
            $(".centerLabel").show();
            $(".centerMarker .pulse,.centerMarker .pin").hide();
        }
    };
    /**
     * Prepara el arreglo para los Grupos :P
     */
    PuntoGrupo.prototype.PrepareGroupArray = function () {
        PuntoGrupo.groupsArray = new Array();
        for (var i = 0; i < gruposPuntos.length; i++) {
            PuntoGrupo.groupsArray.push({
                "name": gruposPuntos[i].Nombre,
                "gid": gruposPuntos[i].GrupoID,
                "puntos": new H.map.Group()
            });
        }
    };
    /**
     * Muestra los Puntos en el mapa, OMG
     */
    PuntoGrupo.prototype.ShowCheckpointsOnMap = function () {
        this.PrepareGroupArray();
        PuntoGrupo._pointsOnMap = new Array();
        console.log("Mostrando Checkpoints");
        this.ClearCheckpoints();
        for (var i = 0; i < window.listaPuntos.length; i++) {
            this.PlaceCheckpoint(i);
        }
    };
    /**
     * Coloca un solo Punto determinado en el mapa
     * @param id Identificador de la instancia del Punto
     */
    PuntoGrupo.prototype.ShowCheckpoint = function (id) {
        this.PrepareGroupArray();
        PuntoGrupo._pointsOnMap = new Array();
        console.log("Mostrando Checkpoint");
        //this.ClearCheckpoints();
        //for(var i=0;i<window.listaPuntos.length;i++){
        this.PlaceCheckpoint(id);
        //}
    };
    /**
     * Muestra los Puntos asignados a ciertos Grupos.
     * @param groups Arreglo de Grupos permitidos a mostrarse
     */
    PuntoGrupo.prototype.ShowGroupsOnMap = function (groups) {
        PuntoGrupo._pointsOnMap = new Array();
        for (var i = 0; i < listaPuntos.length; i++) {
            var found = false;
            for (var j = 0; j < listaPuntos[i].Grupos.length; j++) {
                for (var k = 0; k < groups.length; k++) {
                    if (groups[k] == listaPuntos[i].Grupos[j].GrupoID) {
                        found = true;
                    }
                }
            }
            if (found == true) {
                this.PlaceCheckpoint(i);
            }
        }
    };
    /**
     * Coloca un Punto en el mapa junto con su iBubble
     * @param i Identificador de la instancia del Punto
     * @param autoDestruct TRUE para autodestruir Puntos
     * (Punto Temporal, cuando solo quieres ver Puntos de ciertos Grupos)
     */
    PuntoGrupo.prototype.PlaceCheckpoint = function (i, autoDestruct) {
        if (autoDestruct === void 0) { autoDestruct = false; }
        console.log("Showing Point [" + i + "]");
        var pid = objectIndex(PuntoGrupo.Points, "id", listaPuntos[i].PuntoID);
        if (pid == -1) {
            var length = getTextWidth(listaPuntos[i].Nombre, (18 * DPR) + "px Lato");
            var pointGroupColor = "000000";
            if (listaPuntos[i].Grupos.length > 0) {
                pointGroupColor = ("000000" + parseInt(listaPuntos[i].Grupos[0].Color).toString(16)).substr(-6);
            }
            PuntoGrupo._pointsOnMap.push(listaPuntos[i].PuntoID);
            var labelElem = Track.CreateDomLabel(listaPuntos[i].Nombre, "point");
            var pinIcon = this.CreateDomPin((listaPuntos[i].Grupos.length > 1) ?
                listaPuntos[i].Grupos.length : "", "#" + pointGroupColor, setContrast("#" + pointGroupColor));
            PuntoGrupo.Points.push({
                "id": listaPuntos[i].PuntoID,
                "autoDestruct": autoDestruct,
                "marker": new H.map.DomMarker({ lat: listaPuntos[i].x, lng: listaPuntos[i].y }, {
                    /*icon: new H.map.Icon(svgColorMarker
                    .replace("__FILLCOLOR__", "#"+pointGroupColor)
                    .replace("__TEXT__", (listaPuntos[i].Grupos.length > 1) ? listaPuntos[i].Grupos.length : "")
                    .replace("__FONTSIZE__", 14*DPR)
                    .replace("__TEXTCOLOR__",setContrast("#"+pointGroupColor)),
                    {size: {w:28*DPR,h:34*DPR}})*/
                    icon: new H.map.DomIcon(pinIcon)
                }),
                "label": new H.map.DomMarker({ lat: listaPuntos[i].x, lng: listaPuntos[i].y }, {
                    /*icon: new H.map.Icon(svgStringLabel
                        .replace(/__WIDTH__/g, length)
                        .replace(/__XADJUSTMENT__/g, length / 2)
                        .replace(/__TEXT__/g, listaPuntos[i].Nombre),
                        {
                            anchor: {
                                x: length / 2,
                                y: -5,
                            }
                        }
                    ),*/
                    min: 15,
                    icon: new H.map.DomIcon(labelElem)
                })
            });
            PuntoGrupo.pointsGroup.addObject(PuntoGrupo.Points[PuntoGrupo.Points.length - 1].marker);
            PuntoGrupo.pointsGroup.addObject(PuntoGrupo.Points[PuntoGrupo.Points.length - 1].label);
            //PuntoGrupo.Points[PuntoGrupo.Points.length-1].marker.setVisibility(false);
            //PuntoGrupo.Points[PuntoGrupo.Points.length-1].label.setVisibility(false);
            /*for(var g=0;g<listaPuntos[i].Grupos.length;g++){
                var gid = objectIndex(gruposPuntos,"GrupoID",listaPuntos[i].Grupos[g].GrupoID);
                if(gid >=0){
                    PuntoGrupo.groupsArray[gid].puntos.addObject(PuntoGrupo.Points[PuntoGrupo.Points.length-1].marker);
                }
            }*/
            var groupList = "";
            for (var j = 0; j < listaPuntos[i].Grupos.length; j++) {
                groupList += '<li>&bull; ' + listaPuntos[i].Grupos[j].Nombre + '</li>';
            }
            PuntoGrupo.Points[PuntoGrupo.Points.length - 1].marker.setData('<div class="inputGroup">' +
                '<h6><es>Información</es><en>Information</en></h6>' +
                '<div class="markerInfo">' +
                '<div><span class="ibHeader"><es>Nombre</es><en>Name</en>:</span><span>' + listaPuntos[i].Nombre + '</span></div>' +
                (trim(listaPuntos[i].Horario) == "" ? '' : '<div><span class="ibHeader"><es>Horario</es><en>Schedule</en>:</span><span>' + na(listaPuntos[i].Horario) + '</span></div>') +
                (trim(listaPuntos[i].Email) == "" ? '' : '<div><span class="ibHeader"><es>E-mail</es><en>E-mail</en>:</span><span>' + na(listaPuntos[i].Email) + '</span></div>') +
                (trim(listaPuntos[i].Pagina) == "" ? '' : '<div><span class="ibHeader"><es>Página</es><en>Website</en>:</span><span>' + na(listaPuntos[i].Pagina) + '</span></div>') +
                (trim(listaPuntos[i].Telefono) == "" ? '' : '<div><span class="ibHeader"><es>Teléfono</es><en>Phone</en>:</span><span>' + na(listaPuntos[i].Telefono) + '</span></div>') +
                (trim(listaPuntos[i].Direccion) == "" ? '' : '<div><span class="ibHeader"><es>Dirección</es><en>Address</en>:</span><span>' + na(listaPuntos[i].Direccion) + '</span></div>') +
                (trim(listaPuntos[i].Nota) == "" ? '' : '<div><span class="ibHeader"><es>Nota</es><en>Notes</en>:</span><span>' + na(listaPuntos[i].Nota) + '</span></div>') +
                '<div><span class="ibHeader"><es>Grupo(s)</es><en>Group(s)</en>:</span><span><ul>' + groupList + '</ul></span></div>' +
                '<div><span class="ibHeader"><es>Mapa</es><en>Map</en>:</span><span onclick="openMaps(' + listaPuntos[i].x + ',' + listaPuntos[i].y + ')" class="' + listaPuntos[i].PuntoID + '-bubble_coords"><es>Abrir Mapa Externo</es><en>Open External Map</en> <i class="fas fa-external-link-alt"></i></span></div>' +
                '</div>' +
                '</div>');
            PuntoGrupo.pointsGroup.addEventListener('tap', function (evt) {
                // event target is the marker itself, group is a parent event target
                // for all objects that it contains
                if (evt.target.getData() != undefined) {
                    Checkpoints.CloseBubbles();
                    window.ibubble = new H.ui.InfoBubble(evt.target.getPosition(), {
                        // read custom data
                        content: evt.target.getData()
                    });
                    // show info bubble
                    mapui.addBubble(window.ibubble);
                }
            }, false);
        }
        // map.setViewBounds(PuntoGrupo.pointsGroup.getBounds(),false);
        map.setCenter({ lat: listaPuntos[i].x, lng: listaPuntos[i].y });
    };
    /**
     * Crea un Pin en el DOM que puede personalizarse.
     * @param text Texto para el DOM Pin
     * @param fill Color de relleno del DOM Pin
     * @param textColor Color del texto del DOM Pin
     */
    PuntoGrupo.prototype.CreateDomPin = function (text, fill, textColor) {
        if (text === void 0) { text = ""; }
        if (fill === void 0) { fill = "#FF0000"; }
        if (textColor === void 0) { textColor = "#FFFFFF"; }
        console.warn("Creating DOM Pin", text, fill, textColor);
        ;
        var pin = "<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xml:space='preserve'><path d='M 19 31 C 19 32.7 16.3 34 13 34 C 9.7 34 7 32.7 7 31 C 7 29.3 9.7 28 13 28 C 16.3 28 19 29.3 19 31 Z' fill='#000' fill-opacity='.2'/><path d='M 13 0 C 9.5 0 6.3 1.3 3.8 3.8 C 1.4 7.8 0 9.4 0 12.8 C 0 16.3 1.4 19.5 3.8 21.9 L 13 31 L 22.2 21.9 C 24.6 19.5 25.9 16.3 25.9 12.8 C 25.9 9.4 24.6 6.1 22.1 3.8 C 19.7 1.3 16.5 0 13 0 Z' fill='#fff'/><path d='M 13 2.2 C 6 2.2 2.3 7.2 2.1 12.8 C 2.1 16.1 3.1 18.4 5.2 20.5 L 13 28.2 L 20.8 20.5 C 22.9 18.4 23.8 16.2 23.8 12.8 C 23.6 7.07 20 2.2 13 2.2 Z' fill='" + fill + "'/><text font-size='12' font-weight='500' fill='" + textColor + "' font-family='Lato, Arial, Helvetica' text-anchor='middle' x='13' y='17'>" + text + "</text></svg>";
        var icon = document.createElement('div');
        icon.className = "domPin";
        icon.style.background = "url(\"data:image/svg+xml;charset=UTF-8," + encodeURIComponent(pin) + "\") no-repeat center center";
        return icon;
    };
    /**
     * Oculta los Puntos de un Grupo especifico.
     * @param gid Identificador del Grupo a ocultar
     */
    PuntoGrupo.prototype.HideGroupPoints = function (gid) {
        for (var i = 0; i < listaPuntos.length; i++) {
            for (var g = 0; g < listaPuntos[i].Grupos.length; g++) {
                if (listaPuntos[i].Grupos[g].GrupoID == gid) {
                    try {
                        PuntoGrupo.Points[i].marker.setVisibility(false);
                        PuntoGrupo.Points[i].label.setVisibility(false);
                    }
                    catch (e) { }
                }
            }
        }
    };
    /**
     * Muestra los Puntos de un Grupo especifico
     * @param gid Identificador de Grupo a mostrar
     */
    PuntoGrupo.prototype.ShowGroupPoints = function (gid) {
        for (var i = 0; i < listaPuntos.length; i++) {
            for (var g = 0; g < listaPuntos[i].Grupos.length; g++) {
                if (listaPuntos[i].Grupos[g].GrupoID == gid) {
                    PuntoGrupo.Points[i].marker.setVisibility(true);
                    PuntoGrupo.Points[i].label.setVisibility(true);
                }
            }
        }
    };
    /**
     * Llena la lista en el UI con los Grupos
     */
    PuntoGrupo.prototype.FillGroupList = function () {
        $(".pointgroupsList").html('');
        if (window.gruposPuntos == 0) {
            $("#sidebar").addClass("emptylist");
            $("#sidebar").append('<div class="emptycar"></div>');
            $(".groupsList .noContent").removeClass("hidden");
        }
        else {
            $("#sidebar").removeClass("emptylist");
            $("#sidebar .emptycar").remove();
            $(".groupsList .noContent").addClass("hidden");
        }
        for (var i = 0; i < window.gruposPuntos.length; i++) {
            var pointGroupColor = "000000";
            pointGroupColor = ("000000" + parseInt(gruposPuntos[i].Color).toString(16)).substr(-6);
            PuntoGrupo.groupsArray.push(new H.map.Group());
            if ($("#sidebar").is(":visible")) {
                $(".pointgroupsList").append('<li onclick="Checkpoints.OpenGroup(' + gruposPuntos[i].GrupoID + ')">' +
                    '<div class="left-icon"><i class="zicon_grupos" style="color:#' + pointGroupColor + '"></i></div>' +
                    '<div class="textBox">' +
                    '<div class="listText">' + gruposPuntos[i].Nombre + '</div>' +
                    '</div>' +
                    '<div class="chevron"><span class="lsf lsf-icon" icon="right"></span></div>' +
                    '</li>');
            }
        }
        //this.ShowCheckpointsOnMap();
    };
    /**
     * Abre la vista para crear un Grupo de Puntos
     */
    PuntoGrupo.prototype.CreateGroup = function () {
        $("#add-btn i").html("save");
        $("#color_group_sel")[0].jscolor.fromString("#000");
        $(".removeGroupBtn,.groupPoints").hide();
        $("#main .mainTop .title").html("<es>Crear Grupo</es><en>New Group</en>");
        $(".checkpoints .groupsList").hide();
        $(".checkpoints .groupPage").show();
        $("#close-btn").attr("onclick", "Checkpoints.CloseGroupEdit()");
        $("#add-btn").attr("onclick", "Checkpoints.NewGroup()").removeClass("hidden");
        $("#groupName").val('');
    };
    /**
     * Guarda el nuevo Grupo en plataforma
     */
    PuntoGrupo.prototype.NewGroup = function () {
        var color = $("#color_group").val();
        var colorNum = parseInt("0x" + color.substr(1));
        if (trim($("#groupName").val()).length < 3) {
            alerta(lang("error"), lang("groupnameerror"));
        }
        else {
            waitSidebar(lang("makinggroup"));
            $.ajax({
                url: pointUrl + "CrearGrupo",
                type: "POST",
                dataType: "json",
                timeout: 10000,
                data: {
                    "token": window["userData"]["token"],
                    name: $("#groupName").val(),
                    color: colorNum
                },
                success: function (data) {
                    console.log(data);
                    Checkpoints.CloseGroupEdit();
                    toasty(lang("groupcreated"));
                },
                error: function (data) {
                    console.log(data);
                    closeWaitSidebar();
                    toasty(lang("tryagain"), "error");
                }
            });
        }
    };
    /**
     * Abre la vista para editar un Grupo especifico
     * @param gid Identificador del Grupo a abrir
     */
    PuntoGrupo.prototype.OpenGroup = function (gid) {
        var _this = this;
        try {
            masterClose = function () { _this.CloseGroupEdit(); };
        }
        catch (e) { }
        var pointGroupColor = "000000";
        $(".removeGroupBtn,.groupPoints").show();
        if (gid == 0) {
            $("#groupName").attr("disabled", true).val(lang("nogroup"));
            $(".removeGroupBtn,#color_group_sel").hide();
        }
        else {
            //$("#add-btn").attr("onclick","Checkpoints.SaveGroupChanges()").removeClass("hidden");
            $("#add-btn i").html('save');
            var id = objectIndex(gruposPuntos, "GrupoID", gid);
            PuntoGrupo._editGroup = id;
            $("#groupName").attr("disabled", false).val(gruposPuntos[id].Nombre);
            pointGroupColor = ("000000" + parseInt(gruposPuntos[id].Color).toString(16)).substr(-6);
            $(".removeGroupBtn,#color_group_sel").show();
        }
        //$("#color_group_sel")[0].jscolor.fromString('#'+pointGroupColor);
        $("#main .mainTop .title").html(gruposPuntos[id].Nombre);
        $(".checkpoints .groupsList").hide();
        $(".checkpoints .groupPage").show();
        $("#close-btn").attr("onclick", "Checkpoints.CloseGroupEdit()");
        $(".groupPage .groupPointList").html('');
        for (var i = 0; i < window.listaPuntos.length; i++) {
            if (objectIndex(listaPuntos[i].Grupos, "GrupoID", gid) >= 0) {
                $(".groupPage .groupPointList").append("<li class=\"no-effect\" onclick=\"MapEngine.FocusOnCoords(" + listaPuntos[i].x + "," + listaPuntos[i].y + ")\">" +
                    '<div class="left-icon"><i class="zicon_puntos"></i></div>' +
                    '<div class="listText">' + window.listaPuntos[i].Nombre + '</div>' +
                    '</li>');
                this.PlaceCheckpoint(i, true);
            }
        }
        map.setViewBounds(PuntoGrupo.pointsGroup.getBounds(), false);
        if ($(".groupPage .groupPointList li").length == 0) {
            $(".groupPage .noContent").removeClass("hidden");
        }
        else {
            $(".groupPage .noContent").addClass("hidden");
        }
    };
    /**
     * Abre la vista para editar un Punto especifico.
     * @param id Identificador del Punto a abrir
     */
    PuntoGrupo.prototype.OpenPoint = function (id) {
        if (SesionEngine.Permisos("EdicionPuntos") == true) {
            var pgid = objectIndex(PuntoGrupo.Points, "id", id);
            var pid = objectIndex(listaPuntos, "PuntoID", id);
            if (PuntoGrupo.Points[pgid] == undefined) {
                this.ShowCheckpoint(pid);
            }
            $(".remPoint").show();
            mapbehavior.disable(H.mapevents.Behavior.WHEELZOOM);
            PuntoGrupo._editPoint = id;
            $("#add-btn i").html("save");
            $("#sidebar").scrollTop(0);
            $("#main .mainTop .title").html("<es>Editar Punto</es><en>Checkpoint Edit</en>");
            $(".checkpoints .pointsList").hide();
            $(".checkpoints .pointPage").show();
            $("#add-btn").attr("onclick", "Checkpoints.SavePointChanges()").removeClass("hidden");
            $("#close-btn").attr("onclick", "Checkpoints.CloseGroupEdit()");
            $(".pointPage .pointGroupList").html('');
            var puntoID = listaPuntos[pid].PuntoID;
            $("#nombrePunto").val(listaPuntos[pid].Nombre);
            $("#telPunto").val(listaPuntos[pid].Telefono);
            $("#emailPunto").val(listaPuntos[pid].Email);
            $("#webPunto").val(listaPuntos[pid].Pagina);
            $("#timePunto").val(listaPuntos[pid].Horario);
            $("#dirPunto").val(listaPuntos[pid].Direccion);
            $("#webPunto").val(listaPuntos[pid].Pagina);
            $("#notesPunto").val(listaPuntos[pid].Nota);
            $(".centerLabel .label").html(listaPuntos[pid].Nombre);
            $(".centerLabel").show();
            var pointGroupColor = "000000";
            pgid = objectIndex(PuntoGrupo.Points, "id", id);
            PuntoGrupo.Points[pgid].marker.setVisibility(false);
            PuntoGrupo.Points[pgid].label.setVisibility(false);
            if (listaPuntos[pid].Grupos.length > 0) {
                pointGroupColor = ("000000" + parseInt(listaPuntos[pid].Grupos[0].Color).toString(16)).substr(-6);
            }
            $(".centerMarker").css({
                "background": "url('data:image/svg+xml;utf8," + svgCenterMarker.replace("__FILLCOLOR__", "#" + pointGroupColor) + "') no-repeat center center",
                "background-position": "50% calc(50% - 17px)"
            }).show();
            $(".centerMarker .pulse,.centerMarker .pin").hide();
            for (var i = 0; i < window.gruposPuntos.length; i++) {
                var checked = "";
                if (objectIndex(listaPuntos[pid].Grupos, "GrupoID", window.gruposPuntos[i].GrupoID) >= 0) {
                    checked = "checked='checked'";
                }
                $(".pointPage .pointGroupList").append('<p>' +
                    '<input type="checkbox" class="filled-in" ' + checked + ' id="group-' + window.gruposPuntos[i].GrupoID + '" value="' + window.gruposPuntos[i].GrupoID + '" />' +
                    '<label for="group-' + window.gruposPuntos[i].GrupoID + '">' + window.gruposPuntos[i].Nombre + '</label>' +
                    '</p>');
            }
        }
    };
    /**
     * Guarda los cambios de un Grupo en plataforma
     */
    PuntoGrupo.prototype.SaveGroupChanges = function () {
        var color = $("#color_group").val();
        var colorNum = parseInt("0x" + color.substr(1));
        if (trim($("#groupName").val()).length < 3) {
            alerta(lang("error"), lang("groupnameerror"));
        }
        else {
            waitSidebar(lang("updatinggroup"));
            $("#add-btn").addClass("hidden");
            $.ajax({
                url: pointUrl + "ActualizarGrupo",
                type: "POST",
                dataType: "json",
                timeout: 10000,
                data: {
                    "token": window["userData"]["token"],
                    gid: gruposPuntos[PuntoGrupo._editGroup].GrupoID,
                    name: $("#groupName").val(),
                    color: colorNum
                },
                success: function (data) {
                    console.log(data);
                    $("#add-btn").removeClass("hidden");
                    closeWaitSidebar();
                    toasty(lang("groupupdated"));
                },
                error: function (data) {
                    console.log(data);
                    closeWaitSidebar();
                    toasty(lang("tryagain"));
                }
            });
        }
    };
    /**
     * Abre la vista para crear un Punto nuevo
     */
    PuntoGrupo.prototype.NewCheckpoint = function () {
        $(".remPoint").hide();
        mapbehavior.disable(H.mapevents.Behavior.WHEELZOOM);
        $(".centerLabel .label").html("");
        $(".centerLabel").show();
        $(".centerMarker").css({
            "background": "url('data:image/svg+xml;utf8," + svgCenterMarker.replace("__FILLCOLOR__", "#000000") + "') no-repeat center center",
            "background-position": "50% calc(50% - 17px)"
        }).show();
        $(".centerMarker .pulse,.centerMarker .pin").hide();
        $(".checkpoints .pointsList").hide();
        $(".checkpoints .pointPage").show();
        PuntoGrupo._creatingPoint = true;
        $(".pointPage input,.pointPage textarea").val('');
        $(".pointPage .pointGroupList").html('');
        if (!$("#sidebar").is(":visible")) {
            this.ShowList();
        }
        else {
            for (var i = 0; i < window.gruposPuntos.length; i++) {
                $(".pointPage .pointGroupList").append('<p>' +
                    '<input type="checkbox" class="filled-in"  id="group-' + window.gruposPuntos[i].GrupoID + '" value="' + window.gruposPuntos[i].GrupoID + '" />' +
                    '<label for="group-' + window.gruposPuntos[i].GrupoID + '">' + window.gruposPuntos[i].Nombre + '</label>' +
                    '</p>');
            }
        }
        $("#add-btn i").html("save");
        $("#add-btn").attr("onclick", "Checkpoints.CreatePoint()").removeClass("hidden");
        $("#close-btn").attr("onclick", "Checkpoints.CloseGroupEdit()");
        $("#main .mainTop .title").html("<es>Crear Punto</es><en>New Checkpoint</en>");
    };
    /**
     * Guarda el nuevo Punto creado en plataforma
     */
    PuntoGrupo.prototype.CreatePoint = function () {
        if (trim($("#nombrePunto").val()).length < 3) {
            alerta(lang("error"), lang("pointnameerror"));
        }
        else {
            var grupos = new Array();
            $(".pointGroupList input:checked").each(function (i, val) {
                var gid = objectIndex(gruposPuntos, "GrupoID", parseInt($(this).val()));
                grupos.push({
                    gid: $(this).val(),
                    name: gruposPuntos[gid].Nombre,
                    color: gruposPuntos[gid].Color
                });
            });
            if (grupos.length == 0) {
                alerta(lang("error"), lang("pointnogroup"));
            }
            else {
                map.draggable = false;
                waitSidebar(lang("makingpoint"));
                $("#add-btn").addClass("hidden");
                $.ajax({
                    url: pointUrl + "CrearPunto",
                    type: "POST",
                    dataType: "json",
                    timeout: 10000,
                    data: {
                        "token": window["userData"]["token"],
                        name: $("#nombrePunto").val(),
                        x: map.getCenter().lat,
                        y: map.getCenter().lng,
                        tel: $("#telPunto").val(),
                        mail: $("#emailPunto").val(),
                        pag: $("#webPunto").val(),
                        hora: $("#timePunto").val(),
                        dir: $("#dirPunto").val(),
                        nota: $("#notesPunto").val(),
                        grupos: grupos
                    },
                    success: function (data) {
                        console.log(data);
                        $("#add-btn").addClass("hidden");
                        toasty(lang("pointcreated"));
                        map.draggable = true;
                        closeWaitSidebar();
                        $("#add-btn").removeClass("hidden");
                        Checkpoints.CloseGroupEdit();
                    },
                    error: function (data) {
                        console.log(data);
                        map.draggable = true;
                        toasty(lang("tryagain"), "error");
                        closeWaitSidebar();
                        $("#add-btn").removeClass("hidden");
                    }
                });
            }
        }
    };
    /**
     * Muestra un Punto en el mapa al ser seleccionado
     * y lo oculta si ya estaba mostrandose, limite de 10.
     * @param point Identificador de Punto en plataforma
     * @param id Identificador de Punto en arreglo de Puntos
     */
    PuntoGrupo.prototype.SelectPoint = function (point, id) {
        $(".centerMarker").hide();
        $(".centerLabel").hide();
        var pgid = objectIndex(PuntoGrupo.Points, "id", point);
        if ($("#puntosel-" + point + " .check").hasClass("fa-check-square")) {
            $("#puntosel-" + point + " .check").removeClass("fa-check-square").addClass("fa-square");
            PuntoGrupo.selectedPoints.splice(PuntoGrupo.selectedPoints.indexOf(point), 1);
            PuntoGrupo.Points[pgid].marker.setVisibility(false);
            PuntoGrupo.Points[pgid].label.setVisibility(false);
        }
        else {
            if (PuntoGrupo.selectedPoints.length == 10) {
                toasty(lang('pointlimit'), "warn");
            }
            else {
                $("#puntosel-" + point + " .check").addClass("fa-check-square").removeClass("fa-square");
                PuntoGrupo.selectedPoints.push(point);
                if (PuntoGrupo.Points[pgid] == undefined) {
                    this.ShowCheckpoint(id);
                }
                else {
                    PuntoGrupo.Points[pgid].marker.setVisibility(true);
                    PuntoGrupo.Points[pgid].label.setVisibility(true);
                }
                MapEngine.FocusOnCoords(listaPuntos[id].x, listaPuntos[id].y);
            }
        }
    };
    /**
     * Guarda los cambios a un Punto en plataforma
     */
    PuntoGrupo.prototype.SavePointChanges = function () {
        if (trim($("#nombrePunto").val()).length < 3) {
            alerta(lang("error"), lang("pointnameerror"));
        }
        else {
            map.draggable = false;
            var pid = objectIndex(listaPuntos, "PuntoID", PuntoGrupo._editPoint);
            var puntoID = listaPuntos[pid].PuntoID;
            var grupos = new Array();
            $(".pointGroupList input:checked").each(function (i, val) {
                var gid = objectIndex(gruposPuntos, "GrupoID", parseInt($(this).val()));
                grupos.push({
                    gid: $(this).val(),
                    name: gruposPuntos[gid].Nombre,
                    color: gruposPuntos[gid].Color
                });
            });
            if (grupos.length == 0) {
                alerta(lang("error"), lang("pointnogroup"));
            }
            else {
                $("#add-btn").addClass("hidden");
                waitSidebar(lang("updatingpoint"));
                $.ajax({
                    url: pointUrl + "ActualizarPunto",
                    type: "POST",
                    dataType: "json",
                    timeout: 10000,
                    data: {
                        "token": window["userData"]["token"],
                        pid: puntoID,
                        name: $("#nombrePunto").val(),
                        x: map.getCenter().lat,
                        y: map.getCenter().lng,
                        tel: $("#telPunto").val(),
                        mail: $("#emailPunto").val(),
                        pag: $("#webPunto").val(),
                        hora: $("#timePunto").val(),
                        dir: $("#dirPunto").val(),
                        nota: $("#notesPunto").val(),
                        grupos: grupos
                    },
                    success: function (data) {
                        console.log(data);
                        PuntoGrupo.Points[pid].marker.setPosition(map.getCenter());
                        $("#add-btn").addClass("hidden");
                        toasty(lang("pointupdated"));
                        map.draggable = true;
                        closeWaitSidebar();
                        $("#add-btn").removeClass("hidden");
                    },
                    error: function (data) {
                        console.log(data);
                        map.draggable = true;
                        toasty(lang("tryagain"), "error");
                        closeWaitSidebar();
                        $("#add-btn").removeClass("hidden");
                    }
                });
            }
        }
    };
    /**
     * Pide confirmacion para eliminar un Grupo
     */
    PuntoGrupo.prototype.RemoveGroup = function () {
        confirmarMulti(lang("delgroup"), lang("delgroupsure"), [lang("cancel"), lang("removegrouponly"), lang("removepoints")], "Checkpoints.DelGroup()", "Checkpoints.DelGroupWithPoints()");
    };
    /**
     * Una vez confirmada la eliminacion del Grupo,
     * se elimina con sus Puntoa de la plataforma
     */
    PuntoGrupo.prototype.DelGroupWithPoints = function () {
        waitSidebar(lang("delgroupwithpointslbl"));
        $.ajax({
            url: pointUrl + "EliminarGruposConPuntos",
            type: "POST",
            timeout: 10000,
            data: {
                "token": window["userData"]["token"],
                "gid": gruposPuntos[PuntoGrupo._editGroup].GrupoID
            },
            success: function (data) {
                console.log(data);
                window.gruposPuntos = data.data;
                Checkpoints.CloseGroupEdit();
                toasty(lang("groupremoved"));
            },
            error: function (data) {
                console.log(data);
                closeWaitSidebar();
                toasty(lang("tryagain"), "error");
            }
        });
    };
    /**
     * Elimina uno o varios Grupos de la Plataforma
     */
    PuntoGrupo.prototype.DelGroup = function () {
        waitSidebar(lang("delgrouplbl"));
        $.ajax({
            url: pointUrl + "EliminarGrupos",
            type: "POST",
            timeout: 10000,
            data: {
                "token": window["userData"]["token"],
                "gid": gruposPuntos[PuntoGrupo._editGroup].GrupoID
            },
            success: function (data) {
                console.log(data);
                window.gruposPuntos = data.data;
                Checkpoints.CloseGroupEdit();
                toasty(lang("groupremoved"));
            },
            error: function (data) {
                console.log(data);
                closeWaitSidebar();
                toasty(lang("tryagain"), "error");
            }
        });
    };
    /**
     * Cierra la vista de edicion de un Grupo
     */
    PuntoGrupo.prototype.CloseGroupEdit = function () {
        var _this = this;
        $(".centerLabel").hide();
        $("#sidebar").scrollTop(0);
        if ($(".checkpoints .pointPage").is(":visible")) {
            mapbehavior.enable(H.mapevents.Behavior.WHEELZOOM);
            $("#main .mainTop .title").html("<es>Puntos</es><en>Checkpoints</en>");
            $(".checkpoints .pointPage").hide();
            $(".checkpoints .pointsList").show();
            $("#close-btn").attr("onclick", "Checkpoints.CloseSidebar('groupsList')");
            $(".centerMarker").hide();
            $(".centerMarker .pulse,.centerMarker .pin").show();
            //$("#add-btn").attr("onclick","Checkpoints.NewCheckpoint()");
            if (PuntoGrupo._creatingPoint == false) {
                var pid = objectIndex(listaPuntos, "PuntoID", PuntoGrupo._editPoint);
                if (PuntoGrupo.selectedPoints.indexOf(PuntoGrupo.Points[pid].id) >= 0) {
                    PuntoGrupo.Points[pid].marker.setVisibility(true);
                    PuntoGrupo.Points[pid].label.setVisibility(true);
                }
            }
            this.ObtenerPuntosConGrupos();
            PuntoGrupo._creatingPoint = false;
            //$("#add-btn i").html("add");
        }
        else if ($(".checkpoints .groupPage").is(":visible")) {
            this.SelfDestruct();
            try {
                masterClose = function () { _this.CloseSidebar('groupsList'); };
            }
            catch (e) { }
            //$("#add-btn").attr("onclick","Checkpoints.CreateGroup()");
            $("#main .mainTop .title").html("<es>Grupos</es><en>Groups</en>");
            $(".checkpoints .groupsList").show();
            $(".checkpoints .groupPage").hide();
            $("#close-btn").attr("onclick", "Checkpoints.CloseSidebar('groupsList')");
            //this.RemoveCheckpoints();
            this.ObtenerPuntosConGrupos();
            try {
                map.setViewBounds(PuntoGrupo.pointsGroup.getBounds(), false);
            }
            catch (e) { }
        }
    };
    /**
     * Muestra la lista de Puntos
     */
    PuntoGrupo.prototype.ShowList = function () {
        var _this = this;
        selectMenu(".checks-menu");
        try {
            masterClose = function () { _this.CloseSidebar('pointsList'); };
        }
        catch (e) { }
        $("#main .mainTop").addClass("no-logo");
        $("#main .mainTop .title").html("<es>Puntos</es><en>Checkpoints</en>");
        //$("#add-btn").attr("onclick","Checkpoints.NewCheckpoint()").removeClass("hidden");
        //$("#add-btn i").html("add");
        waitSidebar("<es>Cargando Puntos...</es><en>Loading Checkpoints...</en>");
        if (PuntoGrupo._creatingPoint == true) {
            this.OpenSidebar("pointPage");
        }
        else {
            this.OpenSidebar("pointsList");
        }
        this.ObtenerPuntosConGrupos(false);
    };
    /**
     * Muestra la lista de Grupos de Puntos
     */
    PuntoGrupo.prototype.ShowGroups = function () {
        var _this = this;
        selectMenu(".checkgroups-menu");
        try {
            masterClose = function () { _this.CloseSidebar('groupsList'); };
        }
        catch (e) { }
        $("#main .mainTop").addClass("no-logo");
        $("#main .mainTop .title").html("<es>Grupos</es><en>Groups</en>");
        //$("#add-btn i").html("add");
        waitSidebar("<es>Cargando Grupos...</es><en>Loading Groups...</en>");
        this.OpenSidebar("groupsList");
        this.ObtenerPuntosConGrupos();
    };
    /**
     * Abre el Sidebar en una seccion especifica de PuntoGrupo
     * @param section Seccion de PuntoGrupo a abrir en el Sidebar
     */
    PuntoGrupo.prototype.OpenSidebar = function (section) {
        $(".mainTop .tool.right").hide();
        $("#sidebar .toHide").hide();
        $("#sidebar .checkpoints").show();
        $("#sidebar,#close-btn").removeClass("hidden");
        $("#sidebar .checkpoints > div").hide();
        $(".checkpoints ." + section).show();
        $("#close-btn").attr("onclick", "Checkpoints.CloseSidebar('" + section + "')");
        views();
        Config.disableFullApp();
    };
    /**
     * Cierra el Sidebar y oculta en una seccion especifica de PuntoGrupo
     * @param section Seccion de PuntoGrupo a cerrar en el Sidebar
     */
    PuntoGrupo.prototype.CloseSidebar = function (section) {
        $(".centerMarker").hide();
        resetBack();
        $("#main .mainTop").removeClass("no-logo");
        $("#main .mainTop .title").html("");
        $("#sidebar,#add-btn,#close-btn,.otherMethod,.undoButton").addClass("hidden");
        $(".checkpoints ." + section + ",.centerAim").hide();
        $(".undoButton").addClass("disabled");
        $(".mainTop .tool.right").show();
        views();
        try {
            map.setViewBounds(PuntoGrupo.pointsGroup.getBounds());
        }
        catch (e) { }
        Config.enableFullApp();
    };
    /**
     * Autodestruye un Punto Temporal
     * (cuando se quiere consultar un Punto on-demand)
     */
    PuntoGrupo.prototype.SelfDestruct = function () {
        var toDestroy = new Array();
        for (var i = 0; i < PuntoGrupo.Points.length; i++) {
            if (PuntoGrupo.Points[i].autoDestruct == true) {
                toDestroy.push(PuntoGrupo.Points[i].id);
            }
        }
        for (var i = 0; i < toDestroy.length; i++) {
            var pid = objectIndex(PuntoGrupo.Points, "id", toDestroy[i]);
            PuntoGrupo.pointsGroup.removeObject(PuntoGrupo.Points[pid].marker);
            PuntoGrupo.pointsGroup.removeObject(PuntoGrupo.Points[pid].label);
            PuntoGrupo.Points.splice(pid, 1);
        }
    };
    /**
     * Limpia y retira los Puntos dibujados en el mapa
     */
    PuntoGrupo.prototype.RemoveCheckpoints = function () {
        PuntoGrupo._pointsOnMap = new Array();
        PuntoGrupo.pointsGroup.removeAll();
        PuntoGrupo.Points = new Array();
        PuntoGrupo.selectedPoints = new Array();
        this.CloseBubbles();
    };
    /**
     * Remueve todo lo que tenga que ver con PuntoGrupos,
     * Geocercas y detiene el RC
     */
    PuntoGrupo.prototype.ClearCheckpoints = function () {
        PuntoGrupo._pointsOnMap = new Array();
        Track.StopTracking();
        this.RemoveCheckpoints();
        Geocercas.selectedShapes = new Array();
        Geocercas.polyGroup.removeAll();
    };
    /**
     * Cierra los iBubbles abiertos actualmente
     */
    PuntoGrupo.prototype.CloseBubbles = function () {
        if (window.ibubble != null) {
            window.ibubble.close();
            window.ibubbles = null;
        }
    };
    PuntoGrupo.pointsGroup = null;
    PuntoGrupo.selectedPoints = new Array();
    PuntoGrupo._pointsOnMap = new Array();
    PuntoGrupo._editGroup = -1;
    PuntoGrupo._editPoint = -1;
    PuntoGrupo._creatingPoint = false;
    PuntoGrupo.Points = new Array();
    PuntoGrupo.groupsArray = new Array();
    return PuntoGrupo;
}());
