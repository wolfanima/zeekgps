/// <reference path ="jquery.d.ts"/> 
/**
 * MENU
 * Clase con metodos basicos del menu, nada complicado :)
 */
var Menu = /** @class */ (function () {
    function Menu() {
        /**
         * class: 	Clase de la opcion de menu
         * onclick:	Funcion a ejecutrar al hacer click
         * icon: 	Icono de la opcion
         * title_es:Titulo en español
         * title_en:Titulo en ingles
         * is_multi:Tiene submenu
         * target:	Identificador de submenu
         * sumenu:	Objeto con opciones de submenu
         * skip:	No se muestra en menu (para opciones depreciadas)
         * divider: Divisor
        */
        this.menuStruct = [];
        this.zeekGpsMenuStruct = [
            {
                class: "SoloRastreo hidden",
                onclick: "resetMenu();Track.SoloRastreo();",
                icon: 'zicon_auto',
                title_es: "Rastrear Vehículo",
                title_en: "Track Vehicle"
            },
            {
                is_multi: true,
                onclick: "toggleMenuArrow('rastreo')",
                target: "rastreo",
                title_es: "Rastreo Vehicular",
                title_en: "Vehicle Tracking",
                submenu: [
                    {
                        class: "RastreoContinuo",
                        onclick: "resetMenu();Track.SelectCars();",
                        icon: 'zicon_rastreo-continuo',
                        title_es: "Rastreo Continuo",
                        title_en: "Real Time Tracking"
                    },
                    {
                        class: "UltimaPosicion",
                        onclick: "resetMenu();Track.OpenLastPosition();",
                        icon: 'zicon_ultima-posicion',
                        title_es: "Última Posición",
                        title_en: "Last Position"
                    },
                    {
                        class: "Historico",
                        onclick: "resetMenu();Track.OpenHistory();",
                        icon: 'zicon_historial',
                        title_es: "Histórico",
                        title_en: "History"
                    },
                    {
                        skip: true,
                        class: "HistoricoConPuntos",
                        onclick: "resetMenu();Track.OpenHistoryWithPoints()",
                        icon: 'timelapse',
                        title_es: "Hist&oacute;rico con Puntos",
                        title_en: "History &amp; Checkpoints"
                    },
                    {
                        skip: true,
                        class: "TiempoMuerto",
                        onclick: "resetMenu();Track.OpenDeadTime();",
                        icon: 'av_timer',
                        title_es: "Tiempo Muerto",
                        title_en: "Dead Time"
                    },
                    {
                        skip: true,
                        class: "_Eventos",
                        onclick: "resetMenu();Track.OpenCarEvents();",
                        icon: 'notifications',
                        title_es: "Eventos",
                        title_en: "Events"
                    },
                    {
                        skip: true,
                        class: "ExcesosVelocidad",
                        onclick: "resetMenu();Track.OpenSpeedLimit();",
                        icon: 'network_check',
                        title_es: "Excesos de Velocidad",
                        title_en: "Speed Limits"
                    },
                    {
                        skip: true,
                        class: "Ralenti",
                        onclick: "resetMenu();Track.OpenIdleTime();",
                        icon: 'directions_car',
                        title_es: "Ralentí",
                        title_en: "Idle Time"
                    },
                    {
                        skip: true,
                        class: "ConsultaOrdenesTrabajo",
                        onclick: "resetMenu();Orders.OpenWorkOrders();",
                        icon: 'assignment',
                        title_es: "Ordenes de Trabajo",
                        title_en: "Work Orders"
                    },
                    {
                        skip: true,
                        class: "ConsultaPuntos",
                        onclick: "resetMenu();Track.OpenVisited();",
                        icon: 'assistant_photo',
                        title_es: "Puntos Visitados",
                        title_en: "Visited Checkpoints"
                    },
                    {
                        divider: true
                    }
                ]
            },
            {
                is_multi: true,
                onclick: "toggleMenuArrow('georeferencias')",
                target: "georeferencias",
                title_es: "Georeferencias",
                title_en: "Georeferences",
                submenu: [
                    {
                        skip: true,
                        class: "",
                        onclick: "resetMenu();Geos.CreateGeoPoly()",
                        icon: 'panorama_fish_eye',
                        title_es: "Geocerca Circular",
                        title_en: "Circular Geofence"
                    },
                    {
                        skip: true,
                        class: "",
                        onclick: "resetMenu();Geos.CreateGeoSquare()",
                        icon: 'crop_square',
                        title_es: "Geocerca Cuadrada",
                        title_en: "Square Geofence"
                    },
                    {
                        skip: true,
                        class: "",
                        onclick: "resetMenu();Geos.CreateComplex()",
                        icon: 'change_history',
                        title_es: "Geocerca Compleja",
                        title_en: "Complex Geofence"
                    },
                    {
                        skip: true,
                        class: "",
                        onclick: "resetMenu();Geos.CreateRoute()",
                        icon: 'timeline',
                        title_es: "Crear Ruta",
                        title_en: "Create Route"
                    },
                    {
                        class: "ConsultaGeocercas geos-menu",
                        onclick: "resetMenu();Geos.ShowList();",
                        icon: 'zicon_georeferencias',
                        title_es: "Consultar Georeferencias",
                        title_en: "Geofences List"
                    },
                    {
                        skip: true,
                        class: "ConsultaPuntos",
                        onclick: "resetMenu();Checkpoints.NewCheckpoint()",
                        icon: 'add_location',
                        title_es: "Crear Punto",
                        title_en: "Create Checkpoint"
                    },
                    {
                        class: "ConsultaPuntos checkgroups-menu",
                        onclick: "resetMenu();Checkpoints.ShowGroups()",
                        icon: 'zicon_grupos',
                        title_es: "Consultar Grupos",
                        title_en: "Groups List"
                    },
                    {
                        class: "ConsultaPuntos checks-menu",
                        onclick: "resetMenu();Checkpoints.ShowList();",
                        icon: 'zicon_puntos',
                        title_es: "Consultar Puntos",
                        title_en: "Checkpoint List"
                    },
                    {
                        divider: true
                    }
                ]
            },
            {
                class: "fuel-menu",
                onclick: "resetMenu();Fuel.Open()",
                icon: 'fas fa-gas-pump',
                title_es: "Fuel",
                title_en: "Fuel"
            },
            {
                class: "limpiarMapa",
                onclick: "resetMenu();MapEngine.ClearMap()",
                icon: 'zicon_limpiar-mapa',
                title_es: "Limpiar Mapa",
                title_en: "Clear Map"
            },
            {
                divider: true
            },
            {
                class: "ConsultaVehiculos",
                onclick: "resetMenu();Cars.ShowList();",
                icon: 'zicon_auto',
                title_es: "Unidades",
                title_en: "Vehicles"
            },
            {
                class: "share-menu",
                onclick: "resetMenu();Share.OpenShare()",
                icon: 'far fa-share-alt',
                title_es: "Compartir Rastreo",
                title_en: "Share Tracking"
            },
            {
                class: "onlyRC push-menu",
                onclick: "resetMenu();PushNotifs.Open();",
                icon: 'zicon_notificaciones-push',
                title_es: "Notificaciones Push",
                title_en: "Push Notifications"
            },
            {
                class: "settings-menu",
                onclick: "resetMenu();Config.OpenSettings();",
                icon: 'zicon_config',
                title_es: "Configuración",
                title_en: "Settings"
            },
            {
                class: "help-menu",
                onclick: "resetMenu();Config.OpenHelp();",
                icon: 'zicon_ayuda',
                title_es: "Ayuda",
                title_en: "Help"
            },
            {
                divider: true
            },
            {
                class: "debug console-menu hidden",
                onclick: "resetMenu();Config.OpenConsole()",
                icon: 'fas fa-terminal',
                title_es: "Consola",
                title_en: "Console"
            },
            {
                class: "",
                onclick: "resetMenu();SesionEngine.CerrarSesion();",
                icon: 'zicon_cerrar-sesion',
                title_es: "Cerrar Sesión",
                title_en: "Logout"
            }
        ];
    }
    /**
     * Abre y muestra el menu, easy
     */
    Menu.prototype.Open = function () {
        //$(".sidenav").sidenav("open");
        //$(".fade").fadeTo(animSpeed,1);
        //masterClose = function(){this.Hide();};
        //Menu._isOpen = true;
    };
    /**
     * Construct
     */
    Menu.prototype.Construct = function () {
        var _this = this;
        checkBuild();
        $("#sidenav .menu-content").html('');
        //ES APP DE TRANSPORTE EMPRESARIAL?
        if (isTEApp) {
            if (TE.isDriver) {
                //MENU CONDUCTOR
                this.menuStruct = TE.teAppMenuStructDriver;
            }
            else {
                //MENU PASAJERO
                this.menuStruct = TE.teAppMenuStruct;
            }
        }
        else {
            this.menuStruct = this.zeekGpsMenuStruct;
        }
        this.menuStruct.forEach(function (elem) {
            $("#sidenav .menu-content").append(_this.renderMenuOption(elem));
        });
        if (SesionEngine.Permisos("Fuel") == false) {
            $(".fuel-menu").addClass("hidden");
        }
    };
    /**
     * renderMenuOption
     */
    Menu.prototype.renderMenuOption = function (elem) {
        var _this = this;
        var menuHTML = "";
        if (elem.skip != true) {
            if (elem.divider == true) { //DIVIDER
                menuHTML = '<li><div class="divider"></div></li>';
            }
            else {
                if (elem.is_multi == true) { //TIENE SUBMENU
                    menuHTML = "<ul class=\"collapsible collapsible-accordion\">\n\t\t\t\t\t\t\t\t\t<li onclick=\"" + elem.onclick + "\">\n\t\t\t\t\t\t\t\t\t\t<a class=\"collapsible-header " + elem.target + "\">\n\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">arrow_right</i>\n\t\t\t\t\t\t\t\t\t\t\t<es>" + elem.title_es + "</es>\n\t\t\t\t\t\t\t\t\t\t\t<en>" + elem.title_en + "</en>\n\t\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t\t\t<div class=\"collapsible-body " + elem.target + "-collapsible\">\n\t\t\t\t\t\t\t\t\t\t\t<ul>";
                    elem.submenu.forEach(function (subelem) {
                        menuHTML += _this.renderMenuOption(subelem);
                    });
                    menuHTML += "\t\t</ul>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t</ul>";
                }
                else { //ES OPCION SENCILLA
                    menuHTML = "<li class=\"" + elem.class + "\">\n\t\t\t\t\t\t\t\t\t<a  href=\"#!\" class=\"sidenav-close\" onclick=\"" + elem.onclick + "\">\n\t\t\t\t\t\t\t\t\t\t<i class=\"" + elem.icon + "\"></i>\n\t\t\t\t\t\t\t\t\t\t<es>" + elem.title_es + "</es>\n\t\t\t\t\t\t\t\t\t\t<en>" + elem.title_en + "</en>\n\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t</li>";
                }
            }
        }
        return menuHTML;
    };
    /**
     * Oculta el menu, not so easy
     */
    Menu.prototype.Hide = function () {
        $("*").scrollTop(0);
        //$('.sidenav').sidenav('close');
        //Esta abierto el menu?
        if (Menu._isOpen == true) {
            //Animaciones chidoris
            //$(".fade").fadeTo(animSpeed,0,function(){$(this).hide();});
            $(".menu").fadeOut(animSpeed, function () {
                Menu._isOpen = false;
                $(".collapsible-header.rastreo i").html('arrow_right');
                $(".collapsible-header.georeferencias i").html('arrow_right');
                try {
                    masterClose = function () { navigator.app.exitApp(); };
                }
                catch (e) { }
            });
        }
    };
    /**
     * Obtiene el estado abierto/cerrado del menu
     */
    Menu.prototype.GetStatus = function () {
        return Menu._isOpen;
    };
    /**
     * Establece el estado del menu
     * @param openStatus TRUE si esta abierto, FALSE si no
     */
    Menu.prototype.SetStatus = function (openStatus) {
        Menu._isOpen = openStatus;
    };
    //Indica el estado del menu
    //True = Abierto
    //False = Cerrado [Default]
    Menu._isOpen = false;
    return Menu;
}());
