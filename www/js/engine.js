var ultimoClickCreacionGeo = new Date(),
appName = 		"Zeek GPS",
DEBUG = 		false,
isWL = 			false,
isHuawei = 		false,
isTEApp = 		false, 					//Es App de Transporte Empresarial?
debug = 		false,
sonidos = 		new Array(),
isIOS= 			false,
markers = 		new Array(),
isLandscape = 	false,
map,
mapInit = 		false,
version = 		"",
gpsVersion = 	"1.4.3",
teVersion = 	"1.0.0",
buildDate =		"2021-08-20 18:24:25",
animSpeed = 	200,
masterClose = 	null,
fontStyle = 	"Arial",  
keyboardHeight = 0,
divForMeasurement,
measureDiv,
actionTooltips =false,
MenuCtrl = 		null, 
Cars = 			null,
Geos = 			null,
Fuel = 			null,
Checkpoints = 	null,
MapEngine = 	null,
SesionEngine = 	null,
Closer =		null,
Share = 		null,
System = 		null,
Track = 		null,
Orders = 		null,
Config = 		null,
Interface = 	null,
PushNotifs =	null,
Support = 		null,
TE = 			null,
authUrl = 		"https://apps.zeekgps.com/zeekgpsapp/Sesion/",
rasStaticUrl = 	"https://apps.zeekgps.com/zeekgpsapp/RastreoEstatico/",
geoUrl = 		"https://apps.zeekgps.com/zeekgpsapp/Geocercas/",
pointUrl = 		"https://apps.zeekgps.com/zeekgpsapp/PuntoGrupo/",
interfaceUrl = 	"https://apps.zeekgps.com/zeekgpsapp/Interfaces/",
zeekappsUrl = 	"https://auto.zeekgps.com/zeekgpsapp/ZeekApps/",
pushUrl = 		"https://apps.zeekgps.com/zeekapijs/CMovilApi3WS.asmx/",
cmovilUrl = 	"https://apps.zeekgps.com/CMovilApi3WS/CMovilApi3WS.asmx/",
apiBaseUrl = 	"https://auto.zeekgps.com/zeek/api",
licencia =		'stakentecomMYona',
app_id = 		'prnafudrejucran6reSp',
app_code = 		'npTBY-I8ukNObDUO_u8oPQ',
hereApiKey = 	'S_IOLCVkvw7EdkayIHGLNP7vFmr5aGfn01Q1H1UrQ_Q',
stringArray = 	['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
	'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'],
supportStringArray = 	['0','1','2','3','4','5','6','7','8','9',
	'A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z'],
bateria = {
	level: -1,
	isPlugged: false
};

window.idioma = 		"es";
window.culture =		"es-MX";
window.distMetric = 	"Km";
window.fluidMetric = 	"Lt";
window.tempMetric = 	"c";
window.pushList = 		new Array();
window.etiquetas = 		new Array();
window.definedClasses = false;
window.focusingPoint =	false;
window.pushActivated = 	false;
window.pushRegistered = false;
window.pushAllowed = 	false;
window.solicitarPosicion=true;
window.positionInterval = null;
window.geoPermisoOtorgado = false;
window.fromPanicButton = false;
window._plataforma = "android";
window.pushQueue =		[];
//Son los que tienen la geocerca, para agregar vertices, moverla, o editarla
var icoVertice = "img/geoIcons/pin.png";
var DPR = 1;

var svgCenterMarker = '<svg xmlns="http://www.w3.org/2000/svg" width="28px" height="34px">' +
			  '<path d="M 19 31 C 19 32.7 16.3 34 13 34 C 9.7 34 7 32.7 7 31 C 7 29.3 9.7 28 13 28 C 16.3 28 19' +
			  ' 29.3 19 31 Z" fill="#000" fill-opacity=".2"/>' +
			  '<path d="M 13 0 C 9.5 0 6.3 1.3 3.8 3.8 C 1.4 7.8 0 9.4 0 12.8 C 0 16.3 1.4 19.5 3.8 21.9 L 13 31 L 22.2' +
			  ' 21.9 C 24.6 19.5 25.9 16.3 25.9 12.8 C 25.9 9.4 24.6 6.1 22.1 3.8 C 19.7 1.3 16.5 0 13 0 Z" fill="#fff"/>' +
			  '<path d="M 13 2.2 C 6 2.2 2.3 7.2 2.1 12.8 C 2.1 16.1 3.1 18.4 5.2 20.5 L 13 28.2 L 20.8 20.5 C' +
			  ' 22.9 18.4 23.8 16.2 23.8 12.8 C 23.6 7.07 20 2.2 13 2.2 Z" fill="__FILLCOLOR__"/>' +
 			  '</svg>';

document.addEventListener("deviceready", onDeviceReady, false);
window.addEventListener("batterystatus", onBatteryStatus, false);
window.addEventListener('native.keyboardhide', function(event)  {
	keyboardHideHandler(1);
});
window.addEventListener('keyboardDidHide', function(event)  {
	keyboardHideHandler(1);
});
//Listener cuando el teclado SE VA a ocultar
window.addEventListener('keyboardWillHide', function(event)  {
	keyboardHideHandler(0);
});
window.addEventListener('keyboardDidShow', function(event)  {
	keyboardShowHandler(event,1);
});
//Listener cuando el teclado SE VA a mostrar
window.addEventListener('keyboardWillShow', function(event)  {
	keyboardShowHandler(event,0);
});
function keyboardShowHandler(event,status) {

	try{
		keyboardHeight = e.keyboardHeight;
		if (device.platform.toLowerCase() == "ios") {
			if(keyboardIsVisible==false){
				window.scrollTo(0,0);
			}
			$("html").css("padding-bottom",keyboardHeight);
		}
	}catch(e){}
	
	if(status == 1){
		keyboardIsVisible = true;
	}
}
/* Cuando el teclado se oculte redibujamos el viewport */
function keyboardHideHandler() {
	views();
	keyboardIsVisible = false;
}
function onBatteryStatus(status) {
	bateria = status;
}
/* Listener para el boton de Retroceso en moviles */
document.addEventListener('backbutton', function() {
	try{
		trackEvent('Evento', 'Back Key Presionado');
	}catch(e){}
	try{
		if($("#wait").is(":visible") == false){
			masterClose();
		}
	}catch(e){};
},false);
function versionCompare(version1,version2){
   var result=false;

    if(typeof version1!=='object'){ version1=version1.toString().split('.'); }
    if(typeof version2!=='object'){ version2=version2.toString().split('.'); }

    for(var i=0;i<(Math.max(version1.length,version2.length));i++){

        if(version1[i]==undefined){ version1[i]=0; }
        if(version2[i]==undefined){ version2[i]=0; }

        if(Number(version1[i])<Number(version2[i])){
            result=true;
            break;
        }
        if(version1[i]!=version2[i]){
            break;
        }
    }
    return(result);
}
/* Obtiene los Vehiculos del cliente ondemand */
function getVehiculos(){
	SesionEngine.getVehiculosCliente();
}
function selectMenu(elem) {
	$(elem).addClass("selected-menu");
}
function forceConsole() {
	closeWait();
	$("#loginPage").addClass("hidden");
    $("#console").removeClass("hidden");
    $("#console_").html(print_console());
}
function resetBack() {
	try{
		if(TE.viajeEnCurso == false){
			$(".actioncircle").removeClass("hidden");
		}
		if(Closer.closingAll == false){
			setTimeout(()=>{
				if($(".uiView").is(":visible") == false){
					masterClose = ()=>{closeApp();};
				}
			},1000);
		}
	}catch(e){}
	$(".sidenav li").removeClass("selected-menu");
}
/* Wrapper para cerrar la app */
function closeApp() {
	try{
		navigator.app.exitApp();
	}catch(e){}
}
function checkBuild() {
	try{
		switch(BuildInfo.packageName) {
			case "com.zeekgps.gpsapp": //ZEEK GPS
				appName = "Zeek GPS";
				isTEApp = false;
				version = gpsVersion;
			break;
			case "com.argus.tech.teapp": //APP TRANSPORTE EMPRESARIAL
			case "com.argustech.teapp":
				appName = "Transporte Empresarial";
				isTEApp = true;
				version = teVersion;
				TEApp();
			break;
		}
	}catch(e){
		if(!!window.cordova == true){
			console.error("BuildInfo",e);
		}
		if(isTEApp) {
			version = teVersion;
		}else{
			version = gpsVersion;
		}
	}
}
function onDeviceReady() {
	checkBuild();
	var huaweiConfirmation = false;
	try{
		var paramkeyArray=["isHuawei"];
	    CustomConfigParameters.get(function(configData){
			//console.warn(configData);
			if(configData.isHuawei == "true"){
				huaweiConfirmation = true;
				isHuawei = true;
				console.warn("isHuawei","YES");
			}
	    },function(err){
	      console.error(err);
	    },paramkeyArray);
	}catch(e){
		console.error("CustomConfigParameters Catch",e);
	}
	window._plataforma = device.platform.toLowerCase();
	try{
		if(window._plataforma == "ios") {
			isHuawei = false;
		}else{
			if(huaweiConfirmation == false){
				isHuawei = false;
			}
		}
	}catch(e){}


  	try{
		/*if(isWL == true) {
			StatusBar.backgroundColorByHexString("#295811");
		}else{
			StatusBar.backgroundColorByHexString("#d9400e");
		}
		StatusBar.overlaysWebView(false);
		StatusBar.styleBlackTranslucent();
		StatusBar.styleLightContent();
		StatusBar.show();*/
	}catch(e){
		alerta("StatusBar",e);
	}
	
	try{
		window.open = cordova.InAppBrowser.open;
	}catch(e){
		console.error("InAppBrowser",e);
	}
	
	if(device.platform.toLowerCase() === "android"){
		$('head').append('<link rel="stylesheet" href="css/android.css" type="text/css" />');
	}
	if(device.platform.toLowerCase() === "ios"){
		isIOS = true;
		console.log("IOS FOUND");
		loadRemoteCSS("ios");
		$(".waves-effect").removeClass("waves-effect waves-light");
		$('head').append('<link rel="stylesheet" href="css/ios.css" type="text/css" />');
	}else{
		$(".doze").removeClass("hidden");
	}
	//checkDoze();
	MobileAccessibility.usePreferredTextZoom(false);
	window.pushResumed = null;
	formSelect();
	domReady();
}
/* Al cargar el DOM */
function onLoad() {
	if(!!window.cordova == false) {
		domReady();
	}
}
function domReady() {
	if(!debug) {
		$(".debug").addClass('hidden');
	}
	$('.alert-modal').modal();
	$(".confirm-multi-modal").modal({dismissible:false});
	$(".confirm-modal").modal({dismissible:false});
	$(".prompt-modal").modal({dismissible:false});
	window.lastWindowWidth = window.innerWidth;
	showChangelog();
		$('.actioncircle').floatingActionButton({
		direction: 	'left',
		hoverEnabled:false

	});
	if(getKey("zeekgpsLang") != null){
		changeLang(getKey("zeekgpsLang"));
	}else{
		try{
			if(navigator.language.substr(0,2) == "en") {
				changeLang("en");
			}else{
				changeLang("es");
			}
		}catch(e){
			console.error("Error tratando de obtener idioma nativo.")
			changeLang("es");
		}
	}
	$(document).on("click","#trip_summary .rate_star",function(){
		$("#trip_summary .rate_star").removeClass("pos");
		window.currentStar = parseInt($(this).attr("star"));
		for(var star=1;star<=window.currentStar;star++){
			$("#trip_summary .rate_star[star="+star+"]").addClass("pos");
		}
		$("#trip_summary .rate_message").html($(this).attr("message"));
		$("#trip_summary .btn.rating-btn").removeClass("disabled");
		if (window.currentStar < 5) {
			if (window.currentStar >= 3) {
				$("#trip_summary .rate_opinion").attr("placeholder","¿Qué podemos mejorar?");
			}else{
				$("#trip_summary .rate_opinion").attr("placeholder","¿Qué salió mal?");
			}
		}
	});
	$(".actioncircle").click(function() {
		if($(".actioncircle .btn-floating.btn-large i").hasClass("fa-times")) {
			$(".actioncircle .btn-floating.btn-large i").removeClass("fa-times").addClass("fa-chevron-left");
		}else{
			$(".actioncircle .btn-floating.btn-large i").removeClass("fa-chevron-left").addClass("fa-times");
		}
	});
	$(".loginForm .show-pass").click(function(){
		if($("#loginPass").prop("type") == "text") {
			$("#loginPass").prop("type","password");
			$(".loginForm .show-pass i").removeClass("fa-eye").addClass("fa-eye-slash");
		}else{
			$("#loginPass").prop("type","text");
			$(".loginForm .show-pass i").removeClass("fa-eye-slash").addClass("fa-eye");
		}
		
	});
	$("#sidebar").scroll(function(){
		try{
			if($(".mainTop .tool.right i").hasClass("fa-arrow-alt-circle-up") && $("#sidebar").scrollTop() > 0) {
				$(".mainTop .tool.right").show();
			}
			if($(".mainTop .tool.right i").hasClass("fa-arrow-alt-circle-up") && $("#sidebar").scrollTop() == 0){
				$(".mainTop .tool.right").hide();
			}
		}catch(e){}
	});
	window.eggCounter = 0;
	$(document).on("click","#ayuda .version",function(e){
		
		//verCambios();
	});
	resetBack();
	$(document).keyup(function(e) {
	     if (e.keyCode == 27) { // escape key maps to keycode `27`
	        masterClose();
	    }
	});
	
	window.onFocus = false;
	try{
		FastClick.attach(document.body);
	}catch(e){
		
	}
	var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
	if(iOS == true || isIOS == true){
		$(".waves-effect").removeClass("waves-effect waves-light");
	}

	if(!!window.cordova == false){
		if(getKey("zeekgpsLang") != null){
			changeLang(getKey("zeekgpsLang"));
		}else{
			changeLang("es");
		}
		showChangelog();
	}
	if(getKey("zeekgpsDistMetric") == null){
		setKey("zeekgpsDistMetric",window.distMetric);
		$(".distMetricLabel").html("<es>Kilómetros</es><en>Kilometers</en>");
	} 
	if(getKey("zeekgpsFluidMetric") == null){
		setKey("zeekgpsFluidMetric",window.fluidMetric);
		$(".distMetricLabel").html("<es>Litros</es><en>Liters</en>");
	}
	if(getKey("zeekgpsTempMetric") == null){
		setKey("zeekgpsTempMetric",window.tempMetric);
		$(".tempMetricLabel").html("<es>Centígrados</es><en>Celsius</en>");
	}
	
	$("#loginPage .version").html(`<span class="version_string">${version}</span>`);
	hereInit();
	views();
	if(loadSet("zeekUserdata").length != 0){
		try{
			if(navigator.onLine == true) {
				if(isTEApp) {
					wait(lang("loggingin"),lang("pleasewait"));
					TE.IniciarSesion(getKey("zeekgpsUser"),getKey("zeekgpsPass"));
				}else{
					window.justLogged = true;
					window.userData = loadSet("zeekUserdata");
					window.distMetric = capitalizeFirstLetter( getKey("zeekgpsDistMetric") );
					window.fluidMetric = capitalizeFirstLetter(getKey("zeekgpsFluidMetric"));
					window.tempMetric = getKey("zeekgpsTempMetric");
					SesionEngine.PrepUser();
					wait(lang("loggingin"),lang("pleasewait"));
					$(".user-view .name").html(userData.user.Nombre);
					
					SesionEngine.GetUserData();
				}
			}else{

				window.justLogged = false;
				$("#loginPage").removeClass("hidden");
				closeWait();
			}
		}catch(e){
			saveSet("zeekUserdata",[]);
			console.error("zeekUserData Retrieve: "+e.message);
			window.justLogged = false;
			$("#loginPage").removeClass("hidden");
			closeWait();
		}
	}else{
		try{
			if(isTEApp){
				StatusBar.backgroundColorByHexString("#4a62bc");
			}else{
				if(isWL == true) {
					StatusBar.backgroundColorByHexString("#295811");
				}else{
					StatusBar.backgroundColorByHexString("#d9400e");
				}
			}
			StatusBar.overlaysWebView(false);
			StatusBar.styleBlackTranslucent();
			StatusBar.styleLightContent();
			StatusBar.show();
		}catch(e){}
		$("#loginPage").removeClass("hidden");
		closeWait(); 
	}
	declareSidenav();
	
	$("#color_polyCircle").change(function(){
		if(Geocercas._editing == true){
			Geocercas.ShapeInstance[Geocercas._editShapeID]._color = $("#color_polyCircle").val();
			Geocercas.objectList.removeObject(Geocercas.ShapeInstance[Geocercas._editShapeID]._instance);
			Geocercas.ShapeInstance[Geocercas._editShapeID].Load();
		}else{
			Geocercas._polyGeoCircleProps.color = hexToRgbA($("#color_polyCircle").val(),0.3);
			Geocercas._polyGeoCircleProps.colorHex = $("#color_polyCircle").val();
			$(".centerMarker .pin").css("background",$(this).val());
			deletePolyInstance();
			repaintPolyInstance();
		}
	});
	
	$("#color_polySquare").change(function(){
		if(Geocercas._editing == true){
			Geocercas.ShapeInstance[Geocercas._editShapeID]._color = $("#color_polySquare").val();
			Geocercas.objectList.removeObject(Geocercas.ShapeInstance[Geocercas._editShapeID]._instance);
			Geocercas.ShapeInstance[Geocercas._editShapeID].Load();
		}else{
			Geocercas._polyGeoSquareProps.color = hexToRgbA($("#color_polySquare").val(),0.3);
			Geocercas._polyGeoSquareProps.colorHex = $("#color_polySquare").val();
			$(".centerMarker .pin").css("background",$(this).val());
			deleteSquareInstance();
			repaintSquareInstance();
		}
	});
	
	$("#color_GeoRuta").change(function(){
		if(Geocercas._editing == true){
			Geocercas.ShapeInstance[Geocercas._editShapeID]._color = $("#color_GeoRuta").val();
			Geocercas.ShapeInstance[Geocercas._editShapeID].Reload();
		}else{
			deleteRouteInstance();
			repaintRouteInstance();
		}
	});
	
	$("#color_polyComplex").change(function(){
		if(Geocercas._editing == true){
			Geocercas.ShapeInstance[Geocercas._editShapeID]._color = $("#color_polyComplex").val();
			Geocercas.objectList.removeObject(Geocercas.ShapeInstance[Geocercas._editShapeID]._instance);
			Geocercas.ShapeInstance[Geocercas._editShapeID].Load();
		}else{
			deleteComplexInstance();
			repaintComplexInstance();
		}
	});
	$("#trackAddTagList-historial,#trackAddTagList-rastreoContinuo").change(function(){
		var val = $(this).children("option:selected").val();
		var vista = $(this).children("option:selected").attr("vista");
		
		$("#"+vista+" .trackAddTagList label[_id="+val+"]").trigger("click");
		formSelect();
	});
	//VEHICLE TOOLS
	$("#vehiculos select.orderVehicles").change(function(){
		Cars.FillCarList($(this).val(),$("#vehiculos select.orderType").val());
	});
	$("#vehiculos select.orderType").change(function(){
		Cars.FillCarList($("#vehiculos select.orderVehicles").val(),$(this).val());
	});
	$("#vehiculos select.itemsPage").change(function(){
		Cars.FillCarList($("#vehiculos select.orderVehicles").val(),$("#vehiculos select.orderType").val());
	});
	//ORDER TOOLS
	$("#ordenes select.orderOrders").change(function(){
		Orders.FillOrders($(this).val(),$("#ordenes select.orderType").val());
	});
	$("#ordenes select.orderType").change(function(){
		Orders.FillOrders($("#ordenes select.orderVehicles").val(),$(this).val());
	});
	$("#ordenes select.itemsPage").change(function(){
		Orders.FillOrders($("#ordenes select.orderVehicles").val(),$("#ordenes select.orderType").val());
	});
	$(document).click(function(e) {
		if($(e.target).hasClass('toolbox') == false && $(e.target).hasClass("tool") == false && $(e.target).hasClass("fa-ellipsis-v") == false) {
			try{
				Orders.CloseToolbox();
			}catch(e){}
		}
	});
	$(document).on("click","#sidebar,.top,.mainTop,#map,#timeControls,canvas",function(e) {
		if($(".timeOptions").is(":visible") ) {
			try{
				Track.TrackingOptions();
			}catch(e){}
		}
	});
	$('.timepickerIni').timepicker({
		default: $(".timepickerIni").val(), // Set default time: 'now', '1:30AM', '16:30'
		fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
		twelvehour: true, // Use AM/PM or 24-hour format
		donetext: 'OK', // text for done-button
		cleartext: '', // text for clear-button
		canceltext: '<es>Cancelar</es><en>Cancel</en>', // Text for cancel-button
		autoclose: false, // automatic close timepicker
		ampmclickable: true, // make AM PM clickable
		aftershow: function(){} //Function for after opening timepicker
	});
	$('.timepickerFin').timepicker({
		default: $(".timepickerFin").val(), // Set default time: 'now', '1:30AM', '16:30'
		fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
		twelvehour: true, // Use AM/PM or 24-hour format
		donetext: 'OK', // text for done-button
		cleartext: '', // text for clear-button
		canceltext: '<es>Cancelar</es><en>Cancel</en>', // Text for cancel-button
		autoclose: false, // automatic close timepicker
		ampmclickable: true, // make AM PM clickable
		aftershow: function(){} //Function for after opening timepicker
	});
	$("#color_Poligonal").change(function(){
		if(Geocercas._editing == true){
			Geocercas.ShapeInstance[Geocercas._editShapeID].color = $("#color_Poligonal").val();
			Geocercas.polyGroup.removeObject(Geocercas.ShapeInstance[Geocercas._editShapeID].instance);
			Geocercas.ShapeInstance[Geocercas._editShapeID].Load();
		}else{
			Geocercas.polyGroup.removeAll();
			try{
				Geos.LoadObjects();
			}catch(e){}
		}
	});
	
	$(".limitCar input").change(function(){
		if($(this).is(":checked")){
			$(".speedLimit").removeClass("hidden");
		}else{
			$(".speedLimit").addClass("hidden");
		}
	});

	$(document).on('blur', 'input[type="text"], input[type="password"]',function(){
		window.onFocus = false;
		setTimeout(function(){ 
			if(window.onFocus == false){
				$(".forgot").show();
			}
		},1000);
	});
	$(document).on('focus', 'input[type="text"], input[type="password"]',function(){
		window.onFocus = true;
		$(".forgot").hide();
	});
	$("#loginUser").keyup(function(e) {
		if(e.keyCode == 13){
			$("#loginPass").focus();
		}
	});
	$("#manual_qr").keyup(function(e) {
		if(e.keyCode == 13){
			TE.ManualQRConf();
		}
	});
	$("#loginPass").keyup(function(e) {
		if(e.keyCode == 13) {
			IniciarSesion();
		}
	});
	$(".langSwitch input").change(function(){
		if($(".langSwitch input").is(":checked")){
			changeLang("en");
		}else{
			changeLang("es");
		}
	});
	
	$("#nombrePunto").keyup(function(e){
		$(".centerLabel .label").html($(this).val());
	});
	
	
	$(document).on("click",".fade",function() {
		MenuCtrl.Hide();
		if($(".showcase").is(":visible")){
			$(".showcase").fadeOut("fast");
			$(".mainTop .fade,#map .fade").hide();
			try{
		        clearTimeout(window.showcaseTimeout);
		    }catch(e){}
		}
	});
	
	$(document).on("change","#timeMetric", function(){
		console.log("Time Metric: "+$(this).val());
		changeTimeMetric($(this).val());
	});
	$(document).on("change","#FuelDayMetric", function(){
		console.log("Fuel Day Metric: "+$(this).val());
		Fuel.ChangeDayMetric($(this).val());
	});


	if(iOS == true){
		$('.scrollFix').css("pointer-events","none");

		$('body').on('touchstart', function(e) {
			$('.scrollFix').css("pointer-events","auto");
		});
		$('body').on('touchmove', function(e) {
			$('.scrollFix').css("pointer-events","none");
		});
		$('body').on('touchend', function(e) {
			setTimeout(function() { 
				$('.scrollFix').css("pointer-events", "none");
			},100);
		});
	}
	$(document).on("click",".emptySearch",function(){
		$(".search").val('');
		try{
			window.carInfoList.search();
		}catch(e){}
		try{
			window.carEventsList.search();
		}catch(e){}
		try {
			window.pushEvList.search();
		}catch(e){}
		try {
			window.pushCarList.search();
		}catch(e){}
		try{
			window.motorOffList.search();
		}catch(e){}
		try{
			window.autoRCList.search();
		}catch(e){}
	});
	$(document).on("change","#group-filter",function(){
		if($(this).val() == "all") {
			$(".list.checkpointList li").show();
		}else{
			$(".list.checkpointList li").hide();
			$(".list.checkpointList li.group-"+$(this).val()).show();
		}
	});	
}

$(window).resize(function(){
	views();
	try{
		TE.routeMapPadding();
	}catch(e){}
});
function TEApp() {
	if(isTEApp){
		changeLang("es");
		version = teVersion;
		$(".langSwitch").remove();
		$("label[for='loginUser']").html('Nombre de Usuario');
		$("#loginPage .loginLogo").addClass("teapp");
		$("body").removeClass("transporte flotillas").addClass("light");
        $(".user-view .servicelogo").prop("src","img/teapp/logo_transporte.png"); 
        $("#favicon").prop("href","img/teapp/favicon.ico");
        $(".theme").attr("href","css/teapp.css");
        $(".onlyRC").addClass("hidden");
        $("title").html("Transporte Empresarial App");
        $(".theme-color-meta").attr("content","#4a62bc");
	}else{
		$(".isTEApp").remove();
		$("title").html("ZeekGPS App");
		$(".actioncircle").remove();
		$("#loginPage .loginLogo").addClass("zeeklogo");
	}
}
function declareSidenav() {
	var sidenavOptions = {
		draggable: 	false,
		onOpenStart: function() {
			setMenuDraggable(true);
			resetMenu(true);
		},
		onOpenEnd: function() {
		},
		onCloseStart: function() {
			setMenuDraggable(false);
			resetMenu(true);
		},
		onCloseEnd: function() {
			resetMenu(true);
		}
	};
	
	var elems = document.querySelectorAll('.sidenav');
    window.sidenav = M.Sidenav.init(elems, sidenavOptions);
}
function setMenuDraggable(status) {
	window.sidenav[0].options.draggable = status;
}
function verCambios() {
	window.eggCounter++;
		if(window.eggCounter >= 5){
            if(isWL == false) {
				alerta("Agradecimientos","I+D\nSael, Juan, Giovanni, Ramiro, Nivardo, Pavel y Héctor.\n\nArgus Team\nClaudia, Denisse, Edgar, Alan, Fernando, Sheila y Manuel.\n\nIconografía y Gráficos\nRenata");
			}
			$(".debug").removeClass('hidden');
		}
		displayChangelog();
}
/* Mueve la vista hasta la cima de la misma */
function goTop(view){
	$(view).scrollTop(0);
} 
/* Cambia la metrica del tiempo para servicios que necesiten intervalos */
function changeTimeMetric(time) {
	if(!!window.cordova == false) { //PC
		var format = "YYYY-MM-DD[ ]";
	}else{
		var format = "YYYY-MM-DD[T]";
	}
	switch(time){
		case "2h":
			var now = new moment();
			var past = moment().subtract(2,'hour');
			$("#histFin").val(now.format(format+"HH:mm"));
			$("#histIni").val(past.format(format+"HH:mm"));
		break;
		case "8h":
			var now = new moment();
			var past = moment().subtract(8,'hour');
			$("#histFin").val(now.format(format+"HH:mm"));
			$("#histIni").val(past.format(format+"HH:mm"));
		break;
		case "12h":
			var now = new moment();
			var past = moment().subtract(12,'hour');
			$("#histFin").val(now.format(format+"HH:mm"));
			$("#histIni").val(past.format(format+"HH:mm"));
		break;
		case "td":
			var now = new moment();
			$("#histFin").val(now.format(format+"HH:mm"));
			$("#histIni").val(now.format(format)+"00:00");
		break;
		case "yd":
			var now = new moment();
			var past = moment().subtract(1,'day');
			$("#histFin").val(past.format(format)+"23:59");
			$("#histIni").val(past.format(format)+"00:00");
		break;
		case "week":
			var now = new moment();
			$("#histFin").val(now.format(format+"HH:mm"));
			var past = now.day(1);
			$("#histIni").val(past.format(format)+"00:00");
		break;
		case "month":
			var now = new moment();
			$("#histFin").val(now.format(format+"HH:mm"));
			var past = now.date(1);
			$("#histIni").val(past.format(format)+"00:00");
		break;
	}
}

/* Cambia el lenguaje de la app y carga el archivo CSS que bloquea el
lenguaje anterior y habilita el seleccionado <es></es><en></en> */
function changeLang(lang){
	$(".lang").attr("href","css/"+lang+".css");
	window.idioma = lang;
	setKey("zeekgpsLang",lang);
	$("input.hasPlaceholder").each(function(){
		$(this).attr("placeholder",$(this).attr(lang));
	});
	if(lang == "en"){
		$(".switchy.langSwitch input").attr("checked",true);
		moment.locale("en");
		window.culture = "en-US";
	}else{
		$(".switchy.langSwitch input").attr("checked",false);
		moment.locale("es");
		window.culture = "es-MX";
	}
	dozeTexts();
	var built = new moment(buildDate);
	$("#ayuda .version").html(`<span class="version_string">${version}</span><br/>${built.format("[<es>]DD [de] MMMM [de] YYYY [-] HH:mm[</es>][<en>]MMMM Do YYYY [-] HH:mm[</en>]")}`);
	$("#langSelect").val(lang);
	console.log("New Language:",lang);
}
/* Regresa la cadena (tag) del lenguaje actual */
function lang(tag){
	return langs[window.idioma][tag];
}
/* */
function openSidebar(section){
	Geos.OpenSidebar(section);
}
/* Comienza el proceso de inicio de sesion */
function IniciarSesion(){
	var user = trim($("#loginUser").val());
	var pass = trim($("#loginPass").val());
	SesionEngine.Sesion(user,pass);
}
function loadRemoteCSS(os) {
	$.getStylesheet(`https://auto.zeekgps.com/zeek/remote/zeekgps_${os}.css?`+new Date().getTime(),`${os}CSS`).done(
		function() {
		console.log( `Remote "${os}" CSS file OK` );
	})
	.fail(() =>{
		setTimeout(()=>{
			loadRemoteCSS(os);
		},3000);
	});
}
(function($) {
  $.getStylesheet = function (href,className) {
    var $d = $.Deferred();
    var $link = $('<link/>', {
       rel: 'stylesheet',
       type: 'text/css',
       href: href,
       class: className
    }).appendTo('head');
    $d.resolve($link);
    return $d.promise();
  };
})(jQuery);
function hideActionCircle() {
	$(".actioncircle").addClass("hidden");
}
function resetMenu(justMenuThings = false){

	try{
        clearTimeout(window.showcaseTimeout);
    }catch(e){}
	$(".showcase").fadeOut("fast");
	$(".mainTop .fade,#map .fade").hide();
	
	setTimeout(function(){
		$("a.collapsible-header i.material-icons").html("arrow_right");
		$("ul.collapsible-accordion li .collapsible-body").hide();
		$("ul.collapsible-accordion li a").removeClass("active");
		$("ul.collapsible-accordion li").removeClass("active");
		$("#sidenav").scrollTop(0);
	},10);
	if(justMenuThings == false){
		Closer.CloseAll();
	}else{
		try{
			//sidenav.close();
		}catch(e){}
	}
}
function pushAccordion(elem) {
	views();
	if(!$(`.pushEvents .${elem} .inputGroup .content`).is(":visible") && $(window).width() <= 600) {
		$(".pushEvents .inputGroup .content").slideUp();
		$(".pushEvents .inputGroup h6 i").removeClass("fa-angle-down").addClass("fa-angle-right");
		$(`.pushEvents .${elem} .inputGroup .content`).slideDown();
		$(`.pushEvents .${elem} .inputGroup h6 i`).addClass("fa-angle-down").removeClass("fa-angle-right");
	}
}
function views(){
	isLandscape = $(window).width() > $(window).height();
	if($(window).width() > 992 || $(window).height() > 992){
		$(".fullApp").removeClass("hidden");
	}else{
		$(".fullApp").addClass("hidden");
		$("body,#sidenav,#main").removeClass("sidenav-open sidenav-fixed");
		$("#sidenav").attr("style","");
	}

	if($(window).width() > $("html").height()) { //LANDSCAPE
		$("#push .inputGroup .content").height($("html").height()-175);
		if(Config._fullApp && $("#sidebar").is(":visible") == false){
			try{
				sidenav.close();
			}catch(e){}
			try{
				if($(window).width() > 992 && SesionEngine._isLogged){
					$("#sidenav,#main,body").addClass("sidenav-open");
					$("#sidenav").addClass("sidenav-fixed");
				}else{
					$("#sidenav,#main,body").removeClass("sidenav-open sidenav-fixed");
				}
			}catch(e){}
		}
	}else{ //PORTRAIT
		$("#push .inputGroup .content.single").height(($("html").height()-146));
		if($(window).width() <= 600) {
			$("#push .inputGroup .content.double").height(($("html").height()-190));
		}else{
			$("#push .inputGroup .content.double").height(($("html").height()-195)/2);
		}
		$("#sidenav").removeClass("sidenav-open sidenav-fixed");
		$("body").removeClass("sidenav-open");
	}
	$(".menu ul").css({"height":$("html").height()-100});
	sidebarFixer();
	$("#map.full,#sidebar.right,.centerMarker.full,.centerAim.full,.centerLabel.full").css("height", $("html").height()-50);
	//$("#map.full,.centerMarker.full").css("height", $("html").height()-50);
	$("#map.topView,.centerMarker.topView,.centerAim.topView,.centerLabel.topView").css("height",$("html").height()*0.55);
	$("#map.left,.centerMarker.left,.centerAim.left,.centerLabel.left").css("height", $("html").height()-50);
	$("#sidebar.bottom").height( $("html").height()*0.45-50 );
	$(".waitSidebar").css({"height":$("#sidebar").height(),"width":$("#sidebar").outerWidth()});
	
	if($(window).width() <= 865 || $("html").height() <= 690) {
		$(".dynamicBox").addClass("subVista").css({"max-height":"100%","height":"calc(100% - 50px)"});	
	}else{
		$(".dynamicBox").removeClass("subVista").css({"max-height":"calc(100% - 200px)","height":"auto"});	
	}
	$(document).on("change","#trackTimeSelect",function(){
		Track.StartTrackingTimer(parseInt($(this).val()));
	});
	map.getViewPort().resize(); 
	try{
		TE.AdjustPanicPill();
	}catch(e){}
}
function reportSidebarScroll(e) {
	try{
		eventSlider.noUiSlider.set(e.target.scrollTop);
	}catch(err){}
}
function sidebarFixer() {
	
	if(window.onFocus == false){
		//LANDSCAPE
		if($(window).width() > $("html").height()){
			$("#sidebar").removeClass("bottom").addClass("right");
			if($("#sidebar").is(":visible")){
				$("#map,.centerMarker,.centerAim,.centerLabel").removeClass("topView full").addClass("left");
			}else{
				$("#map,.centerMarker,.centerAim,.centerLabel").addClass("full").removeClass("left topView");
			}
		}
		//PORTRAIT
		if($("html").height() >= $(window).width()){
			$("#sidebar").removeClass("right").addClass("bottom");
			if($("#sidebar").is(":visible")){
				$("#map,.centerMarker,.centerAim,.centerLabel").removeClass("left full").addClass("topView");
			}else{
				$("#map,.centerMarker,.centerAim,.centerLabel").addClass("full").removeClass("left topView");
			}
		}
	}
}
function wait(msg,subMsg = "",showButton = false){
	if(showButton) {
		$("#wait .button").removeClass("hidden");
	}else{
		$("#wait .button").addClass("hidden");
	}
	$("#wait").removeClass("hidden");
	$("#wait .waitMsg").html(msg);
	$("#wait .waitSubMsg").html(subMsg);
}
function closeWait(){
	$("#wait").addClass("hidden");
}
//Elimina los espacios en blanco al inicio y final de una cadena.
function trim(s) {
	var l=0; var r=s.length -1;
	while(l < s.length && s[l] == ' ')
	{	l++; }
	while(r > l && s[r] == ' ')
	{	r-=1;	}
	return s.substring(l, r+1);
}

function waitSidebar(msg){
	//console.warn("Wait on Sidebar: "+msg);
	$(".waitSidebar").css({"height":$("#sidebar").height(),"width":$("#sidebar").outerWidth()}).removeClass("hidden");
	$(".waitSidebar .waitMsg").html(msg);
}
function closeWaitSidebar(){
	$(".waitSidebar").addClass("hidden");
}

function timeVal(time){
	var elements = time.split(":");
	var hora = parseInt(elements[0]);
	var mins = parseInt(elements[1].substr(0,2));
	var stage = elements[1].substr(2,4);
	
	if(stage == "PM"){
		if(hora < 12) {
			hora+=12;
		}
	}
	if(stage == "AM"){
		if(hora == 12){
			hora = 0;
		}
	}
	return parseInt(hora+""+mins);
}
function fixTime(time){
	var elements = time.split(":");
	var hora = parseInt(elements[0]);
	var mins = parseInt(elements[1].substr(0,2));
	var stage = elements[1].substr(2,4);
	
	if(stage == "PM"){
		if(hora < 12) {
			hora+=12;
		}
	}
	if(stage == "AM"){
		if(hora == 12){
			hora = 0;
		}
	}
	if(hora == 24){
		hora = 0;
	}
	return fix(hora)+":"+fix(mins)+":00";
}
function convertPoints(coords,zeekCoords){
	var puntos = new Array();
	
	if(zeekCoords == true){
		for(var i=0;i<coords.length;i++){
			if(coords[i].Latitud == undefined) {
				puntos.push({
					"Latitud": coords[i].lat,
					"Longitud":coords[i].lng
				});

			}else{
				puntos.push({
					"Latitud": coords[i].Latitud,
					"Longitud":coords[i].Longitud
				});
			}
		}
	}else{
		for(var i=0;i<coords.length;i++){
			puntos.push({
				"Latitud": coords[i][0],
				"Longitud":coords[i][1]
			});
		}
	}
	return JSON.stringify(puntos);
}
function hereInit(){
	window.mapPlatform = new H.service.Platform({
		'app_id': 	app_id,
		'app_code': app_code,
		useCIT: 	false,
		useHTTPS: 	true
	});

	var pixelRatio = window.DPR || 1;
	var defaultLayers = mapPlatform.createDefaultLayers({
		tileSize: 256,
  		ppi: 320/*devicePixelRatio === 1 ? 250: 320*/
	});
	//Step 2: initialize a map  - not specificing a location will give a whole world view.
	map = new H.Map(document.getElementById('map'),
	  defaultLayers.normal.map, {
		center: {lat: 21.448359390290534, lng: -101.71065691357435},
		zoom: 4,
		pixelRatio: 2
	  });

	//Step 3: make the map interactive
	// MapEvents enables the event system
	// Behavior implements default interactions for pan/zoom (also on mobile touch environments)
	window.mapbehavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

	// Create the default UI components
	window.mapui = H.ui.UI.createDefault(map, defaultLayers,(window.idioma == "en" ? "en-US" : "es-ES"));
	window.mapui.getControl('mapsettings').setVisibility(true);
	window.mapui.getControl('zoom').setVisibility(false);
	if(isTEApp){
		window.mapui.getControl('mapsettings').setAlignment('top-left');
	}else{
		window.mapui.getControl('mapsettings').setAlignment('top-right');
	}

	// Get an instance of the geocoding service:
	window.geocoder = 	mapPlatform.getGeocodingService();
	window.places = 	new H.places.Around(mapPlatform.getPlacesService());
	// Now use the map as required...
	
	console.warn("Tip of the day: Learn to express yourself.");
	defineClasses();
	TEApp();
	if(PushNotifs == null){
		PushNotifs = new Push();
	}
	//Geocercas.ShapeList = loadSet("shapeList");
	//Geos.LoadShapes();
	
	if(mapInit == false){
		mapInit = true;
		
		map.addEventListener("mapviewchange", function(){
			
			if($("#sidebar .checkpoints").is(":visible") && window.focusingPoint == false){
				$(".centerMarker").hide();
				$(".centerLabel").hide();
			}
			if(!$("#sidebar .checkpoints").is(":visible") ){
				$(".centerLabel").hide();
			}
			window.focusingPoint = false;
		}, false);
		map.addEventListener("pointerdown", function(){
			if($(".timeOptions").is(":visible") ) {
				try{
					Track.TrackingOptions();
				}catch(e){}
			}
		}, false);

		map.addEventListener("mapviewchangeend", function(){
			
			if($(".centerMarker").is(":visible")){
				$(".centerLabel").show();
			}
			
		},false);
	}
	setTimeout(()=>{
		reloadStatusBar();
	},500);
	map.addEventListener("mapviewchangeend", zoomLabelVisible,false);
}
function zoomLabelVisible() {
	try{
		if(map.getZoom() >= 15) {
			Tracking._markerInstance.forEach((e)=>{
				e.label.setVisibility(true);
			});
		}else{
			Tracking._markerInstance.forEach((e)=>{
				e.label.setVisibility(false);
			});
		}
	}catch(e){}
}
function reloadStatusBar() {
	try{
		if(isTEApp){
        	StatusBar.backgroundColorByHexString('#4a62bc');
        }
		StatusBar.overlaysWebView(false);
		StatusBar.styleBlackTranslucent();
		StatusBar.styleLightContent();
		StatusBar.show();
	}catch(e){}
}
function defineClasses() {
	if(Support == null) 	{	Support = 		new SupportClass();}
	if(PushNotifs == null) 	{ 	PushNotifs = 	new Push();}
	if(TE == null) 	{ 			TE =			new TEAppClass();}
	if(MenuCtrl == null) 	{ 	MenuCtrl =		new Menu();}
	if(MapEngine == null) 	{  	MapEngine =		new MapControls();}
	if(SesionEngine == null){ 	SesionEngine = 	new Sesion();}
	if(Geos == null) 		{  	Geos =			new Geocercas();}
	if(System == null) 		{	System =		new Sistema();}
	if(Checkpoints == null) { 	Checkpoints =	new PuntoGrupo();}
	if(Cars == null) 		{	Cars =			new Vehiculos();}
	if(Track == null) 		{ 	Track =			new Tracking();}
	if(Config == null) 		{	Config =		new Settings();}
	if(Orders == null) 		{ 	Orders =		new Ordenes();}
	if(Interface == null) 	{	Interface =		new Interfaces();}
	if(Closer == null) 		{	Closer = 		new CloserClass();}
	if(Fuel == null) 		{	Fuel = 			new FuelClass();}
	if(Share == null) 		{	Share =			new ShareClass();
		loadBackDoor();
		Config.checkDoze();
		/*setInterval(function() {
			try{
				SesionEngine.changeStatusBar();
			}catch(e){}
		},2000);*/
		//MenuCtrl.Construct();
	}
}
/* Reinicia la obtencion de posicion y activa el timer */
function watchCurrentPosition(forceWatch = false) {
	if(TE.panicActive == false || forceWatch == true) {
		stopWatchingPosition();
	    if(window.geoPermisoOtorgado == false){
	    	getCurrentPosition();
	    }else{
	        positionFunction();
	    }
	}else{
		TE.PanicButton();
	}
}
/* Detiene la observacion de la posicion del usuario */
function stopWatchingPosition() {
	try{
		//clearInterval(window.positionTimer);
        navigator.geolocation.clearWatch(window.positionInterval);
	}catch(e){}
}
function getCurrentPosition() {
    navigator.geolocation.getCurrentPosition((position)=>{
    	window.geoPermisoOtorgado = true;
	    //GUARDAMOS LAS COORDENADAS
	    window.myPosition = position.coords;
    	watchCurrentPosition(true);
    },(err)=>{
	    console.error("Geolocation error",err);
	    if(err.code == 1){ //PERMISO DENEGADO
	        stopWatchingPosition();
	        if(window.solicitarPosicion) {
	            window.solicitarPosicion = false;
	            var plat = "Web";
	            if(!!window.cordova == true){
	                if (window._plataforma == "ios") {
	                    plat = "iOS";
	                }else{
	                    plat = "Android";
	                }
	            }
	            alerta("Permiso de Ubicación","La aplicación no cuenta con permiso de Ubicación, por favor actívelo en la Configuración de su dispositivo "+plat+" para poder enviar su ubicación al centro de seguridad.");
	        }
	        closeWait();
	    }else{
	    	window.solicitarPosicion = true;
	    	setTimeout(()=>{
	            getCurrentPosition();
	        },1000);
	    }
    },
    {enableHighAccuracy: true,timeout: 10000,maximumAge: 5000,desiredAccuracy: 10 });
}
/* Obtiene la posicion del usuario */
function positionFunction() {
	window.positionInterval = navigator.geolocation.watchPosition((position)=>{
		window.geoPermisoOtorgado = true;
	    //GUARDAMOS LAS COORDENADAS
	    window.myPosition = position.coords;
	},(err)=>{
		console.error("watchPosition",err);
	},
    {enableHighAccuracy: true,timeout: 10000,maximumAge: 5000,desiredAccuracy: 10, frequency: 5000 });
}
function showPosition(position) {
    map.setCenter({lat:position.coords.latitude, lng: position.coords.longitude});
  	map.setZoom(12);
}
function fix(num) {
	return (num <=9 ? "0"+num : num);	
}
function getDistance(lat1,lon1,lat2,lon2) {
	var R = 6371000; // metros, radio de la Tierra
	var l1 = toRad(lat1);
	var l2 = toRad(lat2);
	var delta = toRad(lat2-lat1);
	var lambda = toRad(lon2-lon1);
	
	var a = Math.sin(delta/2) * Math.sin(delta/2) +
			Math.cos(l1) * Math.cos(l2) *
			Math.sin(lambda/2) * Math.sin(lambda/2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	
	return R * c;
}

function na(txt){
	if(txt == undefined){
		return "N/A";
	}else{
		try{
			return (trim(txt) == "") ? "N/A" : txt;
		}catch(e){
			return txt;
		}
	}
}
function empty(txt){
	if(txt == undefined){
		return "";
	}else{
		try{
			return (trim(txt) == "") ? "" : txt;
		}catch(e){
			return txt;
		}
	}
}
//PROTOTIPO PARA PASAR NUMERO A RADIANES
if (Number.prototype.toRadians === undefined) {
    Number.prototype.toRadians = function() { return this * Math.PI / 180; };
}
function toRad(degrees) {
	return degrees * 0.017453292519943295;
}
this.clone = function () { return jQuery.extend(true, {}, this); }
//Regresa el indice de un valor en un atributo dentro de un objeto/arreglo.
function objectIndex(array, attr, value) {
	try{
		var res = -1;
	    for(var i = 0; i < array.length; i += 1) {
	        if(array[i][attr] === value) {
	            res = i;
	        }
	    }
	}catch(e){}
	return res;
}
//Funcion para calcular cuanto se le tiene que sumar a una coordenada
	//Para que sea un metro de diferencia
function calcMetros(latitud, longitud) {
	//var original = new nokia.maps.geo.Coordinate(latitud, longitud);
	//var calcular = new nokia.maps.geo.Coordinate(latitud, longitud);
	var original = { lat: latitud, lng: longitud };
	var calcular = { lat: latitud, lng: longitud };
	var mLatitud = 0, mLongitud = 0;
	var listo = false;
	while (!listo) {
		//if (parseFloat(original.distance(calcular)) < 0.99) {
		if (parseFloat(getDistance(original.lat, original.lng, calcular.lat, calcular.lng)) < 0.99) {
			mLatitud += .000001;
			//calcular = new nokia.maps.geo.Coordinate(latitud + mLatitud, longitud);
			calcular = { lat: latitud + mLatitud, lng: longitud };
		}
		else {
			listo = true;
		}
	}
	listo = false;
	//calcular = new nokia.maps.geo.Coordinate(latitud, longitud);
	calcular = { lat: latitud, lng: longitud };
	while (!listo) {
		//if (parseFloat(original.distance(calcular)) < 0.99) {
		if (parseFloat(getDistance(original.lat, original.lng, calcular.lat, calcular.lng)) < 0.99) {
			mLongitud += .000001;
			//calcular = new nokia.maps.geo.Coordinate(latitud, longitud + mLongitud);
			calcular = { lat: latitud, lng: longitud + mLongitud };
		}
		else {
			listo = true;
		}
	}
	var coordenada = [mLatitud, parseFloat(mLongitud.toFixed(6))];
	return coordenada;
}

function addMarkersToMap() {
	var mapC = map.getCenter();
	markers.push({
		'marker':new H.map.Marker({lat:mapC.lat, lng:mapC.lng})
	});
	map.addObject(markers[markers.length-1].marker);
}
function deletePolyInstance() {
	try{
		map.removeObject(Geocercas.geoPolyMarker);
		//$("#slider_polyRadius").attr("disabled",true);
	}catch(e){}
}

function repaintPolyInstance() {
	if(Geocercas._repainting == false){
		Geocercas._repainting = true;
		try{
			map.removeObject(Geocercas.geoPolyMarker);
		}catch(e){}
		var coordenada = map.getCenter();
		Geos.PrintCirclePoly(coordenada);
		//$("#slider_polyRadius").attr("disabled",false);
	}
}

function deleteSquareInstance() {
	try{
		map.removeObject(Geocercas.geoSquareMarker);
		//$("#slider_polySquare").attr("disabled",true);
	}catch(e){}
}

function repaintSquareInstance() {
	if(Geocercas._repainting == false){
		Geocercas._repainting = true;
		try{
			map.removeObject(Geocercas.geoSquareMarker);
		}catch(e){}
		try{
			var coordenada = map.getCenter();
			Geos.PrintSquarePoly(coordenada);
			//$("#slider_polySquare").attr("disabled",false);
		}catch(e){}
	}
}
function deleteComplexInstance() {
	try{
		Geocercas.complexGroup.removeAll();
	}catch(e){}
}

function repaintComplexInstance() {
	try{
		Geos.UpdateComplexMaker();
	}catch(e){}
}
function deleteRouteInstance() {
	try{
		Geocercas.routeGroup.removeAll();
	}catch(e){}
}

function repaintRouteInstance() {
	try{
		Geos.UpdateRouteMaker();
	}catch(e){}
}

function hexToRgbA(hex,alpha = 1){
	//console.log("Color",hex);
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+','+alpha+')';
    }
    throw new Error('Bad Hex');
}

function addMarkerToMap(evt){
	// Log 'tap' and 'mouse' events:
	//addMarkersToMap(map)
	var coord = map.screenToGeo(evt.currentPointer.viewportX,
	evt.currentPointer.viewportY);
	/*if(Geocercas._CreatingRoute == false && Geocercas._CreatingComplex == false && Geocercas._editing == false){
		Geos.AddMarkerWithCoords(coord.lat,coord.lng);
	}*/
	if(Geocercas._editing == true){
		if(Geocercas.ShapeInstance[Geocercas._editShapeID].tipo == "GeoRuta" && Geocercas._inputMethod == 0){
			Geos.AddRoutePoint(coord.lat,coord.lng);
		}
		if(Geocercas.ShapeInstance[Geocercas._editShapeID].tipo == "Poligonal" && Geocercas._inputMethod == 0){
			Geos.AddComplexPoint(coord.lat,coord.lng);
		}
	}
	if(Geocercas._CreatingRoute == true){
		if(Geocercas._inputMethod == 0){
			Geos.AddRoutePoint(coord.lat,coord.lng);
		}
	}
	if(Geocercas._CreatingComplex == true){
		if(Geocercas._inputMethod == 0){
			Geos.AddComplexPoint(coord.lat,coord.lng);
		}
	}
}
function keyExists(key) {
	return (getKey(key) != null);
}
//GUARDA UNA LLAVE,VALOR
function setKey(key,val) {
	localStorage.setItem(key,val);
}
function clearKey(key) {
	localStorage.removeItem(key);	
}
//GUARDA UN OBJETO (JSON)
function saveSet(key,val) {
	localStorage.setItem(key,JSON.stringify(val));
}
//OBTIENE EL VALOR DE UNA LLAVE
function getKey(key) {
	return localStorage.getItem(key);	
}
//OBTIENE EL VALOR (JSON) DE UNA LLAVE SET
function loadSet(key,trueVal = false) {
	try {
		var retVal = JSON.parse(localStorage.getItem(key));
		return (retVal == null && trueVal == false) ? new Array() : retVal;	
	}catch(e) {
		return new Array();	
	}
}
//Dependiendo de la plataforma devuelve la direccion y extension del archivo de audio.
function getMediaURL(s) {
    if(device.platform.toLowerCase() === "android") return "/android_asset/www/sounds/" + s +".ogg";
	if(device.platform.toLowerCase() === "ios" || device.manufacturer.toLowerCase() == "apple") return "sounds/" + s +".wav";
    return s;
}

//FUNCION QUE REPRODUCE UN SONIDO (CORDOVA+WEB BROWSER)
function playSound(file) {
	try{
		//REPRODUCIR EN CORDOVA (DISPOSITIVO)
		if(device.platform.toLowerCase() === "android"){
			
			sounds[file].play();
		}else{
			var my_media = new Media(getMediaURL(file),
				// success callback
				function () {
					console.log("playAudio():Audio Success");
				},
				// error callback
				function (err) {
					console.log("playAudio():Audio Error: " + err);
					alert("playAudio():Audio Error: " + JSON.stringify(err,null, 4));
				}
			);
			// Play audio
			my_media.play();
		}
	}catch(e) {
		//alert(e);
		//REPRODUCIR EN BROWSER	
		if (!sonidos[file]) {
			var audio = new Audio("sounds/"+file+".ogg");
			//audio.loop = "false";
			audio.volume = 1;
			audio.preload = "auto";
			sonidos[file] = audio;
			sonidos[file].play();
		} else {
			if($(".sonidos").is(":visible") || $("#eventos").is(":visible")) {
				sonidos[file].pause();
				sonidos[file].currentTime = 0;
				sonidos[file].play();
			}else{
				sonidos[file].play();
			}
		}
	}
}
//WRAPPER PARA LAS NOTIFICACIONES RAPIDAS (TOASTS)
function toasty(message,tipo,fn) {
	fn = typeof fn !== 'undefined' ? fn : '';
	/*try{
		window.plugins.toast.showLongBottom(message);
	}catch(e) {*/
		M.Toast.dismissAll();
		M.toast({html: message});
		//SI NO SE PUEDE MOSTRAR UN TOAST NATIVO
		//USAMOS UNO IMPLEMENTADO
		/*toastr.options = {
		  "closeButton": true,
		  "debug": false,
		  "newestOnTop": true,
		  "progressBar": false,
		  "positionClass": "toast-top-center",
		  "preventDuplicates": true,
		  "onclick": null,
		  "showDuration": "300",
		  "hideDuration": "1000",
		  "timeOut": "4000",
		  "extendedTimeOut": "1000",
		  "showEasing": "linear",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}
		toastr.remove();
		if(fn != '')
			toastr.options.onclick = function() {eval(fn);};
		switch(tipo) {
			case "error":
				toastr.error(message);
			break;
			case "warn":
			case "warning":
				toastr.warning(message);
			break;
			case "success":
				toastr.success(message);
			break;
			default:
				toastr.info(message);
			break;
		}*/
	//}
}
function removeTags(str) {
    if ((str===null) || (str===''))
        return '';
    else
        str = str.toString();
          
    // Regular expression to identify HTML tags in
    // the input string. Replacing the identified
    // HTML tag with a null string.
    return str.replace( /(<([^>]+)>)/ig, '');
}
//WRAPPER PARA EL DIALOGO DE ALERTA NATIVO
function alerta(titulo,mensaje,label = "Aceptar",callback = null) {
	var _tema = 'light';//(Config.getDarkMode() ? 'dark' : 'light');
	window.alertCallbackFn = callback;
	try{
		if(_plataforma == "ios"){
			mensaje = removeTags(mensaje);
		}
		navigator.notification.alert(mensaje, alertCallback, titulo, label,_tema);
	}catch(e){
		//alert(mensaje);
		$("#alertModal .alert-title").html(titulo);
		$("#alertModal .alert-body").html(mensaje);
		$("#alertModal").modal({'dismissible':false});
		$('#alertModal').modal('open');
		if(callback != null){
			$("#alertModal .modal-close").attr("onclick",`setTimeout("${callback.toString()}",500)`);
		}
	}
}
function alertCallback() {
	if(alertCallbackFn != null) {
		eval(alertCallbackFn);
	}
}
/* [PROMPT] Solicita una cadena al usuario, la guarda en la variable global window.promptValue */
function solicitar(titulo,solicitud,textoOk,textoCancelar,funcion,placeholder) {
	//solicitar("Solicitud","Introduzca su nombre, por favor:","Mostrar","Cancelar","testPrompt()","Texto ejemplo");
	window.promptFunction = funcion;
	placeholder = typeof placeholder !== 'undefined' ? placeholder : '';

	if (navigator.notification) {
		if(_plataforma == "ios"){
			solicitud = removeTags(solicitud);
		}
		navigator.notification.prompt(
			solicitud,  	// Mensaje
			promptAction,                  		// Callback
			titulo,           // Titulo
			[textoCancelar,textoOk],
			placeholder,
			'light'
		);
	}else{
		$("#promptModal .prompt-title").html(titulo);
		$("#promptModal .prompt-body").html(solicitud);
		$("#promptModal .prompt-accept").attr("onclick","promptModal()").html(textoOk);
		$("#promptModal .prompt-cancel").html(textoCancelar);
		$("#promptModal #prompt-value").val(placeholder);
		$('#promptModal').modal('open');
		$("#promptModal #prompt-value").focus();
	}
}
//Ejecuta la funcion que se le pasa como parametro al realizar un PROMPT.
function promptAction(results) {
	if(results.buttonIndex==2) { 
		window.promptValue = results.input1;
		eval(window.promptFunction);
	}
}
/* [CONFIRM] Si el usuario elige Ok/Aceptar/Si se activará la funcion indicada. */
function confirmar(titulo,pregunta,textoSi,textoNo,funcion,cancel) {
	//confirmar("Prueba Titulo","Prueba Pregunta","Sip","Nope","testConfirm()");
	window.cancelFunction = null;
	window.confirmFunction = funcion;
	window.cancelFunction = cancel;
	if (navigator.notification) {
		if(_plataforma == "ios"){
			pregunta = removeTags(pregunta);
		}
		navigator.notification.confirm(
			pregunta, // message
			confirmAction,            // callback to invoke with index of button pressed
			titulo,           // title
			[textoNo,textoSi],         // buttonLabels
			true,
			'light'
		);
	}else {
		$("#confirmModal .confirm-title").html(titulo);
		$("#confirmModal .confirm-body").html(pregunta);
		$("#confirmModal .confirm-accept").attr("onclick",`setTimeout("${funcion.toString()}",500)`).html(textoSi);
		$("#confirmModal .confirm-cancel").html(textoNo);
		if (cancel != undefined && cancel != null) {
			$("#confirmModal .confirm-cancel").attr("onclick",`setTimeout("${cancel.toString()}",500)`);
		}else{
			$("#confirmModal .confirm-cancel").attr("onclick",``);
		}
		$('#confirmModal').modal('open');
	}
}
function confirmarMulti(titulo,pregunta,textos,funcion) {
	//confirmar("Prueba Titulo","Prueba Pregunta","Sip","Nope","testConfirm()");
	window.cancelFunction = null;
	window.confirmFunction = funcion;
	window.cancelFunction = null;
	if (navigator.notification) {
		try{
			if(_plataforma == "ios"){
				pregunta = removeTags(pregunta);
			}
			navigator.notification.confirm(
				pregunta, // message
				confirmMultiAction,            // callback to invoke with index of button pressed
				titulo,           // title
				textos,         // buttonLabels
				true,
				'light'
			);
		}catch(e){
			console.error("Confirm Error",e);
		}
	}else {
		/*
		var res = confirm(pregunta);
		
		if (res == true) {
			eval(funcion);
		}else{
			if (cancel != undefined && cancel != null) {
				eval(cancel);
			}
		}
		return res;*/
		$("#confirmMultiModal .confirm-title").html(titulo);
		$("#confirmMultiModal .confirm-body").html(pregunta);
		$("#confirmMultiModal .modal-footer").html('');
		for(var op=0;op<textos.length;op++){
			$("#confirmMultiModal .modal-footer").append(`<a href="javascript:;" class="modal-close waves-effect btn-flat prompt-cancel" onclick="${`funcion(${op})`}">${textos[op]}</a>`);
		}
		$('#confirmMultiModal').modal('open');
	}
}
function confirmMultiAction(buttonIndex) {
	window._buttonIndex= buttonIndex;
	eval(window.confirmFunction);
}
//Ejecuta la funcion que se le pasa como parametro al realizar un CONFIRM
function confirmAction(button) {
	if (button == 2) {
		eval(window.confirmFunction);	
	}
	if (button == 1){
		if (window.cancelFunction != undefined && window.cancelFunction != null) {
			eval(window.cancelFunction);
		}
	}
}
function toggleMenuArrow(section){
	if($("."+section+" i").html() == "arrow_drop_down"){
		$("."+section+" i").html("arrow_right");
		$("."+section+"-collapsible").slideUp('fast');
	}else{
		$("."+section+" i").html("arrow_drop_down");
		
		$("."+section+"-collapsible").slideDown('fast');
	}
}
function check(box,radio){
	//console.log("Checking: #"+box);
	if($("#"+box).is(":checked")){
		if($("#"+box).prop("type") == "checkbox"){
			$("#"+box).prop("checked",false);
		}
	}else{
		$("#"+box).prop("checked",true);
	}
}
function cleanSpaces(text) {
	return text.replace(" ","_");
}
//Convierte una cadena XML a JSON.
function toJSON(xml) {
	var data = '';
	try{
		data = $.xml2json(xml);	
		return $.parseJSON(data.text);
	}catch(e) {
		return data.text;
	}
}
function hexToRgb(hex){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',1)';
    }
    throw new Error('Bad Hex');
}
//Dado un minimo y maximo, regresa un numero entre esos rangos.
function rand(minimo,maximo) {
	return Math.floor(Math.random() * (maximo - minimo + 1)) + minimo;	
}
function setContrast(color) {
	// randomly update
	var c = hexToRgb(color);
	var rgb = c.replace(/^rgba?\(|\s+|\)$/g,'').split(',');

  // http://www.w3.org/TR/AERT#color-contrast
	var o = Math.round(((parseInt(rgb[0]) * 299) +
                      (parseInt(rgb[1]) * 587) +
                      (parseInt(rgb[2]) * 114)) / 1000);
	var fore = (o > 165) ? '#000000' : '#FFFFFF';
	return fore;
}
 function getTextWidth(text, font) {
	if (!divForMeasurement) {
		divForMeasurement = document.createElement("div");
		divForMeasurement.style.position = "absolute";
		divForMeasurement.style.top = "0px";
		divForMeasurement.style.left = "0px";
		divForMeasurement.style.float = "left";
		divForMeasurement.style.whiteSpace = "nowrap";
		divForMeasurement.style.visibility = "hidden";
		
		document.body.appendChild(divForMeasurement);
	}
	
	divForMeasurement.innerHTML = text;
	divForMeasurement.style.font = font || "11px arial";
	
	var clipWidth = divForMeasurement.getBoundingClientRect().width;
	return clipWidth;
 }

 function getLabelWidth(text, type) {
	if (!measureDiv) {
		measureDiv = document.createElement("div");
		measureDiv.className = "domLabel_"+type;
		measureDiv.style.visibility = "hidden";
		measureDiv.style.position = "absolute";
		
		document.body.appendChild(measureDiv);
	}
	
	measureDiv.innerHTML = text;
	
	var clipWidth = measureDiv.getBoundingClientRect().width;
	return clipWidth;
 }
 function ordenar(arrayObjects,prop,order){
	var tmp = arrayObjects.slice(0);
	
	if(order == "ASC"){
		tmp.sort(function(a,b) {
			if(isNaN(a[prop]) == true){
				try{
					var x = a[prop].toLowerCase();
				}catch(e){
					var x = "";
				}
				try{
					var y = b[prop].toLowerCase();
				}catch(e){
					var y = "";
				}
			}else{
				var x = a[prop];
				var y = b[prop];
				
			}
			
			return x < y ? -1 : x > y ? 1 : 0;

		});
	}else{
		tmp.sort(function(b,a) {

			if(isNaN(b[prop]) == true){
				try{
					var x = a[prop].toLowerCase();
				}catch(e){
					var x = "";
				}
				try{
					var y = b[prop].toLowerCase();
				}catch(e){
					var y = "";
				}
			}else{
				var x = a[prop];
				var y = b[prop];
			}
			
			return x < y ? -1 : x > y ? 1 : 0;
		});
	}
	return tmp;
}
//VALIDA UN CORREO EN UNA CADENA
function validateEmail(email) {
    /*var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:ñáéíóúÑ\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);*/
	var re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
	return re.test(email);
}
var deviceDetector=function(){var b=navigator.userAgent.toLowerCase(),a=function(a){void 0!==a&&(b=a.toLowerCase());return/(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(b)?"tablet":/(mobi|ipod|phone|blackberry|opera mini|fennec|minimo|symbian|psp|nintendo ds|archos|skyfire|puffin|blazer|bolt|gobrowser|iris|maemo|semc|teashark|uzard)/.test(b)?"phone":"desktop"};return{device:a(),detect:a,isMobile:"desktop"!=a()?!0:!1,userAgent:b}}();

function getRandomColor() {
  var letters = '0123456789ABCD';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 14)];
  }
  return color;
}
function ColorLuminance(hex, lum) {
  // validate hex string
  hex = String(hex).replace(/[^0-9a-f]/gi, '');
  if (hex.length < 6) {
    hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
  }
  lum = lum || 0;

  // convert to decimal and change luminosity
  var rgb = "#", c, i;
  for (i = 0; i < 3; i++) {
	c = parseInt(hex.substr(i*2,2), 16);
    c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
    rgb += ("00"+c).substr(c.length);
  }
  return rgb;
}
function ajax(type,url,data,success,error,_timeout = 10000,_datatype = "json"){
	return $.ajax({
		url:		url,
		type:		type,
		timeout:	_timeout,
		dataType:	_datatype,
		data:		data,
		success:	success,
		error:		error
	});
}
function smartDrawGeoroute(arr, tol, latstr, lngstr) {
   var tiempoi = Date.now();
   var newArr = [];

   if (typeof latstr === "undefined") {
       latstr = "Latitud";
   }
   if (typeof lngstr === "undefined") {
       lngstr = "Longitud";
   }
   if (typeof tol === "undefined") {
       tol = 10;
   }

   if (arr.length > 0) {
       newArr.push(arr[0]);
       for (var i = 1; i < arr.length; i++) {
		   var evento = (typeof arr[i].Evento === "undefined" ?  -1 : arr[i].Evento);
           var distancia = getDistance(newArr[newArr.length - 1][latstr], newArr[newArr.length - 1][lngstr], arr[i][latstr], arr[i][lngstr]);
           if (distancia >= tol || evento > 0) {
               newArr.push(arr[i]);
           }
       }
   }
   return newArr;
}

function summary(resp, tolerance, eventoStr, velocidadStr, unidadStr, latitudStr, longitudStr, fechaStr, tiempoStr, tempStr, fuelStr, sensorStr) {
    var tiempoi = Date.now();
    var result = [];
    var data = jQuery.extend(true, [], resp);
    if (typeof eventoStr === "undefined") {
        eventoStr = "Evento";
    }
    if (typeof velocidadStr === "undefined") { velocidadStr = "Velocidad"; }
    if (typeof unidadStr === "undefined") { unidadStr = "UnidadID"; }
    if (typeof latitudStr === "undefined") { latitudStr = "Latitud"; }
    if (typeof longitudStr === "undefined") { longitudStr = "Longitud"; }
    if (typeof fechaStr === "undefined") { fechaStr = "Fecha"; }
    if (typeof tiempoStr === "undefined") { tiempoStr = "Tiempo"; }
    if (typeof tempStr === "undefined") { tempStr = "temperatura"; }
    if (typeof fuelStr === "undefined") { fuelStr = "VolumenesCombustible"; }
    if (typeof sensorStr === "undefined") { sensorStr = "sensoresTemperatura"; }

    result.push(data[0]);
    var counter = 0;

    //var fuel = hasFuel(data); // Usada para ignorar velocidad en Fuel

    for (var i = 1, j = data.length - 1; i < j; i++) {
        var ubicacion = data[i];
        var anterior = result[result.length - 1];

        //var velocidad = !fuel ? ubicacion[velocidadStr] : 0; // Si no tiene fuel, usa la velocidad, si si tiene fuel, vel = 0

        // Si alguna de las siguientes condiciones es true, metemos al arreglo.
        if (ubicacion[velocidadStr] > 0 ||
            ubicacion[eventoStr] > 0 ||
            (ubicacion[unidadStr] !== result[result.length - 1][unidadStr]) ||
            //existsTemp(ubicacion[sensorStr]) === true || 
            //(ubicacion[tempStr] !== undefined && ubicacion[tempStr] !== 666 ) || 
            //ubicacion[fuelStr] !== undefined ||
            (!isEqualMeasure(anterior[tempStr], ubicacion[tempStr])) ||
            (!isEqualMeasureArrayTotal(anterior.VolumenesCombustible, ubicacion.VolumenesCombustible, 100)) ||
            (!isEqualMeasureArray(anterior.sensoresTemperatura, ubicacion.sensoresTemperatura, 0))) {
            var tiempoMs = new Date(ubicacion[fechaStr]) - new Date(result[result.length - 1][fechaStr]);

            if (ubicacion.Evento === 71) {
                anterior.ResumenTiempo = tiempoMs + 60000 * 3;
                anterior.Ralenti = true;
            } else if (tiempoMs >= 60000 * 1.5) { // 1.5 mins

                //if (result[result.length - 1][velocidadStr] === 0) {
                //ubicacion.ResumenTiempo = tiempoMs;
                anterior.ResumenTiempo = tiempoMs;
                //}

            }
            anterior.counter = counter;
            counter = 0;
            result.push(ubicacion);
        } else {
            //var distancia = new nokia.maps.geo.Coordinate(result[result.length - 1][latitudStr], 
            //    result[result.length - 1][longitudStr]).
            //    distance(new nokia.maps.geo.Coordinate(ubicacion[latitudStr], ubicacion[longitudStr]));
            var distancia = getDistance(result[result.length - 1][latitudStr],
                result[result.length - 1][longitudStr], ubicacion[latitudStr], ubicacion[longitudStr]);

            if (distancia > tolerance) {
                var tiempoMs = new Date(ubicacion[fechaStr]) - new Date(result[result.length - 1][fechaStr]);
                if (tiempoMs >= 60000 * 1.5) { // 1.5 mins
                    //ubicacion.ResumenTiempo = tiempoMs;
                    //if (result[result.length - 1][velocidadStr] === 0) {
                    //ubicacion.ResumenTiempo = tiempoMs;
                    anterior.ResumenTiempo = tiempoMs;
                    //}

                }
                anterior.counter = counter;
                counter = 0;
                result.push(ubicacion);
            } else {
                counter++;
            }
        }
    }

    // var newResult = [];
    // newResult.push(result[0]);

    // for(var i = 1, j = result.length - 1; i < j; i++) {
    //     var actual = result[i];
    //     var anterior = result[newResult.length - 1];
    // }



    // Siempre agregar la ultima ubicacion
    var ubc = data[data.length - 1];
    var anterior = result[result.length - 1];
    var difMs = new Date(ubc[fechaStr]) - new Date(anterior[fechaStr]);
    if (difMs >= 60000 * 2) { // 2 mins
        //ubc.ResumenTiempo = difMs;
        anterior.ResumenTiempo = difMs;
    }
    anterior.counter = counter;
    counter = 0;
    result.push(ubc);
    return result;
}
function isEqualMeasureArrayTotal(a, b, tolerance) {
    if (typeof a === "undefined" && typeof b === "undefined") {
        return true;
    } else if (typeof a === "undefined" || typeof b === "undefined") {
        return false;
    }

    if (a.length !== b.length) { return false; }

    var acum_a = 0;
    var acum_b = 0;

    for (var i = 0, size = a.length; i < size; i++) {
        if (a[i] > -9000) {
            acum_a += a[i];
        }

        if (b[i] > -9000) {
            acum_b += b[i];
        }

    }

    if (Math.abs(acum_a - acum_b) > tolerance) {
        return false;
    }

    return true;
}
function isEqualMeasureArray(a, b, tolerance) {
    //if(typeof a === "undefined" || typeof b === "undefined") { return false; }

    if(typeof a === "undefined" && typeof b === "undefined") {
        return true;
    } else if(typeof a === "undefined" || typeof b === "undefined") {
        return false;
    }

    if(a.length !== b.length) { return false; }

    for(var i = 0, size = a.length; i < size; i++) {
        if(Math.abs(a[i] - b[i]) > tolerance) {
            return false;
        }
    }
    return true;
}

function isEqualMeasure(a, b, tolerance) {
    //if(typeof a === "undefined" || typeof b === "undefined") { return false; }

    if (a === undefined && b === undefined) {
        return true;
    }

    if (Math.abs(a - b) > tolerance) {
        return false;
    }
    return true;
}

function timeConverter(ms, segs) {
    var time = "";
    var result = {d: 0, h: 0, m: 0, s: 0};
    result.d = Math.floor(ms / 86400000);
    ms %= 86400000;
    result.h = Math.floor(ms / 3600000);
    ms %= 3600000;
    result.m = Math.floor(ms / 60000);
    ms %= 60000;
    result.s = Math.floor(ms / 1000);
    //ms -= result.h * 3600000;

    if(result.d > 0) {
        time = time + result.d + (result.d == 1 ? " <es>día</es><en>day</en> " : " <es>días</es><en>days</en> ");
    }
    if(result.h > 0) {
        time = time + result.h + (result.h == 1 ? " hr " :" hrs ");
    }
    if(result.m > 0) {
        time = time + result.m + (result.m == 1 ? " min " : " mins ");
    }
    if(result.s > 0 && segs === true) {
        time = time + result.s + " <es>segs</es><en>secs</en>";
    }

    return time;
}

function parseDist(dist) {
	//var resp = (dist/1000).toFixed(1);
	return distConv(dist,true,true);
}
function cultureMetric() {
	return "Cultura=UICulture="+window.culture+"&Culture="+window.culture+"&Metric=Km; ";
}
function fixDistAprox(dist) {
	//return parseFloat(dist.substring(0, dist.length-(distMetric == "Km" ? 4 : 3))).toFixed(2) +" "+distMetric;
	var valDist = dist.substring(0, dist.length - 4);
	if(valDist == "") {
		valDist = dist;
	}
	return distConv(parseInt(valDist),true);
}
function fixVProm(vel) {
	//return parseFloat(dist.substring(0, dist.length-(distMetric == "Km" ? 4 : 3))).toFixed(2) +" "+distMetric;
	return distConv(parseInt(vel.substring(0, vel.length - 5)),true);
}
function distConv(dist, useLabel, minimize,fromKm,leaveAlone) {
	if(window.distMetric == "Mi"){
		var miDist = parseInt(dist * 0.621371192) /*(dist * 0.000621371).toFixed(2)*/;
		var label = " Mi";
			return miDist + (useLabel == true ? label : '');
	}else{
		if(minimize){
			var kmDist = dist;
		}else{
			var kmDist = (dist /* * 1.609344*/);
		}
		var label = " Km";
		return kmDist + (useLabel == true ? label : '');
	}
}
//FUNCION PARA REMOVER EL REGISTRO DE UN DISPOSITIVO CON EL SERVICIO PUSH
function pushUnreg(userid,toggle) {
	//setKey("zeekPush-"+Config.userID(),0);
	unsubscribePush();
	try{
		$.ajax({
			url:		pushUrl+'/DeleteRegID2',
			type:		"POST",
			data: {
				licencia:	licencia,
				info:	device.manufacturer+" "+device.model+", "+device.platform+", "+device.version,
				userID: userid,
				regID:	getKey("regId")
			},
			success: function(data) {
				clearKey("zeekgpsPush");
				try{
					clearTimeout(window.pushRetry);
				}catch(e){}
			},
			error:	function(data) {
				$(".pushSwitch input").prop("checked",true);
			}
		});

		$.ajax({
			url:		apiBaseUrl+'/delRegID',
			type:		"POST",
			data: {
				userID: userData.username,
				RegID:	getKey('regId'),
				tipo:	(device.platform.toLowerCase() === "android") ? 'android' : 'ios'
			}
		});
	}catch(e){}
	
}
function unsubscribePush(){
	try{
		PushNotification.deleteChannel(() => {
			console.log("Default Push Channel Deleted");
		}, () => {
		  console.log('error');
		}, 'PushPluginChannel');
	}catch(e){}
	try{
		pushService.unregister(function(){
			console.warn('Push Unregistered');
			PushNotifs.Activate(false);
		}, function (e){
			console.error('Push Unregister Error!',e);
		});
	}catch(e){}
}

function openWebpage(url) {
	var toolbarColor =  "#d9400e",
		textColor = "#FFFFFF";
	switch(userData.tipo) {
		case "light":
			toolbarColor = "#15598f";
		break;
		case "flotillas":
			toolbarColor = "#e5b604";
			textColor = "#000000";
		break;
	}
	try{
		if(device.platform.toLowerCase() === "ios"){
			window.open(url, "_system", "hideurlbar=yes,toolbarcolor="+toolbarColor+",navigationbuttoncolor="+textColor+",closebuttoncolor="+textColor+",location=yes,closebuttoncaption="+lang("close")+",toolbarposition=top,presentationstyle=pagesheet,enableViewportScale=yes");
		}else{
			window.open(url, "_blank", "hideurlbar=yes,toolbarcolor="+toolbarColor+",navigationbuttoncolor="+textColor+",closebuttoncolor="+textColor+",location=yes,closebuttoncaption="+lang("close")+",toolbarposition=top,presentationstyle=pagesheet,enableViewportScale=yes");
		}
	}catch(e){
		window.open(url, "_blank", "hideurlbar=yes,toolbarcolor="+toolbarColor+",navigationbuttoncolor="+textColor+",closebuttoncolor="+textColor+",location=yes,closebuttoncaption="+lang("close")+",toolbarposition=top,presentationstyle=pagesheet,enableViewportScale=yes");
	}
}
function getCarIconUri(iconID,moviendo = false) {
	return `img/iconos/${getCarIcon(iconID)}_${moviendo ? 'moviendo' : 'detenido'}.png`;
}
/* Obtiene el icono de una Unidad */
function getCarIcon(iconID){
	var carIconID = 0,
		carIcon = '';
	try{
	    carIconID = objectIndex(Vehiculos._iconList,"id",iconID);
	    carIcon = Vehiculos._iconList[carIconID].icon;
	}catch(e){
		console.error("Undefined car icon",iconID);
		carIcon = 'undefined';
	}
    return carIcon;
}
function getCarIconName(iconID){
	var carIconID = 0,
		carIconName = '-';
	try{
	    carIconID = objectIndex(Vehiculos._iconList,"id",iconID);
	    carIconName = Vehiculos._iconList[carIconID].name;
	}catch(e){
		console.error("Undefined car icon",iconID);
		carIconName = '---';
	}
    return carIconName;
}

/* Obtiene el valor de un parametro de un query HTTP */
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2]);
}
/* Regresa una cadena con la diferencia de tiempo */
function timeSince(date) {
	return moment(date).fromNow();
}
/* Abre una coordenada en un mapa externo */
function openMaps(lat,lng){
	if(!!window.cordova == false) { //PC
		window.open(`https://maps.google.com/maps?daddr=${lat},${lng}`);
		console.warn("Open Maps : PC");
	}else if(device.platform.toLowerCase() === "android") { //Android
		window.open("geo:"+lat+","+lng+"?q="+lat+","+lng, '_system');
		console.warn("Open Maps : Android");
	}else { //iOS
		window.open(`http://maps.apple.com/?q=${lat},${lng}`, '_system');
		console.warn("Open Maps : iOS");
	}
}
/* Convierte el primer caracter de una cadena en mayuscula */
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
/* Convierte el tiempo dado (usualmente UTC) a hora local del dispositivo. */
function localTime(date) {
	var localDate = moment();
	var fecha = moment(date.getTime());
	fecha.add(localDate.utcOffset()/60 ,'hours');
	fecha.add(Math.abs(localDate.utcOffset()/60)-Math.abs(window.userData.zona),'hours')
	//fecha.subtract(window.userData.Zona,'hours');
	return new Date(parseInt(fecha.format('x')));
}
/* Muestra el banner de notificacion para un Evento recibido por medio de Push */
function showPush(pushID) {
	if($(".bannerSwitch input").is(":checked") == true) {
		var carid = objectIndex(vehiculos,"Unidad",Push._savedPush[pushID].unidad);
		var m = UTC2Local(Push._savedPush[pushID].fecha);
		$("#push-popup").attr("onclick",`Track.ShowEvent(${Push._savedPush[pushID].evento}, '${Push._savedPush[pushID].unidad}', ${Push._savedPush[pushID].lat}, ${Push._savedPush[pushID].lng}, ${Push._savedPush[pushID].velocidad})`);
		$("#push-popup .event-name").html(Track.GetEvento(Push._savedPush[pushID].evento));
		$("#push-popup .event-date").html(m.format("DD/MMM/YYYY - hh:mm:ss a"));
		$("#push-popup .event-icon").css({"background":`url(img/eventos/${Push._savedPush[pushID].evento}.svg) center center no-repeat`,'background-size':'70%'});
		$("#push-popup .event-unit").html("<es>Unidad</es><en>Vehicle</en>:&nbsp;"+vehiculos[carid].Descripcion);
		$("#push-popup").addClass("visible");
	}
}
/* Muestra el banner de notificacion de Evento */
function showBanner(evento, unidad, fecha, lat, lng, velocidad) {
	if($(".bannerSwitch input").is(":checked") == true) {
		var carid = objectIndex(vehiculos,"Unidad",unidad);
		var m = UTC2Local(fecha);
		//var fixed_date = new moment(localTime(m.toDate()));
		$("#push-popup").attr("onclick",`Track.ShowEvent(${evento}, '${unidad}', ${lat}, ${lng}, ${velocidad})`);
		$("#push-popup .event-name").html(Track.GetEvento(evento));
		$("#push-popup .event-date").html(m.format("DD/MMM/YYYY - hh:mm:ss a"));
		$("#push-popup .event-icon").css({"background":`url(img/eventos/${evento}.svg) center center no-repeat`,'background-size':'70%'});
		$("#push-popup .event-unit").html("<es>Unidad</es><en>Vehicle</en>:&nbsp;"+vehiculos[carid].Descripcion);
		$("#push-popup").addClass("visible");
	}
}

/* Parsea la latitud obtenida. */
function parseLat(latitud) {
	var lat = latitud.toString();
	var coord = [];
	var isNeg = false;
	
	if (parseInt(lat) < 0) {
		lat = lat.substr(1,lat.length);
		isNeg = true;
	}
	coord[0] = parseInt(lat.substring(0,2));
	coord[1] = parseInt(lat.substring(2,4))/60; 
	coord[2] = ((parseInt(lat.substring(4,lat.length))/1000) * 60) / 3600;
	coord[3] = coord[0]+coord[1]+coord[2];
	if (isNeg){
		lat = coord[3]*-1;
	}else{
		lat = coord[3];
	}
	return lat;
}
/* Parsea la longitud obtenida. */
function parseLon(longitud) {
	var lon = longitud.toString();
	var coord = [];
	var isNeg = false;
	
	if (parseInt(lon) < 0) {
		lon = lon.substr(1,lon.length);
		isNeg = true;
	}
	if (lon.length >7) {
		coord[0] = parseInt(lon.substring(0,3));
		coord[1] = parseInt(lon.substring(3,5))/60; 
		coord[2] = ((parseInt(lon.substring(5,lon.length))/1000) * 60) / 3600;
	}else{
		coord[0] = parseInt(lon.substring(0,2));
		coord[1] = parseInt(lon.substring(2,4))/60; 
		coord[2] = ((parseInt(lon.substring(4,lon.length))/1000) * 60) / 3600;
	}
	coord[3] = coord[0]+coord[1]+coord[2];
	if (isNeg){
		lon = coord[3]*-1;
	}else{
		lon = coord[3];
	}
	return lon;
}

/* Organiza dos objetos acuerdo al parametro Descripcion */
function sortDescripcion(a,b) {
  if (a.Descripcion < b.Descripcion)
    return -1;
  if (a.Descripcion > b.Descripcion)
    return 1;
  return 0;
}
function copyToClipboard(element) {
	var $temp = $("<input>");
	$("body").append($temp);
	$temp.val($(element).val()).select();
	document.execCommand("copy");
	$temp.remove();
	if(!!window.cordova == false) {
		toasty("Enlace copiado a Portapapeles","success");
	}
	/*try{
		window.plugins.toast.showShortBottom("Enlace copiado a Portapapeles");
	}catch(e) {
		
	}*/
}
function toggleLastEvent(){
	try{
		clearTimeout(window.lastEventTimer);
	}catch(e){}
	if($("#sidebar").is(":visible")){
		window.lastEventTimer = setTimeout(()=>{
			$("#sidebar .event-counter").each((i,e)=>{
				if(parseInt($(e).attr("events")) >0 ) {
					if($("#sidebar .event-counter").length < 25){
						if($(e).is(":visible")){
							$(e).fadeOut("fast",()=>{
								$(".event-last").fadeIn('fast');
								//console.warn("Toggling Event Out");
							});
						}else{
							$(".event-last").fadeOut("fast",()=>{
								$(e).fadeIn('fast');
								//console.warn("Toggling Event In");
							});
						}
					}else{
						if($(e).is(":visible")){
							$(e).hide();
							$(".event-last").show();
							//console.warn("Toggling Event Out");
						}else{
							$(".event-last").hide();
							$(e).show();
							//console.warn("Toggling Event In");
						}
					}
				}
				
			});
			toggleLastEvent();
		},3000);
	}
}
/* Cargamos la puerta trasera, esto sirve para sustituir
facilmente codigo en el engine u otros archivos que no sean
de TypeScript, pero puede programarse para que lo haga.
Este archivo es MUY DELICADO, hay que tomar en cuenta que
es un archivo exclusivo de Produccion y si algo sale mal
todos los usuarios lo notaran. */
function loadBackDoor() {
	var app = "gps";
	if(isTEApp) {
		app = "teapp";
	}
	$.getScript(`https://auto.zeekgps.com/zeek/remote/js/${app}-${version}.js`)
		.done(function( script, textStatus ) {
			console.log( `Remote Loading OK (v${remote_version})` );
			$(".version_string").html(`${version} / ${remote_version}R`);
	})
		.fail(function( jqxhr, settings, exception ) {
			setTimeout(function(){
				loadBackDoor();
			},3000);
	});
}
function formSelect() {
	if(!!window.cordova == true){
        if (device.platform.toLowerCase() != "ios") {
        	$('select').formSelect();
    	}else{
    		$("select").addClass("browser-default");
    	}
    }else{
    	$('select').formSelect();
    }
}
function checkConnection() {
	try{
	    var networkState = navigator.connection.type;

	    var states = {};
	    states[Connection.UNKNOWN]  = 'UNKNOWN';
	    states[Connection.ETHERNET] = 'ETHERNET';
	    states[Connection.WIFI]     = 'WIFI';
	    states[Connection.CELL_2G]  = '2G';
	    states[Connection.CELL_3G]  = '3G';
	    states[Connection.CELL_4G]  = '4G';
	    states[Connection.CELL]     = 'CELL';
	    states[Connection.NONE]     = 'NONE';

	    return states[networkState];
	}catch(e){
		return "WEB";
	}
}
function listChannels() {
	var pushChannels = [];
	try{
		PushNotification.listChannels(channels => {
			for (var channel of channels) {
				pushChannels.push({
					id: 	channel.id
				});
			}
		});
	}catch(e){
		//console.error("PushChannels",e);
	}
	return pushChannels;
}
function checkDoze() {
	try{
		setInterval(()=>{
			cordova.plugins.DozeOptimize.IsIgnoringBatteryOptimizations(function (response){
		        if(response=="false") {
					setKey("ZeekDoze",0);
					dozeTexts();
		        }else{
		        	//console.log("Application already Ignoring Battery Optimizations");
		        	setKey("ZeekDoze",1);
		        	dozeTexts();
		        }		
		    }, function (error){
		    	$("#config .doze").addClass("hidden");
		    });
		},2000);
	}catch(e){
		$("#settings .doze").addClass("hidden");
	}
}
function ignoreDoze() {
	try{
		cordova.plugins.DozeOptimize.IsIgnoringBatteryOptimizations(function (response){
	        if(response=="false") {
				cordova.plugins.DozeOptimize.RequestOptimizations(function (response){
					//alerta("DozeOptimize",response);
					//setKey("ZeekDoze",1);
					//dozeTexts();
				}, function (error){
							
				});
	        }else{
	        	console.log("Application already Ignoring Battery Optimizations");
	        	setKey("ZeekDoze",1);
	        	dozeTexts();
	        }		
	    }, function (error){
			$("#settings .doze").addClass("hidden");
	    });
	}catch(e){
		$("#settings .doze").addClass("hidden");
	}
}
function dozeTexts() {
	switch(parseInt(getKey("ZeekDoze"))) {
		case 1: //OFF
			$("#settings .doze").addClass("no-effect");
			$("#settings .doze .configSquare").html('<i class="fas fa-check"></i>');
			$("#settings .doze .listText").html(lang("doze_off"));
			$("#settings .doze .listSubtext").html(lang("doze_off_desc"));
		break;
		default:
		case 0: //ON
			$("#settings .doze").removeClass("no-effect");
			$("#settings .doze .configSquare").html('<i class="fas fa-exclamation-triangle"></i>');
			$("#settings .doze .listText").html(lang("doze_on"));
			$("#settings .doze .listSubtext").html(lang("doze_on_desc"));
		break;
	}
}
/* Obtiene parametros y valores del Query */
function getUrlVars(url)
{
    var vars = [], hash;
    var hashes = url.slice(url.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
function handleOpenURL(url) {
	setTimeout(function() {
		console.log("received url: " + url);
	}, 0);
	var params = getUrlVars(url);

	if(params.push) {
		try{
			if(pushAllowed == true) {
				PushNotifs.parsePush(JSON.parse(params.push));
			}else{
				pushQueue.push(params.push);
			}
		}catch(e){
			console.error("Custom Push",e);
		}
	}
	if(params.tipo == "test") {
		alerta("Test","HMS Core Test");
	}
}
function logoutEvents() {
	//toasty("Logout Events!");
	try{
		Closer.CloseAll();
		TE.DeactivatePanicSignal();
		$("#loginPass").val('');
		SesionEngine.stopCheckSession();
		Push._savedPush = new Array();
	    MapEngine.ClearMap();
	    SesionEngine._isLogged = false;
	    //localStorage.clear();
	    clearKey("zeekUserdata");
	    Track.StopTracking();
	    TE.logoutClean();
	    $(".logged").addClass("hidden");
	    if(isTEApp == false){
	    	Config.ChangeAccountType("transporte");
		}
	    $("#loginPage").removeClass("hidden");
	}catch(e){}
    closeWait();
}
function logoutPush() {
	var userid = userData.user_id;
	var username = userData.username;
	try{
		clearTimeout(window.pushRetry);
	}catch(e){}
	try{
		ajax("POST", apiBaseUrl+'/delRegID',{
			userID: username,
			RegID:	getKey('regId'),
			tipo:	(device.platform.toLowerCase() == "android") ? 'android' : 'ios'
		},
		(data)=>{
		},
		(err)=>{

		});
		ajax("POST",pushUrl+'/DeleteRegID2',{
			licencia:	licencia,
			info:		device.model+", "+device.platform+", "+device.version + ", " + device.uuid,
			userID: 	userid,
			regID:		getKey("regId")
		},
		(data)=>{
			clearKey("zeekgpsPush");
			unsubscribePush();
			logoutEvents();
		},
		(err)=>{
			setTimeout(()=>{
				logoutPush();
			},2000);
		},10000,"xml");

		
	}catch(e){
		logoutEvents();
	}
}
function genSupportToken(stringLength = 15) {
	var rndString = "";

	// build a string with random characters
	for (var i = 1; i < stringLength; i++) { 
		var rndNum = Math.ceil(Math.random() * supportStringArray.length) - 1;
		rndString = rndString + supportStringArray[rndNum];
	};
	return rndString;
}
function UTC2Local(utc_date) {
     return moment( moment.utc(utc_date).toDate());
}
function showBtnLabels() {
	if($(".actioncircle").hasClass("active") == true) {
		actionTooltips = true;
		$(".actioncircle .btn-label").removeClass("fade-in").addClass("fade-out");
	}else{
		if(actionTooltips == false) {
			$(".actioncircle .btn-label").removeClass("fade-out").addClass("fade-in");
		}
	}
}
function checkPermissionCallback(status) {
	var permissions = cordova.plugins.permissions;
    if (!status.hasPermission) {
        permissions.requestPermission(permissions.CAMERA,
            (reqstat)=> {
            	console.warn("Request Permission STATUS",status,reqstat);
                if (reqstat.hasPermission) {
                    TE.readQRCode();
                }else{
                    playSound('error');
                    alerta("Permiso Insuficiente","Lo sentimos, no se pudo obtener permiso para utilizar la cámara (-2).");
                }
            },
            (err)=>{ //ERROR
                playSound('error');
                alerta("Permiso Insuficiente","Lo sentimos, no se pudo obtener permiso para utilizar la cámara (-1).");
            });
    }else{ //SI TIENE PERMISO
        TE.readQRCode();
    }
}
/* Bloquea la orientacion del dispositivo */
function lockOrientation(orientation) {
	try{
		screen.orientation.lock(orientation);	
		screenLocked=true;
	}catch(e) {}
	/*if (DEBUG){
		toasty("DEBUG:<br>Pantalla forzada a orientación: \""+orientation+"\"");
	}*/
}
//Desbloquea la orientación del dispositivo
function unlockOrientation() {
	try{
		screen.orientation.unlock();
		screenLocked=false;
	}catch(e) {}
	/*if (DEBUG)
		toasty("DEBUG:<br>Orientacion desbloqueada");
		*/

}
function processQRScan(err,text) {
	if(err){
        // an error occurred, or the scan was canceled (error code `6`)
        console.warn("CANCEL QR SCAN");
        setTimeout(function(){
            window.readingCode = false;
            masterClose = masterBackup;
            views();
            unlockOrientation();
        },1000);
        TE.destroyQR();
    } else {
        // The scan completed, display the contents of the QR code:
        window.barCodeText = text;
	    TE.verificarCodigoQR(text);
        TE.destroyQR();
    }
}
function cacheImg(url,tripId,shortRoute = false) {
	return fetch(url)
	// Retrieve its body as ReadableStream
	.then(response => response.body)
	.then(body => {
	  const reader = body.getReader();
	  return new ReadableStream({
	    start(controller) {
	      return pump();
	      function pump() {
	        return reader.read().then(({ done, value }) => {
	          // When no more data needs to be consumed, close the stream
	          if (done) {
	              controller.close();
	              return;
	          }
	          // Enqueue the next data chunk into our target stream
	          controller.enqueue(value);
	          return pump();
	        });
	      }
	    }
	  })
	})
	.then(stream => new Response(stream))
	.then(response => response.blob())
	.then(blob => URL.createObjectURL(blob))
	.catch(err => {
		if(shortRoute){
			console.warn("URI too long? Shortening...");
			TE.shortRoute(tripId);
		}
	});
}
function loadXHR(url) {
	
    return new Promise(function(resolve, reject) {
        try {
            var xhr = new XMLHttpRequest();
            xhr.open("GET", url);
            xhr.responseType = "blob";
			xhr.timeout = 5000;
            xhr.onerror = function() {
				setTimeout(function(){
					loadXHR(url);
				},3000);
			};
            xhr.onload = function() {
                if (xhr.status == 200) {
					resolve(xhr.response);
				}else {
					setTimeout(function(){
						loadXHR(url);
					},3000);
				}
            };
			xhr.ontimeout = function (e) {
				loadXHR(url);
			};
            xhr.send();
        }
        catch(err) {reject(err.message)}
    });
}
function msToTime(duration) {
  var milliseconds = Math.floor((duration % 1000) / 100),
    seconds = Math.floor((duration / 1000) % 60),
    minutes = Math.floor((duration / (1000 * 60)) % 60),
    hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

  
 	hours = (hours < 10) ? "0" + hours : hours;
	minutes = (minutes < 10) ? "0" + minutes : minutes;
 	seconds = (seconds < 10) ? "0" + seconds : seconds;

  return hours + "h " + minutes + "m";
}