/// <reference path ="jquery.d.ts"/> 
var TEAppClass = /** @class */ (function () {
    function TEAppClass() {
        this.apiUrl = 'https://apps.zeekgps.com/tp/api/';
        this.apiToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEiLCJDbGllbnRlSWQiOiJNSUdVRUxPTkRSSVZJTkciLCJVc3VhcmlvSWQiOiJNSUdVRUxPTkRSSVZJTkciLCJuYmYiOjE2MTg4NjEyMzEsImV4cCI6MTYxOTQ2NjAzMSwiaWF0IjoxNjE4ODYxMjMxfQ.n-gu9WOxOXszOVftEMcEUShY-PfemhCUBU3ixbKwRVQ';
        this.nombreEmpresa = "Samsung Mexicana SA de CV";
        this.nombreUnidad = "";
        this.ruta = -1;
        this.rateId = -1;
        this.currentTrip = -1;
        this.isDriver = false;
        this.drawnRoute = false;
        this.viajeEnCurso = false;
        this.panicActive = false;
        this.showingPanicPrompt = false;
        this.panicSignalTimeout = null;
        this.showingSuggestion = false;
        this.unitMarker = null;
        this.unitLabel = null;
        this.regToken = "";
        this.congrats = [
            "¡Excelente trabajo!",
            "¡Bien hecho!",
            "¡Super!",
            "¡Muy bien!",
            "¡Hasta la próxima!"
        ];
        this.teAppMenuStruct = [
            {
                class: "tea-horario-menu",
                onclick: "resetMenu();TE.OpenHorario()",
                icon: 'fas fa-clock',
                title_es: "Horario",
                title_en: "Trip Times"
            },
            {
                class: "tea-viajes-menu",
                onclick: "resetMenu();TE.OpenViajes()",
                icon: 'fas fa-route',
                title_es: "Mis Viajes",
                title_en: "Past Trips"
            },
            {
                class: "tea-checkin-menu",
                onclick: "TE.CheckIn()",
                icon: 'fas fa-user-check',
                title_es: "Abordaje",
                title_en: "Check-in"
            },
            {
                class: "settings-menu",
                onclick: "resetMenu();Config.OpenSettings();",
                icon: 'zicon_config',
                title_es: "Configuración",
                title_en: "Settings"
            },
            {
                class: "help-menu",
                onclick: "resetMenu();Config.OpenHelp();",
                icon: 'zicon_ayuda',
                title_es: "Ayuda",
                title_en: "Help"
            },
            {
                divider: true
            },
            {
                class: "debug console-menu hidden",
                onclick: "resetMenu();Config.OpenConsole()",
                icon: 'fas fa-terminal',
                title_es: "Consola",
                title_en: "Console"
            },
            {
                class: "",
                onclick: "resetMenu();SesionEngine.CerrarSesion();",
                icon: 'zicon_cerrar-sesion',
                title_es: "Cerrar Sesión",
                title_en: "Logout"
            }
        ];
        //MENU CONDUCTOR
        this.teAppMenuStructDriver = [
            {
                class: "tea-horario-menu",
                onclick: "resetMenu();TE.OpenHorario()",
                icon: 'fas fa-clock',
                title_es: "Horario de Viajes",
                title_en: "Trip Times"
            },
            {
                class: "tea-viajes-menu",
                onclick: "resetMenu();TE.OpenViajes()",
                icon: 'fas fa-route',
                title_es: "Mis Viajes Pasados",
                title_en: "Past Trips"
            },
            {
                class: "settings-menu",
                onclick: "resetMenu();Config.OpenSettings();",
                icon: 'zicon_config',
                title_es: "Configuración",
                title_en: "Settings"
            },
            {
                class: "help-menu",
                onclick: "resetMenu();Config.OpenHelp();",
                icon: 'zicon_ayuda',
                title_es: "Ayuda",
                title_en: "Help"
            },
            {
                divider: true
            },
            {
                class: "debug console-menu hidden",
                onclick: "resetMenu();Config.OpenConsole()",
                icon: 'fas fa-terminal',
                title_es: "Consola",
                title_en: "Console"
            },
            {
                class: "",
                onclick: "resetMenu();SesionEngine.CerrarSesion();",
                icon: 'zicon_cerrar-sesion',
                title_es: "Cerrar Sesión",
                title_en: "Logout"
            }
        ];
        this.reportSubjects = [
            "Comportamiento del conductor",
            "Conducción",
            "Ruta inconveniente",
            "Otra razón"
        ];
        this.tripList = [];
        this.routeMarkerGroup = new H.map.Group();
        map.addObject(this.routeMarkerGroup);
        checkBuild();
    }
    /**
     * Sesion
     */
    TEAppClass.prototype.Sesion = function (usuario, contrasena) {
        this.IniciarSesion(usuario, contrasena);
    };
    /**
     * IniciarSesion
     */
    TEAppClass.prototype.IniciarSesion = function (usuario, contrasena) {
        var _this = this;
        wait("Iniciando Sesión", "Por favor, espere...");
        this.ajaxAuth("POST", this.apiUrl + 'passenger/Login/authenticate', {
            username: usuario,
            password: contrasena
        }, function (data, response, xhr) {
            if (xhr.status == 200) {
                window.justLogged = true;
                setKey("zeekgpsUser", usuario);
                setKey("zeekgpsPass", contrasena);
                _this.jwtToken = data.token;
                _this.jwtTokenDecoded = _this.decodeJWT(_this.jwtToken);
                window.userData = new Object();
                userData.username = usuario;
                _this.processActiveTrip(data.viaje);
                _this.extractUserData();
                SesionEngine.Startup();
            }
            else {
                $("#loginPage").removeClass("hidden");
                toasty("Lo sentimos, ha ocurrido un error al autenticar, por favor intente más tarde (-3).", "error");
                closeWait();
            }
        }, function (err) {
            if (err.status == 400) {
                toasty("Lo sentimos, sus datos son incorrectos, por favor corríjalos.", "error");
            }
            else {
                toasty("Lo sentimos, ha ocurrido un error al autenticar, por favor intente más tarde (-2).", "error");
            }
            console.error(err);
            $("#loginPage").removeClass("hidden");
            closeWait();
        });
    };
    /**
     * processActiveTrip
     */
    TEAppClass.prototype.processActiveTrip = function (data) {
        var _this = this;
        if (data != null) {
            this.viajeEnCurso = true;
            setTimeout(function () {
                _this.tripData = _this.constructTripData(data);
                _this.ruta = _this.tripData.id;
                _this.nombreUnidad = _this.tripData.nombreUnidad;
                _this.displayCurrentLiveTrip(_this.ruta);
                _this.RastreoContinuo();
            }, 0);
        }
    };
    /**
     * extractUserData
     */
    TEAppClass.prototype.extractUserData = function () {
        var _this = this;
        userData.userID = parseInt(this.jwtTokenDecoded.UsuarioId);
        userData.Rol = this.jwtTokenDecoded.Rol;
        userData.ClienteId = this.jwtTokenDecoded.ClienteId;
        userData.tipo = "light";
        userData.nombreUsuario = this.jwtTokenDecoded.Nombre;
        userData.nombreEmpresa = this.jwtTokenDecoded.Empresa;
        if (userData.Rol == "Driver") {
            this.isDriver = true;
        }
        else {
            this.isDriver = false;
        }
        saveSet("zeekUserdata", userData);
        setTimeout(function () {
            _this.ShowSuggestion();
        }, 1000);
    };
    /**
     * ajaxAuth
     */
    TEAppClass.prototype.ajaxAuth = function (method, url, data, successCB, errorCB, _timeout) {
        if (method === void 0) { method = "POST"; }
        if (_timeout === void 0) { _timeout = 30000; }
        var ajaxOptions = {
            'url': url,
            'method': method,
            'headers': {
                'Authorization': "Bearer " + this.apiToken,
                'Content-Type': "application/json",
            },
            'timeout': _timeout,
            'data': JSON.stringify(data),
            'success': successCB,
            'error': errorCB,
            'cache': false
        };
        $.ajax(ajaxOptions);
    };
    /**
     * ajax
     */
    TEAppClass.prototype.ajax = function (method, url, data, successCB, errorCB, _timeout) {
        if (method === void 0) { method = "POST"; }
        if (_timeout === void 0) { _timeout = 30000; }
        var ajaxOptions = {
            'url': url,
            'method': method,
            'headers': {
                'Authorization': "Bearer " + this.jwtToken,
                'Content-Type': "application/json",
            },
            'timeout': _timeout,
            'success': successCB,
            'error': errorCB,
            'cache': false
        };
        if (data != null) {
            ajaxOptions.data = JSON.stringify(data);
        }
        $.ajax(ajaxOptions);
    };
    /**
     * CheckIn
     */
    TEAppClass.prototype.CheckIn = function () {
        var _this = this;
        if (this.viajeEnCurso) {
            alerta("Viaje Activo", "Ya se encuentra a bordo con viaje en ruta, para volver abordar una unidad antes debe confirmar que bajó de la unidad.");
        }
        else {
            this.HideSuggestion();
            resetMenu();
            selectMenu(".tea-checkin-menu");
            $("#manual_qr").val('');
            hideActionCircle();
            $("#checkin").removeClass("hidden");
            masterClose = function () { _this.CloseCheckIn(); };
            this.QRPrep();
        }
    };
    /**
     * CancelCheckIn
     */
    TEAppClass.prototype.CancelCheckIn = function () {
        this.cleanRoute();
        this.destroyCheckIn();
        this.ruta = -1;
        this.destroyQR();
        $("html").removeClass("scanning-qr");
    };
    /**
     * destroyCheckIn
     */
    TEAppClass.prototype.destroyCheckIn = function () {
        $("#main .mainTop").removeClass("no-logo");
        $("#main .mainTop .title").html("");
        $("#close-btn").addClass("hidden");
        $("#checkin").removeClass("hidden");
        $(".checkin_success,.checkin_manual").addClass("hidden");
        $(".checkin_process").removeClass("hidden");
        if (this.viajeEnCurso == false) {
            $("#map .trip_card").removeClass("current_trip");
            $("#map .trip_card").html('').addClass("hidden");
        }
    };
    /**
     * CloseCheckIn
     */
    TEAppClass.prototype.CloseCheckIn = function () {
        if ($("#map .trip_card").hasClass("current_trip") && this.viajeEnCurso == false) { //CHECK-IN
            this.CancelCheckIn();
        }
        else {
            $("#checkin").addClass("hidden");
            resetBack();
            if (this.viajeEnCurso == false) {
                this.ShowSuggestion();
            }
        }
    };
    /**
     * ManualQR
     */
    TEAppClass.prototype.ManualQR = function () {
        $(".checkin_process").addClass("hidden");
        $(".checkin_manual").removeClass("hidden");
        $("#manual_qr").val('');
        $("#manual_qr").focus();
    };
    /**
     * ManualQRConf
     */
    TEAppClass.prototype.ManualQRConf = function () {
        this.HideSuggestion();
        var rutaId = trim($("#manual_qr").val().toUpperCase());
        if (rutaId == "") {
            alerta("Error", "Por favor, introduzca un código de abordaje.");
        }
        else {
            $("#manual_qr").blur();
            this.verificarCodigoQR(rutaId);
        }
    };
    /**
     * verificarCodigoQR
     */
    TEAppClass.prototype.verificarCodigoQR = function (codigoQR) {
        var _this = this;
        wait("Verificando código...", "Por favor, espere.");
        this.ajax("POST", this.apiUrl + 'passenger/Trips/preview', codigoQR, function (data, resp, xhr) {
            switch (parseInt(xhr.status)) {
                case 200:
                    if (_this.jwtTokenDecoded.Empresa == data.nombreEmpresa) {
                        _this.tripData = _this.constructTripData(data);
                        _this.processQRTrip(codigoQR);
                    }
                    else {
                        toasty("Lo sentimos, no puedes tomar el viaje de otra empresa (-3).", "warn");
                        closeWait();
                    }
                    break;
                default:
                    toasty("Lo sentimos, no ha sido posible verificar este código QR, ha sido eliminado o no es válido (-1).", "error");
                    break;
            }
        }, function (err) {
            switch (err.status) {
                case 404:
                    toasty("Lo sentimos, este código QR no existe o ha sido eliminado (-4).", "error");
                    break;
                case 400:
                    toasty("Lo sentimos, este código QR no es válido, intenta escanearlo o escribirlo nuevamente (-3).", "error");
                    break;
                default:
                    toasty("Lo sentimos, no ha sido posible verificar este código QR, revise su conexión e intente de nuevo (-2).", "error");
                    break;
            }
            closeWait();
        });
    };
    TEAppClass.prototype.QRPrep = function () {
        this.CancelCheckIn();
        try {
            if (device.platform.toLowerCase() == "android") {
                var permissions = cordova.plugins.permissions;
                permissions.checkPermission(permissions.CAMERA, checkPermissionCallback, null);
            }
            else {
                this.readQRCode();
            }
        }
        catch (e) {
        }
    };
    /**
     * readQRCode
     */
    TEAppClass.prototype.readQRCode = function () {
        var _this = this;
        //console.warn("READING QR CODE");
        window.readingCode = true;
        window.masterBackup = masterClose;
        window.barCodeText = "";
        masterClose = function () { };
        this.startScanner();
        setTimeout(function () {
            if ($("html").hasClass("scanning-qr")) {
                _this.startScanner();
            }
        }, 500);
    };
    /**
     * startScanner
     */
    TEAppClass.prototype.startScanner = function () {
        var _this = this;
        try {
            QRScanner.prepare(function (err, status) {
                if (err) {
                    console.error("QRScanner Prep Error", err._message);
                }
                else {
                    //console.log('QRScanner is initialized. Status:');
                    //console.log(status);
                    QRScanner.scan(processQRScan);
                    QRScanner.show(function (status) {
                        $("html").addClass("scanning-qr");
                        console.warn("QRScanner Show", status);
                    });
                    QRScanner.getStatus(function (status) {
                        if (status.canEnableLight == true) {
                            _this.showTorchButton();
                        }
                    });
                }
            });
        }
        catch (e) {
            $("html").removeClass("scanning-qr");
            console.error("QRScanner Error", e);
        }
    };
    /**
     * cancelScan
     */
    TEAppClass.prototype.cancelScan = function () {
        $("html").removeClass("scanning-qr");
        QRScanner.cancelScan();
    };
    /**
     * showTorchButton
     */
    TEAppClass.prototype.showTorchButton = function () {
        $("#scanUI .scan-torch").removeClass("hidden");
    };
    /**
     * toggleTorch
     */
    TEAppClass.prototype.toggleTorch = function () {
        QRScanner.getStatus(function (status) {
            if (status.lightEnabled == true) {
                $(".scan-torch").html('<i class="fal fa-bolt"></i>').removeClass("torch-enabled");
                QRScanner.disableLight(function (err, status) { });
            }
            else {
                $(".scan-torch").html('<i class="fas fa-bolt"></i>').addClass("torch-enabled");
                QRScanner.enableLight(function (err, status) { });
            }
        });
    };
    /**
     * destroyQR
     */
    TEAppClass.prototype.destroyQR = function () {
        $("html").removeClass("scanning-qr");
        try {
            QRScanner.getStatus(function (status) {
                if (status.lightEnabled == true) {
                    QRScanner.disableLight(function (err, status) {
                        $(".scan-torch").html('<i class="fal fa-bolt"></i>').removeClass("torch-enabled");
                        setTimeout(function () {
                            QRScanner.destroy();
                        }, 0);
                    });
                }
                else {
                    setTimeout(function () {
                        QRScanner.destroy();
                    }, 0);
                }
            });
        }
        catch (e) { }
    };
    /**
     * processQRTrip
     */
    TEAppClass.prototype.processQRTrip = function (liveId) {
        var viaje = this.tripData;
        var f_inicio = UTC2Local(viaje.fechaInicio);
        this.currentTrip = liveId;
        this.nombreUnidad = viaje.nombreUnidad;
        $("#checkin").addClass("hidden");
        $("#map .trip_card,#close-btn").removeClass("hidden");
        $("#close-btn").attr("onclick", "TE.CancelCheckIn()");
        $("#main .mainTop").addClass("no-logo");
        $("#map .trip_card").addClass("current_trip");
        this.AdjustTitle("Confirmar Abordaje");
        viaje.conductor = this.constructDriverData(viaje.conductor);
        $(".checkin_process,.checkin_manual").addClass("hidden");
        $("#map .trip_card").html("\n            <div class=\"carta\">\n                <div class=\"trip-container\">\n                    <div class=\"trip-basic-data\">\n                        <div class=\"trip-info\">\n                            <div class=\"trip-place trip-start\">\n                                <div class=\"trip-location\"><div class=\"trip-icon green\"></div> <span>" + viaje.direccionInicio + "</span></div>\n                            </div>\n                            <div class=\"trip-divider\"></div>\n                            <div class=\"trip-place trip-end\">\n                                <div class=\"trip-location\"><div class=\"trip-icon red\"></div> <span>" + viaje.direccionDestino + "</span></div>\n                            </div>\n                        </div>\n                        <div class=\"trip-data\">\n                            <div class=\"trip-route-unit\">\n                                <div class=\"trip-route trip-box\">\n                                    <div class=\"trip-title\">Ruta</div>\n                                    <span>" + viaje.nombreRuta + "</span>\n                                </div>\n                                <div class=\"trip-unit trip-box\">\n                                    <div class=\"trip-title\">Unidad</div>\n                                    <span>" + viaje.nombreUnidad + "</span>\n                                </div>\n                            </div>\n                            <div class=\"trip-driver\">\n                                <div class=\"trip-start-time trip-box\">\n                                    <div class=\"trip-title\">Hora de Partida</div>\n                                    <span>" + f_inicio.format("hh:mm A") + "</span>\n                                </div>\n                                " + (this.isDriver == false ? "\n                                <div class=\"trip-driver-mugshot trip-box\">\n                                    <div class=\"trip-title\">Conductor</div>\n                                    <div class=\"trip-driver-info\">\n                                        <div class=\"trip-driver-image\" style=\"background:url('" + viaje.conductor.photo_url + "') no-repeat center center;background-size:cover;\"></div>\n                                        <div class=\"trip-driver-name\">\n                                            <span>" + viaje.conductor.nombre + "</span>\n                                            <div><span><i class=\"fas fa-star\"></i> " + viaje.conductor.estrellas.toFixed(2) + "</span></div>\n                                        </div>\n                                    </div>\n                                </div>" :
            "<div class=\"trip-route-time trip-box\">\n                                    <div class=\"trip-title\">Distancia</div>\n                                    <span>" + this.calcRouteLength(viaje.ruta.puntos) + " km</span>\n                                </div>") + "\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"buttons_checkin center-align\">\n                <button class=\"btn waves-effect waves-light\" onclick=\"TE.ConfirmCheckIn()\">\n                    Confirmar\n                </button>\n                <button class=\"btn waves-effect waves-light red\" onclick=\"TE.CancelCheckIn()\">\n                    Cancelar\n                </button>\n            </div>\n        ");
        this.drawTripRoute(viaje.ruta.puntos);
        this.routeMapPadding();
        closeWait();
    };
    /**
     * DriverStartTrip
     */
    TEAppClass.prototype.DriverStartTrip = function (tripId) {
        confirmar("Iniciar Viaje", "<b>¿Está seguro/a de iniciar este viaje seleccionado?</b>", "Confirmar", "Cancelar", "TE.ConfirmDriverStartTrip(" + tripId + ")");
    };
    /**
     * ConfirmDriverStartTrip
     */
    TEAppClass.prototype.ConfirmDriverStartTrip = function (tripId) {
        var _this = this;
        wait("Confirmando inicio de viaje...", "Por favor, espere.");
        this.ajax("POST", this.apiUrl + "driver/Trips/start", tripId, function (data, resp, xhr) {
            if (xhr.status == 200) {
                var tripIndex = objectIndex(_this.tripList, "id", tripId);
                $(".teapp_label_endtrip").addClass("driver_end");
                $("#map .trip_card").removeClass("current_trip");
                $("#map .trip_card").html('').addClass("hidden");
                _this.ruta = ('idViajeConductor' in data ? data.idViajeConductor : -1);
                _this.currentTrip = tripId;
                _this.tripData = _this.tripList[tripIndex];
                _this.viajeEnCurso = true;
                _this.nombreUnidad = _this.tripData.nombreUnidad;
                _this.destroyTripTimes();
                _this.CloseHorario();
                _this.tripStartTime = new moment();
                _this.RastreoContinuo();
            }
            else {
                alerta("Error de Confirmación", "Lo sentimos, no ha sido posible confirmar el inicio del viaje (-1).");
            }
            closeWait();
        }, function (err) {
            if (err.status == 404) {
                alerta("Error de Confirmación", "Lo sentimos, este viaje fue eliminado o no existe (-4).");
            }
            else {
                alerta("Error de Confirmación", "Lo sentimos, no ha sido posible confirmar el inicio del viaje (-2).");
            }
            closeWait();
        });
    };
    /**
     * destroyTripTimes
     */
    TEAppClass.prototype.destroyTripTimes = function () {
        $("#main .mainTop").removeClass("no-logo");
        $("#main .mainTop .title").html("");
        $("#close-btn").addClass("hidden");
        $("#te_horario").removeClass("hidden");
        if (this.viajeEnCurso == false) {
            $("#map .trip_card").removeClass("current_trip");
            $("#map .trip_card").html('').addClass("hidden");
        }
    };
    /**
     * ConfirmCheckIn
     */
    TEAppClass.prototype.ConfirmCheckIn = function () {
        var _this = this;
        if (this.isDriver) {
            $(".teapp_label_endtrip").addClass("driver_end");
            wait("Confirmando abordaje de pasajero...", "Por favor, espere.");
        }
        else {
            wait("Confirmando abordaje...", "Por favor, espere.");
        }
        this.ajax("POST", this.apiUrl + "passenger/Trips/preview/start", this.currentTrip, function (data, resp, xhr) {
            switch (xhr.status) {
                case 200:
                    _this.ruta = ('idViajePasajero' in data ? data.idViajePasajero : -1);
                    _this.viajeEnCurso = true;
                    _this.destroyCheckIn();
                    _this.CloseCheckIn();
                    _this.displayCurrentLiveTrip(_this.currentTrip);
                    _this.RastreoContinuo();
                    break;
                default:
                    alerta("Error de Abordaje", "Lo sentimos, no ha sido posible confirmar el abordaje; aun asi aborde el transporte (-1).");
                    break;
            }
            closeWait();
        }, function (err) {
            switch (err.status) {
                case 404:
                    alerta("Error de Abordaje", "Lo sentimos, este viaje no existe o ya concluyó (-4).");
                    break;
                default:
                    alerta("Error de Abordaje", "Lo sentimos, no ha sido posible confirmar el abordaje; aun asi aborda el transporte (-2).");
                    break;
            }
            closeWait();
        });
    };
    /**
     * displayCurrentLiveTrip
     */
    TEAppClass.prototype.displayCurrentLiveTrip = function (tripId) {
        this.processQRTrip(tripId);
        this.AdjustTitle("");
        $("#close-btn").addClass("hidden");
        $("#main .mainTop").removeClass("no-logo");
        $("#map .trip_card .buttons_checkin").html("\n            <button class=\"btn waves-effect waves-light zeekColor current-trip-options\" onclick=\"TE.CurrentTripOptions()\"></button>\n            <button class=\"btn waves-effect waves-light red just-icon\" onclick=\"TE.PanicButton()\"><i class=\"fas fa-exclamation-triangle\"></i></button>\n        ").addClass("current-trip-buttons");
    };
    /**
     * CurrentTripOptions
     */
    TEAppClass.prototype.CurrentTripOptions = function () {
        $("#trip_options").removeClass("hidden");
    };
    /**
     * CloseTripOptions
     */
    TEAppClass.prototype.CloseTripOptions = function () {
        $("#trip_options").addClass("hidden");
    };
    /**
     * CurrentTripSecurity
     */
    TEAppClass.prototype.CurrentTripSecurity = function () {
        $("#trip_security").removeClass("hidden");
    };
    /**
     * CloseSecurityOptions
     */
    TEAppClass.prototype.CloseSecurityOptions = function () {
        if ($("#trip_report").is(":visible")) {
            $("#trip_report").addClass("hidden");
            $("#trip_security").removeClass("hidden");
        }
        else {
            $("#trip_security").addClass("hidden");
        }
    };
    /**
     * ReportTrip
     */
    TEAppClass.prototype.ReportTrip = function () {
        $("#reportBody").val('');
        $("#reportBody,#reportReason").attr("disabled", false);
        M.textareaAutoResize($('#reportBody'));
        $("#reportReason").html('<option selected disabled>Elija una razón</option>');
        this.reportSubjects.forEach(function (razon) {
            $("#reportReason").append("<option>" + razon + "</option>");
        });
        $("#trip_security").addClass("hidden");
        $("#trip_report").removeClass("hidden");
    };
    /**
     * OpenViajes
     */
    TEAppClass.prototype.OpenViajes = function () {
        var _this = this;
        selectMenu(".tea-viajes-menu");
        hideActionCircle();
        masterClose = function () { _this.CloseViajes(); };
        $("#te_viajes").removeClass("hidden");
        this.requestTrips();
        $("#te_viajes .subVista").scrollTop(0);
    };
    /**
     * CloseViajes
     */
    TEAppClass.prototype.CloseViajes = function (forceClose) {
        if (forceClose === void 0) { forceClose = false; }
        if ($("#map .trip_card").is(":visible") && $("#te_viajes").is(":visible") == false && forceClose == false) {
            this.CloseTrip();
        }
        else {
            $("#te_viajes .tripList").html('');
            $("#te_viajes").addClass("hidden");
            resetBack();
            if (this.viajeEnCurso) {
                this.displayCurrentLiveTrip(this.currentTrip);
            }
            else {
                this.ShowSuggestion();
            }
        }
    };
    /**
     * requestTrips
     */
    TEAppClass.prototype.requestTrips = function () {
        var _this = this;
        this.tripList = [];
        wait("Obteniendo viajes...", "Por favor espere");
        this.ajax("GET", this.apiUrl + ((this.isDriver ? 'driver' : 'passenger') + "/Trips/fulfilled"), null, function (data, resp, xhr) {
            if (xhr.status == 200) {
                _this.tripList = _this.constructTripData(data);
            }
            else {
                toasty("Lo sentimos, no fue posible obtener tus viajes pasados (-1).", "error");
            }
            _this.loadTrips();
        }, function (err) {
            _this.loadTrips();
            toasty("Lo sentimos, no fue posible obtener tus viajes pasados (-2).", "error");
        });
    };
    /**
     * loadTrips
     */
    TEAppClass.prototype.loadTrips = function () {
        var _this = this;
        var cont = 1;
        $("#te_viajes .tripList").html('');
        var anio = new Date().getFullYear();
        this.tripList.sort(function (a, b) { return new Date(b.fechaInicio) - new Date(a.fechaInicio); });
        var pastDate = "", viajes = 0;
        this.tripList.forEach(function (viaje, tripId) {
            var f_inicio = UTC2Local(viaje.fechaInicio), f_destino = UTC2Local(viaje.fechaDestino);
            if (_this.isDriver == false) {
                viaje.conductor = _this.constructDriverData(viaje.conductor);
            }
            viajes++;
            if (pastDate != f_inicio.format("MM-DD-YYYY")) {
                $("#te_viajes .tripList").append("\n                    <div class=\"horario_date\"><div>" + f_inicio.format((f_inicio.format("Y") != anio ? "DD [de] MMMM [de] YYYY" : "DD [de] MMMM")) + "</div></div>\n                ");
                pastDate = f_inicio.format("MM-DD-YYYY");
            }
            $("#te_viajes .tripList").append("\n                <div class=\"carta\" onclick=\"TE.OpenTrip(" + viaje.id + ")\">\n                    <div class=\"trip-route-img tripid-" + viaje.id + "\"></div>\n                    <div class=\"trip-container\">\n                        <div class=\"trip-basic-data\">\n                            <div class=\"trip-info\">\n                                <div class=\"trip-place trip-start\">\n                                    <div class=\"trip-location\"><div class=\"trip-icon green\"></div> <span>" + viaje.direccionInicio + "</span></div>\n                                    <div class=\"trip-date\"><div class=\"trip-divider\"></div><span>" + f_inicio.format("hh:mm A") + "</span></div>\n                                </div>\n                                <div class=\"trip-divider\"></div>\n                                <div class=\"trip-place trip-end\">\n                                    <div class=\"trip-location\"><div class=\"trip-icon red\"></div> <span>" + viaje.direccionDestino + "</span></div>\n                                    <div class=\"trip-date\">" + f_destino.format("hh:mm A") + "</div>\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"trip-chevron\">\n                            <i class=\"fas fa-chevron-right\"></i>\n                        </div>\n                    </div>\n                </div>\n            ");
            setTimeout(function () {
                _this.renderRouteMap(viaje.id);
            }, 10 * cont);
            cont++;
        });
        if (viajes == 0) {
            $("#te_viajes .noContent").removeClass("hidden");
        }
        else {
            $("#te_viajes .noContent").addClass("hidden");
        }
        closeWait();
    };
    /**
     * renderRouteMap
     */
    TEAppClass.prototype.renderRouteMap = function (tripId) {
        var tripLocation = objectIndex(this.tripList, "id", tripId);
        var url = "https://image.maps.ls.hereapi.com/mia/1.6/route?apiKey=" + hereApiKey + this.constructRouteMap(tripId, this.tripList[tripLocation].ruta.puntos);
        //console.warn("Route IMG",tripId,url);
        cacheImg(url, tripId, true).then(function (mapUrl) {
            $(".tripid-" + tripId).css({ "background": "url(" + mapUrl + ") no-repeat center center", "background-size": "cover" });
        });
    };
    /**
     * shortRoute
     */
    TEAppClass.prototype.shortRoute = function (tripId) {
        var tripLocation = objectIndex(this.tripList, "id", tripId);
        var origRoute = JSON.parse(JSON.stringify(this.tripList[tripLocation].ruta));
        var shortenedRoute = [];
        shortenedRoute.push(origRoute[0]); //PUNTO INICIAL
        for (var shIndex = 1; shIndex < origRoute.length; shIndex++) {
            if (shIndex % 2 != 0) {
                shortenedRoute.push(origRoute[shIndex]);
            }
        }
        var url = "https://image.maps.ls.hereapi.com/mia/1.6/route?apiKey=" + hereApiKey + this.constructRouteMap(tripId, shortenedRoute);
        //console.warn("Route IMG",tripId,url);
        cacheImg(url, tripId, false).then(function (mapUrl) {
            $(".tripid-" + tripId).css({ "background": "url(" + mapUrl + ") no-repeat center center", "background-size": "cover" });
        });
    };
    /**
     * OpenTrip
     */
    TEAppClass.prototype.OpenTrip = function (tripId) {
        this.HideSuggestion();
        this.cleanRoute(true);
        $("#te_viajes").addClass("hidden");
        $("#map .trip_card,#close-btn").removeClass("hidden");
        $("#close-btn").attr("onclick", "TE.CloseViajes()");
        $("#main .mainTop").addClass("no-logo");
        this.AdjustTitle("Información de Viaje");
        $(".mainTop .fade,#map .fade").hide();
        var anio = new Date().getFullYear();
        var tripIndex = objectIndex(this.tripList, "id", tripId);
        var viaje = this.tripList[tripIndex];
        this.drawTripRoute(viaje.ruta.puntos);
        if (this.isDriver == false) {
            viaje.conductor = this.constructDriverData(viaje.conductor);
        }
        var f_inicio = UTC2Local(viaje.fechaInicio);
        var f_destino = UTC2Local(viaje.fechaDestino);
        $("#map .trip_card").html("\n                <div class=\"carta\">\n                    <div class=\"trip-container\">\n                        <div class=\"trip-basic-data\">\n                            <div class=\"trip-info\">\n                                <div class=\"trip-place trip-start\">\n                                    <div class=\"trip-location\"><div class=\"trip-icon green\"></div> <span>" + viaje.direccionInicio + "</span></div>\n                                    <div class=\"trip-date\"><div class=\"trip-divider\"></div><span>" + f_inicio.format((f_inicio.format("Y") != anio ? "MMM DD YYYY, hh:mm A" : "MMM DD, hh:mm A")) + "</span></div>\n                                </div>\n                                <div class=\"trip-divider\"></div>\n                                <div class=\"trip-place trip-end\">\n                                    <div class=\"trip-location\"><div class=\"trip-icon red\"></div> <span>" + viaje.direccionDestino + "</span></div>\n                                    <div class=\"trip-date\">" + f_destino.format((f_destino.format("Y") != anio ? "MMM DD YYYY, hh:mm A" : "MMM DD, hh:mm A")) + "</div>\n                                </div>\n                            </div>\n                            <div class=\"trip-data\">\n                                <div class=\"trip-route-unit\">\n                                    <div class=\"trip-route trip-box\">\n                                        <div class=\"trip-title\">Ruta</div>\n                                        <span>" + viaje.nombreRuta + "</span>\n                                    </div>\n                                    <div class=\"trip-unit trip-box\">\n                                        <div class=\"trip-title\">Unidad</div>\n                                        <span>" + viaje.nombreUnidad + "</span>\n                                    </div>\n                                </div>\n                                " + (this.isDriver == false ? "\n                                <div class=\"trip-driver\">\n                                    <div class=\"trip-driver-mugshot trip-box\">\n                                        <div class=\"trip-title\">Conductor</div>\n                                        <div class=\"trip-driver-info\">\n                                            <div class=\"trip-driver-image\" style=\"background:url('" + viaje.conductor.photo_url + "') no-repeat center center;background-size:cover;\"></div>\n                                            <div class=\"trip-driver-name\">\n                                                <span>" + viaje.conductor.nombre + "</span>\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"trip-driver-stars trip-box\">\n                                        <div class=\"trip-title\">Calificaci\u00F3n</div>\n                                        <span>" + this.renderStars(viaje.calificacion) + "</span>\n                                    </div>\n                                </div>" : "") + "\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            ");
        this.routeMapPadding();
    };
    /**
     * routeMapPadding
     */
    TEAppClass.prototype.routeMapPadding = function () {
        if (this.drawnRoute == true) {
            if (isLandscape) {
                map.getViewPort().setPadding(0, $("#map .trip_card").outerWidth(), 0, 0);
            }
            else {
                map.getViewPort().setPadding(0, 0, $("#map .trip_card").outerHeight(), 0);
            }
            map.setViewBounds(this.routeMarkerGroup.getBounds(), false);
        }
        this.AdjustPanicPill();
    };
    /**
     * CloseTrip
     */
    TEAppClass.prototype.CloseTrip = function () {
        var _this = this;
        this.drawnRoute = false;
        this.cleanRoute(true);
        $("#main .mainTop").removeClass("no-logo");
        $("#main .mainTop .title").html("");
        $("#map .trip_card").html('').addClass("hidden");
        $("#close-btn").addClass("hidden");
        $("#te_viajes").removeClass("hidden");
        try {
            masterClose = function () { _this.CloseViajes(); };
        }
        catch (e) { }
    };
    /**
     * cleanRoute
     */
    TEAppClass.prototype.cleanRoute = function (forceClean) {
        if (forceClean === void 0) { forceClean = false; }
        if (this.viajeEnCurso == false || forceClean == true) {
            this.routeMarkerGroup.removeAll();
            this.drawnRoute = false;
            this.unitMarker = null;
            this.unitLabel = null;
        }
    };
    /**
     * logoutClean
     */
    TEAppClass.prototype.logoutClean = function () {
        this.viajeEnCurso = false;
        this.cleanRoute(true);
        $("#map .trip_card").removeClass("current_trip");
        $("#map .trip_card").html('').addClass("hidden");
    };
    /**
     * drawTripRoute
     */
    TEAppClass.prototype.drawTripRoute = function (tripLocations) {
        this.drawnRoute = true;
        var viaje = tripLocations;
        console.log("DRAWING TRIP ROUTE");
        var puntos = new H.geo.LineString();
        for (var j = 0; j < viaje.length; j++) {
            puntos.pushPoint({ lat: viaje[j].latitud, lng: viaje[j].longitud });
        }
        //var color = ColorLuminance(getRandomColor(),-0.4);
        var color = "#4a62bc";
        this.routePolyLine = new H.map.Polyline(puntos, {
            style: {
                lineWidth: 6,
                strokeColor: hexToRgbA(color, 0.85),
                fillColor: hexToRgbA(color, 0.85),
            },
            arrows: {
                fillColor: 'white',
                frequency: 15,
                width: 2,
                length: 1.8
            }
        });
        this.routeMarkerGroup.addObject(this.routePolyLine);
        //PIN DE INICIO
        var iconElem = this.CreateDomIcon("img/iconos/pin-start.png", "startend");
        var startMarker = new H.map.DomMarker({ lat: viaje[0].latitud, lng: viaje[0].longitud }, {
            icon: new H.map.DomIcon(iconElem)
        });
        var labelElem = this.CreateDomLabel(lang("route_start"), "point");
        var startLabel = new H.map.DomMarker({ lat: viaje[0].latitud, lng: viaje[0].longitud }, {
            min: 11,
            icon: new H.map.DomIcon(labelElem)
        });
        startMarker.setZIndex(0);
        startLabel.setZIndex(0);
        this.routeMarkerGroup.addObject(startMarker);
        this.routeMarkerGroup.addObject(startLabel);
        //PIN DE META
        var iconElem = this.CreateDomIcon("img/iconos/pin-end.png", "startend");
        var endMarker = new H.map.DomMarker({ lat: viaje[viaje.length - 1].latitud, lng: viaje[viaje.length - 1].longitud }, {
            icon: new H.map.DomIcon(iconElem)
        });
        var labelElem = this.CreateDomLabel(lang("route_end"), "point");
        var endLabel = new H.map.DomMarker({ lat: viaje[viaje.length - 1].latitud, lng: viaje[viaje.length - 1].longitud }, {
            min: 11,
            icon: new H.map.DomIcon(labelElem)
        });
        endMarker.setZIndex(0);
        endLabel.setZIndex(0);
        this.routeMarkerGroup.addObject(endMarker);
        this.routeMarkerGroup.addObject(endLabel);
        map.setViewBounds(this.routeMarkerGroup.getBounds(), false);
        try {
            this.routeMarkerGroup.removeEventListener('tap', TE.FocusOnMarker, false);
        }
        catch (e) { }
        this.routeMarkerGroup.addEventListener('tap', TE.FocusOnMarker, false);
        //this.RenderTrackingMarker(); 
    };
    /**
     * FocusOnMarker
     */
    TEAppClass.prototype.FocusOnMarker = function (evt) {
        map.setCenter(evt.target.getPosition());
        map.setZoom(17);
    };
    /**
     * EndLiveTrip
     */
    TEAppClass.prototype.EndLiveTrip = function () {
        if (this.isDriver) {
            confirmar("Terminar Viaje", "Al confirmar que desea terminar el viaje acepta que ha llegado a su punto de destino y todos los pasajeros han bajado de la unidad, <b><u>¿está seguro/a de hacerlo?</u></b>", "Confirmar", "Cancelar", "TE.ConfirmEndTrip()");
        }
        else {
            confirmar("Terminar Viaje", "Al confirmar que desea terminar el viaje acepta que bajará o bajó de la unidad, <b><u>¿está seguro/a de hacerlo?</u></b>", "Confirmar", "Cancelar", "TE.ConfirmEndTrip()");
        }
    };
    /**
     * ConfirmEndTrip
     */
    TEAppClass.prototype.ConfirmEndTrip = function () {
        var _this = this;
        wait("Terminando viaje...", "Por favor, espere.");
        this.ajax("POST", "" + this.apiUrl + (this.isDriver ? 'driver' : 'passenger') + "/Trips/finish", this.ruta, function (data, resp, xhr) {
            if (xhr.status == 200) {
                _this.tripStartTime = UTC2Local(data.fechaInicio);
                _this.tripEndTime = UTC2Local(data.fechaDestino);
                if (_this.isDriver == false) {
                    _this.rateId = ('idViajePasajeroHistorial' in data ? data.idViajePasajeroHistorial : -1);
                }
                else {
                    _this.rateId = data.idViajeConductorHistorial;
                }
                _this.CloseTripOptions();
                _this.viajeEnCurso = false;
                $("#map .trip_card").removeClass("current_trip");
                $("#map .trip_card").html('').addClass("hidden");
                _this.cleanRoute();
                $("#trip_summary").removeClass("hidden");
                _this.FillTripSummary();
                _this.DeactivatePanicSignal();
            }
            else {
                if (_this.isDriver) {
                    alerta("Lo sentimos, no ha sido posible terminar el viaje. Vuelva a intentar de nuevo, si sigue con el problema por favor contacte a la Administración de la plataforma (-1).");
                }
                else {
                    alerta("Lo sentimos, no ha sido posible terminar el viaje. Baje de la unidad y vuelva a intentar de nuevo (-1).");
                }
            }
            closeWait();
        }, function (err) {
            toasty("Lo sentimos, no ha sido posible terminar el viaje, revise su conexión e intente de nuevo (-2).", "error");
            closeWait();
        });
    };
    /**
     * FillTripSummary
     */
    TEAppClass.prototype.FillTripSummary = function () {
        var viaje = this.tripData;
        if (this.isDriver) {
            var tiempo = msToTime(this.tripEndTime.diff(this.tripStartTime));
            var distancia = this.calcRouteLength(viaje.ruta.puntos);
            viaje.conductor = this.constructDriverData(viaje.conductor);
            $("#trip_summary .teapp_box_options").html("\n                <div class=\"trip_end_driver_message\">" + this.getDriverEndCongrats() + "</div>\n                <div class=\"trip_end_driver_info\">\n                    <div class=\"trip_end_info_row\">\n                        <div class=\"trip_end_info_label\">Tiempo</div>\n                        <div class=\"trip_end_info_value\">" + tiempo + "</div>\n                    </div>\n                    <div class=\"trip_end_info_row\">\n                        <div class=\"trip_end_info_label\">Distancia</div>\n                        <div class=\"trip_end_info_value\">" + distancia + " km</div>\n                    </div>\n                </div>\n                <div class=\"trip_end_report_label\" onclick=\"TE.ToggleReportModule()\">Agregar Reporte de Viaje<span><i class=\"fas fa-chevron-right\"></i></span></div>\n                <div class=\"row trip_end_report_box hidden\">\n                    <div class=\"col input-field\">\n                        <textarea id=\"trip_end_report_details\" class=\"materialize-textarea\"></textarea>\n                        <label for=\"trip_end_report_details\">Descripci\u00F3n de Reporte</label>\n                        <button class=\"btn waves-effect waves-light zeekColor full-btn send-driver-report-btn\" onclick=\"TE.SendDriverReport()\">Enviar</button>\n                    </div>\n                </div>\n                <button class=\"btn waves-effect waves-light zeekColor close-trip-summary-btn\" onclick=\"TE.CloseTripSummary()\">Cerrar</button>\n            ");
        }
        else {
            $("#trip_summary .teapp_box_options").html("\n                <div class=\"trip_end_avatar\" style=\"background:url('" + viaje.conductor.photo_url + "') no-repeat center center;background-size:cover;\"></div>\n                <div class=\"trip_end_drivername\">" + viaje.conductor.nombre + "</div>\n                <div class=\"trip_end_rating\">\n                    <div class=\"rate_message\">Otorga una calificaci\u00F3n al Conductor</div>\n                    <div class=\"rate_stars\">\n                        <div class=\"rate_star\" star=\"1\" message=\"P\u00E9simo\"></div>\n                        <div class=\"rate_star\" star=\"2\" message=\"Malo\"></div>\n                        <div class=\"rate_star\" star=\"3\" message=\"Regular\"></div>\n                        <div class=\"rate_star\" star=\"4\" message=\"Bueno\"></div>\n                        <div class=\"rate_star\" star=\"5\" message=\"Excelente\"></div>\n                    </div>\n                </div>\n                <div class=\"trip_end_report_label\" onclick=\"TE.ToggleReportModule()\">Agregar Comentario<span><i class=\"fas fa-chevron-right\"></i></span></div>\n                <div class=\"row trip_end_report_box hidden\">\n                    <div class=\"col input-field\">\n                        <textarea id=\"trip_end_report_passenger_details\" class=\"materialize-textarea\"></textarea>\n                        <label for=\"trip_end_report_passenger_details\">Introduzca aqu\u00ED su Comentario</label>\n                    </div>\n                </div>\n                <button class=\"btn waves-effect waves-light zeekColor disabled rating-btn\" onclick=\"TE.SendRating()\">Enviar</button>\n            ");
        }
    };
    /**
     * getDriverEndCongrats
     */
    TEAppClass.prototype.getDriverEndCongrats = function () {
        return this.congrats[rand(0, this.congrats.length - 1)];
    };
    /**
     * SendDriverReport
     */
    TEAppClass.prototype.SendDriverReport = function () {
        var _this = this;
        var reportBody = trim($("#trip_end_report_details").val());
        if (reportBody == "") {
            toasty("Por favor escriba la descripción del reporte de viaje a enviar.", "warn");
        }
        else {
            toasty("Enviando reporte, por favor espere...");
            $(".send-driver-report-btn,#trip_end_report_details").addClass("disabled");
            this.ajax("POST", this.apiUrl + 'driver/trips/report', {
                id: this.rateId,
                report: reportBody
            }, function (data, resp, xhr) {
                if (xhr.status == 200) {
                    $(".trip_end_report_label")
                        .html("<div class=\"trip_report_sent\">Reporte de Viaje enviado:</div><div class=\"trip_report_body\">" + reportBody + "</div>")
                        .attr("onclick", "")
                        .removeClass("report-open")
                        .addClass("report-sent");
                    _this.ToggleReportModule(true);
                    toasty("Reporte de Fin de Viaje enviado con éxito.", "success");
                }
                else {
                    toasty("Lo sentimos, el reporte no ha podido ser enviado (-1).", "error");
                }
                $(".send-driver-report-btn,#trip_end_report_details").removeClass("disabled");
            }, function (err) {
                toasty("Lo sentimos, el reporte no ha podido ser enviado, revise su conexión e intente de nuevo (-2).", "error");
                $(".send-driver-report-btn,#trip_end_report_details").removeClass("disabled");
            });
        }
    };
    /**
     * ToggleReportModule
     */
    TEAppClass.prototype.ToggleReportModule = function (forceHide) {
        if (forceHide === void 0) { forceHide = false; }
        if ($(".trip_end_report_box").is(":visible") || forceHide == true) {
            $(".trip_end_report_label").removeClass("report-open");
            $(".trip_end_report_label span").html('<i class="fas fa-chevron-right"></i>');
            $(".close-trip-summary-btn").removeClass("hidden");
            $(".trip_end_report_box").addClass("hidden");
        }
        else {
            $(".trip_end_report_label").addClass("report-open");
            $(".trip_end_report_label span").html('<i class="fas fa-chevron-down"></i>');
            $(".close-trip-summary-btn").addClass("hidden");
            $(".trip_end_report_box").removeClass("hidden");
        }
    };
    /**
     * CloseTripSummary
     */
    TEAppClass.prototype.CloseTripSummary = function () {
        var _this = this;
        $("#trip_summary").addClass("hidden");
        setTimeout(function () {
            _this.ShowSuggestion();
        }, 3000);
    };
    /**
     * SendRating
     */
    TEAppClass.prototype.SendRating = function () {
        var _this = this;
        toasty("Enviando calificación...");
        $(".rating-btn").addClass("disabled");
        this.ajax("POST", this.apiUrl + "passenger/Trips/rate", {
            id: this.rateId,
            rating: $(".rate_star.pos").length,
            report: trim($("#trip_end_report_passenger_details").val())
        }, function (data, resp, xhr) {
            if (xhr.status == 200) {
                toasty("Calificación enviada con éxito.", "success");
                $("#trip_summary").addClass("hidden");
                $("#trip_summary .teapp_box_options").html('');
                try {
                    map.setViewBounds(Tracking._markerGroup.getBounds());
                }
                catch (e) { }
                setTimeout(function () {
                    _this.ShowSuggestion();
                }, 3000);
            }
            else {
                toasty("Lo sentimos, no ha sido posible enviar la calificación/reporte. Por favor, vuelva a intentar después (-1).", "error");
                $(".rating-btn").removeClass("disabled");
            }
        }, function (err) {
            $(".rating-btn").removeClass("disabled");
            toasty("Lo sentimos, no ha sido posible enviar la calificación/reporte. Por favor, revise su conexión y vuelva a intentar (-2).", "error");
        });
    };
    /**
     * RenderTrackingMarker
     */
    TEAppClass.prototype.RenderTrackingMarker = function (nombreUnidad, _lat, _lng) {
        if (this.viajeEnCurso) {
            if (this.unitMarker == null) {
                var iconElem = Track.CreateDomIcon("img/teapp/teapp_unit.png", "teapp_unit");
                this.unitMarker = new H.map.DomMarker({ lat: _lat, lng: _lng }, {
                    icon: new H.map.DomIcon(iconElem),
                    zIndex: 1
                });
                this.routeMarkerGroup.addObject(this.unitMarker);
                var iconLabel = this.CreateDomLabel(nombreUnidad, 'icon');
                this.unitLabel = new H.map.DomMarker({ lat: _lat, lng: _lng }, {
                    zIndex: 1,
                    icon: new H.map.DomIcon(iconLabel)
                });
                this.routeMarkerGroup.addObject(this.unitLabel);
            }
            else {
                this.unitMarker.setPosition({ lat: _lat, lng: _lng });
                this.unitLabel.setPosition({ lat: _lat, lng: _lng });
            }
        }
    };
    /**
     * RastreoContinuo
     */
    TEAppClass.prototype.RastreoContinuo = function () {
        var _this = this;
        if (this.viajeEnCurso) {
            this.ajax("POST", "" + this.apiUrl + (this.isDriver ? 'driver' : 'passenger') + "/Units/track", this.ruta, function (data, resp, xhr) {
                if (xhr.status == 200) {
                    _this.RenderTrackingMarker(_this.nombreUnidad, data.ubication.latitude, data.ubication.longitude);
                }
                setTimeout(function () {
                    _this.RastreoContinuo();
                }, 10000);
            }, function (err) {
                setTimeout(function () {
                    _this.RastreoContinuo();
                }, 3000);
            });
        }
    };
    /**
     * Abre el Sidebar
     * @param section Seccion en especifico del Sidebar a abrir
     */
    TEAppClass.prototype.OpenSidebar = function (section) {
        $("#sidebar .toHide").hide();
        $("#sidebar .teappBlock").show();
        $("#sidebar,#close-btn").removeClass("hidden");
        $("#sidebar .teappBlock > div").hide();
        $(".teappBlock ." + section).show();
        $("#close-btn").attr("onclick", "TE.CloseSidebar('" + section + "')");
        $(".mainTop .tool.right").hide();
        views();
        Config.disableFullApp();
    };
    /**
     * Cierra el Sidebar y cancela la seccion en la que se encontraba
     * @param section Seccion del Sidebar a cancelar
     */
    TEAppClass.prototype.CloseSidebar = function (section) {
        $("#te_viajes").removeClass("hidden");
        $("#main .mainTop").removeClass("no-logo");
        $("#main .mainTop .title").html("");
        $("#sidebar,#add-btn,#close-btn").addClass("hidden");
        $(".teappBlock ." + section).hide();
        $(".undoButton").addClass("disabled");
        $("#sidebar").removeClass("emptylist");
        views();
        Config.enableFullApp();
    };
    /**
     * Cambia el titulo del top y del showcase
     * dependiendo del modulo abierto
     */
    TEAppClass.prototype.AdjustTitle = function (title) {
        $("#main .mainTop").addClass("no-logo");
        $("#main .mainTop.no-logo .title,.tap-title,.showcase-tap-title").html(title);
    };
    /**
     * constructRouteMap
     */
    TEAppClass.prototype.constructRouteMap = function (tripId, ruta) {
        var jsonQuery = {
            r: "",
            lc: "ff4a62bc",
            sc: "ff4a62bc",
            lw: 4,
            h: 150,
            w: parseInt($(".tripid-" + tripId).width()),
            noicon: true,
            style: "mini",
            ppi: 512,
            poithm: 1,
            t: 10,
            q: 100,
            nocp: true
        };
        var tripLen = ruta.length - 1;
        var posArray = [];
        ruta.forEach(function (pos) {
            if ('lat' in pos && 'lng' in pos) {
                posArray.push(pos.lat);
                posArray.push(pos.lng);
            }
            if ('latitud' in pos && 'longitud' in pos) {
                posArray.push(pos.latitud);
                posArray.push(pos.longitud);
            }
        });
        jsonQuery.r = posArray.join(",");
        if ('lat' in ruta[0] && 'lng' in ruta[0]) {
            jsonQuery.m = ruta[0].lat + "," + ruta[0].lng + "," + ruta[tripLen].lat + "," + ruta[tripLen].lng;
            jsonQuery.poix0 = ruta[0].lat + "," + ruta[0].lng + ";ff00cc00;;;\u2022";
            jsonQuery.poix1 = ruta[tripLen].lat + "," + ruta[tripLen].lng + ";ffdd0000;;;\u2022";
        }
        if ('latitud' in ruta[0] && 'longitud' in ruta[0]) {
            jsonQuery.m = ruta[0].latitud + "," + ruta[0].longitud + "," + ruta[tripLen].latitud + "," + ruta[tripLen].longitud;
            jsonQuery.poix0 = ruta[0].latitud + "," + ruta[0].longitud + ";ff00cc00;;;\u2022";
            jsonQuery.poix1 = ruta[tripLen].latitud + "," + ruta[tripLen].longitud + ";ffdd0000;;;\u2022";
        }
        return "&" + $.param(jsonQuery);
    };
    /**
     * renderStars
     */
    TEAppClass.prototype.renderStars = function (stars) {
        var percent = (stars / 5) * 100;
        return "<div class=\"trip-stars\"><div class=\"trip-stars-over\" style=\"width:" + percent + "%\"></div></div>";
    };
    /**
     * Crea icono en el DOM que puede personalizarse
     * @param carIcon Nombre del icono
     * @param auxClass Clase auxiliar para el icono
     */
    TEAppClass.prototype.CreateDomIcon = function (carIcon, auxClass) {
        if (auxClass === void 0) { auxClass = ""; }
        var icon = document.createElement('div');
        icon.className = "domIcon " + auxClass;
        icon.style.background = "url(" + carIcon + ") no-repeat center center";
        return icon;
    };
    /**
     * Crea un Label en el DOM que puede personalizarse
     * @param text Texto del Label
     * @param type Tipo del Label
     */
    TEAppClass.prototype.CreateDomLabel = function (text, type) {
        if (text === void 0) { text = "Test"; }
        if (type === void 0) { type = "icon"; }
        var _length = getLabelWidth(text, type);
        var label = document.createElement('div');
        label.className = "domLabel_" + type;
        label.style.left = ((_length / 2) * -1) + "px";
        label.innerHTML = text;
        return label;
    };
    /**
     * getRouteLength
     */
    TEAppClass.prototype.getRouteLength = function (routePoly) {
        var geometry = routePoly.getGeometry();
        var distance = 0;
        var last = geometry.extractPoint(0);
        for (var i = 1; i < geometry.getPointCount(); i++) {
            var point = geometry.extractPoint(i);
            distance += last.distance(point);
            last = point;
        }
        if (routePoly.isClosed()) {
            distance += last.distance(geometry.extractPoint(0));
        }
        return parseFloat((distance / 1000).toFixed(1));
    };
    /**
     * calcRouteLength
     */
    TEAppClass.prototype.calcRouteLength = function (ruta) {
        var puntos = new H.geo.LineString();
        for (var j = 0; j < ruta.length; j++) {
            puntos.pushPoint({ lat: ruta[j].latitud, lng: ruta[j].longitud });
        }
        var routePoly = new H.map.Polyline(puntos);
        return this.getRouteLength(routePoly);
    };
    /**
     * OpenHorario
     */
    TEAppClass.prototype.OpenHorario = function () {
        var _this = this;
        selectMenu(".tea-horario-menu");
        hideActionCircle();
        masterClose = function () { _this.CloseHorario(); };
        $("#te_horario").removeClass("hidden");
        this.requestTimes();
        $("#te_horario .subVista").scrollTop(0);
    };
    /**
     * CloseHorario
     */
    TEAppClass.prototype.CloseHorario = function (forceClose) {
        if (forceClose === void 0) { forceClose = false; }
        if ($("#map .trip_card").is(":visible") && $("#te_horario").is(":visible") == false && forceClose == false) {
            this.cleanRoute(true);
            $("#main .mainTop").removeClass("no-logo");
            $("#main .mainTop .title").html("");
            $("#close-btn").addClass("hidden");
            $("#map .trip_card").removeClass("current_trip");
            $("#map .trip_card").html('').addClass("hidden");
            $("#te_horario").removeClass("hidden");
        }
        else {
            $("#te_horario .tripList").html('');
            $("#te_horario").addClass("hidden");
            resetBack();
            if (this.viajeEnCurso) {
                this.displayCurrentLiveTrip(this.currentTrip);
            }
            else {
                this.ShowSuggestion();
            }
        }
    };
    /**
     * ShowSuggestion
     */
    TEAppClass.prototype.ShowSuggestion = function () {
        var _this = this;
        if (this.showingSuggestion == false && this.viajeEnCurso == false) {
            this.showingSuggestion = true;
            if (this.isDriver) {
                $(".trip_suggestion_passenger").addClass("hidden");
                $(".trip_suggestion_driver").removeClass("hidden");
                $(".trip_suggestion_passenger .teapp_box").fadeOut(0);
                $(".trip_suggestion_driver .teapp_box").fadeOut(0).fadeIn(function () {
                    _this.showingSuggestion = false;
                });
            }
            else {
                $(".trip_suggestion_driver .teapp_box").fadeOut(0);
                $(".trip_suggestion_passenger .teapp_box").fadeOut(0).fadeIn(function () {
                    _this.showingSuggestion = false;
                });
                $(".trip_suggestion_driver").addClass("hidden");
                $(".trip_suggestion_passenger").removeClass("hidden");
            }
        }
    };
    /**
     * HideSuggestion
     */
    TEAppClass.prototype.HideSuggestion = function () {
        $(".trip_suggestion_passenger,.trip_suggestion_driver").addClass("hidden");
    };
    /**
     * requestTimes
     */
    TEAppClass.prototype.requestTimes = function () {
        var _this = this;
        this.tripList = [];
        wait("Obteniendo Horario...", "Por favor espere");
        this.ajax("GET", "" + this.apiUrl + (this.isDriver ? 'driver' : 'passenger') + "/Trips/" + (this.isDriver ? 'assigned' : 'available'), (this.isDriver ? null : ''), function (data, resp, xhr) {
            if (xhr.status == 200) {
                _this.tripList = _this.constructTripData(data);
            }
            else {
                toasty("Lo sentimos, no ha sido posible obtener la lista de viajes.", "error");
            }
            _this.loadTimes();
        }, function (err) {
            toasty("Lo sentimos, no ha sido posible obtener la lista de viajes.", "error");
            closeWait();
            _this.loadTimes();
        });
    };
    /**
     * loadTimes
     */
    TEAppClass.prototype.loadTimes = function () {
        var _this = this;
        $("#te_horario .tripList").html('');
        var anio = new Date().getFullYear(), pastDate = "", viajes = 0;
        this.tripList.sort(function (b, a) { return new Date(b.fechaInicio) - new Date(a.fechaInicio); });
        this.tripList.forEach(function (viaje, i) {
            var f_inicio = UTC2Local(viaje.fechaInicio);
            var f_destino = UTC2Local(viaje.fechaDestino);
            /*if( moment(f_inicio).isAfter() &&
                (this.isDriver == false ||
                    (this.isDriver && userData.userID == viaje.idConductor)
                )
            ){*/
            if (f_inicio.isSame(new Date(), 'day')) {
                /*if(pastDate != f_inicio.format("MM-DD-YYYY") && this.isDriver == true) {
                    $("#te_horario .tripList").append(`
                        <div class="horario_date"><div>${f_inicio.format((f_inicio.format("Y") != anio ? "DD [de] MMMM [de] YYYY" : "DD [de] MMMM"))}</div></div>
                    `);
                    pastDate = f_inicio.format("MM-DD-YYYY");
                }*/
                viajes++;
                $("#te_horario .tripList").append("\n                        <div class=\"carta\" onclick=\"TE.PreviewTrip(" + viaje.id + ")\">\n                            <div class=\"trip-container\">\n                                <div class=\"trip-basic-data\">\n                                    <div class=\"trip-info\">\n                                        <div class=\"trip-place trip-start\">\n                                            <div class=\"trip-location\"><div class=\"trip-icon green\"></div> <span>" + viaje.direccionInicio + "</span></div>\n                                            <!--<div class=\"trip-date\"><div class=\"trip-divider\"></div><span>" + f_inicio.format((f_inicio.format("Y") != anio ? "MMM DD YYYY, hh:mm A" : "MMM DD, hh:mm A")) + "</span></div>-->\n                                        </div>\n                                        <div class=\"trip-divider\"></div>\n                                        <div class=\"trip-place trip-end\">\n                                            <div class=\"trip-location\"><div class=\"trip-icon red\"></div> <span>" + viaje.direccionDestino + "</span></div>\n                                            <!--<div class=\"trip-date\">" + f_destino.format((f_destino.format("Y") != anio ? "MMM DD YYYY, hh:mm A" : "MMM DD, hh:mm A")) + "</div>-->\n                                        </div>\n                                    </div>\n                                    <div class=\"trip-data\">\n                                        <div class=\"trip-route-unit\">\n                                            <div class=\"trip-route trip-box\">\n                                                <div class=\"trip-title\">Hora de Partida</div>\n                                                <span>" + f_inicio.format("hh:mm A") + "</span>\n                                            </div>\n                                            <div class=\"trip-unit trip-box\">\n                                                <div class=\"trip-title\">Distancia</div>\n                                                <span>" + _this.calcRouteLength(viaje.ruta.puntos) + " km</span>\n                                            </div>\n                                        </div>\n                                        <div class=\"trip-route-unit\">\n                                            <div class=\"trip-route trip-box\">\n                                                <div class=\"trip-title\">Ruta</div>\n                                                <span>" + viaje.nombreRuta + "</span>\n                                            </div>\n                                            <div class=\"trip-unit trip-box\">\n                                                <div class=\"trip-title\">Unidad</div>\n                                                <span>" + viaje.nombreUnidad + "</span>\n                                            </div>\n                                        </div>\n                                    \n                                    </div>\n                                </div>\n                                <div class=\"trip-chevron\">\n                                    <i class=\"fas fa-chevron-right\"></i>\n                                </div>\n                            </div>\n                        </div>\n                    ");
            }
            //}
        });
        if (viajes == 0) {
            $("#te_horario .noContent").removeClass("hidden");
        }
        else {
            $("#te_horario .noContent").addClass("hidden");
        }
        closeWait();
    };
    /**
     * PreviewTrip
     */
    TEAppClass.prototype.PreviewTrip = function (tripId) {
        var teId = objectIndex(this.tripList, "id", tripId);
        if (teId >= 0) { //VIAJE VALIDO
            this.HideSuggestion();
            this.cleanRoute(true);
            $("#te_horario").addClass("hidden");
            $("#map .trip_card,#close-btn").removeClass("hidden");
            $("#close-btn").attr("onclick", "TE.CloseHorario()");
            $("#main .mainTop").addClass("no-logo");
            $("#map .trip_card").addClass("current_trip");
            this.AdjustTitle("Vista Previa de Ruta");
            var viaje = this.tripList[teId];
            if (this.isDriver == false) {
                viaje.conductor = this.constructDriverData(viaje.conductor);
            }
            var f_inicio = UTC2Local(viaje.fechaInicio);
            $("#map .trip_card").html("\n                <div class=\"carta\">\n                    <div class=\"trip-container\">\n                        <div class=\"trip-basic-data\">\n                            <div class=\"trip-info\">\n                                <div class=\"trip-place trip-start\">\n                                    <div class=\"trip-location\"><div class=\"trip-icon green\"></div> <span>" + viaje.direccionInicio + "</span></div>\n                                </div>\n                                <div class=\"trip-divider\"></div>\n                                <div class=\"trip-place trip-end\">\n                                    <div class=\"trip-location\"><div class=\"trip-icon red\"></div> <span>" + viaje.direccionDestino + "</span></div>\n                                </div>\n                            </div>\n                            <div class=\"trip-data\">\n                                <div class=\"trip-route-unit\">\n                                    <div class=\"trip-route trip-box\">\n                                        <div class=\"trip-title\">Ruta</div>\n                                        <span>" + viaje.nombreRuta + "</span>\n                                    </div>\n                                    <div class=\"trip-unit trip-box\">\n                                        <div class=\"trip-title\">Unidad</div>\n                                        <span>" + viaje.nombreUnidad + "</span>\n                                    </div>\n                                </div>\n                                <div class=\"trip-driver\">\n                                    <div class=\"trip-start-time trip-box\">\n                                        <div class=\"trip-title\">Hora de Partida</div>\n                                        <span>" + f_inicio.format("hh:mm A") + "</span>\n                                    </div>\n                                    " + (this.isDriver ?
                "<div class=\"trip-route-distance trip-box\">\n                                            <div class=\"trip-title\">Distancia</div>\n                                            <div class=\"trip-driver-info\">\n                                                <span>" + this.calcRouteLength(viaje.ruta.puntos) + " km</span>\n                                            </div>\n                                        </div>" :
                "<div class=\"trip-driver-mugshot trip-box\">\n                                            <div class=\"trip-title\">Conductor</div>\n                                            <div class=\"trip-driver-info\">\n                                                <div class=\"trip-driver-image\" style=\"background:url('" + viaje.conductor.photo_url + "') no-repeat center center;background-size:cover;\"></div>\n                                                <div class=\"trip-driver-name\">\n                                                    <span>" + viaje.conductor.nombre + "</span>\n                                                    <div><span><i class=\"fas fa-star\"></i> " + viaje.conductor.estrellas.toFixed(2) + "</span></div>\n                                                </div>\n                                            </div>\n                                        </div>") + "\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                " + (this.isDriver ? "\n                <div class=\"buttons_checkin center-align\">\n                    <button class=\"btn waves-effect waves-light full-btn driver_starttrip_btn " + (this.viajeEnCurso ? "disabled" : "") + "\" onclick=\"" + (this.viajeEnCurso ? "" : "TE.DriverStartTrip(" + tripId + ")") + "\"></button>\n                </div>" : '') + "\n                \n            ");
            this.drawTripRoute(viaje.ruta.puntos);
            this.routeMapPadding();
        }
        else {
            //VIAJE INVALIDO
        }
    };
    /**
     * constructDriverData
     */
    TEAppClass.prototype.constructDriverData = function (data) {
        var resp = new Object();
        resp.id = ('id' in data ? data.id : -1);
        resp.nombre = data.nombre + " " + ('apellido' in data ? data.apellido : '');
        resp.nombre = ('nombreConductor' in data ? data.nombreConductor : resp.nombre);
        if ('photo_url' in data) {
            resp.photo_url = (data.photo_url == '' ? 'img/teapp/user_avatar.png' : data.photo_url);
        }
        else {
            resp.photo_url = 'img/teapp/user_avatar.png';
        }
        if ('rutaImagenPerfil' in data) {
            resp.photo_url = (data.rutaImagenPerfil == '' ? 'img/teapp/user_avatar.png' : data.rutaImagenPerfil);
        }
        else {
            resp.photo_url = 'img/teapp/user_avatar.png';
        }
        resp.estrellas = (data.calificacionPromedio == null || data.numeroCalificaciones == 0 ? 5 : data.calificacionPromedio);
        resp.estrellas = ('calificacion' in data ? data.calificacion : resp.estrellas);
        return resp;
    };
    /**
     * constructTripData
     */
    TEAppClass.prototype.constructTripData = function (data) {
        var _this = this;
        if (Array.isArray(data) == false) {
            if (data.nombreRuta == null) {
                data.nombreRuta = '---';
            }
            if ('idConductorNavigation' in data) {
                data.conductor = data.idConductorNavigation;
            }
            if (this.isDriver) {
                if ('idViajeConductor' in data) {
                    data.id = data.idViajeConductor;
                }
                if ('idViajeConductorHistorial' in data) {
                    data.id = data.idViajeConductorHistorial;
                }
            }
            else {
                if ('idViajeConductor' in data) {
                    data.id = data.idViajeConductor;
                }
                if ('idViajePasajero' in data) {
                    data.id = data.idViajePasajero;
                }
                if ('idViajePasajeroHistorial' in data) {
                    data.id = data.idViajePasajeroHistorial;
                }
            }
            if ('idRuta' in data == false) {
                data.idRuta = data.id;
            }
            if ('ruta' in data) {
                if ('tpParada' in data.ruta) {
                    data.paradas = data.tpParada;
                }
                if ('tpPuntosRuta' in data.ruta) {
                    data.puntos = data.tpPuntosRuta;
                }
            }
            else {
                data.ruta = new Object();
                data.ruta.puntos = data.puntos;
                data.ruta.paradas = data.paradas;
                if ('tpPuntosRutaHistorials' in data) {
                    data.ruta.puntos = data.tpPuntosRutaHistorials;
                }
                if ('tpParadasHistorials' in data) {
                    data.ruta.paradas = data.tpParadasHistorials;
                }
            }
        }
        else {
            data.forEach(function (trip, i) {
                if (trip.nombreRuta == null) {
                    trip.nombreRuta = '---';
                }
                if ('idConductorNavigation' in trip) {
                    trip.conductor = trip.idConductorNavigation;
                }
                if (_this.isDriver) {
                    if ('idViajeConductor' in trip) {
                        trip.id = trip.idViajeConductor;
                    }
                    if ('idViajeConductorHistorial' in trip) {
                        trip.id = trip.idViajeConductorHistorial;
                    }
                }
                else {
                    if ('idViajeConductor' in trip) {
                        trip.id = trip.idViajeConductor;
                    }
                    if ('idViajePasajero' in trip) {
                        trip.id = trip.idViajePasajero;
                    }
                    if ('idViajePasajeroHistorial' in trip) {
                        trip.id = trip.idViajePasajeroHistorial;
                    }
                }
                if ('idRuta' in trip == false) {
                    trip.idRuta = trip.id;
                }
                if ('ruta' in trip) {
                    if ('tpParada' in trip.ruta) {
                        trip.ruta.paradas = trip.ruta.tpParada;
                    }
                    if ('tpPuntosRuta' in trip.ruta) {
                        trip.ruta.puntos = trip.ruta.tpPuntosRuta;
                    }
                }
                else {
                    trip.ruta = new Object();
                    trip.ruta.puntos = trip.puntos;
                    trip.ruta.paradas = trip.paradas;
                    if ('tpPuntosRutaHistorials' in trip) {
                        trip.ruta.puntos = trip.tpPuntosRutaHistorials;
                    }
                    if ('tpParadasHistorials' in trip) {
                        trip.ruta.paradas = trip.tpParadasHistorials;
                    }
                }
            });
        }
        return data;
    };
    /**
     * fixUnitName
     */
    TEAppClass.prototype.fixUnitName = function (name) {
        return name.replace(/st/g, "");
    };
    /**
     * PanicButton
     */
    TEAppClass.prototype.PanicButton = function () {
        if (this.panicActive) {
            this.CloseSecurityOptions();
            this.ShowPanicBubble(0);
        }
        else {
            if (this.showingPanicPrompt == false) {
                this.showingPanicPrompt = true;
                confirmar("Activar Botón de Pánico", "<b>¿Está seguro/a de activar la alerta?</b> Su alerta será notificada y su ubicación actual se compartirá con el centro de seguridad.", "Activar", "Cancelar", "TE.ConfirmPanicButton()", "TE.CancelPanicButtonActivate()");
            }
        }
    };
    /**
     * CancelPanicButtonActivate
     */
    TEAppClass.prototype.CancelPanicButtonActivate = function () {
        this.showingPanicPrompt = false;
    };
    /**
     * ConfirmPanicButton
     */
    TEAppClass.prototype.ConfirmPanicButton = function () {
        if (this.panicActive) {
            this.ShowPanicBubble(0);
        }
        else {
            this.ActivatePanicSignal();
        }
    };
    /**
     * ActivatePanicSignal
     */
    TEAppClass.prototype.ActivatePanicSignal = function (repeatedSignal) {
        var _this = this;
        if (repeatedSignal === void 0) { repeatedSignal = false; }
        wait("Activando...", "Por favor espere.");
        this.sendPanicData(this.generatePanicData(), function (data, resp, xhr) {
            if (xhr.status == 200) {
                watchCurrentPosition(true);
                _this.panicActive = true;
                _this.ShowPanicPill();
                _this.panicSignalTimeout = setTimeout(function () {
                    _this.sendPanicSignal();
                }, 10000);
            }
            else {
                toasty("Lo sentimos, ha ocurrido un error al activar el botón de pánico, por favor intente de nuevo.", "error");
            }
            closeWait();
        }, function (err) {
            _this.showingPanicPrompt = false;
            closeWait();
            toasty("Lo sentimos, ha ocurrido un error al activar el botón de pánico, revise su conexión e intente de nuevo.", "error");
        });
    };
    /**
     * sendPanicData
     */
    TEAppClass.prototype.sendPanicData = function (panicData, successCB, errorCB) {
        this.ajax("POST", this.apiUrl + ((this.isDriver ? 'driver' : 'passenger') + "/panic/activate"), panicData, successCB, errorCB);
    };
    /**
     * sendPanicSignal
     */
    TEAppClass.prototype.sendPanicSignal = function () {
        var _this = this;
        this.sendPanicData(this.generatePanicData(), function (data, resp, xhr) {
            _this.panicSignalTimeout = setTimeout(function () {
                if (_this.panicActive) {
                    _this.sendPanicSignal();
                }
            }, 10000);
        }, function (err) {
            _this.panicSignalTimeout = setTimeout(function () {
                if (_this.panicActive) {
                    _this.sendPanicSignal();
                }
            }, 5000);
        });
    };
    /**
     * DeactivatePanicSignal
     */
    TEAppClass.prototype.DeactivatePanicSignal = function () {
        if (this.panicActive) {
            this.showingPanicPrompt = false;
            this.ajax("POST", this.apiUrl + ((this.isDriver ? 'driver' : 'passenger') + "/panic/deactivate"), this.generatePanicData(), function (data, resp, xhr) {
                if (xhr.status == 200) {
                    toasty("Alerta de Pánico deshabilitada.", "warn");
                    stopWatchingPosition();
                }
            }, function (err) {
            });
            this.panicActive = false;
            $("#panicUI").removeClass("show_panic");
            $("body").removeClass("panic-active");
            this.HidePanicBubble();
            clearTimeout(this.panicSignalTimeout);
        }
    };
    /**
     * generatePanicData
     */
    TEAppClass.prototype.generatePanicData = function () {
        var panicData = {
            latitud: (typeof window.myPosition == "undefined" ? null : window.myPosition.latitude),
            longitud: (typeof window.myPosition == "undefined" ? null : window.myPosition.longitude)
        };
        if (this.isDriver) {
            panicData.idViajeConductor = this.ruta;
        }
        else {
            panicData.idViajePasajero = this.ruta;
        }
        return panicData;
    };
    /**
     * ShowPanicPill
     */
    TEAppClass.prototype.ShowPanicPill = function () {
        $("body").addClass("panic-active");
        this.AdjustPanicPill();
        $("#panicUI").addClass("show_panic");
        this.ShowPanicBubble();
    };
    /**
     * ShowPanicBubble
     */
    TEAppClass.prototype.ShowPanicBubble = function (delay) {
        if (delay === void 0) { delay = 1500; }
        setTimeout(function () {
            $(".panic_bubble").addClass("show_bubble");
        }, delay);
    };
    /**
     * AdjustPanicPill
     */
    TEAppClass.prototype.AdjustPanicPill = function () {
        if ($(window).width() > $("html").height()) { //LANDSCAPE
            $("body.panic-active #panicUI").css("bottom", 10);
        }
        else {
            $("body.panic-active #panicUI").css("bottom", $(".trip_card.current_trip").outerHeight() - 5);
            setTimeout(function () {
                $("body.panic-active #panicUI").css("bottom", $(".trip_card.current_trip").outerHeight() - 5);
            }, 250);
        }
    };
    /**
     * HidePanicBubble
     */
    TEAppClass.prototype.HidePanicBubble = function () {
        $(".panic_bubble").removeClass("show_bubble");
    };
    /**
     * SendTripReport
     */
    TEAppClass.prototype.SendTripReport = function () {
        toasty("Enviando reporte...");
        $("#trip_report .btn").addClass("disabled");
        $("#reportBody,#reportReason").attr("disabled", true);
    };
    /**
     * decodeJWT
     */
    TEAppClass.prototype.decodeJWT = function (jwt) {
        var tokens = jwt.split(".");
        return JSON.parse(atob(tokens[1]));
    };
    /**
     * EnterRegToken
     */
    TEAppClass.prototype.EnterRegToken = function () {
        var _this = this;
        masterClose = function () { _this.ExitRegister(); };
        $("#reg_token").val('');
        $("#teapp_regtoken").removeClass("hidden");
    };
    /**
     * closeRegisterForm
     */
    TEAppClass.prototype.closeRegisterForm = function () {
        $("#teapp_register input").removeClass("valid invalid").val('');
        M.updateTextFields();
        $("#teapp_register,.teapp_reg_errors").addClass("hidden");
        $("#teapp_regtoken").removeClass("hidden");
        $("#teapp_register label").removeClass("active");
    };
    /**
     * ExitRegister
     */
    TEAppClass.prototype.ExitRegister = function (forceClose) {
        if (forceClose === void 0) { forceClose = false; }
        if (forceClose) {
            this.closeRegisterForm();
        }
        if ($("#teapp_register").is(":visible")) {
            this.closeRegisterForm();
        }
        else {
            masterClose = function () { closeApp(); };
            $("#teapp_regtoken").addClass("hidden");
        }
    };
    /**
     * VerifyRegToken
     */
    TEAppClass.prototype.VerifyRegToken = function () {
        var _this = this;
        this.regToken = trim($("#reg_token").val());
        if (this.regToken == '') {
            toasty("Por favor introduzca un código de registro.", "error");
        }
        else {
            wait("Verificando código...", "Por favor, espere.");
            this.ajax("POST", this.apiUrl + "login/verify", this.regToken, function (data, resp, xhr) {
                closeWait();
                if (xhr.status == 200) {
                    _this.regUserData = data;
                    _this.StartRegister();
                }
                else {
                    toasty("No se pudo validar el código, por favor intente más tarde.");
                }
            }, function (err) {
                closeWait();
                switch (err.status) {
                    case 404:
                        toasty("No se encontró el código. Por favor introduzca un código de registro válido.");
                        break;
                    default:
                        toasty("No se pudo validar el código, por favor intente más tarde (-2).");
                        break;
                }
            });
        }
    };
    /**
     * StartRegister
     */
    TEAppClass.prototype.StartRegister = function () {
        $(".teapp_company").html(this.regUserData.nombre.toUpperCase());
        $("#teapp_regtoken").addClass("hidden");
        $("#teapp_register").removeClass("hidden");
    };
    /**
     * RegisterUser
     */
    TEAppClass.prototype.RegisterUser = function () {
        var _this = this;
        var errors = [], nombre = trim($("#teapp_nombre").val()), apellido_materno = trim($("#teapp_apellido_materno").val()), apellido_paterno = trim($("#teapp_apellido_paterno").val()), correo = trim($("#teapp_correo").val()), 
        /*conf_correo=trim($("#teapp_correo_conf").val()),*/
        pass = trim($("#teapp_pass").val()), pass_conf = trim($("#teapp_pass_conf").val());
        $("#teapp_register input").removeClass("invalid");
        if (nombre == '') {
            errors.push('Introduzca un nombre válido.');
            $("#teapp_nombre").addClass("invalid");
        }
        if (apellido_materno == '') {
            errors.push('Introduzca su apellido materno.');
            $("#teapp_apellido_materno").addClass("invalid");
        }
        if (apellido_paterno == '') {
            errors.push('Introduzca su apellido paterno.');
            $("#teapp_apellido_paterno").addClass("invalid");
        }
        if (correo == '') {
            //errors.push('Introduzca un correo.');
            errors.push('Introduzca un nombre de usuario');
            $("#teapp_correo").addClass("invalid");
        }
        if (correo.match(/^[a-zA-Z0-9._]{5,20}$/gi) == null) {
            errors.push('Introduzca un nombre de usuario válido con un mínimo de 5 caracteres (solo se admiten letras y números, sin espacios, caracteres permitidos: . y _).');
            $("#teapp_correo").addClass("invalid");
        }
        /*if(conf_correo == '') {
            errors.push('Confirme su correo.');
            $("#teapp_correo_conf").addClass("invalid");
        }
        if(validateEmail(correo) == false) {
            errors.push('Introduzca un correo válido.');
            $("#teapp_correo").addClass("invalid");
        }
        if(validateEmail(conf_correo) == false) {
            errors.push('Utilice un correo válido para confirmarlo.');
            $("#teapp_correo_conf").addClass("invalid");
        }
        if(correo != conf_correo) {
            errors.push('Los correos no coinciden.');
            $("#teapp_correo").addClass("invalid");
            $("#teapp_correo_conf").addClass("invalid");
        }*/
        if (pass == '') {
            errors.push('Introduzca una contraseña.');
            $("#teapp_pass").addClass("invalid");
        }
        if (pass.length < 6) {
            errors.push('La contraseña debe contener 6 caracteres como mínimo.');
            $("#teapp_pass").addClass("invalid");
        }
        if (pass != pass_conf) {
            errors.push('Las contraseñas no coinciden.');
            $("#teapp_pass").addClass("invalid");
            $("#teapp_pass_conf").addClass("invalid");
        }
        if (errors.length > 0) {
            $(".teapp_reg_errors").removeClass('hidden');
            $(".teapp_error_list").html("");
            errors.forEach(function (err) {
                $(".teapp_error_list").append("<li>" + err + "</li>");
            });
            location.href = '#reg_errors';
        }
        else {
            wait("Registrando cuenta...", "Por favor espere.");
            this.ajax("POST", this.apiUrl + "login/register", {
                nombre: nombre,
                apellidoPaterno: apellido_paterno,
                apellidoMaterno: apellido_materno,
                rol: this.regUserData.rol,
                idEmpresa: this.regUserData.idEmpresa,
                usuario: correo,
                contrasena: pass,
                token: this.regToken
            }, function (data, resp, xhr) {
                closeWait();
                if (xhr.status == 200) {
                    _this.regToken = '';
                    $("#loginUser").val(correo);
                    alerta("Cuenta Registrada", "Su cuenta ha sido registrada en la empresa <b>" + _this.regUserData.nombre.toUpperCase() + "</b>, ya puede iniciar sesi\u00F3n con sus datos.");
                    _this.ExitRegister(true);
                }
                else {
                    toasty("Lo sentimos, no se pudo registrar su cuenta, por favor intente de nuevo más tarde (-1).");
                }
            }, function (err) {
                closeWait();
                switch (err.status) {
                    case 404:
                        toasty("Lo sentimos, este nombre de usuario ya se encuentra registrado, por favor introduzca otro (-4).");
                        break;
                    default:
                        toasty("Lo sentimos, no se pudo registrar su cuenta, por favor intente de nuevo más tarde (-2).");
                        break;
                }
            });
        }
    };
    return TEAppClass;
}());
