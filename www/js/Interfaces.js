/**
 * INTERFACES
 * Clase que controla las acciones de IO, esta clase engloba
 * el Apagado de Motor, se puede adaptar a otras interfaces de plataforma.
 */
var Interfaces = /** @class */ (function () {
    function Interfaces() {
    }
    /**
     * Envia una instruccion a una interfaz en plataforma
     */
    Interfaces.prototype.AccionIO = function (unidad, interfaz, activar) {
        var carid = objectIndex(vehiculos, "Unidad", unidad);
        wait(lang("slowwarning"));
        ajax("POST", interfaceUrl + "AccionIO", {
            "activar": activar,
            "interfaz": interfaz,
            "unidad": unidad,
            "token": window['userData']['token']
        }, function (res) {
            window.resp = res;
            var data = res.data;
            if (data != null) {
                if (parseInt(data[0]) == 1111 || parseInt(data[0]) == 111) {
                    if (interfaz == "Motor") {
                        if (activar == false) {
                            toasty(lang("motoroff_off"));
                            $("#am-" + unidad).prop("checked", false);
                            $("#am-" + unidad).attr("onchange", "Interface.ConfirmMotor('" + unidad + "',false)");
                        }
                        else {
                            toasty(lang("motoroff_on"));
                            $("#am-" + unidad).prop("checked", true);
                            $("#am-" + unidad).attr("onchange", "Interface.ConfirmMotor('" + unidad + "',true)");
                        }
                    }
                }
                else if (parseInt(data[0]) == 0) {
                    if (interfaz == "Motor") {
                        toasty(lang("motoroff_not"));
                    }
                }
                else if (parseInt(data[0]) == -30 || data[1] == "errorNoCompleto") {
                    if (interfaz == "Motor") {
                        toasty(lang("error_no_complete").replace("%s", vehiculos[carid].Descripcion));
                    }
                }
                else {
                    if (interfaz == "Motor") {
                        if (activar == false) {
                            toasty(lang("motoroff_already_off"));
                        }
                        else {
                            toasty(lang("motoroff_already_on"));
                        }
                    }
                }
            }
            else {
                toasty(lang("error_no_complete").replace("%s", vehiculos[carid].Descripcion));
            }
            closeWait();
        }, function (err) {
            toasty(lang("tryagain"));
            closeWait();
        }, 60000);
    };
    /**
     * Obtiene las interfaces en plataforma asignadas
     * a la cuenta actual del usuario
     */
    Interfaces.prototype.GetInterfaces = function (soloConsulta) {
        var _this = this;
        if (soloConsulta === void 0) { soloConsulta = false; }
        if (new Sesion().Permisos("ApagadoMotor") == true && new Sesion().Permisos("SoloRastreo") == false && isTEApp == false) {
            ajax("POST", interfaceUrl + "GetInterfaces", {
                "unidades": Tracking._rastrearUnidades,
                "token": window['userData']['token']
            }, function (res) {
                try {
                    window.interfaceUnits = res.data;
                    $("#apagadoMotor .list").html('');
                    if (interfaceUnits.length == 0) {
                        $("#apagadoMotor .selector-search").addClass("hidden");
                        $("#apagadoMotor .noContent").removeClass("hidden");
                    }
                    else {
                        $("#apagadoMotor .selector-search").removeClass("hidden");
                        $("#apagadoMotor .noContent").addClass("hidden");
                    }
                    for (var i = 0; i < interfaceUnits.length; i++) {
                        var carid = objectIndex(vehiculos, "Unidad", interfaceUnits[i].UnidadID);
                        var uuid = objectIndex(ultimasUbicaciones, "Unidad", interfaceUnits[i].UnidadID);
                        var mid = interfaceUnits[i].ListaIOs.indexOf("Motor");
                        vehiculos[carid].EstadoMotor = interfaceUnits[i].ListaEstado[mid];
                        if (soloConsulta == false && uuid != -1) {
                            var unitVelocity = 0;
                            try {
                                unitVelocity = ultimasUbicaciones[uuid].Velocidad;
                            }
                            catch (e) { }
                            $("#apagadoMotor .list").append("\n                                    <li class=\"ma-" + interfaceUnits[i].UnidadID + "\">\n                                        <div class=\"left-icon car-icon\"><span style=\"background-image:url('" + getCarIconUri(vehiculos[carid].IconoID, (unitVelocity > 0)) + "')\"></span></div>\n                                        <div class=\"textBox\">\n                                            <div class=\"listText infoCarname\">" + vehiculos[carid].Descripcion + "</div>\n                                            <div class=\"listText listSubtext engineStatus-" + (vehiculos[carid].EstadoMotor == false ? 'on' : 'off') + "\">" + (vehiculos[carid].EstadoMotor == false ? '<es>Activado</es><en>Enabled</en>' : '<es>Desactivado</es><en>Disabled</en>') + "</div>\n                                            <div class=\"listText listSubtext infoLiveStatus\"><strong>" + (ultimasUbicaciones[uuid].Velocidad == 0 ? '<es>Detenido</es><en>Stopped</en>' : '<es>En Movimiento</es><en>In Transit</en>') + "</strong></div>\n                                            <div class=\"listSubtext\"><es>&Uacute;ltimo Reporte</es><en>Last Report</en>: <span class=\"infoLastReport\">" + timeSince(ultimasUbicaciones[uuid].Fecha) + "</span></div>\n                                        </div>\n                                        <div class=\"configSquare\">\n                                            <div class=\"button turn-on\" onclick=\"Interface.ConfirmMotor('" + interfaceUnits[i].UnidadID + "',true)\"><es>ACTIVAR</es><en>ENABLE</en></div>\n                                        </div> \n                                        <div class=\"configSquare\">\n                                            <div class=\"button turn-off\" onclick=\"Interface.ConfirmMotor('" + interfaceUnits[i].UnidadID + "',false)\"><es>DESACTIVAR</es><en>DISABLE</en></div>\n                                        </div> \n                                    </li>\n                                ");
                            var options = {
                                valueNames: ['infoCarname'],
                                listClass: "list",
                                item: '<li><div><div class="infoCarname"></div></div></li>',
                                searchClass: "motorOffSearch"
                            };
                            window.motorOffList = new List("apagadoMotor", options);
                        }
                        $("." + interfaceUnits[i].UnidadID + "-motoribHeader").removeClass("hidden");
                        $("." + interfaceUnits[i].UnidadID + "-bubble_motorstate").html("" + (vehiculos[carid].EstadoMotor == false ? '<es>Activado</es><en>Enabled</en>' : '<es>Desactivado</es><en>Disabled</en>'));
                    }
                }
                catch (e) {
                    if (soloConsulta == false) {
                        toasty("Lo sentimos, no fue posible obtener la interfaz de apagado de motor.", "error");
                        window.interfaceUnits = [];
                        _this.CloseApagadoMotor();
                    }
                }
                closeWait();
            }, function (err) {
                if (soloConsulta == false) {
                    window.interfaceUnits = [];
                    _this.CloseApagadoMotor();
                    toasty(lang("tryagain"));
                    closeWait();
                }
            }, (soloConsulta == true ? 5000 : 60000));
        }
    };
    /**
     * Pide confirmacion al usuario para cambiar el estado del Apagado de Motor
     */
    Interfaces.prototype.ConfirmMotor = function (unidad, estado) {
        var carid = objectIndex(vehiculos, "Unidad", unidad);
        $("#am-" + unidad).prop("checked", estado);
        confirmar(estado == true ? lang("motoroff_ask_on") : lang("motoroff_ask_off"), lang("motoroff_ask").replace("%s", vehiculos[carid].Descripcion), lang("accept"), lang("cancel"), "Interface.AccionIO('" + unidad + "','Motor'," + estado + ")");
    };
    /**
     * Abre la vista para el Apagado de Motor
     */
    Interfaces.prototype.OpenApagadoMotor = function () {
        var _this = this;
        try {
            masterClose = function () { _this.CloseApagadoMotor(); };
        }
        catch (e) { }
        $(".search").val('');
        wait(lang("loading_units"));
        this.GetInterfaces();
        $("#apagadoMotor").removeClass("hidden");
        $(".subVista").scrollTop(0);
    };
    /**
     * Cierra la vista para el Apagado de Motor
     */
    Interfaces.prototype.CloseApagadoMotor = function () {
        resetBack();
        $("#apagadoMotor").addClass("hidden");
    };
    return Interfaces;
}());
