var Vehiculos = /** @class */ (function () {
    function Vehiculos() {
    }
    /**
     * Inicializa los campos de los selects de
     * la vista de Vehiculos
     */
    Vehiculos.prototype.BuildSelects = function () {
        var data, order, carclass;
        if (idioma == "en") {
            data = Vehiculos._dataFieldsEN;
            order = Vehiculos._orderFieldsEN;
            carclass = Vehiculos._classFieldsEN;
        }
        else {
            data = Vehiculos._dataFieldsES;
            order = Vehiculos._orderFieldsES;
            carclass = Vehiculos._classFieldsES;
        }
        $("select.orderVehicles").html('');
        for (var i = 0; i < data.length; i++) {
            if (data[i][0] == "") {
                $("select.orderVehicles").append('<option value="' + data[i][0] + '" disabled>' + data[i][1] + '</option>');
            }
            else {
                $("select.orderVehicles").append('<option value="' + data[i][0] + '">' + data[i][1] + '</option>');
            }
        }
        $("select.orderType").html('');
        for (var i = 0; i < order.length; i++) {
            if (order[i][0] == "") {
                $("select.orderType").append('<option value="' + order[i][0] + '" disabled>' + order[i][1] + '</option>');
            }
            else {
                $("select.orderType").append('<option value="' + order[i][0] + '">' + order[i][1] + '</option>');
            }
        }
        $("select#classCar").html('');
        for (var i = 0; i < carclass.length; i++) {
            if (carclass[i][0] == "") {
                $("select#classCar").append('<option value="' + carclass[i][0] + '" disabled>' + carclass[i][1] + '</option>');
            }
            else {
                $("select#classCar").append('<option value="' + carclass[i][0] + '">' + carclass[i][1] + '</option>');
            }
        }
        this.BuildIcons();
        formSelect();
        //$('.tooltipped').tooltip({delay: 50});
    };
    /**
     * Inicializa campo de iconos
     */
    Vehiculos.prototype.BuildIcons = function () {
        $(".iconList").html('');
        var grupoIconos = new Array();
        var grupoHTML = "";
        for (var i = 0; i < Vehiculos._iconGroups.length; i++) {
            grupoHTML = "";
            for (var j = 0; j < Vehiculos._iconGroups[i].icons.length; j++) {
                grupoHTML += "<div style=\"background-image:url('img/iconos/" + Vehiculos._iconGroups[i].icons[j] + "_moviendo.png')\" onclick=\"Cars.SelectIcon('" + Vehiculos._iconGroups[i].icons[j] + "')\" class=\"carIcon-" + Vehiculos._iconGroups[i].icons[j] + " tooltipped carIcon\" data=\"" + this.parseIconName(Vehiculos._iconGroups[i].icons[j]) + "\"></div>";
            }
            grupoIconos.push(grupoHTML);
        }
        for (var i = 0; i < Vehiculos._iconGroups.length; i++) {
            $(".iconList").append("<div class=\"inputGroup\">\n                <h6>" + Vehiculos._iconGroups[i].title + "</h6>\n                <div class=\"iconGroup\">\n                    " + grupoIconos[i] + "\n                </div>\n            </div>");
        }
    };
    /**
     * Cambia los guion-bajo de los nombre de
     * los iconos por espacios
     * @param name String del nombre de icono
     */
    Vehiculos.prototype.parseIconName = function (name) {
        var parsedString = name.replace("_", " ");
        return parsedString;
    };
    /**
     * Muestra lista de Vehiculos
     */
    Vehiculos.prototype.ShowList = function () {
        var _this = this;
        selectMenu(".ConsultaVehiculos");
        try {
            masterClose = function () { _this.CloseCars(); };
        }
        catch (e) { }
        this.BuildSelects();
        $("#vehiculos").removeClass("hidden");
        this.FillCarList();
    };
    /**
     * Rellena la lista de Vehiculos de manera ordenada
     * @param by Ordenamiento
     * @param order ASCendente o DESCendente
     */
    Vehiculos.prototype.FillCarList = function (by, order) {
        if (by === void 0) { by = "Descripcion"; }
        if (order === void 0) { order = "ASC"; }
        console.log("Ordering by: " + by + " (" + order + ")");
        $(".vehicleList tbody").html('');
        var tmp = ordenar(vehiculos, by, order);
        for (var i = 0; i < tmp.length; i++) {
            $(".vehicleList tbody").append("<tr onclick=\"Cars.OpenCar('" + tmp[i].Unidad + "')\">\n                <td class=\"desc\">" + tmp[i].Descripcion + "</td>\n                <td class=\"brand\">" + (tmp[i].Marca == undefined ? "" : tmp[i].Marca) + "</td>\n                <td class=\"model\">" + (tmp[i].Modelo == undefined ? "" : tmp[i].Modelo) + "</td>\n            </tr>");
        }
        if (tmp.length == 0) {
            $(".orderList tbody").append("<tr>\n                <td colspan=\"3\" align=\"center\" style=\"text-align:center !important\"><es>No hay veh\u00EDculos en esta cuenta</es><en>No units available on this account</en></td>\n            </tr>");
        }
        var options = {
            valueNames: ['desc', 'noserie', 'brand', 'model', 'placas', 'detalle', 'color', 'tags'],
            pagination: true,
            page: $("#vehiculos select.itemsPage").val()
        };
        window.carList = new List('carList', options);
        window.carList.on('updated', function () {
            $("#carList .list").scrollLeft(0);
            //console.log("Click!");
        });
    };
    /**
     * Cierra la vista de Vehiculos
     */
    Vehiculos.prototype.CloseCars = function () {
        $(".subVista").scrollTop(0);
        if ($("#vehiculos .editCar").is(":visible")) {
            var carid = vehiculos[Vehiculos._editCarID].Unidad;
            $("#vehiculos .tool.right").attr("onclick", "Cars.EditCar('" + carid + "')").html("<es>EDITAR</es><en>EDIT</en>");
            $("#vehiculos .editCar").addClass("hidden");
            $("#vehiculos .carDetails,#vehiculos .tool.right").removeClass("hidden");
        }
        else if ($("#vehiculos .carDetails").is(":visible")) {
            Vehiculos._editCarID = -1;
            $("#vehiculos .carDetails,#vehiculos .tool.right").addClass("hidden");
            $("#vehiculos #carList").removeClass("hidden");
        }
        else {
            resetBack();
            $("#vehiculos").addClass("hidden");
        }
    };
    /**
     * Abre la vista de informacion de Vehiculo
     * @param carid Identificador de Vehiculo
     */
    Vehiculos.prototype.OpenCar = function (carid) {
        $("#vehiculos .subVista").scrollTop(0);
        var id = objectIndex(vehiculos, "Unidad", carid);
        Vehiculos._editCarID = id;
        $("#vehiculos #carList").addClass("hidden");
        if (SesionEngine.Permisos("EdicionVehiculos") == true) {
            $("#vehiculos .tool.right").removeClass("hidden");
        }
        $("#vehiculos .carDetails").removeClass("hidden");
        $(".descVal").html(na(vehiculos[id].Descripcion));
        $(".noserieVal").html(na(vehiculos[id].NoSerie));
        $(".brandVal").html(na(vehiculos[id].Marca));
        $(".modelVal").html(na(vehiculos[id].Modelo));
        $(".yearVal").html(na(vehiculos[id].Anio));
        $(".colorVal").html(na(vehiculos[id].Color));
        $(".platesVal").html(na(vehiculos[id].Placas));
        $(".driverVal").html(na(vehiculos[id].Chofer));
        $(".loadVal").html(na(vehiculos[id].Carga));
        if (vehiculos[id].EstadoLimite == 0) {
            $(".speedVal").html(na(vehiculos[id].Limite) + " Km/h");
        }
        else {
            $(".speedVal").html("<es>Desactivado</es><en>Disabled</en>");
        }
        $(".detsVal").html(na(vehiculos[id].Detalle));
        $(".fuelVal").html("$ " + (vehiculos[id].Precio / 100));
        $(".perfVal").html((vehiculos[id].Rendimiento / 100) + " (Km/L)");
        $("#vehiculos .tool.right").attr("onclick", "Cars.EditCar('" + carid + "')");
        this.ResetIcons();
        $(".iconNameVal").html(getCarIconName(vehiculos[id].IconoID));
        $("i.iconNameGraphic").css("background", "url('" + getCarIconUri(vehiculos[id].IconoID, true) + "') no-repeat center center");
    };
    /**
     * Selecciona un icono de la lista de iconos
     * @param iconid Identificador de icono
     */
    Vehiculos.prototype.SelectIcon = function (iconid) {
        this.ResetIcons();
        $(".carIcon-" + iconid).addClass("carIconSel");
    };
    /**
     * Deselecciona los Iconos
     */
    Vehiculos.prototype.ResetIcons = function () {
        $(".carIcon").removeClass("carIconSel");
    };
    /**
     * [LOCKED]
     * Abre la vista para editar un Vehiculo
     * @param carid Identificador de Vehiculo a editar
     */
    Vehiculos.prototype.EditCar = function (carid) {
        $(".subVista").scrollTop(0);
        var id = objectIndex(vehiculos, "Unidad", carid);
        $("#vehiculos .carDetails").addClass("hidden");
        $(".editCar").removeClass("hidden");
        $("#vehiculos .tool.right").attr("onclick", "Cars.SaveCarChanges()").html("<es>GUARDAR</es><en>SAVE</en>");
        $("#descCar").val(empty(vehiculos[id].Descripcion));
        /*$(".noserieVal").html(na(vehiculos[id].NoSerie));
        $(".brandVal").html(na(vehiculos[id].Marca));
        $(".modelVal").html(na(vehiculos[id].Modelo));
        $(".yearVal").html(na(vehiculos[id].Anio));*/
        $("#colorCar").val(empty(vehiculos[id].Color));
        $("#platesCar").val(empty(vehiculos[id].Placas));
        $("#driverCar").val(empty(vehiculos[id].Chofer));
        $("#loadCar").val(empty(vehiculos[id].Carga));
        //$(".speedVal").html(na(vehiculos[id].Limite)+" Km/h");
        $("#detsCar").val(empty(vehiculos[id].Detalle));
        $("#fuelCar").val(vehiculos[id].Precio / 100);
        $("#perfCar").val(vehiculos[id].Rendimiento / 100);
        $("#speedStep").val(vehiculos[id].Limite);
        $("#classCar").val(vehiculos[id].TipoVehiculo);
        if (vehiculos[id].EstadoLimite == 0) { //LIMITE ACTIVADO
            $(".speedLimitCar").prop("checked", true);
            $(".speedLimit").removeClass("hidden");
        }
        else {
            $(".speedLimitCar").prop("checked", false);
            $(".speedLimit").addClass("hidden");
        }
        $(".tagList").html('');
        var tags = vehiculos[id].Etiquetas.split(",");
        for (var i = 0; i < window.etiquetas.length; i++) {
            var checked = "";
            if (tags.indexOf(window.etiquetas[i]) > -1) {
                checked = 'checked="checked"';
            }
            $(".tagList").append('<p class="checkField">' +
                '<input type="checkbox" class="filled-in" id="tag-' + window.etiquetas[i] + '" ' + checked + ' value="' + window.etiquetas[i] + '" />' +
                '<label for="tag-' + window.etiquetas[i] + '">' + window.etiquetas[i] + '</label>' +
                '</p>');
        }
        views();
    };
    /**
     * [LOCKED]
     * Guarda los cambios realizados en el Vehiculo
     */
    Vehiculos.prototype.SaveCarChanges = function () {
        var _this = this;
        var tags = new Array();
        $(".tagContainer input:checked").each(function (i, val) {
            tags.push($(this).val());
        });
        var _tags = tags.join(",");
        var iconid = objectIndex(Vehiculos._iconList, "icon", $(".carIconSel").attr("data"));
        var _iconoID = Vehiculos._iconList[iconid].id;
        wait("<es>Guardando cambios, por favor espere...</es><en>Saving changes, please wait...</en>");
        ajax("POST", authUrl + "ModificaVehiculo", {
            token: window["userData"]["token"],
            Anio: vehiculos[Vehiculos._editCarID].Anio,
            Carga: $("#loadCar").val(),
            Chofer: $("#driverCar").val(),
            Color: $("#colorCar").val(),
            Descripcion: $("#descCar").val(),
            Detalle: $("#detsCar").val(),
            EstadoLimite: ($(".limitCar input").is(":checked") == true ? 0 : -1),
            Etiquetas: _tags,
            IconoID: _iconoID,
            Limite: (parseInt($("#speedStep").val()) < 50 ? 50 : parseInt($("#speedStep").val())),
            Marca: vehiculos[Vehiculos._editCarID].Marca,
            Modelo: vehiculos[Vehiculos._editCarID].Modelo,
            NoSerie: vehiculos[Vehiculos._editCarID].NoSerie,
            Placas: $("#platesCar").val(),
            Precio: parseFloat($("#fuelCar").val()) * 100,
            Rendimiento: parseFloat($("#perfCar").val()) * 100,
            TipoVehiculo: $("#classCar").val(),
            Unidad: vehiculos[Vehiculos._editCarID].Unidad
        }, function (data) {
            console.log(data);
            vehiculos[Vehiculos._editCarID].Carga = $("#loadCar").val();
            vehiculos[Vehiculos._editCarID].Chofer = $("#driverCar").val();
            vehiculos[Vehiculos._editCarID].Color = $("#colorCar").val();
            vehiculos[Vehiculos._editCarID].Descripcion = $("#descCar").val();
            vehiculos[Vehiculos._editCarID].Detalle = $("#detsCar").val();
            vehiculos[Vehiculos._editCarID].Etiquetas = _tags;
            vehiculos[Vehiculos._editCarID].IconoID = _iconoID;
            vehiculos[Vehiculos._editCarID].Limite = (parseInt($("#speedStep").val()) < 50 ? 50 : parseInt($("#speedStep").val()));
            vehiculos[Vehiculos._editCarID].EstadoLimite = ($(".limitCar input").is(":checked") == true ? 0 : -1);
            vehiculos[Vehiculos._editCarID].Placas = $("#platesCar").val();
            vehiculos[Vehiculos._editCarID].Precio = parseFloat($("#fuelCar").val()) * 100;
            vehiculos[Vehiculos._editCarID].Rendimiento = parseFloat($("#perfCar").val()) * 100;
            vehiculos[Vehiculos._editCarID].TipoVehiculo = $("#classCar").val();
            _this.CloseCars();
            _this.OpenCar(vehiculos[Vehiculos._editCarID].Unidad);
            $("#carSearch").val('');
            Cars.FillCarList();
            closeWait();
            toasty(lang("carupdated"));
        }, function (data) {
            closeWait();
            toasty(lang("tryagain"), "error");
        });
    };
    /**
     * Mueve el viewport hacia el UI de Iconos
     */
    Vehiculos.prototype.OpenIcons = function () {
        $(".subVista").scrollTop($(".iconContainer").offset().top - 55);
    };
    /**
     * Mueve el viewport hacia el UI de Etiquetas
     */
    Vehiculos.prototype.OpenTags = function () {
        $(".subVista").scrollTop($(".tagContainer").offset().top - 55);
    };
    /**
     * Obtiene la lista de Vehiculos del cliente
     */
    Vehiculos.prototype.getVehiculosCliente = function () {
        $.ajax({
            url: rasStaticUrl + "getVehiculosCliente2",
            type: "POST",
            timeout: 15000,
            data: {
                "token": window["userData"]["token"]
            },
            success: function (data) {
                //console.log(data);
                window.vehiculos = data.data;
                if (window.vehiculos == null) {
                    window.justLogged = false;
                    $("#loginPage").removeClass("hidden");
                    closeWait();
                    alerta(lang("error"), lang("expiredsesion"));
                    localStorage.removeItem("zeekUserdata");
                }
                else {
                    $(".user-view .email").html(window.vehiculos.length + " <en>vehicles</en><es>vehículos</es>");
                    if (window.justLogged == true) {
                        window.justLogged = false;
                        $("#loginPage").addClass("hidden");
                        closeWait();
                    }
                    Cars.ExtractTags();
                }
            },
            error: function (data) {
                console.log(data);
                if (window.justLogged == true) {
                    alerta(lang("error"), lang("tryagain"));
                    $("#loginPage").show();
                    closeWait();
                }
            }
        });
    };
    /**
     * Solicita al usuario el nombre de la Etiqueta
     * a agregar
     */
    Vehiculos.prototype.AddTag = function () {
        solicitar(lang("addtag"), lang("addtagprompt"), lang("add"), lang("cancel"), "Cars.AddTagToCar()", "");
    };
    /**
     * Agrega al UI la etiqueta introducida
     * por el usuario en AddTag()
     */
    Vehiculos.prototype.AddTagToCar = function () {
        $(".tagList").append('<p class="checkField">' +
            '<input id="tag-' + window.promptValue + '" type="checkbox" class="filled-in"  checked="checked" value="' + window.promptValue + '" />' +
            '<label for="tag-' + window.promptValue + '">' + window.promptValue + '</label>' +
            '</p>');
    };
    /**
     * Extrae las Etiquetas de un Vehiculo y los agrega al
     * compendio de Etiquetas
     */
    Vehiculos.prototype.ExtractTags = function () {
        window.etiquetas = new Array();
        var _etiquetas = new Array();
        for (var i = 0; i < vehiculos.length; i++) {
            var tags = vehiculos[i].Etiquetas.split(",");
            for (var j = 0; j < tags.length; j++) {
                if (_etiquetas.indexOf(tags[j]) == -1 && tags[j] != '') {
                    _etiquetas.push(tags[j]);
                }
            }
        }
        window.etiquetas = _etiquetas.sort();
    };
    /**
     * Obtiene los Permisos del usuario en plataforma
     */
    Vehiculos.prototype.getPermisosApps = function () {
        $.ajax({
            url: rasStaticUrl + "getPermisosApps",
            type: "POST",
            timeout: 10000,
            data: {
                "token": window["userData"]["token"]
            },
            success: function (data) {
                console.log(data);
                window.permisos = data;
            },
            error: function (data) {
                console.log(data);
                if (window.justLogged == true) {
                    alerta(lang("error"), lang("tryagain"));
                    $("#loginPage").show();
                    closeWait();
                }
            }
        });
    };
    Vehiculos._order = "ASC";
    Vehiculos._editCarID = -1;
    Vehiculos._iconList = [
        { id: 4, icon: "adulto", name: "Adulto" },
        { id: 20, icon: "ambulancia", name: "Ambulancia" },
        { id: 3, icon: "anciano", name: "Anciano" },
        { id: 41, icon: "atencion", name: "Atención" },
        { id: 1, icon: "auto", name: "Auto" },
        { id: 15, icon: "autobus_grande", name: "Autobús Grande" },
        { id: 14, icon: "autobus_mediano", name: "Autobús Mediano" },
        { id: 8, icon: "barco", name: "Barco" },
        { id: 42, icon: "bicicleta", name: "Bicicleta" },
        { id: 39, icon: "bloqueado", name: "Bloqueado" },
        { id: 21, icon: "bombero", name: "Bombero" },
        { id: 27, icon: "caja", name: "Caja" },
        { id: 37, icon: "camion", name: "Camión" },
        { id: 36, icon: "camion_para_basura", name: "Camión para Basura" },
        { id: 44, icon: "camion_pipa_gas", name: "Pipa" },
        { id: 45, icon: "catamaran", name: "Catamarán" },
        { id: 46, icon: "cuatrimoto", name: "Cuatrimoto" },
        { id: 24, icon: "doble_remolque", name: "Doble Remolque" },
        { id: 38, icon: "en_taller", name: "En Taller" },
        { id: 51, icon: "encicladora", name: "Encicladora" },
        { id: 29, icon: "excavadora", name: "Excavadora" },
        { id: 28, icon: "grua", name: "Grúa" },
        { id: 7, icon: "mascota", name: "Mascota" },
        { id: 16, icon: "minibus", name: "Minibús" },
        { id: 18, icon: "minivan", name: "Minivan" },
        { id: 10, icon: "moto_acuatica", name: "Moto Acuática" },
        { id: 11, icon: "motocicleta", name: "Motocicleta" },
        { id: 50, icon: "motoconformadora", name: "Motoconformadora" },
        { id: 2, icon: "nino", name: "Niño" },
        { id: 12, icon: "patrulla", name: "Patrulla" },
        { id: 6, icon: "perro", name: "Perro" },
        { id: 17, icon: "pickup", name: "Pickup" },
        { id: 47, icon: "pipa_remolque", name: "Pipa Remolque" },
        { id: 26, icon: "plataforma", name: "Plataforma" },
        { id: 25, icon: "rabon", name: "Rabón" },
        { id: 43, icon: "revolvedora", name: "Revolvedora" },
        { id: 40, icon: "sin_pago", name: "Sin Pago" },
        { id: 13, icon: "suv", name: "SUV" },
        { id: 48, icon: "tanque_estacionario", name: "Tanque Estacionario" },
        { id: 19, icon: "taxi", name: "Taxi" },
        { id: 5, icon: "trabajador", name: "Trabajador" },
        { id: 22, icon: "tractocamion", name: "Tractocamion" },
        { id: 52, icon: "tractor_1", name: "Tractor 1" },
        { id: 53, icon: "tractor_2", name: "Tractor 2" },
        { id: 23, icon: "trailer", name: "Trailer" },
        { id: 9, icon: "yate", name: "Yate" }
    ];
    Vehiculos._iconGroups = [
        {
            "title": "<es>Favoritos</es><en>Favorites</en>",
            "icons": ["auto", "camion", "pickup", "taxi", "revolvedora"]
        },
        {
            "title": "<es>Automoviles</es><en>Cars</en>",
            "icons": ["auto", "patrulla", "taxi"]
        },
        {
            "title": "<es>Personas</es><en>People</en>",
            "icons": ["nino", "anciano", "adulto", "trabajador"]
        },
        {
            "title": "<es>Mascotas y Animales</es><en>Pets and Animals</en>",
            "icons": ["perro", "mascota"],
        },
        {
            "title": "<es>Vehículos Acuaticos</es><en>Aquatic Vehicles</en>",
            "icons": ["barco", "yate", "moto_acuatica"]
        },
        {
            "title": "<es>Motocicletas</es><en>Cycles</en>",
            "icons": ["motocicleta", "bicicleta"]
        },
        {
            "title": "<es>Camionetas</es><en>Trucks</en>",
            "icons": ["suv", "pickup", "minivan"]
        },
        {
            "title": "<es>Autobuses</es><en>Buses</en>",
            "icons": ["autobus_mediano", "autobus_grande", "minibus"]
        },
        {
            "title": "<es>Vehículos de Carga</es><en>Cargo Vehicles</en>",
            "icons": ["tractocamion", "trailer", "doble_remolque", "rabon", "plataforma", "caja", "grua", "camion", "revolvedora"]
        },
        {
            "title": "<es>Maquinaria</es><en>Machinery</en>",
            "icons": ["excavadora"]
        },
        {
            "title": "<es>Servicios Publicos</es><en>Public Services</en>",
            "icons": ["patrulla", "ambulancia", "bombero", "camion_para_basura"]
        },
        {
            "title": "<es>Estado de la Unidad</es><en>Unit Status</en>",
            "icons": ["en_taller", "bloqueado", "sin_pago", "atencion"]
        }
    ];
    Vehiculos._dataFieldsEN = [
        ["", "Choose an option"],
        ["Descripcion", "Description"],
        //["NoSerie",     "Serial Number"],
        ["Marca", "Brand"],
        ["Modelo", "Model"],
        /*["Placas",      "Plates"],
        ["Detalle",     "Details"],
        ["Color",       "Color"],
        ["Etiquetas",   "Tags"]*/
    ];
    Vehiculos._dataFieldsES = [
        ["", "Elija una opción"],
        ["Descripcion", "Descripción"],
        //["NoSerie",     "Número de Serie"],
        ["Marca", "Marca"],
        ["Modelo", "Modelo"],
        /*["Placas",      "Placas"],
        ["Detalle",     "Detalles"],
        ["Color",       "Color"],
        ["Etiquetas",   "Etiquetas"]*/
    ];
    Vehiculos._orderFieldsEN = [
        ["ASC", "Ascendant"],
        ["DESC", "Descendant"]
    ];
    Vehiculos._orderFieldsES = [
        ["ASC", "Ascendente"],
        ["DESC", "Descendente"]
    ];
    Vehiculos._classFieldsES = [
        [2, "Vehiculos Personales, 2 ejes, puede tener trailer de 1 o 2 ejes"],
        [3, "Pickups / Panels / Vans, 2 ejes, puede tener trailer de 1 o 2 ejes"],
        [4, "Autobuses, 2 a 3 ejes"],
        [5, "Troque de 2 ejes, 6 llantas (dobles traseras)"],
        [6, "Troque de 3 ejes"],
        [7, "Troque de 4 o más ejes"],
        [8, "Trailer Sencillo de 3 o 4 ejes"],
        [9, "Trailer Sencillo de 5 ejes"],
        [10, "Trailer Sencillo, 6 o más ejes"],
        [11, "Trailer Doble Remolque de 5 o menos ejes"],
        [12, "Trailer Doble Remolque de 6 ejes"],
        [13, "Trailer Doble Remolque de 7 o más ejes"],
        [1, "Motocicleta, 2 o 3 llantas"]
    ];
    Vehiculos._classFieldsEN = [
        [2, "Personal Vehicles, 2 axles, can have a 1 or 2 axle trailer"],
        [3, "Pickups / Panels / Vans, 2 axles, cn have a 1 or 2 axle trailer"],
        [4, "Buses, 2 to 3 axles"],
        [5, "Truck with 2 axles, 6 tires (double rear)"],
        [6, "Truck with 3 axles"],
        [7, "Truck with 4 or more axles"],
        [8, "Single Trailer with 3 or 4 axles"],
        [9, "Single Trailer with 5 axles"],
        [10, "Single Trailer with 6 or more axles"],
        [11, "Double Trailer with 5 or less axles"],
        [12, "Double Trailer with 6 axles"],
        [13, "Double Trailer with 7 or more axles"],
        [1, "Motorcycle, 2 to 3 tires"]
    ];
    return Vehiculos;
}());
