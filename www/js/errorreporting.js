var DEBUGMODE = true;

console.oldlog = console.log;

console.errorold = console.error;

console.warnold = console.warn;
window.remoteLog = [];
window.remoteWarn = [];
window.remoteError = [];
console.warn = (function (logger) {
    window.Warn = Error; // does this do anything?  proper inheritance...?
    Warn.prototype.writewarn = function (args) {
        // via @fredrik SO trace suggestion; wrapping in special construct so it stands out
        var suffix = {
            "@": (this.lineNumber
                    ? this.fileName + ':' + this.lineNumber + ":1" // add arbitrary column value for chrome linking
                    : extractLineNumberFromStack(this.stack)
            )
        };
        var output = "", arg, i;
        var logArg = [];
        var ts = moment().format("HH:mm:ss.SSS");
        output += "<span class='log-timestamp'>"+ts+"</span> &#10095; "
        for (i = 0; i < args.length; i++) {
            arg = args[i];
            output += "<span class=\"log-" + (typeof arg) + "\">";
            
            

            if (
                typeof arg === "object" &&
                typeof JSON === "object" &&
                typeof JSON.stringify === "function"
            ) {
                output += JSON.stringify(arg);
                var objString = '';
                try{
                    objString = (Object.keys(arg).length == 0 ? arg.toString() : ''); 
                    output+=` [${objString}]`;
                }catch(e){}
                logArg.push({
                    type:   (typeof arg),
                    arg:    JSON.stringify(arg),
                    string: objString
                }); 
            } else {
                output += arg;
                logArg.push({
                    type:   (typeof arg),
                    arg:    arg
                }); 
            }
            
            output += "</span>&nbsp;";
        }
        window.remoteWarn.push({
            ts:     ts,
            args:   logArg
        });
        if(DEBUG){
            logger.innerHTML += `<div class="warn">${output} ${(!!window.cordova == true ? "" : `<span class="log-number">${JSON.stringify(suffix)}</span>`)}</div>`;
        }
        args = args.concat([suffix]);
        // via @paulirish console wrapper
        if (console && console.warnold && !!window.cordova == false) {
            if (console.warnold.apply) { console.warnold.apply(console, args); } else { console.warnold(args); } // nicer display in some browsers
        
        }
        //return args;
    };

    return function (params) {
        /// <summary>
        /// Paulirish-like console.log wrapper
        /// </summary>
        /// <param name="params" type="[...]">list your logging parameters</param>

        // only if explicitly true somewhere
        if (typeof DEBUGMODE === typeof undefined || !DEBUGMODE) return;

        // call handler extension which provides stack trace
        return Warn().writewarn(Array.prototype.slice.call(arguments, 0)); // turn into proper array
    };//--  fn  returned

})(document.getElementById("console"));//--- _warn

console.error = (function (logger) {
    window.Err = Error; // does this do anything?  proper inheritance...?
    Err.prototype.writeerr = function (args) {
        // via @fredrik SO trace suggestion; wrapping in special construct so it stands out
        var suffix = {
            "@": (this.lineNumber
                    ? this.fileName + ':' + this.lineNumber + ":1" // add arbitrary column value for chrome linking
                    : extractLineNumberFromStack(this.stack)
            )
        };
        var output = "", arg, i;
        var logArg = [];
        var ts = moment().format("HH:mm:ss.SSS");
        output += "<span class='log-timestamp'>"+ts+"</span> &#10095; "
        for (i = 0; i < args.length; i++) {
            arg = args[i];
            output += "<span class=\"log-" + (typeof arg) + "\">";
            
            if (
                typeof arg === "object" &&
                typeof JSON === "object" &&
                typeof JSON.stringify === "function"
            ) {
                output += JSON.stringify(arg);
                var objString = '';
                try{
                    objString = (Object.keys(arg).length == 0 ? arg.toString() : ''); 
                    output+=` [${objString}]`;
                }catch(e){}
                logArg.push({
                    type:   (typeof arg),
                    arg:    JSON.stringify(arg),
                    string: objString
                }); 
            } else {
                output += arg;   
                logArg.push({
                    type:   (typeof arg),
                    arg:    arg
                }); 
            }

            output += "</span>&nbsp;";
        }
        window.remoteError.push({
            ts:     ts,
            args:   logArg
        });
        if(DEBUG){
            logger.innerHTML += `<div class="error">${output} ${(!!window.cordova == true ? "" : `<span class="log-number">${JSON.stringify(suffix)}</span>`)}</div>`;
        }
        args = args.concat([suffix]);
        // via @paulirish console wrapper
        if (console && console.errorold && !!window.cordova == false) {
            if (console.errorold.apply) { console.errorold.apply(console, args); } else { console.errorold(args); } // nicer display in some browsers
        }

        //return args;
    };

    return function (params) {
        /// <summary>
        /// Paulirish-like console.log wrapper
        /// </summary>
        /// <param name="params" type="[...]">list your logging parameters</param>

        // only if explicitly true somewhere
        if (typeof DEBUGMODE === typeof undefined || !DEBUGMODE) return;

        // call handler extension which provides stack trace
        return Err().writeerr(Array.prototype.slice.call(arguments, 0)); // turn into proper array
    };//--  fn  returned

})(document.getElementById("console"));//--- _error

console.log = (function (logger) {
    window.Log = Error; // does this do anything?  proper inheritance...?
    Log.prototype.writelog = function (args) {
        // via @fredrik SO trace suggestion; wrapping in special construct so it stands out
        var suffix = {
            "@": (this.lineNumber
                    ? this.fileName + ':' + this.lineNumber + ":1" // add arbitrary column value for chrome linking
                    : extractLineNumberFromStack(this.stack)
            )
        };
        var output = "", arg, i;
        var logArg = [];
        var ts = moment().format("HH:mm:ss.SSS");
        output += "<span class='log-timestamp'>"+ts+"</span> &#10095; "
        for (i = 0; i < args.length; i++) {
            arg = args[i];
            output += "<span class=\"log-" + (typeof arg) + "\">";
            
            if (
                typeof arg === "object" &&
                typeof JSON === "object" &&
                typeof JSON.stringify === "function"
            ) {
                output+=JSON.stringify(arg);
                var objString = '';
                try{
                    objString = (Object.keys(arg).length == 0 ? arg.toString() : ''); 
                    output+=` [${objString}]`;
                }catch(e){}
                logArg.push({
                    type:   (typeof arg),
                    arg:    JSON.stringify(arg),
                    string: objString
                });   
            } else {
                output += arg;
                 logArg.push({
                    type:   (typeof arg),
                    arg:    arg
                });   
            }

            output += "</span>&nbsp;";
        }
        window.remoteLog.push({
            ts:     ts,
            args:   logArg
        });
        if(DEBUG){
            logger.innerHTML += `<div class="log">${output} ${(!!window.cordova == true ? "" : `<span class="log-number">${JSON.stringify(suffix)}</span>`)}</div>`;
        }
        args = args.concat([suffix]);
        // via @paulirish console wrapper
        if (console && console.oldlog && !!window.cordova == false) {
            if (console.oldlog.apply) { console.oldlog.apply(console, args); } else { console.oldlog(args); } // nicer display in some browsers
        }

        //return args;
    };

    return function (params) {
        /// <summary>
        /// Paulirish-like console.log wrapper
        /// </summary>
        /// <param name="params" type="[...]">list your logging parameters</param>

        // only if explicitly true somewhere
        if (typeof DEBUGMODE === typeof undefined || !DEBUGMODE) return;

        // call handler extension which provides stack trace
        return Log().writelog(Array.prototype.slice.call(arguments, 0)); // turn into proper array
    };//--  fn  returned

})(document.getElementById("console"));//--- _log

function extractLineNumberFromStack(stack) {
    /// <summary>
    /// Get the line/filename detail from a Webkit stack trace.  See https://stackoverflow.com/a/3806596/1037948
    /// </summary>
    /// <param name="stack" type="String">the stack string</param>

    if(!stack) return '?'; // fix undefined issue reported by @sigod

    // correct line number according to how Log().write implemented
    var line = stack.split('\n')[2];
    // fix for various display text
    line = (line.indexOf(' (') >= 0
        ? line.split(' (')[1].substring(0, line.length - 1)
        : line.split('at ')[1]
        );
    return line;
}
function print_console() {
    var con = new Object();
    try{
        con.log = window.remoteLog;
        con.warn = window.remoteWarn;
        con.error = window.remoteError;
    }catch(e){}
    window.remoteConsole = con;
    var temp = [];
    if(typeof remoteConsole.log != "undefined"){
        for(var t=0;t<remoteConsole.log.length;t++){
            if(typeof remoteConsole.log[t].ts != "undefined"){
                temp.push({
                    ts: remoteConsole.log[t].ts,
                    args: remoteConsole.log[t].args,
                    type:"log"
                });
            }
        }
    }
    if(typeof remoteConsole.warn != "undefined"){
        for(var t=0;t<remoteConsole.warn.length;t++){
            if(typeof remoteConsole.warn[t].ts != "undefined"){
                temp.push({
                    ts: remoteConsole.warn[t].ts,
                    args: remoteConsole.warn[t].args,
                    type:"warn"
                });
            }
        }
    }
    if(typeof remoteConsole.error != "undefined"){
        for(var t=0;t<remoteConsole.error.length;t++){
            if(typeof remoteConsole.error[t].ts != "undefined"){
                try{
                    temp.push({
                        ts: remoteConsole.error[t].ts,
                        args: remoteConsole.error[t].args,
                        type:"error"
                    });
                }catch(e){}
            }
        }
    }
    temp.sort(compareTS);
    con = temp;
    var con_log = '';
    for(var log=0;log<con.length;log++){
        con_log+= `<div id="${con[log].type}">`;
        con_log+=`<div><span class="log-string log-ts">${con[log].ts}</span>`;
        if(typeof con[log].args != "undefined"){
            for(var argId=0;argId<con[log].args.length;argId++) {
                con_log+=`<span class="log-${con[log].args[argId].type}">${con[log].args[argId].arg}</span>`;
            }
        }
        con_log+='</div></div>';
    }
    return con_log;
}
function compareTS(a, b) {
  if (a.ts > b.ts) return 1;
  if (b.ts > a.ts) return -1;

  return 0;
}